<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataOptionTypesSeeder
 *
 * @author ershovsw
 */
use Illuminate\Database\Seeder;
class CompanyDataOptionSeeder extends Seeder {
    //put your code here
    public function run() {
 

		$date = new DateTime;
		$types = array(							
				array('type'=>'1', 'name'=>'Seller') ,
				array('type'=>'1', 'name'=>'Buyer') ,
				array('type'=>'1', 'name'=>'Bonus') ,
				array('type'=>'1', 'name'=>'Referral') ,
				array('type'=>'1', 'name'=>'Adjustment') ,
				array('type'=>'1', 'name'=>'Other') ,
				
				array('type'=>'2', 'name'=>'Listing In Process') ,
				array('type'=>'2', 'name'=>'Listing Active') ,
				array('type'=>'2', 'name'=>'Listing Active Completed') ,
				array('type'=>'2', 'name'=>'In Escrow') ,
				array('type'=>'2', 'name'=>'Closed') ,
				array('type'=>'2', 'name'=>'Transaction Completed') ,
				array('type'=>'2', 'name'=>'Expired Listing') ,
				array('type'=>'2', 'name'=>'DFT') 			
		);
		
		
				
		foreach($types as $sVal) {
			$data[] = array(
				'option_name'	=> $sVal["name"],
				'type_id'		=> $sVal["type"],
				'option_status' =>'Active',
				'option_desc' 	=> '',
				'company_id' 	=> '1',	
				'created_at'	=> $date->format('Y-m-d H:i:s'),
				'updated_at'	=> $date->format('Y-m-d H:i:s')
			);
		}

		// Uncomment the below to run the seeder
		DB::table('company_data_options')->insert($data);
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsersSeeder
 *
 * @author ershovsw
 */
use Illuminate\Database\Seeder;
use App\State;
class StatesSeeder extends Seeder {
    //put your code here
    public function run() {
//        State::truncate();
 

		$date = new DateTime;
		$states = array(
			//US
			array(	'abbr' => 'AL', 'name' => 'Alabama' ),
			array(	'abbr' => 'AK', 'name'	=> 'Alaska' ),
			array(	'abbr' => 'AZ', 'name'	=> 'Arizona' ),
			array(	'abbr' => 'AR', 'name'	=> 'Arkansas' ),
			array(	'abbr' => 'CA', 'name'	=> 'California' ),
			array(	'abbr' => 'CO', 'name'	=> 'Colorado' ),
			array(	'abbr' => 'CT', 'name'	=> 'Connecticut' ),
			array(	'abbr' => 'DE', 'name'	=> 'Delaware' ),
			array(	'abbr' => 'DC', 'name'	=> 'District Of Columbia' ),
			array(	'abbr' => 'FL', 'name'	=> 'Florida' ),
			array(	'abbr' => 'GA', 'name'	=> 'Georgia' ),
			array(	'abbr' => 'HI', 'name'	=> 'Hawaii' ),
			array(	'abbr' => 'ID', 'name'	=> 'Idaho' ),
			array(	'abbr' => 'IL', 'name'	=> 'Illinois' ),
			array(	'abbr' => 'IN', 'name'	=> 'Indiana' ),
			array(	'abbr' => 'IA', 'name'	=> 'Iowa' ),
			array(	'abbr' => 'KS', 'name'	=> 'Kansas' ),
			array(	'abbr' => 'KY', 'name'	=> 'Kentucky' ),
			array(	'abbr' => 'LA', 'name'	=> 'Louisiana' ),
			array(	'abbr' => 'ME', 'name'	=> 'Maine' ),
			array(	'abbr' => 'MD', 'name'	=> 'Maryland' ),
			array(	'abbr' => 'MA', 'name'	=> 'Massachusetts' ),
			array(	'abbr' => 'MI', 'name'	=> 'Michigan' ),
			array(	'abbr' => 'MN', 'name'	=> 'Minnesota' ),
			array(	'abbr' => 'MS', 'name'	=> 'Mississippi' ),
			array(	'abbr' => 'MO', 'name'	=> 'Missouri' ),
			array(	'abbr' => 'MT', 'name'	=> 'Montana' ),
			array(	'abbr' => 'NE', 'name'	=> 'Nebraska' ),
			array(	'abbr' => 'NV', 'name'	=> 'Nevada' ),
			array(	'abbr' => 'NH', 'name'	=> 'New Hampshire' ),
			array(	'abbr' => 'NJ', 'name'	=> 'New Jersey' ),
			array(	'abbr' => 'NM', 'name'	=> 'New Mexico' ),
			array(	'abbr' => 'NY', 'name'	=> 'New York' ),
			array(	'abbr' => 'NC', 'name'	=> 'North Carolina' ),
			array(	'abbr' => 'ND', 'name'	=> 'North Dakota' ),
			array(	'abbr' => 'OH', 'name'	=> 'Ohio' ),
			array(	'abbr' => 'OK', 'name'	=> 'Oklahoma' ),
			array(	'abbr' => 'OR', 'name'	=> 'Oregon' ),
			array(	'abbr' => 'PA', 'name'	=> 'Pennsylvania' ),
			array(	'abbr' => 'RI', 'name'	=> 'Rhode Island' ),
			array(	'abbr' => 'SC', 'name'	=> 'South Carolina' ),
			array(	'abbr' => 'SD', 'name'	=> 'South Dakota' ),
			array(	'abbr' => 'TN', 'name'	=> 'Tennessee' ),
			array(	'abbr' => 'TX', 'name'	=> 'Texas' ),
			array(	'abbr' => 'UT', 'name'	=> 'Utah' ),
			array(	'abbr' => 'VT', 'name'	=> 'Vermont' ),
			array(	'abbr' => 'VA', 'name'	=> 'Virginia' ),
			array(	'abbr' => 'WA', 'name'	=> 'Washington' ),
			array(	'abbr' => 'WV', 'name'	=> 'West Virginia' ),
			array(	'abbr' => 'WI', 'name'	=> 'Wisconsin' ),
			array(	'abbr' => 'WY', 'name'	=> 'Wyoming' ),
			//Canadia
			array(	'abbr' => 'BC', 'name'	=> 'British Columbia'  ),
			array(	'abbr' => 'ON', 'name'	=> 'Ontario'  ),
			array(	'abbr' => 'NL', 'name'	=> 'Newfoundland and Labrador'  ),
			array(	'abbr' => 'NS', 'name'	=> 'Nova Scotia'  ),
			array(	'abbr' => 'PE', 'name'	=> 'Prince Edward Island'  ),
			array(	'abbr' => 'NB', 'name'	=> 'New Brunswick'  ),
			array(	'abbr' => 'QC', 'name'	=> 'Quebec'  ),
			array(	'abbr' => 'MB', 'name'	=> 'Manitoba'  ),
			array(	'abbr' => 'SK', 'name'	=> 'Saskatchewan'  ),
			array(	'abbr' => 'AB', 'name'	=> 'Alberta'  ),
			array(	'abbr' => 'NT', 'name'	=> 'Northwest Territories'  ),
			array(	'abbr' => 'NU', 'name'	=> 'Nunavut'  ),
			array(	'abbr' => 'YT', 'name'	=> 'Yukon Territory'  )
		);
				
		foreach($states as $sKey => $sVal) {
			$data[] = array(
				'state_abbr'	=> $states[$sKey]['abbr'],
				'state_name'	=> $states[$sKey]['name'],
				'created_at'	=> $date->format('Y-m-d H:i:s'),
				'updated_at'	=> $date->format('Y-m-d H:i:s')
			);
		}

		// Uncomment the below to run the seeder
		DB::table('states')->insert($data);
    }
}

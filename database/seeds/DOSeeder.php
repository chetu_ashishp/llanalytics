<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataOptionTypesSeeder
 *
 * @author ershovsw
 */
use Illuminate\Database\Seeder;

class DOSeeder extends Seeder {

    //put your code here
    public function run() {


        $date = new DateTime;
        $data_type = array(
            'type_name' => 'test',
            'created_at' => $date->format('Y-m-d H:i:s'),
            'updated_at' => $date->format('Y-m-d H:i:s')
        );


        DB::table('data_types')->insert($data_type);
        $data = array(
            'type_id' => 1,
            'option_name' => 'test',
            'option_desc' => 'test',
            'option_status' => 'Active',
            'created_at' => $date->format('Y-m-d H:i:s'),
            'updated_at' => $date->format('Y-m-d H:i:s')
        );


        DB::table('data_options')->insert($data);


        // Uncomment the below to run the seeder
    }

}

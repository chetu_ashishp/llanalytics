<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\DataType;
use App\DataOption;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		
		//$this->call('RoleSeeder');
		
		/*$this->call('UsersSeeder');		
		$this->call('StatesSeeder');
		$this->call('CountriesTableSeeder');
		$this->call('DataOptionTypeSeeder');
		$this->call('SettingsSeeder');
		$this->call('DOSeeder');
		$this->call('CompanySeeder');
		$this->call('CompanyDataOptionTypeSeeder');
		$this->call('CompanyDataOptionSeeder');
		$this->call('ClientsSeeder');
		*/

	//	$this->call('CompanyDataOptionTypeSeeder2');	
	//	$this->call('CompanyDataOptionTypeSeeder3');
		
		
		//$this->call('RoutesSeeder');
		
		$this->command->info('Done!');
	}

}

<?php

use Illuminate\Database\Seeder;
class RoleSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('roles')->truncate();

		$roles = array(
                    array('role_name'=>'Super Admin', 'role_description'=>'Description Here'),
                    array('role_name'=>'Standard Administrator', 'role_description'=>'Description Here'),
                    array('role_name'=>'Companies', 'role_description'=>'Description Here'),                                        
                    array('role_name'=>'Public User', 'role_description'=>'Description Here')
		);

		// Uncomment the below to run the seeder
		 DB::table('roles')->insert($roles);
	}

}

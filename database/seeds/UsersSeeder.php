<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsersSeeder
 *
 * @author ershovsw
 */
use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder {
    //put your code here
    public function run() {
//        User::truncate();
 
        User::create( [
            'email' => 'ashishp@gmail.com' ,
            'password' => Hash::make( 'ashishp' ) ,
            'name' => 'ashish'         	
        ] );
    }
}

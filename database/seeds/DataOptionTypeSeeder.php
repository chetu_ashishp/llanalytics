<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataOptionTypesSeeder
 *
 * @author ershovsw
 */
use Illuminate\Database\Seeder;
use App\State;
class DataOptionTypeSeeder extends Seeder {
    //put your code here
    public function run() {
 

		$date = new DateTime;
		$types = array(			
			'Title' ,
			'Company Type' ,
			'Referral Source' ,
		);
				
		foreach($types as $sVal) {
			$data[] = array(
				'type_name'	=> $sVal,				
				'created_at'	=> $date->format('Y-m-d H:i:s'),
				'updated_at'	=> $date->format('Y-m-d H:i:s')
			);
		}

		// Uncomment the below to run the seeder
		DB::table('data_types')->insert($data);
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataOptionTypesSeeder
 *
 * @author ershovsw
 */
use Illuminate\Database\Seeder;
use App\State;
class ClientsSeeder extends Seeder {
    //put your code here
    public function run() {
 
		$date = new DateTime;
		$data = array(
					'client_number'	=> "VT1-".substr( time(),-4),					
					'last_name'		=> "Bob",
					'first_name'	=> "Brown",
					'spouse_fname'	=> "Cassie",
					'spouse_lname'	=> "Brown",
					'email'	=> "bob.brown768gt@gmail.com",
					'primary_phone'	=> "1234567896",
					'alternate_phone'	=> "1234567823",
					'other_phone'	=> "1234567854",
					'date_met'	=>  $date->format('Y-m-d'),
					'referral_source'	=> 1,
					'refsource_details'	=> "How did you hear details ",					
					'notes'	=> "some notes here",
					'company_id'	=> 1,				
					'website'	=> "dbrown.com",				
					'created_at'	=> $date->format('Y-m-d H:i:s'),
					'updated_at'	=> $date->format('Y-m-d H:i:s')
		);
		
		// Uncomment the below to run the seeder
		DB::table('clients')->insert($data);
    }
}

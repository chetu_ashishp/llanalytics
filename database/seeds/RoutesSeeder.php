<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class RoutesSeeder extends Seeder {

    public function run() {
        // Uncomment the below to wipe the table clean before populating
        // DB::table('roles')->truncate();

        $routes = Route::getRoutes();
        $date = new DateTime;
        foreach ($routes as $route) {
            $methods = $route->methods();
            $data[] = array(
                'role_id' => 1,
                'route' => $route->uri(),
                'method' => $methods[0],
                'permission' => 1,
                'created_at' => $date->format('Y-m-d H:i:s'),
                'updated_at' => $date->format('Y-m-d H:i:s')
            );
        }


        // Uncomment the below to run the seeder
        DB::table('role_meta')->insert($data);
    }

}

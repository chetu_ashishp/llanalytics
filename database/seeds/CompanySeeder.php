<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataOptionTypesSeeder
 *
 * @author ershovsw
 */
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder {

    //put your code here
    public function run() {


        $date = new DateTime;
        $data = array(
            'company_number' => 1,
            'company_name' => 'test',
            'company_type' => 1,
            'fname' => 'test',
            'lname' => 'test',
            'email' => 'test@test.com',
            'company_title' => 1,
            'primary_phone' => 'primary_phone',
            'alternate_phone' => 'primary_phone',
            'addr_line1' => 'primary_phone',
            'addr_line2' => 'primary_phone',
            'city' => 'city',
            'state_id' => 1,
            'country_id' => 1,
            'zip' => '21842',
            'referral_source' => 1,
            'website' => 'website',
            'linkedin_url' => 'website',
            'facebook_url' => 'website',
            'twitter_url' => 'website',
            'googleplus_url' => 'website',
            'status' => 'Active',
            'created_at' => $date->format('Y-m-d H:i:s'),
            'updated_at' => $date->format('Y-m-d H:i:s')
        );


        DB::table('companies')->insert($data);


        // Uncomment the below to run the seeder
    }

}

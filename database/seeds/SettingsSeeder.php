<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SettingsSeeder
 *
 * @author ershovsw
 */
use Illuminate\Database\Seeder;
use App\State;
class SettingsSeeder extends Seeder {
    //put your code here
    public function run() {

		$date = new DateTime;
		$set = array(
				array(
						'key'=>'from_name_email_notice',
						'value'=>'Laurie Lundeen',
						'who_edited'=>'1',
						'created_at' => $date->format('Y-m-d H:i:s'),
						'updated_at' => $date->format('Y-m-d H:i:s')
				),
				array(
						'key'=>'from_email_notice',
						'value'=>'info@WHATDOMAIN.com',
						'who_edited'=>'1',
						'created_at' => $date->format('Y-m-d H:i:s'),
						'updated_at' => $date->format('Y-m-d H:i:s')
				),
				array(
						'key'=>'email_testing',
						'value'=>'no',
						'who_edited'=>'1',
						'created_at' => $date->format('Y-m-d H:i:s'),
						'updated_at' => $date->format('Y-m-d H:i:s')
				),
		
				array(
						'key'=>'testing_email',
						'value'=>'',
						'who_edited'=>'1',
						'created_at' => $date->format('Y-m-d H:i:s'),
						'updated_at' => $date->format('Y-m-d H:i:s')
				),
				
		);		
	
		// Uncomment the below to run the seeder
		DB::table('settings')->insert($set);
    }
}

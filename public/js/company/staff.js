(function ($) {
    var check = true;
    $('.phoneUS').mask('000.000.0000');
    $(document).ready(function () {

        if (getUrlParameter('staff') == 'add') {
            $('a#ltransactions').click();
            $(document).ajaxComplete(function (event, xhr, settings) {
                if (check) {
                    $('button[data-route="addTransaction"]').click();
                    check = false;
                }
            });
        } else if (getUrlParameter('staff') == 'edit' && getUrlParameter('transaction_id')!= undefined) {
            $('a#ltransactions').click();
            var id=getUrlParameter('transaction_id');
            $(document).ajaxComplete(function (event, xhr, settings) {
                if (check) {
                    $('button.edittransaction-btn[data-id="'+id+'"]').click();
                    check = false;
                }
            });
        }
    });
/*$('body').on('submit', '.form-edit', function (e) {
	 e.preventDefault();
     var element = $(this);
	 var _data = {};
	 $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });
	alert(_data.id);	
	return false;
});*/
    $('body').on('click', '#cancelAddForm', function (e) {
        $('#newtransaction-div').hide('slow');
		 $('#editstaff-div').hide('slow');
        $('#transactions-div').show('slow');
        $('#trasaction-add-form')[0].reset();
		 //$('#staff-add-form')[0].reset();
    });
    
    $('body').on('click', '#cancelPaymentAdjustment', function (e) {
        $('#add_payment_adjustment-div').hide('slow');
		 $('#edittransaction-div').hide('slow');
        $('#viewinvoice-div').show('slow');
       // $('#trasaction-add-form')[0].reset();
		 //$('#staff-add-form')[0].reset();
    });
    
    $('body').on('click', '#cancelEditForm', function (e) {

        $('#edittransaction-div').html('');
        $('#edittransaction-div').hide('slow');
        $('#transactions-div').show('slow');
    });

//deleteStaff
$('body').on('click', '.deleteStaff', function (e) {

            var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });

            _data.value = element.val();
		 $('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to delete this Staff ?</h6></div>')
		.dialog({
			modal: true, title: 'Delete Staff', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {

				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");

					//window.location.reload();
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});
        
       return false;
        

    });
    
    $('body').on('click', '.resetStaff,.resetomsstaff', function (e) {

            var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });

            _data.value = element.val();
		 $('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to Reset Staff from deleted to previous status ?</h6></div>')
		.dialog({
			modal: true, title: 'Reset Staff', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {

				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");

					//window.location.reload();
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});
        
       return false;
        

    });
    
    // delete payment_adjustment method to implement 
    $('body').on('click', '.deletePaymentAdjustment', function (e) {
       // alert('deletePaymentAdjustment');
            var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });

            _data.value = element.val();
		 $('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to delete this Payments / Adjustments ?</h6></div>')
		.dialog({
			modal: true, title: 'Delete Payments / Adjustments', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {
                    //console.log(ajaxUrl, _data);
                    //console.log(_data.billing);
                   var billingId = _data.billing;
				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");                            
                   // $('.viewinvoice-btn[data-id='+billingId+']').trigger( "click" );
                   setTimeout(function () {
                        $('.viewinvoice-btn[data-id='+billingId+']').trigger( "click" );
                     }, 1500);
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});
        
       return false;        

    });
    
	$('body').on('click', '.deleteOmsStaff', function (e) {
		e.preventDefault();     
		var element = $(this);
		var _data = {};
		$.each(element.get(0).attributes, function (i, attrib) {
			_data[attrib.name.replace('data-', '')] = attrib.value;
		});
		_data.value = element.val();
		
		$('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to delete this Staff ?</h6></div>')
		.dialog({
			modal: true, title: 'Delete Staff', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {
					//console.log(ajaxUrl, _data);
				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");
					//window.location.reload();
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});

        return false;
	});
		
	$('body').on('click', '.deleteCompany', function (e) {
		    
		var element = $(this);
		var _data = {};
		$.each(element.get(0).attributes, function (i, attrib) {
			_data[attrib.name.replace('data-', '')] = attrib.value;
		});
		_data.value = element.val();
		
        
        $.ajax({
                type: "POST",
                 url: ajaxUrl + 'validateajaxPermission',
                 dataType: 'json',
                 data: _data,
                 async: false,
                 beforeSend: function () {
                     if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                     $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                     }                       
                 },
                 success: function( data ) {
                  if (data.success) {

                     returnval = true;
                     return true;
                  } 
                  else {                             
                         if ('array' in data.data) {
                              $(".header__row").html("");
                              jQuery.each(data.data.array.message, function() {
                                  var dd = 'not aurhrised';
                                  $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                  /*setTimeout(function () {
                                     $(".header__row").fadeOut(function(){
                                      // $(this).remove();
                                     });
                                   }, 2000);*/
                                   $(".header__row").fadeIn().delay(20000).fadeOut();
                              });
                         }                         
                     }
                 },
                 complete: function () {
                     if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                             $.unblockUI();
                         }
                     if (element !== false) {
                         element.removeClass('working');
                     }
                     $('.mainnav a').css('pointer-events', ' auto');
                      //return false;
                 },
            });
         if(returnval) {
      
            $('<div></div>').appendTo('body')
            .html('<div><h6>Are you sure to delete this company ?</h6></div>')
            .dialog({
                modal: true, title: 'Delete Company', zIndex: 10000, autoOpen: true,
                width: 'auto', modal: true, resizable: false,
                buttons: {
                    Confirm: function () {

                       doAjax(ajaxUrl, _data, false);
                        $(this).dialog("close");
                        //window.location.reload();
                    },
                    No: function () {
                        $(this).dialog("close");
                    }
                },
                Cancel: function (event, ui) {
                    $(this).remove();
                }
            });
       } 
       return false;

    });
	
    $('body').on('change', '#transaction_status', function (e) {

        var s = $("#transaction_status :selected").text().toLowerCase();

        if (s.indexOf("listing") > -1) {
//            $('.list_info').show();
        }
        else {
//            $('.list_info').hide();
        }

    });
   
    /*$('body').on('click', '.editOmsStaff, .storeomsstaff, .companystaff, #subscription_history, #billing_history, .companycreate', function (e) {
    
            var returnval = false;
            var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });         
             
                $.ajax({
                   type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }
                        $(".header__row").html('');
                    },
                    success: function( data ) {
                     if (data.success) {
                        returnval = true;
                        return true;
                     } 
                     else {                             
                            //alert('here');
                            if ('array' in data.data) {
                                 jQuery.each(data.data.array.message, function() {
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                     $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                         //return false;
                    },
            });
            if(!returnval) {
               
               return false;
            }
              
            
    });*/
    

})(jQuery);

var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        matches = [];

        substrRegex = new RegExp(q, 'i');

        $.each(strs, function(i, str) {
            if (substrRegex.test(str)) {
                matches.push(str);
            }
        });

        cb(matches);
    };
};
var initializeTypeAhead = function() {
    $('input[name="escrow_officer"]').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'escrowOfficers',
            source: substringMatcher(escrow_officers)
        });

    $('input[name="loan_officer"]').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'escrowOfficers',
            source: substringMatcher(loan_officers)
        });
}

//RemoveStaff for company (id, nroute, token){
function RemoveStaff(id, nroute, token) {
	var _data = {};
	_data['id'] = id;
	_data['path'] = 'Staffs';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
	 doAjax(ajaxUrl, _data, false);	
}

function purchaseSubscription() {
    $('#subscription_history').click(); 
}

function RemoveOmsStaff(id, nroute, token) {
	var _data = {};
	_data['id'] = id;
	_data['path'] = 'OmsStaff';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
	//console.log('removeomsstaff');
	console.log(ajaxUrl, _data);
	 doAjax(ajaxUrl, _data, false);	
}

function checkValue(str) {
    if (str.replace(/\s/g, "") != "" && isNumber(str))
        return true;

}

function isNumber(o) {
    return typeof o === 'number' && isFinite(o);
}

function getDateDiff(time1, time2) {
    var str1 = time1.split('/');
    var str2 = time2.split('/');
    var t1 = new Date(str1[2], str1[0] - 1, str1[1]);
    var t2 = new Date(str2[2], str2[0] - 1, str2[1]);

    var diffMS = t1 - t2;
    var diffS = diffMS / 1000;
    var diffM = diffS / 60;
    var diffH = diffM / 60;
    var diffD = diffH / 24;
    return Math.round(diffD);
}

function storeStaff() {
    $('#staff-add-form')[0].reset();
    $('#lstaff').click();
}



function updateTransaction() {
    $('#ltransactions').click();
}

function updateStaff() {
	$('#lstaff').click();
}



function deleteStaff() {
    $('#lstaff').click();
}
function resetstaff() {
   $('#lstaff').click();
}
function resetomsstaff() {
  location.reload();
}

function deleteOmsStaff() {
	location.reload();
}

function addStaff() {
	$('#transactions-div').hide('slow');
    $('#newtransaction-div').show('slow');
}

function editStaff() {
		//initializeTypeAhead();
	$('#transactions-div').hide('slow');
    $('#newtransaction-div').hide('slow');
    $('edittransaction-div').hide('slow');
    $('#editstaff-div').show('slow');
}

function viewinvoice() {
   	$('#transactions-div').hide('slow');
    $('#transactions-div2').hide();
    $('#viewinvoice-div').show();
}

function addPaymentAdjustment() {
   	$('#transactions-div').hide('slow');
    $('#viewinvoice-div').hide('slow');
    $('#add_payment_adjustment-div').show();
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}  

function storepaymentadjustment() {
    
    $('#add_payment_adjustment-div').hide('slow');
    $('#edittransaction-div').hide('slow');
    $('#transactions-div').hide('slow');
    $('#viewinvoice-div').hide();
    var id = $("input[name=billing_id]").val();
    $('.viewinvoice-btn[data-id='+id+']').click();
}

function updateCompanyStaff() {
    redirectTo('companyportal/staff');
}
/*$('.bootstrap-table').dataTable( {
  "lengthMenu": [ 10, 25, 50, 75, 100 ]
} );*/
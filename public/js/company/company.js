(function($) {
	
	$('#datepicker').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});
	$('.phoneUS').mask('000.000.0000');	
	
	 $('body').on('click', '.del-addr-rec', function (e) {
	        e.preventDefault();
	        
	        itemRemove = $(this).data('section');
	        
	        $('#sec'+itemRemove).remove();
	    });	
	 
	 $('body').on('click', '.mailingaddr', function (e) {
	        
	        if( $( this ).is(':checked') ){
	        	
	        	$( ".mailing" ).each(function( index, element ) {	        		
		      
		            if( $(element).is(':checked') ){
		            	
		            	$('.mailingaddr').prop('checked', false);
		            	alert('Only one mailing address may be set for a client');		            	
		            	return false;
		            }
		         });
            }	        
	    });	 
	 
	 $('body').on('click', '.mailing', function (event) {
	   
		    $( this ).addClass('current');
	        
	        if( $( this ).is(':checked') ){
	        	
	        	$( ".mailing" ).each(function( index, element ) {
	        		
		            // element == this
		            if( $(element).is(':checked') && !$(element).hasClass('current')){
		            	
		            	 $(event.target).prop('checked', false);
		            	alert('Only one mailing address may be set for a client');		            	
		            	return false;
		            }
		         });
         }
	        
	        $( this ).removeClass('current');
	    });

    $('body').on('click', '.approveSubscribedCompany', function (e) {
		e.preventDefault();     
		var element = $(this);
		var _data = {};
		$.each(element.get(0).attributes, function (i, attrib) {
			_data[attrib.name.replace('data-', '')] = attrib.value;
		});
		_data.value = element.val();		
        
        $.ajax({
                type: "POST",
                 url: ajaxUrl + 'validateajaxPermission',
                 dataType: 'json',
                 data: _data,
                 async: false,
                 beforeSend: function () {
                     if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                     $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                     }                       
                 },
                 success: function( data ) {
                  if (data.success) {

                     returnval = true;
                     return true;
                  } 
                  else {                             
                         if ('array' in data.data) {
                              $(".header__row").html("");
                              jQuery.each(data.data.array.message, function() {
                                  var dd = 'not aurhrised';
                                  $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                  
                                   $(".header__row").fadeIn().delay(20000).fadeOut();
                              });
                         }                         
                     }
                 },
                 complete: function () {
                     if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                             $.unblockUI();
                         }
                     if (element !== false) {
                         element.removeClass('working');
                     }
                     $('.mainnav a').css('pointer-events', ' auto');
                      //return false;
                 },
            });
         if(returnval) {
        ////////
		$('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to '+_data.show+' this Company ?</h6></div>')
		.dialog({
			modal: true, title: _data.show+' Company', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {
                   var billingId = _data.billing;
				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close"); 
                    window.setTimeout(function(){location.reload()},2000);
      
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});
       } 
       return false;
    });	 	 
})(jQuery);


var closeAddrModal = function() {
	$('.modal-add-addr').modal('hide'); 
	$('.form-add')[0].reset();	 
}

if( $('#website').length>0){
	var website = document.getElementById('website');
}
if( $('#linkedin_url').length>0){
	var linkedin_url = document.getElementById('linkedin_url');
}
if( $('#facebook_url').length>0){
	var facebook_url = document.getElementById('facebook_url');
}
if( $('#twitter_url').length>0){
	var twitter_url = document.getElementById('twitter_url');
}
if( $('#googleplus_url').length>0){
	var googleplus_url = document.getElementById('googleplus_url');
}

function RemoveCompany(id, nroute, token) {
	var _data = {};
	_data['id'] = id;
	_data['path'] = 'Company';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
	 doAjax(ajaxUrl, _data, false);	
}
function deleteCompany() {
	redirectTo('company');
}
function RemoveSubscription(id, nroute, token) { alert('RemoveSubscription');
	var _data = {};
	_data['id'] = id;
	_data['path'] = 'Subscriptions';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
    console.log(ajaxUrl, _data);
	 doAjax(ajaxUrl, _data, false);	
}

var checkUrlValidity = function() {	
	
	var patt = new RegExp("https?://.+");	
	
	if( $('#website').val()=='' || patt.test(website.value)){		
			website.setCustomValidity('');
	} else {
	   	website.setCustomValidity('You must enter a valid url for example: https://example.com');
	}	
	
	if( $('#linkedin_url').val()=='' || patt.test(linkedin_url.value)){		
		linkedin_url.setCustomValidity('');
	} else {
		linkedin_url.setCustomValidity('You must enter a valid url for example: https://example.com');
	}	
	
	if( $('#facebook_url').val()=='' || patt.test(facebook_url.value)){		
		facebook_url.setCustomValidity('');
	} else {
		facebook_url.setCustomValidity('You must enter a valid url for example: https://example.com');
	}
	
	if( $('#twitter_url').val()=='' || patt.test(twitter_url.value)){		
		twitter_url.setCustomValidity('');
	} else {
		twitter_url.setCustomValidity('You must enter a valid url for example: https://example.com');
	}
	
	
	if( $('#googleplus_url').val()=='' || patt.test(googleplus_url.value)){		
		googleplus_url.setCustomValidity('');
	} else {
		googleplus_url.setCustomValidity('You must enter a valid url for example: https://example.com');
	}
};

if( $('#website').length>0){
	website.addEventListener('change', checkUrlValidity, false);
}

if( $('#linkedin_url').length>0){
	linkedin_url.addEventListener('change', checkUrlValidity, false);
}

if( $('#facebook_url').length>0){
	facebook_url.addEventListener('change', checkUrlValidity, false);
}

if( $('#twitter_url').length>0){
	twitter_url.addEventListener('change', checkUrlValidity, false);
}

if( $('#googleplus_url').length>0){
	googleplus_url.addEventListener('change', checkUrlValidity, false);
}

$('#subscription_list').change(function (e) {  
  e.preventDefault();     
		var element = $(this);
		var _data = {};
		$.each(element.get(0).attributes, function (i, attrib) {
			_data[attrib.name.replace('data-', '')] = attrib.value;
		});
		_data.value = element.val();
        _data.ssubscription = $( "#subscription_list option:selected" ).val();
      $.ajax({
        type: "POST",
        url: ajaxUrl + 'ajax' + _data.path,
        dataType: 'json',
        data: _data,
        beforeSend: function () { 
        },
        success: function (data) {
        
           $.each(data, function(key, val) {
          $("#fee_type").val(val.fee_type);
          $("#cost_per_unit").val(val.cost_per_unit);
          $("#billing_frequency").val(val.billing_frequency);
          $("#subscription_users_allowed").val(val.subscription_users_allowed);
          $("#subscription_users_allowed").val(val.subscription_users_allowed);
        });
          
        },
        complete: function () {
          
           
        }
    });
});

$("#datepicker").datepicker({
  onSelect: function(selectedDate) {
      
      var formatdate = selectedDate.split("/");
      var newFormatDate = formatdate[1]+'/'+formatdate[0]+'/'+formatdate[2];
     var expected_month = '';
     var billing_frequency = $( "#billing_frequency" ).val();
    
    switch (billing_frequency) {
       case 'Annually':            
           expected_month = 12;
       break;

       case 'Quarterly':
            expected_month = 3;
       break;
       
       case 'Monthly':
            expected_month = 1;
       break;

       case 'Half Yearly': 
           expected_month = 6;
       break;

       default: expected_month = 1;
    }
    expected_month = parseInt(expected_month);
    var end_date = getFutureDate(newFormatDate,expected_month);     
    $(".endDate").val(end_date);
  }
});

$('body').on('click', '#subscribe_package', function (e) {
    
        e.preventDefault();
        var element = $('#subscription-purchase-form');
        var _data = {};
        var f = $("#subscription-purchase-form");
        var form_input = f.serialize();
        $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });
        _data['form-input'] = form_input;
        doAjax(ajaxUrl, _data, false);
        $('#myModalHorizontal').modal('hide');
        $('.company_add_subscription').hide();     
        e.stopPropagation(); //This line would take care of it
});

$('body').on('click', '.company_add_subscription', function (e) {
     
        var returnval = false;
        var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });         
             
                $.ajax({
                   type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }
                        $(".header__row").html();
                    },
                    success: function( data ) {
                     if (data.success) {
                        
                        returnval = true;
                        return true;
                     } 
                     else {                             
                            if ('array' in data.data) {
                                 $(".header__row").html("");
                                 jQuery.each(data.data.array.message, function() {
                                     var dd = 'not aurhrised';
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                     $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                        
                    },
            });
            if(!returnval) {           
                return false;
            }
       
    });
    
    $('body').on('click', '.resetcompanyunapproved', function (e) {

            var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });

            _data.value = element.val();
		 $('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to Reset This Comapany to previous status ?</h6></div>')
		.dialog({
			modal: true, title: 'Reset Company', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {

				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");

					//window.location.reload();
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});
        
       return false;
        

    });
    
    
    var getFutureDate = function(start_date,expected_month) {
        var calcval = null;
        var term = expected_month;

        var set_start = start_date.split('/');  

        var day = set_start[0];  
        var month = (set_start[1] - 1);  // January is 0 so August (8th month) is 7
        var year = set_start[2];
        var datetime = new Date(year, month, day);
        var newmonth = (month + parseInt(term));  // Must convert term to integer
        var newdate = datetime.setMonth(newmonth);

        newdate = new Date(newdate);

        day = newdate.getDate();
        month = newdate.getMonth() + 1;
        year = newdate.getFullYear();

        // This is British date format. See below for US.
        calcval = (((day <= 9) ? "0" + day : day) + "/" + ((month <= 9) ? "0" + month : month) + "/" + year);

        // mm/dd/yyyy
        calcval = (((month <= 9) ? "0" + month : month) + "/" + ((day <= 9) ? "0" + day : day) + "/" + year);
       // alert(calcval);
        return calcval;
}

function resetcompanyunapproved() {
    location.reload();
}
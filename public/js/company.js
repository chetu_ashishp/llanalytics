(function ($) {
   
	
	$('.modal-view').on('hidden.bs.modal', function (e) {
		  
		$('.data-table').bootstrapTable('refresh', {
		        silent: true,
		        url: ajaxUrl + '/ajaxsetReadAlert',
		        query: {alertid: $('.alertid').val(), filter: $('.alertfilter').val()  }
		}); 
	})
   
	
	
	
	 $('body').on('change', '.alertfilter', function (e) {
			 $('.data-table').bootstrapTable('refresh', {
			        silent: true,
			        url: ajaxUrl + '/ajaxFilterAlerts',
			        query: {filter: $('.alertfilter').val()  }
			}); 
	    });

	
	 $('body').on('click', '.setComplete', function (e) {
		
		 $('.data-table').bootstrapTable('refresh', {
		        silent: true,
		        url: ajaxUrl + '/ajaxCompleteAlert',
		        query: {alertid: $(this).data('item'), filter: $('.alertfilter').val()  }
		});
		 
    });
	 
	 
	 
	 
})(jQuery);



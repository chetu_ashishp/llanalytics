(function($) {
	 
	$('body').on('change', '#type_id', function (e) {
	        
		if( $("#type_id :selected").text()=='Transaction Status'){
			$('#considered_status_flag').show();			
	 	}
	 	else{
	 		$('#considered_status_flag').hide();	 		 
	 	}
	});
	 
	 
	$('body').on('change', '#etype_id', function (e) {
	        
		if( $("#etype_id :selected").text()=='Transaction Status'){
			$('#econsidered_status_flag').show();				
	 	}
	 	else{
	 		$('#econsidered_status_flag').hide();		 		 
	 	}		    	        
	}); 
	 	 
	 
	$('.modal-add').on('show.bs.modal', function (e) {
		 $("#type_id").val("");		
	});
		 
	$('body').on('click', '.addcompany_tool_option, .companytoolsedit', function (e) {
       //alert('sheetcreate');
        var returnval = false;
        var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });         
             
                $.ajax({
                   type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }                       
                    },
                    success: function( data ) {
                     if (data.success) {
                        
                        returnval = true;
                        return true;
                     } 
                     else {                             
                            if ('array' in data.data) {
                                 $(".header__row").html("");
                                 jQuery.each(data.data.array.message, function() {
                                     var dd = 'not aurhrised';
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                   
                                      $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                    },
            });
            if(!returnval) {
               return false;
            }        
            
    }); 
    
    $('body').on('click', '.companytoolsedit', function (e) {
       //alert('sheetcreate');
        var returnval = false;
        var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });         
             
                $.ajax({
                   type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }                       
                    },
                    success: function( data ) {
                     if (data.success) {
                        _data.value = element.val();
                        doAjax(ajaxUrl, _data, false); 
                     } 
                     else {                      
                            if ('array' in data.data) {
                                 $(".header__row").html("");
                                 jQuery.each(data.data.array.message, function() {
                                     var dd = 'not aurhrised';
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                   
                                      $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                    },
            });
            if(!returnval) {
               return false;
            }        
            
    }); 
    
})(jQuery);



function resetToolsAddForm(){
	$('#considered_status_flag').hide();
	//$("input[name=option_status] [value='Active']").attr("selected", "selected");			
}

function resetToolsEditForm(){	
	$('#econsidered_status_flag').hide();
	//$("input[name=option_status] [value='Active']").attr("selected", "selected");
}
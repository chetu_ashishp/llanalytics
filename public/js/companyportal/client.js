(function ($) {
    var check = true;
    $('.phoneUS').mask('000.000.0000');
    $(document).ready(function () {

    });
    
	$('body').on('click', '#deleteCompanyClient', function (e) {
        //alert('deleteCompanyClient');
		e.preventDefault();
		var element = $(this);
		var _data = {};
		$.each(element.get(0).attributes, function (i, attrib) {
			_data[attrib.name.replace('data-', '')] = attrib.value;
		});
		_data.value = element.val();
		
		$('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to delete this Client ?</h6></div>')
		.dialog({
			modal: true, title: 'Delete Client', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {
                    console.log('CompanyPortalStaff: ');
					console.log(ajaxUrl, _data);
                    doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");
					//window.location.reload();
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});
        return false;
	});

})(jQuery);


function RemoveCompanyPortalStaff(id, nroute, token) {
	var _data = {};
	_data['id'] = id;
	_data['path'] = 'CompanyPortalStaff';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
	//console.log('removeomsstaff');
	//console.log(ajaxUrl, _data);
	 doAjax(ajaxUrl, _data, false);	
}

function deleteCompanyClient() {
	//location.reload();
    redirectTo('companyportal/clients');
}

function RemoveCompanyClient(id, nroute, token) {
    
    var _data = {};
	_data['id'] = id;
	_data['path'] = 'CompanyPortalClient';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
	//console.log('removeomsstaff');
	//console.log(ajaxUrl, _data);
	 doAjax(ajaxUrl, _data, false);	
}


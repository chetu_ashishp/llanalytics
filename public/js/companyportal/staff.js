(function ($) {
    var check = true;
    $('.phoneUS').mask('000.000.0000');
    $(document).ready(function () {

    });
    
	$('body').on('click', '.deleteCompanyportalStaff', function (e) {
		e.preventDefault();
		var element = $(this);
		var _data = {};
		$.each(element.get(0).attributes, function (i, attrib) {
			_data[attrib.name.replace('data-', '')] = attrib.value;
		});
		_data.value = element.val();
		
		$('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to delete this Company Portal Staff ?</h6></div>')
		.dialog({
			modal: true, title: 'Delete Company Portal Staff', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {
                    //console.log('CompanyPortalStaff: ');
				//	console.log(ajaxUrl, _data);
				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");
					//window.location.reload();
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});
        return false;
	});

})(jQuery);


function RemoveCompanyPortalStaff(id, nroute, token) {
	var _data = {};
	_data['id'] = id;
	_data['path'] = 'CompanyPortalStaff';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
	//console.log('removeomsstaff');
	//console.log(ajaxUrl, _data);
	 doAjax(ajaxUrl, _data, false);	
}

function deleteCompanyportalStaff() {
	location.reload();
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}  
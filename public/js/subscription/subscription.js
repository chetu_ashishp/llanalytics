(function($) {

    $('.feetype').inputmask({'mask':["9{0,5}.9{0,2}", "999"]});
    
    $('body').on('click', '.deletesubscriptionpackage22', function (e) {
		e.preventDefault();     
		var element = $(this);
		var _data = {};
		$.each(element.get(0).attributes, function (i, attrib) {
			_data[attrib.name.replace('data-', '')] = attrib.value;
		});
		_data.value = element.val();
		
		$('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to delete this Package ?</h6></div>')
		.dialog({
			modal: true, title: 'Delete Package', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {
					//console.log(ajaxUrl, _data);
				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");
					//window.location.reload();
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});

        return false;
	});
    
    $('body').on('click', '.deletesubscriptionpackage', function (e) {
		e.preventDefault();     
		var element = $(this);
		var _data = {};
		$.each(element.get(0).attributes, function (i, attrib) {
			_data[attrib.name.replace('data-', '')] = attrib.value;
		});
		_data.value = element.val();
		
        //
        $.ajax({
                type: "POST",
                 url: ajaxUrl + 'validateajaxPermission',
                 dataType: 'json',
                 data: _data,
                 async: false,
                 beforeSend: function () {
                     if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                     $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                     }                       
                 },
                 success: function( data ) {
                  if (data.success) {

                     returnval = true;
                     return true;
                  } 
                  else {                             
                         if ('array' in data.data) {
                              $(".header__row").html("");
                              jQuery.each(data.data.array.message, function() {
                                  var dd = 'not aurhrised';
                                  $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                 
                                  $(".header__row").fadeIn().delay(20000).fadeOut();
                              });
                         }                         
                     }
                 },
                 complete: function () {
                     if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                             $.unblockUI();
                         }
                     if (element !== false) {
                         element.removeClass('working');
                     }
                     $('.mainnav a').css('pointer-events', ' auto');
                      //return false;
                 },
            });
         if(returnval) {
        ////////
		$('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to delete this Package ?</h6></div>')
		.dialog({
			modal: true, title: 'Delete Package', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {
					//console.log(ajaxUrl, _data);
                   _data.success = false;
				   doAjax(ajaxUrl, _data, false);
				   $(this).dialog("close");
					//window.location.reload();
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});
       } 
       return false;

    });
    
   $('body').on('click', '.subscriptionscreate, .subscriptionsedit2', function (e) {
       //alert('sheetcreate');
        var returnval = false;
        var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });         
             
                $.ajax({
                   type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }                       
                    },
                    success: function( data ) {
                     if (data.success) {
                        
                        returnval = true;
                        return true;
                     } 
                     else {                             
                            if ('array' in data.data) {
                                 $(".header__row").html("");
                                 jQuery.each(data.data.array.message, function() {
                                     var dd = 'not aurhrised';
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                   
                                      $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                    },
            });
            if(!returnval) {
               return false;
            }        
            
    }); 
    
    $('body').on('click', '.resetsubscription', function (e) {
     
            var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });

         _data.value = element.val();
		 $('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to Reset Subscription to Previous Status ?</h6></div>')
		.dialog({
			modal: true, title: 'Reset Subscription', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {

				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});
        
       return false;
        

    });

})(jQuery);

 function RemoveSubscription(id, nroute, token) {
	var _data = {};
	_data['id'] = id;
	_data['path'] = 'Subscriptions';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
	//console.log('removeomsstaff');
	//console.log(ajaxUrl, _data);
	 doAjax(ajaxUrl, _data, false);	
 }

function deleteSubscription() {
    window.location.reload();
}

function resetSubscriptionPackage() {
    window.location.reload();
}
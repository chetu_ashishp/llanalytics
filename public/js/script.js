(function ($) {
    $('body').on('click change', '.ajax', function (e) {
        e.preventDefault();
		var returnval = false;
           
        var element = $(this);
		//console.log(element);
        if (e.type == 'click' && element.is('select')) { return; }
        var _data = {};

        $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });

        _data.value = element.val();
        //console.log(ajaxUrl, _data);
        $.ajax({
                   type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }
                        $(".header__row").html('');
                    },
                    success: function( data ) {
                     if (data.success) {
                        returnval = true;
                        return true;
                     } 
                     else {                             
                            //alert('here');
                            if ('array' in data.data) {
                                 jQuery.each(data.data.array.message, function() {
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                     $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                         //return false;
                    },
            });
            if(!returnval) {
               
               return false;
            } else {
                doAjax(ajaxUrl, _data, false);
            }
    });

    $('body').on('submit', '.form-add, .form-edit', function (e) {
        e.preventDefault();
        var element = $(this);
        var _data = {};
        var form_input = element.serialize();

        $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });
		_data['form-input'] = form_input; 
        //console.log(_data['form-input']);
		if(_data.id == 'staff-edit-form') {
			var primary_contact = $('select[name=primary_contact]').val();
			
			if(primary_contact == 1 && !$('select[name=primary_contact]').attr("readonly")) {
					
				/*if(confirm("Are you sure you want to change primary staff?")) {
				} else {
					return;
				}*/
				$('<div></div>').appendTo('body')
				.html('<div><h6>Are you sure you want to change primary staff ?</h6></div>')
				.dialog({
					modal: true, title: 'Change Primary Staff', zIndex: 10000, autoOpen: true,
					width: 'auto', modal: true, resizable: false,
					buttons: {
						Confirm: function () {
                            console.log(ajaxUrl, _data);
						   doAjax(ajaxUrl, _data, false);
							$(this).dialog("close");

							//window.location.reload();
						},
						No: function () {
							$(this).dialog("close");
						}
					},
					Cancel: function (event, ui) {
						$(this).remove();
					}
				});

			   return false;
			}
		}
            
        doAjax(ajaxUrl, _data, false);
    })


     $('body').on('submit', '.form-search', function (e) {
        e.preventDefault();
        var element = $(this);
        var _data = {};
        var form_input = element.serialize();
        $('.data-table').bootstrapTable('refresh', {
            silent: true,
            url: ajaxUrl + 'search',
            query: {params: form_input}
        });
    })
    
    $('body').on('submit', '.companyportal-form-search', function (e) {
        e.preventDefault();
        var element = $(this);
        var _data = {};
        var form_input = element.serialize();
        $('.data-table').bootstrapTable('refresh', {
            silent: true,
            url: ajaxUrl + 'companyClientSearch',
            query: {params: form_input}
        });
    })

	$('body').on('submit', '.form-company-search', function (e) {
        e.preventDefault();
        var element = $(this);
        var _data = {};
         $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });
        _data.value = element.val();
      
        var form_input = element.serialize();
        $('.data-table').bootstrapTable('refresh', {
            silent: true,
            url: ajaxUrl + 'company/search',
            query: {params: form_input}
        });
    })
	
	$('body').on('submit', '.form-staff-search', function (e) {
        e.preventDefault();
        var element = $(this);
        var _data = {};
        var form_input = element.serialize();
        $('.data-table').bootstrapTable('refresh', {
            silent: true,
            url: ajaxUrl + 'staff/search',
            query: {params: form_input}
        });
    })
    
    $('body').on('submit', '.form-staff-active-search', function (e) {
        e.preventDefault();
        var element = $(this);
        var _data = {};
         $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });
        _data.value = element.val();
      
        var form_input = element.serialize();
       console.log(ajaxUrl + 'staffs/search');
        $('.data-table').bootstrapTable('refresh', {
            silent: true,
            url: ajaxUrl + 'staffs/search',
            query: {params: form_input}
        });
    })
	
    $('body').on('submit', '.form-omsstaff-active-search', function (e) {
        e.preventDefault();
        var element = $(this);
        var _data = {};
         $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });
        _data.value = element.val();
      
        var form_input = element.serialize();
        console.log(ajaxUrl + 'omsstaffs/search');
        $('.data-table').bootstrapTable('refresh', {
            silent: true,
            url: ajaxUrl + 'omsstaffs/search',
            query: {params: form_input}
        });
    })    
    
     $('body').on('submit', '.form-companysubscription-search', function (e) {
        e.preventDefault();
        var element = $(this);
        var _data = {};
         $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });
        _data.value = element.val();
      
        var form_input = element.serialize();
        console.log(ajaxUrl + 'companysubscribedSearch');
        $('.data-table').bootstrapTable('refresh', {
            silent: true,
            url: ajaxUrl + 'companysubscribedSearch',
            query: {params: form_input}
        });
    })    
    
    $('body').on('submit', '.form-subscription-search', function (e) {
        
        e.preventDefault();
        var element = $(this);
        var _data = {};
         $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });
        _data.value = element.val();
      
        var form_input = element.serialize();
        console.log(ajaxUrl + 'subscriptions/search');
        $('.data-table').bootstrapTable('refresh', {
            silent: true,
            url: ajaxUrl + 'subscriptions/search',
            query: {params: form_input}
        });
    })
    /*$('body').on('click', '.oms_Staff, .templates, .sheets, .reportsview', function (e) {
        var returnval = false;
        var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });         
             
                $.ajax({
                   type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }                       
                    },
                    success: function( data ) {
                     if (data.success) {
                        
                        returnval = true;
                        return true;
                     } 
                     else {                             
                            if ('array' in data.data) {
                                 $(".header__row").html("");
                                 jQuery.each(data.data.array.message, function() {
                                     var dd = 'not aurhrised';
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                     $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                    },
            });
            if(!returnval) {
                console.log(returnval);
                return false;
            }            
    });*/
    
    if(typeof tableData !== 'undefined') {

		if($('.data-table').hasClass('not_sortable')){
			$('.data-table').bootstrapTable({
				data: tableData,
				sortable:false
			});

		}else{
			$('.data-table').bootstrapTable({
				data: tableData,
			});
		}

        if($('.data-table tbody').hasClass('sortable')) {
            $('.sortable').sortable();
        }

    } else {
        $('.data-table').bootstrapTable();
    }


    $('body').on('change', '#filter_data_type', function (e) {
    	//$('.data-table').bootstrapTable("filterBy", {name: 'dsad'});
    	_filterTable($(this).val())
    });


    $(window).resize(function () {
        $('#data-table').bootstrapTable('resetView');
    });


   /* $('body').on('keyup', 'input[type=url]', function (e) {
    	 string = this.value;
    	 if(!(/^http:\/\//.test(string))){
             string = "http://" + string;
         }
         this.value=string;
    });*/
    




})(jQuery);



var doAjax = function (ajaxUrl, _data, element) {
    $.ajax({
        type: "POST",
        url: ajaxUrl + 'ajax' + _data.path,
        dataType: 'json',
        data: _data,
        beforeSend: function () {
            //console.log('----');
            console.log(ajaxUrl + 'ajax' + _data.path);
            console.log(ajaxUrl + 'ajax' + _data.path);
            if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
        	$.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
            }
            $(".header__row").html('');
        	/*
            if (element !== false) {
                element.addClass('working');
            }*/
        },
        success: function (data) {
			console.log('data');
            console.log(data);
            var ajaxpath = 'ajax'+_data.path;
            console.log(ajaxpath);
            if(data.false == true && (ajaxpath == 'ajaxSubscriptions' || ajaxpath == 'ajaxCompanyPortalClient' || ajaxpath == 'ajaxTemplates')) {
                data.success = false;
            }
            //console.log(data.success);
            if (data.success == true && 'data' in data) {
              
            	if ('redirect' in data.data) {
                    window.location.replace(data.data.redirect);
                }
                if ('target' in data.data && 'html' in data.data) {
                        
                    if ('append' in data.data) {
                        if (data.data.append == 'append') {

                        	//alert(data.data.target);
                        	//alert(data.data.html);

                            $(data.data.target).append(data.data.html);
                        } else if (data.data.append == 'replace') {
                            $(data.data.target).replaceWith(data.data.html);
                        } else {
                            $(data.data.target).html(data.data.html);
                        }
                    } else {
						//alert(data.data.target);
                        $(data.data.target).html(data.data.html);
                    }
                }
                if ('script' in data.data) {
                    eval(data.data.script);
                }
                if ('alert' in data.data) {
                    $(".header__row").prepend("<div class='alert alert-" + data.data.alert.type + " alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + data.data.alert.message + "</div>");
                }
            } else if (data.success == false && 'data' in data) {
                  
				
            	if ('array' in data.data) {

            		 $(".header__row").html("");
            		 jQuery.each(data.data.array.message, function() {

            			 $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                         /*setTimeout(function () {
                            $(".header__row").fadeOut(function(){
                             // $(this).remove();
                            });
                          }, 2000);*/
                          $(".header__row").fadeIn().delay(20000).fadeOut();
            		 });
                }
                //return false;
            }
        },
        complete: function () {
        	if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                    $.unblockUI();
                }
            if (element !== false) {
                element.removeClass('working');
            }
            $('.mainnav a').css('pointer-events', ' auto');
             //return false;
        }/*,

        error: function(data){
          var errors = data.responseJSON;
          console.log(errors);
        }*/
    });



}

var search = function(){
	 // refreshTable();
}

var create = function() {
}

var store = function() {
    $('.modal-add').modal('hide');

    $('.form-add, .form-edit').closest('form').find("input, textarea, select").not("input[type='radio'], input[type='checkbox']").val("");
    $('.form-add, .form-edit').closest('form').find("input[type='radio'], input[type='checkbox']").removeAttr('checked');

    refreshTable();
}


var edit = function() {
    $('.modal-edit').modal('show');
}

var view = function() {
    $('.modal-view').modal('show');
}

var update = function() {
    $('.modal-edit').modal('hide').html('');
    refreshTable();
}

var removeRecord = function(id, nroute) {

	$('.data-table').bootstrapTable('refresh', {
	        silent: true,
	        url: ajaxUrl + '/ajax' + $('#data-ajax').val(),
	        query: {route: nroute, deleteid: id }
	    });

	 if($('#filter_data_type').length)
		 $('#filter_data_type').val('');
}

var confirmRemove= function(id, record, route){

	if (confirm('Are you sure you want delete the '+record+'?')){
		removeRecord(id, route);
	}
	else{
		return false;
	}
}


var refreshTable = function() {
    $('.data-table').bootstrapTable('refresh', {
        silent: true,
        url: ajaxUrl + '/ajax' + $('#data-ajax').val(),
        query: {route: 'show',  }
    });
    $('#filter_data_type').val('');
}


var _filterTable = function(p) {


	 // $('.data-table').bootstrapTable("filterBy", {type: p});
    $('.data-table').bootstrapTable('refresh', {
        silent: true,
        url: ajaxUrl + 'ajax' + $('#data-ajax').val(),
        query: {route: 'show', typeId: p}
    });

}

var initBootstrapTable= function(tableData){
	  if(typeof tableData !== 'undefined') {
	        $('.data-table').bootstrapTable({
	            data: tableData,
	        });
	    }
}


var IsTypeSupported = function (tagName, typeValue) {
    var input = document.createElement(tagName);
    input.setAttribute("type", typeValue);
    if (input.type !== "text") {
        return true;
    }
    delete input;
    return false;
}

  function split(val) {
    return val.split(/,\s*/);
  }

  function extractLast(term) {
    return split(term).pop();
  }

  var redirectTo = function (url){
	  window.location.href = ajaxUrl+url;
  }



  $(function(){
  	$('.master-nav li.dropdown a.dropdown-toggle').click(function(){
  		var obj = $(this).parent('.dropdown').find('.dropdown-menu').first();
  		obj = obj.find('a').first();
  		if(obj){
  			if(obj.prop('href')){
  				parent.location.href = obj.prop('href');
  			}
  		}
  	});
  });

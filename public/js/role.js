(function ($) {
    var check = true;
    $(document).ready(function () {
        
        $('.role_name').change(function (e) {
               
            $( '.routelist' ).removeClass( "hide" )
            $('.routelist').show();
            var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });
            console.log(ajaxUrl + 'ajax' + _data.path);
            _data.value = element.val();
            _data.roleid = $( ".role_name option:selected" ).val();
            $.ajax({
              type: "POST",
              url: ajaxUrl + 'ajax' + _data.path,
              dataType: 'json',
              data: _data,
              beforeSend: function () {
              },
              success: function (data) {
                      $('input:checkbox').removeAttr('checked');
                      $.each(data, function(key, val) {                    
                        $("#"+val).prop('checked', true);
                    });          
              },
              complete: function () {

              }
          });
        });
    });
    
    $('body').on('click', '.rolepermissionassign, .createrole, .editroles, .dataoption, .dataedit, .datadelete, .updatesetting', function (e) { 
        var returnval = false;
        var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });         
             
                $.ajax({
                   type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }
                         $(".header__row").html('');
                    },
                    success: function( data ) {
                     if (data.success) {
                        
                        returnval = true;
                        return true;
                     } 
                     else {                             
                            if ('array' in data.data) {
                                 $(".header__row").html("");
                                 jQuery.each(data.data.array.message, function() {
                                     var dd = 'not aurhrised';
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                   
                                     $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                    },
            });
            if(!returnval) {
                return false;
            }            
    }); 

})(jQuery);

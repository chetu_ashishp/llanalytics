(function ($) {
    var check = true;
    $(document).ready(function () {

        if (getUrlParameter('transaction') == 'add') {
            $('a#ltransactions').click();
            $(document).ajaxComplete(function (event, xhr, settings) {
                if (check) {
                    $('button[data-route="addTransaction"]').click();
                    check = false;
                }
            });
        }else if (getUrlParameter('transaction') == 'edit' && getUrlParameter('transaction_id')!=undefined) {
            $('a#ltransactions').click();
            var id=getUrlParameter('transaction_id');
            $(document).ajaxComplete(function (event, xhr, settings) {
                if (check) {
                    $('button.edittransaction-btn[data-id="'+id+'"]').click();
                    check = false;
                }
            });
        }
    });

    $('body').on('click', '#cancelAddForm', function (e) {
        $('#newtransaction-div').hide('slow');
        $('#transactions-div').show('slow');
        $('#trasaction-add-form')[0].reset();
    });

    $('body').on('click', '#cancelEditForm', function (e) {

        $('#edittransaction-div').html('');
        $('#edittransaction-div').hide('slow');
        $('#transactions-div').show('slow');
    });

    $('body').on('click', '.deleteTransaction', function (e) {

        if (confirm('Are you sure you want delete the transaction?')) {

            var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });

            _data.value = element.val();
            doAjax(ajaxUrl, _data, false);

        }
        else {
            return false;
        }

    });

    $('body').on('change', '#transaction_status', function (e) {

        var s = $("#transaction_status :selected").text().toLowerCase();

        if (s.indexOf("listing") > -1) {
//            $('.list_info').show();
        }
        else {
//            $('.list_info').hide();
        }

    });

    /*
     
     console.log(s.includes("listing"));
     */
    $('body').on('blur', '#sales_price, #bonus, #agent_proportio, #commission_rate, #agent_split, #agent_split2, #broker_flat_fee, #assist_fee, #origin_list_price, #current_list_price', function () {


        $('#agent_proportio, #commission_rate, #agent_split, #agent_split2, #sp_of_list, #sp_of_origin_list').formatCurrency({
            symbol: '',
			roundToDecimalPlace: 6
        });

		$('#origin_list_price, #current_list_price, #gross_commission, #net_commission, #final_net_commission, #sales_price, #bonus, #broker_flat_fee, #assist_fee').formatCurrency({
            symbol: '',
			roundToDecimalPlace: 2
        });		

		$('#dom_to_contract, #dom_to_close').formatCurrency({
            symbol: '',
			roundToDecimalPlace: 0
        });		
    });


})(jQuery);

var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        matches = [];

        substrRegex = new RegExp(q, 'i');

        $.each(strs, function(i, str) {
            if (substrRegex.test(str)) {
                matches.push(str);
            }
        });

        cb(matches);
    };
};


var initializeTypeAhead = function() {
    $('input[name="escrow_officer"]').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'escrowOfficers',
            source: substringMatcher(escrow_officers)
        });

    $('input[name="loan_officer"]').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'escrowOfficers',
            source: substringMatcher(loan_officers)
        });
}

function RemoveTransaction(id, nroute, token){
	var _data = {};
	_data['id'] = id;
	_data['path'] = 'Transactions';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
	 doAjax(ajaxUrl, _data, false);	
}




function preffilAppField(id, all) {

    json_obj = JSON.parse(all);

    $('#sales_price').val(json_obj.sales_price);    
    $('#nickname').val(json_obj.nickname);
    $('#property_house').val(json_obj.property_house);
    $('#property_addr1').val(json_obj.property_addr1);
    $('#property_addr2').val(json_obj.property_addr2);
    
    $('#city_id').val(json_obj.city_id);
    $('#state').val(json_obj.state);
    $('#zip').val(json_obj.zip);
    $('#subdivision').val(json_obj.subdivision);
    $('#hoa').val(json_obj.hoa);
        
    if(json_obj.coe_date)
    	$('#coe_date').val( moment(json_obj.coe_date).format('MM/DD/YYYY'));
    if(json_obj.contract_date)
    $('#contract_date').val(moment(json_obj.contract_date).format('MM/DD/YYYY'));
        
    $('#other_agent_name').val(json_obj.other_agent_name);
    $('#other_company_name').val(json_obj.other_company_name);
    
    $('#lender').val(json_obj.lender);
    $('#title_company').val(json_obj.title_company);
    $('#escrow_officer').val(json_obj.escrow_officer);
     
    reCalculateFinacicalInfo();
}

function reCalculateFinacicalInfo() {
	
    var sales_price = parseFloat($('#sales_price').asNumber());
    var agent_proportio = parseFloat($('#agent_proportio').asNumber()) / 100;
    var commission_rate = parseFloat($('#commission_rate').asNumber()) / 100;
    var agent_split = parseFloat($('#agent_split').asNumber()) / 100;
    var agent_split2 = parseFloat($('#agent_split2').asNumber()) / 100;
    var bonus = parseFloat($('#bonus').asNumber());
    
    if(agent_proportio==0)
    	agent_proportio=1;
    
    if(agent_split==0)
    	agent_split=1;
	
    if(agent_split2==0)
    	agent_split2=1;
    
    var broker_flat_fee 	= parseFloat($('#broker_flat_fee').asNumber());
    var assist_fee 			= parseFloat($('#assist_fee').asNumber());
    var current_list_price 	= parseFloat($('#current_list_price').asNumber());
    var origin_list_price 	= parseFloat($('#origin_list_price').asNumber());

    //	Gross Commission (calculated, display only after entry of needed values, Selling Price * Agent Proportion * Commission Rate) 
    if (isNumber(sales_price) && isNumber(agent_proportio) && isNumber(commission_rate)) {

        var gross_commission = parseFloat(sales_price * agent_proportio * commission_rate)+bonus;
        gross_commission = Math.round(gross_commission * 100) / 100;
        $('#gross_commission').val(gross_commission);


        //.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')



        // Net Commission (calculated, display only after entry of needed variables, Gross Commission * ( Agent Split * Agent Split 2 ) - Broker Flat Fee) 
        if (isNumber(agent_split) && isNumber(agent_split2) && isNumber(broker_flat_fee)) {
          	
        	var net_commission = gross_commission * agent_split * agent_split2 - broker_flat_fee;
            net_commission = Math.round(net_commission * 100) / 100;
            $('#net_commission').val(net_commission);
        }
        else {
            $('#net_commission').val('');
        }

        // Final Net Commission (calculated, display only after entry of needed variables, Net Commission - Assist Fee)
        if (isNumber(net_commission) && isNumber(assist_fee)) {
            var final_net_commission = net_commission - assist_fee;
            $('#final_net_commission').val(final_net_commission);
        }
        else {
            $('#final_net_commission').val('');
        }

    }
    else {
        $('#gross_commission').val('');
        $('#net_commission').val('');
        $('#final_net_commission').val('');
    }




    // SP as % of List (calculated, display only after entry of variables, Selling Price / List Price 
    if (isNumber(current_list_price) && isNumber(sales_price) && current_list_price) {
        var sp_of_list = sales_price / current_list_price * 100;
        sp_of_list = Math.round(sp_of_list * 100) / 100;
        $('#sp_of_list').val(sp_of_list);
    }
    else {
        $('#sp_of_list').val('');
    }

    // SP as % of Original List (calculated, display only after entry of variables, Selling Price / Original List Price) 
    if (isNumber(origin_list_price) && isNumber(sales_price) && origin_list_price) {
        var sp_of_origin_list = sales_price / origin_list_price * 100;
        sp_of_origin_list = Math.round(sp_of_origin_list * 100) / 100;
        $('#sp_of_origin_list').val(sp_of_origin_list);
    }
    else {
        $('#sp_of_origin_list').val('');
    }








}

function reCalculateDates() {

	
    var coe_date = $('#coe_date').val();
    var listing_date = $('#listing_date').val();
    var contract_date = $('#contract_date').val();

//	DOM to Contract (calculated, display only after entry of variables, Contract Date - Listing Date ( # of Days ))
    if (contract_date && listing_date) {
        var dom_to_contract = getDateDiff(contract_date, listing_date);
        $('#dom_to_contract').val(dom_to_contract);
    }
    else{
    	 $('#dom_to_contract').val("");
    }
    
    
    
    //	DOM to Close (calculated, display only after entry of variables, COE Date - Listing Date ( # of Days ))
    if (coe_date && listing_date) {
        var dom_to_close = getDateDiff(coe_date, listing_date);
        $('#dom_to_close').val(dom_to_close);
    }
    else{
    	$('#dom_to_close').val("");
    }
}


function checkValue(str) {
    if (str.replace(/\s/g, "") != "" && isNumber(str))
        return true;

}

function isNumber(o) {
    return typeof o === 'number' && isFinite(o);
}

function getDateDiff(time1, time2) {
    var str1 = time1.split('/');
    var str2 = time2.split('/');
    var t1 = new Date(str1[2], str1[0] - 1, str1[1]);
    var t2 = new Date(str2[2], str2[0] - 1, str2[1]);

    var diffMS = t1 - t2;
    var diffS = diffMS / 1000;
    var diffM = diffS / 60;
    var diffH = diffM / 60;
    var diffD = diffH / 24;
    return Math.round(diffD);
}

function storeTransaction() {
    $('#trasaction-add-form')[0].reset();
    $('#ltransactions').click();
}

function updateTransaction() {
    $('#ltransactions').click();
}


function deleteTransaction() {
    $('#ltransactions').click();
}


function addTransaction() {
    initializeTypeAhead();
    $('#start_date').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});
    $('#listing_date').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});
    $('#listing_expiration_date').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});
    $('#contract_date').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});
    
    $('#coe_date').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});



    $("#relatedtransid").autocomplete({
        source: ajaxUrl + 'transactions/autocompleate',
        minLength: 2,
        select: function (event, ui) {
            $('#related_trans_id').val(ui.item.id);

            if (confirm("Prefill applicable fields from related transaction?")) {
                preffilAppField(ui.item.id, ui.item.all);
            }
        },
        html: true,
        open: function (event, ui) {
            $(".ui-autocomplete").css("z-index", 1000);
        }
    });

    $('body').on('keyup', '.finance', function (e) {
        reCalculateFinacicalInfo();
    });

    $(".findates").on("dp.change", function (e) {
        reCalculateDates();
    });

    $('#transactions-div').hide('slow');
    $('#newtransaction-div').show('slow');
}

function editTransaction() {

    initializeTypeAhead();

    $('#start_date').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});
    $('#listing_date').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});
    $('#listing_expiration_date').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});
    $('#contract_date').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});
   
    $('#coe_date').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});


    $("#relatedtransid").autocomplete({
        source: ajaxUrl + 'transactions/autocompleate',
        minLength: 2,
        select: function (event, ui) {
            $('#related_trans_id').val(ui.item.id);
            
            var fill_data = false;
            $(".tprefill").each(function() { 
                if( $(this).val()!=''){
                	
            		if( $(this).attr('id')!='sales_price' || ( $(this).attr('id')=='sales_price' && $(this).val()!='0.00' ) ){
            			fill_data = true;
                    	return false; 
            		}                		            		            		
                }   
                console.log($(this).val());
            });
            
            //console.log($('#sales_price').val());
            
            if(fill_data==false){
            		if (confirm("Prefill applicable fields from related transaction?")) {
            			preffilAppField(ui.item.id, ui.item.all);
            		}   
            } 
            
            
        },
        html: true,
        open: function (event, ui) {
            $(".ui-autocomplete").css("z-index", 1000);
        }
    });

    $('body').on('keyup', '.finance', function (e) {
        reCalculateFinacicalInfo();
    });

    $(".findates").on("dp.change", function (e) {
        reCalculateDates();
    });

    $('#transactions-div').hide('slow');
    $('#edittransaction-div').show('slow');
}


function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}  
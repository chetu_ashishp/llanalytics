(function($) {

	
	$('#datepicker').datetimepicker({format: 'MM/DD/YYYY', useCurrent:false});
	$('.phoneUS').mask('000.000.0000');
	
	
	
	 $('body').on('click', '.del-addr-rec', function (e) {
	        e.preventDefault();
	        
	        itemRemove = $(this).data('section');
	        
	        $('#sec'+itemRemove).remove();
	    });
	
	 
	 $('body').on('click', '.mailingaddr', function (e) {
	        
	        if( $( this ).is(':checked') ){
	        	
	        	$( ".mailing" ).each(function( index, element ) {
	        		
		            // element == this
		            if( $(element).is(':checked') ){
		            	
		            	$('.mailingaddr').prop('checked', false);
		            	alert('Only one mailing address may be set for a client');		            	
		            	return false;
		            }
		         });
            }	        
	    });
	 
	 
	 $('body').on('click', '.mailing', function (event) {
	        
		 /* alert(event.type); 
		  alert(event.target );*/ 
		  
		    $( this ).addClass('current');
	        
	        if( $( this ).is(':checked') ){
	        	
	        	$( ".mailing" ).each(function( index, element ) {
	        		
		            // element == this
		            if( $(element).is(':checked') && !$(element).hasClass('current')){
		            	
		            	 $(event.target).prop('checked', false);
		            	alert('Only one mailing address may be set for a client');		            	
		            	return false;
		            }
		         });
         }
	        
	        $( this ).removeClass('current');
	    });

	  $('body').on('click', '.deleteClient', function (e) {
		e.preventDefault();     
		var element = $(this);
		var _data = {};
		$.each(element.get(0).attributes, function (i, attrib) {
			_data[attrib.name.replace('data-', '')] = attrib.value;
		});
		_data.value = element.val();
		
		$('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to delete this client ?</h6></div>')
		.dialog({
			modal: true, title: 'Delete Client', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {
                    console.log(ajaxUrl, _data);
				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");
					//window.location.reload();
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});

        return false;

    });
	 
	 	 
})(jQuery);


var closeAddrModal = function() {
	$('.modal-add-addr').modal('hide'); 
	$('.form-add')[0].reset();
	 
}

if( $('#website').length>0){
	var website = document.getElementById('website');
}
if( $('#linkedin_url').length>0){
	var linkedin_url = document.getElementById('linkedin_url');
}
if( $('#facebook_url').length>0){
	var facebook_url = document.getElementById('facebook_url');
}
if( $('#twitter_url').length>0){
	var twitter_url = document.getElementById('twitter_url');
}
if( $('#googleplus_url').length>0){
	var googleplus_url = document.getElementById('googleplus_url');
}





var checkUrlValidity = function() {
	
	
	var patt = new RegExp("https?://.+");
	
	
	if( $('#website').val()=='' || patt.test(website.value)){		
			website.setCustomValidity('');
	} else {
	   	website.setCustomValidity('You must enter a valid url for example: https://example.com');
	}
	
	
	if( $('#linkedin_url').val()=='' || patt.test(linkedin_url.value)){		
		linkedin_url.setCustomValidity('');
	} else {
		linkedin_url.setCustomValidity('You must enter a valid url for example: https://example.com');
	}
	
	
	if( $('#facebook_url').val()=='' || patt.test(facebook_url.value)){		
		facebook_url.setCustomValidity('');
	} else {
		facebook_url.setCustomValidity('You must enter a valid url for example: https://example.com');
	}
	
	if( $('#twitter_url').val()=='' || patt.test(twitter_url.value)){		
		twitter_url.setCustomValidity('');
	} else {
		twitter_url.setCustomValidity('You must enter a valid url for example: https://example.com');
	}
	
	
	if( $('#googleplus_url').val()=='' || patt.test(googleplus_url.value)){		
		googleplus_url.setCustomValidity('');
	} else {
		googleplus_url.setCustomValidity('You must enter a valid url for example: https://example.com');
	}
			


};


if( $('#website').length>0){
	website.addEventListener('change', checkUrlValidity, false);
}

if( $('#linkedin_url').length>0){
	linkedin_url.addEventListener('change', checkUrlValidity, false);
}

if( $('#facebook_url').length>0){
	facebook_url.addEventListener('change', checkUrlValidity, false);
}

if( $('#twitter_url').length>0){
	twitter_url.addEventListener('change', checkUrlValidity, false);
}

if( $('#googleplus_url').length>0){
	googleplus_url.addEventListener('change', checkUrlValidity, false);
}
function RemoveClient(id, nroute, token) {
	var _data = {};
	_data['id'] = id;
	_data['path'] = 'Clients';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
    console.log(ajaxUrl, _data);
	 doAjax(ajaxUrl, _data, false);	
}
function deleteClient() {
   // $('#lstaff').click();
	redirectTo('clients');
}



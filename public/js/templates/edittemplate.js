(function ($) {
    var check = true;
    
    $('body').on('click', '.deleteEmailTemplate', function (e) {
		e.preventDefault();     
		var element = $(this);
		var _data = {};
		$.each(element.get(0).attributes, function (i, attrib) {
			_data[attrib.name.replace('data-', '')] = attrib.value;
		});
		_data.value = element.val();
		
		$('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to delete this Email Template ?</h6></div>')
		.dialog({
			modal: true, title: 'Delete Staff', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {
				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});

        return false;
	});
    
    $('body').on('click', '.emailtemplatedit', function (e) {
        var returnval = false;
        var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });         
             
                $.ajax({
                   type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }                       
                    },
                    success: function( data ) {
                     if (data.success) {
                        
                        returnval = true;
                        return true;
                     } 
                     else {                             
                            if ('array' in data.data) {
                                 $(".header__row").html("");
                                 jQuery.each(data.data.array.message, function() {
                                     var dd = 'not aurhrised';
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                   
                                      $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                    },
            });
            if(!returnval) {
               return false;
            }        
            
    });
    
})(jQuery);

function deleteEmailTemplate(id, nroute, token) {
	var _data = {};
	_data['id'] = id;
	_data['path'] = 'EmailTemplate';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
    doAjax(ajaxUrl, _data, false);	
}

function RemoveEmailTemplate() {
    location.reload();
}
$('.white-bg').on('copy paste cut', function(e) {
   // e.preventDefault();
});
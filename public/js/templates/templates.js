(function($) {

	
    $('#add-template, #edit-template').on('submit', function(event) {
    	
      if( $(".atleastone:checked").length == 0 ){
    	  alert("At least one of the Client or Transaction fields to include must be checked in order the save");
      	return false;
      }
      
    });
    
    
    $('.forAllUsers').on('click', function(event) {
    	
    	
        if( $(this).is(":checked") ){    
        	$('.users-chk').prop( "checked", false );
        	$('.users-chk').prop( "disabled", true );        	
        }
    	else{    
    	
    		$('.users-chk').prop( "disabled", false );
    	}
        
    });
    
   
    
    $('body').on('click', '.templateDelete', function (e) { 
		e.preventDefault();     
		var element = $(this);
		var _data = {};
		$.each(element.get(0).attributes, function (i, attrib) {
			_data[attrib.name.replace('data-', '')] = attrib.value;
		});
		_data.value = element.val();
		
        //
        $.ajax({
                type: "POST",
                 url: ajaxUrl + 'validateajaxPermission',
                 dataType: 'json',
                 data: _data,
                 async: false,
                 beforeSend: function () {
                     if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                     $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                     }                       
                 },
                 success: function( data ) {
                  if (data.success) {

                     returnval = true;
                     return true;
                  } 
                  else {                             
                         if ('array' in data.data) {
                              $(".header__row").html("");
                              jQuery.each(data.data.array.message, function() {
                                  var dd = 'not aurhrised';
                                  $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                  /*setTimeout(function () {
                                     $(".header__row").fadeOut(function(){
                                      // $(this).remove();
                                     });
                                   }, 2000);*/
                                   $(".header__row").fadeIn().delay(20000).fadeOut();
                              });
                         }                         
                     }
                 },
                 complete: function () {
                     if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                             $.unblockUI();
                         }
                     if (element !== false) {
                         element.removeClass('working');
                     }
                     $('.mainnav a').css('pointer-events', ' auto');
                      //return false;
                 },
            });
         if(returnval) {
        ////////
		$('<div></div>').appendTo('body')
		.html('<div><h6>Are you sure to delete this Template ?</h6></div>')
		.dialog({
			modal: true, title: 'Delete Template', zIndex: 10000, autoOpen: true,
			width: 'auto', modal: true, resizable: false,
			buttons: {
				Confirm: function () {
                    _data.path = 'Templates';
                    console.log(ajaxUrl, _data);
				   doAjax(ajaxUrl, _data, false);
					$(this).dialog("close");
					//window.location.reload();
				},
				No: function () {
					$(this).dialog("close");
				}
			},
			Cancel: function (event, ui) {
				$(this).remove();
			}
		});
       } 
       return false;

    });
    
    $('body').on('click', '.template_view_details, .templatecreate, .edittemplate', function (e) {
        //alert('template_view_details');
        var returnval = false;
        var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });         
             
                $.ajax({
                   type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }                       
                    },
                    success: function( data ) {
                     if (data.success) {
                        
                        returnval = true;
                        return true;
                     } 
                     else {                             
                            if ('array' in data.data) {
                                 $(".header__row").html("");
                                 jQuery.each(data.data.array.message, function() {
                                     var dd = 'not aurhrised';
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                     /*setTimeout(function () {
                                        $(".header__row").fadeOut(function(){
                                         // $(this).remove();
                                        });
                                      }, 2000);*/
                                      $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                         //return false;
                    },
            });
            if(!returnval) {
               // console.log(returnval);
                return false;
            }
        //return false;        
            
    });    

})(jQuery);

function RemoveTemplate(id, nroute, token) {
    var _data = {};
	_data['id'] = id;
	_data['path'] = 'Templates';
	_data['route'] = nroute;	
	_data['_token'] = $("input[name='_token']").val();	
	_data['_token'] = token;
	//console.log('removeomsstaff');
	console.log(ajaxUrl, _data);
	 doAjax(ajaxUrl, _data, false);	
}
(function($) {
    var atleastone = function() {
        var checked = false;

        $.each($('.atleastone'), function() {
            if(this.checked) {
                checked = true;
            }
        });

        if(checked) {
            $('.atleastone').removeAttr('required');
        } else {
            $('.atleastone').attr('required', 'required');
        }
    }

    var oneclient = function() {
        var checked = false;

        $.each($('.oneclient'), function() {
            if(this.checked) {
                checked = true;
            }
        });

        if(checked) {
            $('.oneclient').removeAttr('required');
        } else {
            $('.oneclient').attr('required', 'required');
        }
    }
    
    var users_checkboxes = function() {
        var checked = false;

        $.each($('.users-checkboxes'), function() {
            if(this.checked) {
                checked = true;
            }
        });

        if(checked) {
            $('.users-checkboxes').removeAttr('required');
        } else {
            $('.users-checkboxes').attr('required', 'required');
        }
    }

    $('body').on('click', '.un-color-picker', function(e) {
        $(this).siblings().spectrum('set', '');
    })

    $('body').on('click', '[data-target="#transactionTypeDefaults"]', function() {
        if($(this).find('.glyphicon').hasClass('glyphicon-minus')) {
            $(this).find('.glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        } else {
            $(this).find('.glyphicon').removeClass('glyphicon-plus').addClass('glyphicon-minus');
        }
    })
    
    $('body').on('click', '.forAllUsers', function(){
        if($(this).is(":checked") ){    
            $('.users-chk').removeAttr('checked').attr('disabled', 'disabled');        	
        } else {
            $('.users-chk').removeAttr('disabled'); 
    	}
        users_checkboxes();
    });
    $('body').on('click', '.users-checkboxes', function() {
        users_checkboxes();
    });
    $('body').on('click', '.forAllTransTypes', function(){
        if($(this).is(":checked") ){    
            $('.transType-chk').removeAttr('checked').attr('disabled', 'disabled');        	
        } else {
            $('.transType-chk').removeAttr('disabled'); 
    	}
    });
    $('body').on('click', '.forAllTransStatus', function(){
        if($(this).is(":checked") ){    
            $('.transStatus-chk').removeAttr('checked').attr('disabled', 'disabled');        	
        } else {
            $('.transStatus-chk').removeAttr('disabled'); 
    	}
    });
    
    $('body').on('click', '.atleastone', function(){
        atleastone();
    });

    $('body').on('click', '.oneclient', function(){
        oneclient();
    });
    
    atleastone();
    oneclient();
    users_checkboxes();
    if(typeof color_palette !== 'undefined' && $.isArray(color_palette)) {
        $('.color-picker').spectrum({
            showPaletteOnly: true,
            showPalette: true,
            hideAfterPaletteSelect: true,
            //showInput: true,
            showInitial: true,
            showInput: false,
            preferredFormat: "hex",
            palette: color_palette,
            allowEmpty: true
        });
    }
})(jQuery);

var remove = function() {
    $('#remove-sheet').modal('show');
}

var create_sheet_from = function() {
        var checked = false;

        $.each($('.users-checkboxes'), function() {
            if(this.checked) {
                checked = true;
            }
        });

        if(checked) {
            $('.users-checkboxes').removeAttr('required');
        } else {
            $('.users-checkboxes').attr('required', 'required');
        }
}

$('body').on('click', '.sheetcreate, .sheetaccess, .sheeteditdetails, .editsheet, .createtemplatesheet, .sheetfields', function (e) {
       
        var returnval = false;
        var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });         
             
                $.ajax({
                    type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }                       
                    },
                    success: function( data ) {
                     if (data.success) {
                         //$(".ajax2").addClass("ajax");
                         //$( ".ajax" ).click();
                        returnval = true;
                        return true;
                     } 
                     else {                 
                            if ('array' in data.data) {
                                 $(".header__row").html("");
                                 jQuery.each(data.data.array.message, function() {
                                     var dd = 'not aurhrised';
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                   
                                      $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                    },
            });
            if(!returnval) {
               $('.modal-dialog').modal('hide');
               return false;
             //window.location.reload(true);
               // e.stopPropagation();
            }        
            
    });
    
    $('body').on('click', '#removesheet', function (e) {
       
        var returnval = false;
        var element = $(this);
            var _data = {};
            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });         
             
                $.ajax({
                    type: "POST",
                    url: ajaxUrl + 'validateajaxPermission',
                    dataType: 'json',
                    data: _data,
                    async: false,
                    beforeSend: function () {
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                        $.blockUI({ message: '<h4><img src="'+ajaxUrl+'busy.gif" />Just a moment...</h4>' });
                        }                       
                    },
                    success: function( data ) {
                     if (data.success) {
                        _data.value = element.val();
                        doAjax(ajaxUrl, _data, false);                       
                     } 
                     else {        
                            if ('array' in data.data) {
                                 $(".header__row").html("");
                                 jQuery.each(data.data.array.message, function() {
                                     var dd = 'not aurhrised';
                                     $(".header__row").prepend("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + this + "</div>");
                                   
                                      $(".header__row").fadeIn().delay(20000).fadeOut();
                                 });
                            }                         
                        }
                    },
                    complete: function () {
                
                        if('route' in _data && _data.route != 'cell_update' && _data.route != 'activity_poll') {
                                $.unblockUI();
                            }
                        if (element !== false) {
                            element.removeClass('working');
                        }
                        $('.mainnav a').css('pointer-events', ' auto');
                    },
            });
            if(!returnval) {
               $('.modal-dialog').modal('hide');
               return false;
             //window.location.reload(true);
               // e.stopPropagation();
            }        
            
    }); 
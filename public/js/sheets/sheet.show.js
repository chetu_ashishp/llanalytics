(function ($) {
    var isBreakpoint = function (alias) {
        return $('.device-' + alias).is(':visible');
    }

    var waitForFinalEvent = function () {
        var b = {};
        return function (c, d, a) {
            a || (a = "Brandon Rocks!");
            b[a] && clearTimeout(b[a]);
            b[a] = setTimeout(c, d)
        }
    }();
    var fullDateString = new Date();

    $(window).resize(function () {
        waitForFinalEvent(function () {

            if (isBreakpoint('xs')) {
              //column-views-select
              if($( ".column-views-select" ).val() == '') {
                $('#columnViewsCount').html('0');
              } else {
                $('#columnViewsCount').html('1');
              }
            } else {
              var columnViewsCount = 0;
              $('.column-views').each(function (i, obj) {
                  if (this.checked == 1) {
                    columnViewsCount = columnViewsCount + 1;
                  }
                  $('#columnViewsCount').html(columnViewsCount);
              });
            }


        }, 300, fullDateString.getTime())
    });
})(jQuery);

(function ($) {
    $.widget("ih.resizableColumns", {
        _create: function () {
            this._initResizable();
        },
        _initResizable: function () {

            var colElement, colWidth, originalSize;
            var table = this.element;

            this.element.find("th").resizable({
                handles: {
                    "e": " .resizeHelper"
                },
                minWidth: 10,
                create: function (event, ui) {
                    var minWidth = $(this).find(".columnLabel").width();
                    if (minWidth) {

                        // FF cannot handle absolute resizable helper
                        /*if ($.browser.mozilla) {
                         minWidth += $(this).find(".ui-resizable-e").width();
                         }*/
                        minWidth += $(this).find(".ui-resizable-e").width();

                        $(this).resizable("option", "minWidth", minWidth);
                    }
                },
                start: function (event, ui) {
                    var colIndex = ui.helper.index() + 1;
                    colElement = table.find("colgroup > col:nth-child(" + colIndex + ")");
                    colWidth = parseInt(colElement.get(0).style.width, 10);
                    originalSize = ui.size.width;
                },
                stop:function(event, ui){
                    var resizeDelta = ui.size.width - originalSize;

                    var newColWidth = colWidth + resizeDelta;
                    isfirst = $(this).attr('data-type');

                    if (typeof isfirst !== typeof undefined && isfirst !== false) {
                        //$('col:not(:first)').width(newColWidth);
                        $('col.transaction-common').width(newColWidth);

                    }

                    var sheet = $('#grid').attr('data-sheet-id');
                    var widths = [];
                    $('.sheet-grid colgroup col').each(function (i, obj) {

                        //if(i == 0) {
                        //    firstwidth = $(this).css('width');
                        //    firstwidth = firstwidth.replace('px', '');
                        //   widths[i] = {'transaction': $(this).attr('data-transaction'), 'width': parseInt(firstwidth)};
                        //} else {
                        thiswidth = $(this).css('width');
                        thiswidth = thiswidth.replace('px', '');
                        widths[i] = {'transaction': $(this).attr('data-transaction'), 'width': parseInt(thiswidth)};
                        //widths[i] = {'transaction': $(this).attr('data-transaction'), 'width': $(this).css('width')};
                        //widths[i] = {'transaction': $(this).attr('data-transaction'), 'width': newColWidth};
                        // }
                    });
                    $("#column-fixed").html("").append($("#grid > colgroup").clone()).append($("#grid > thead").clone()).append($("#grid > tbody").clone());
                    $("#column-header-fixed").html("").append($("#grid > colgroup").clone()).append($("#grid > thead").clone()).append($("#grid > tbody").clone());

                    $.cookie('sheet_' + sheet + '_widths', JSON.stringify(widths));
                    $.cookie('sheet_remember', remember_token);

                },
                resize: function (event, ui) {
                    var resizeDelta = ui.size.width - originalSize;

                    var newColWidth = colWidth + resizeDelta;
                    colElement.width(newColWidth);
                    $(this).css("height", "auto");

                }
            });
        }

    });

    $(".resizable").resizableColumns();

    var sheet = $('#grid').attr('data-sheet-id');
    var sheet_widths = $.cookie('sheet_' + sheet + '_widths');
    var sheet_remember = $.cookie('sheet_remember');

    if (sheet_widths && sheet_remember == remember_token) {
        var obj = $.parseJSON(sheet_widths);
        $.each(obj, function (key, value) {
            if(value.transaction) {
                $('col[data-transaction="' + value.transaction + '"]').css("width", value.width);
            } else {
                $('col[data-transaction=""]').css("width", value.width);
            }

        });
    }
    var hidden_transactions = $.cookie('sheet_' + sheet + '_hidden_transactions');
    if (hidden_transactions && sheet_remember == remember_token) {
        var obj = $.parseJSON(hidden_transactions);
        $.each(obj, function (key, value) {
            $('.column-views[data-target="' + value + '"]').attr('checked', false);
            $('.' + value).addClass('column-hidden');
        });
    }

    var columnViewsCount = 0;
    $('.column-views').each(function (i, obj) {
        if (this.checked == 1) {
          columnViewsCount = columnViewsCount + 1;
        }
        console.log(columnViewsCount);
        $('#columnViewsCount').html(columnViewsCount);
    });
})(jQuery);


(function ($) {

        $('select').each(function (i, obj) {
            var element = $(this);

            var placeholder= '<input';
            $.each(this.attributes, function(i, attrib){
                var name = attrib.name;
                var value = attrib.value;
                placeholder = placeholder + ' ' + name + '="' + value + '"';
            });
            placeholder = placeholder + '>';

            //var placeholder = '<input class="form-control cell-active" type="text" data-input-type="text" data-contex="http://laurielundeen.dev/clients/edit/612" data-contex-add="http://laurielundeen.dev/clients/details/612?transaction=add" data-item="612" data-field="last_name" data-cell-type="Client" data-editable="true" data-_token="Kfj8sjoESl3XtpbOSDOSS2lanLsIKxt4rfSFY0Wx" value="Woodworth">';
            $(placeholder).insertBefore(element);
            element.siblings('input').addClass('placeholder').val(element.children("option").filter(":selected").text());
            element.siblings('input').attr('data-contex-menu-items', element.attr('data-contex-menu-items'))

            element.hide();
        });



    $('body').on('focus', '.placeholder', function(e){
        $('.savingspinner').hide();
    var element = $(this);
    var select = element.siblings('select');
    switch_active_cell_placeholder(element);
    select.show();

    var $target = select;
        cellwidth = select.width();
    var $clone = $target.clone();
    $clone.val($target.val()).css({
        position: 'absolute',
        'z-index': 999,
        left: $target.offset().left,
        top: $target.offset().top + $target.height() - 16,
        width: cellwidth+'px',
        'max-height': '300px',
        fontsize: '10px'
    }).attr('size', $clone.find('option').length).change(function () {
        $target.val($clone.val());
        $('.savingspinner').hide();
        console.log('size');
    }).on('click blur', function () {

        console.log('click blur');
        select.siblings('input').val(select.children("option").filter(":selected").text());
        select.siblings('input').show();
        $(this).remove();
    });
        $('.savingspinner').hide();
    select.siblings('.savingspinner').hide();
    select.hide();
    $('body').append($clone);
    $clone.addClass('testtesttest');
    $clone.show();
    $clone.focus();

    })


    $('body').on('focusout', 'selecft', function(e){
        var element = $(this);

        element.siblings('input').val(element.children("option").filter(":selected").text());
        element.siblings('input').show();
        element.hide();
    })

    $('body').on('focusout','.testtefsttest', function(e) {
        var element = $(this);
        element.remove();
        //element.siblings('input').val(element.children("option").filter(":selected").text());
        //element.siblings('input').show();
        //element.hide();
    })

    $('body').on('keydown', '[data-field="sales_price"],[data-field="current_list_price"]', function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    })
    $('body').on('keyup', '[data-field="sales_price"],[data-field="current_list_price"]', function(e) {
        if(e.which >= 37 && e.which <= 40){
            e.preventDefault();
        }
        var $this = $(this);
        var num = $this.val().replace(/\,/g, '');
        $this.val(num.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
    })

    $('[data-contex-menu="true"]').bind("contextmenu", function (event) {

        $("div.contex-menu").remove();
        $(".datepicker").datepicker("hide");
        var element = $(this);
        $('.testtesttest').remove();
        $('.savingspinner').hide();

        event.preventDefault();
        switch_active_cell(element);

        var contex_menu_html = "<div class='contex-menu list-group'>";
        var contex_menu_items = element.attr('data-contex-menu-items');
        var bg_color = element.css('background-color');
        $('.testtesttest').remove();
        $('.savingspinner').hide();
        var trans_id = element.attr('data-trans-id');
        var field_id = element.attr('data-field-id');
        var comment = element.attr('data-comment');
        var _token = element.attr('data-_token');
        var obj = jQuery.parseJSON(contex_menu_items);
        $.each(obj, function (key, value) {
            if ('show' in value && value.show === true) {
                if (key == 'add_comment' && typeof comment !== typeof undefined && comment !== false && comment.length > 0) {
                    return;
                } else if (key == 'view_comment' && typeof comment !== typeof undefined && comment !== false && comment.length == 0) {
                    return;
                } else {
                    if(key == 'set_bg_color') {
                        contex_menu_html = contex_menu_html + "<a href='#' data-target='#modal-container' data-path='CompanySheet' data-route='contex_action' data-_token='" + _token + "' data-trans-id='" + trans_id + "' data-field-id='" + field_id + "' data-action='" + value.label + "' data-current-color='" + bg_color + "' class='list-group-item ajax'>" + value.label + "</a>";
                    } else {
                        contex_menu_html = contex_menu_html + "<a href='#' data-target='#modal-container' data-path='CompanySheet' data-route='contex_action' data-_token='" + _token + "' data-trans-id='" + trans_id + "' data-field-id='" + field_id + "' data-action='" + value.label + "' class='list-group-item ajax'>" + value.label + "</a>";
                    }
                }
            }
        });
        $('.testtesttest').remove();
        $('.savingspinner').hide();
        contex_menu_html = contex_menu_html + "</div>";
        $(contex_menu_html)
            .appendTo("body")
            .css({top: event.pageY + "px", left: event.pageX + "px"});
        $('.testtesttest').remove();
        $('.savingspinner').hide();
    });


    $('[data-cell-type="Client"], [data-cell-type="Transaction"]').bind("contextmenu", function (event) {

        $("div.contex-menu").remove();
        $(".datepicker").datepicker("hide");
        $('.testtesttest').remove();
        $('.savingspinner').hide();
        var element = $(this);
        event.preventDefault();
        switch_active_cell(element);
        var type = element.attr('data-cell-type');
        var item = element.attr('data-contex');
        $('.testtesttest').remove();
        $('.savingspinner').hide();
        contex_menu_html = "<div class='contex-menu list-group con-active-menu'><a href='" + item + "' class='list-group-item'>Edit " + type + "</a>";
        if (type == 'Client') {
            contex_menu_html = contex_menu_html + "<a href='" + element.attr('data-contex-add') + "' class='list-group-item'>Add Transaction</a>";
        }
        contex_menu_html = contex_menu_html + "</div>";
        $(contex_menu_html)
            .appendTo("body")
            .css({top: event.pageY + "px", left: event.pageX + "px"});

        $('.testtesttest').remove();
        $('.savingspinner').hide();
    });


    $(document).bind("click", function (event) {
        $("div.contex-menu").remove();
    });

    $('body').on('click', '.uncheckAll', function() {
        $('.column-views').prop('checked', false);
        var sheet = $('.sheet-grid').attr('data-sheet-id');
        var hidden_transactions = [];
        var columnViewsCount = 0;
        $('.column-views').each(function (i, obj) {
            var checked = (this.checked ? "1" : "0");
            var target = $(this).attr('data-target');
            if (checked == 0) {
                $('.' + target).addClass('column-hidden');
                hidden_transactions[i] = target;
            } else {
                $('.' + target).removeClass('column-hidden');
            }

            if (checked == 1) {
              columnViewsCount = columnViewsCount + 1;
            }
            $('#columnViewsCount').html(columnViewsCount);
        });
        $.cookie('sheet_' + sheet + '_hidden_transactions', JSON.stringify(hidden_transactions));
        $.cookie('sheet_remember', remember_token);
    });
    $('body').on('click', '.checkAll', function() {
        $('.column-views').prop('checked', true);
        var sheet = $('.sheet-grid').attr('data-sheet-id');
        var hidden_transactions = [];
        var columnViewsCount = 0;
        $('.column-views').each(function (i, obj) {
            var checked = (this.checked ? "1" : "0");
            var target = $(this).attr('data-target');
            if (checked == 0) {
                $('.' + target).addClass('column-hidden');
                hidden_transactions[i] = target;
            } else {
                $('.' + target).removeClass('column-hidden');
            }
            if (checked == 1) {
              columnViewsCount = columnViewsCount + 1;
            }
            $('#columnViewsCount').html(columnViewsCount);
        });
        $.cookie('sheet_' + sheet + '_hidden_transactions', JSON.stringify(hidden_transactions));
        $.cookie('sheet_remember', remember_token);

    });

    $('body').on('click', '[data-target="#sheetFilters"]', function() {
        if($(this).find('.glyphicon').hasClass('glyphicon-minus')) {
            $(this).find('.glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        } else {
            $(this).find('.glyphicon').removeClass('glyphicon-plus').addClass('glyphicon-minus');
        }
    })

    $('body').on('dblclick', '[data-editable="true"]', function (e) {
        var element = $(this);
        if(element.parent().hasClass('parent-active')) {
            return;
        }
        edit_active_cell($(this));
    });

    $('body').on('click', '[data-editable="true"]', function (e) {
        var element = $(this);
        if(element.parent().hasClass('parent-active')) {
            return;
        }
        switch_active_cell(element);
        if (element.prop("tagName") == "SELECT") {
            edit_active_cell(element);
        }
    });

    $('body').on('select', '[data-editable="true"]', function (e) {
        var element = $(this);
        if(element.parent().hasClass('parent-active')) {
            return;
        }
        switch_active_cell(element);
        if (element.prop("tagName") == "SELECT") {
            edit_active_cell(element);
        }
    });

    $('.datepicker').datepicker().keydown(function(event) {
        if (event.which === $.ui.keyCode.ENTER) {
            var active_cell = $('.cell-active[data-editable="true"]');
            if (active_cell) {
                new_active_cell = $("#grid tr:eq(" + (active_cell.closest('tr').index() + 2) + ")").find("td:eq(" + (active_cell.closest('td').index()) + ")").find('.input-group').children(":first");
                if (new_active_cell.length) {
                    //switch_active_cell(new_active_cell);
                    //do_save($('.cell-active'), false);

                    switch_active_cell_return(new_active_cell);
                }
            }
            event.preventDefault();
        }
    });

    $(document).keydown(function (e) {

        if (e.which == 37 || e.which == 38 || e.which == 39 || e.which == 40) {
            var active_cell = $('.cell-active[data-editable="true"]');
            if (active_cell) {
                var active_cell_editing = true;
                var new_active_cell = false;
                switch (e.which) {
                    case 37: // left
                        e.preventDefault();
                        new_active_cell = active_cell.closest('tr').find("td:eq(" + (active_cell.closest('td').index() - 1) + ")").find('.input-group').children(":first");
                        break;

                    case 38: // up
                        e.preventDefault();
                        if (active_cell.closest('tr').index() > 0) {
                            new_active_cell = $("#grid tr:eq(" + (active_cell.closest('tr').index() + 0) + ")").find("td:eq(" + (active_cell.closest('td').index()) + ")").find('.input-group').children(":first");
                        } else {
                            return;
                        }
                        break;

                    case 39: // right
                        new_active_cell = active_cell.closest('tr').find("td:eq(" + (active_cell.closest('td').index() + 1) + ")").find('.input-group').children(":first");
                        break;

                    case 40: // down
                        e.preventDefault();
                        console.log(active_cell.closest('tr').index());
                        new_active_cell = $("#grid tr:eq(" + (active_cell.closest('tr').index() + 2) + ")").find("td:eq(" + (active_cell.closest('td').index()) + ")").find('.input-group').children(":first");
                        break;

                    default:
                        return; // exit this handler for other keys
                }

                if(new_active_cell.hasClass('twitter-typeahead')) {
                    switch_active_cell_return(new_active_cell);
                } else if (new_active_cell && new_active_cell.attr('data-editable') == 'true') {
                    console.log(new_active_cell.attr('data-item'));
                    switch_active_cell(new_active_cell);
                }
            }
        } else if (e.which === 9) {
            var active_cell = $('.cell-active[data-editable="true"]');
            if (active_cell) {
                e.preventDefault();
                new_active_cell = active_cell.closest('tr').find("td:eq(" + (active_cell.closest('td').index() + 1) + ")").find('.input-group').children(":first");
                if (new_active_cell.length) {
                    if(new_active_cell.hasClass('twitter-typeahead')) {
                        switch_active_cell_return(new_active_cell);
                    } else {
                        switch_active_cell(new_active_cell);
                    }
                    console.log('here1');
                } else {
                    new_active_cell = $("#grid tr:eq(" + (active_cell.closest('tr').index() + 2) + ")").find("td:eq(1)").find('.input-group').children(":first");
                    if (new_active_cell.length) {
                        if(new_active_cell.hasClass('twitter-typeahead')) {
                            switch_active_cell_return(new_active_cell);
                        } else {
                            switch_active_cell(new_active_cell);
                        }
                    }
                    $('html, body').animate({scrollLeft: 0}, 200);
                    console.log('here');
                }
            }
        } else if (e.which === 13) {
            var active_cell = $('.cell-active[data-editable="true"]');
            if (active_cell) {
                new_active_cell = $("#grid tr:eq(" + (active_cell.closest('tr').index() + 2) + ")").find("td:eq(" + (active_cell.closest('td').index()) + ")").find('.input-group').children(":first");
                if (new_active_cell.length) {
                    //switch_active_cell(new_active_cell);
                    switch_active_cell_return(new_active_cell);
                }
            }
        } else if (e.which === 27) {
            var active_cell = $('.cell-active[data-editable="true"]');
            var active_cell_editing = true;

            if (active_cell && typeof active_cell_editing !== typeof undefined && active_cell_editing !== false) {
                e.preventDefault();
                var data_value_old = active_cell.attr('data-value-old');
                if (typeof data_value_old !== typeof undefined && data_value_old !== false) {
                    active_cell.val(data_value_old);
                }
            }
        } else {

        }
    });

    $('body').on('focus', 'input[data-editable="true"], select[data-editable="true"]', function (e) {
        var element = $(this);
        element.attr('data-value-old', $(this).val());
    });

    $('body').on('focusout keydown', 'input[data-editable="true"], select[data-editable="true"]', function (e) {
        var element = $(this);
        do_save(element, e);
    });

    $('body').on('change', 'select[data-editable="true"], input[data-input-type="Checkbox"][data-editable="true"]', function (e) {
        var element = $(this);
        do_save(element, e);
    });

    $('body').on('click', '.contex_action', function (e) {
        var element = $(this);
        var _data = {};

        $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });

        _data.value = $($('[name="' + element.attr('data-contex-action') + '"]').prop("tagName") + '[name="' + element.attr('data-contex-action') + '"]').val();

        if (_data['contex-action'] == 'contex_form_alert') {
            _data['user'] = $('select[name="contex_form_user"]').val();
            if ($('input[name="contex_form_mail"]').is(':checked')) {
                _data['notify'] = $('input[name="contex_form_mail"]').val();
            }

        }

        if (((_data['contex-action'] == 'contex_form_alert' || _data['contex-action'] == 'contex_form_color' || _data['contex-action'] == 'contex_form_comment') && _data['user'] !== '' && _data.value != '') || (_data['contex-action'] == 'contex_form_comment' && _data['user'] !== '')) {
            doAjax(ajaxUrl, _data, false);
        } else {
            alert('Please fill out all required fields');
        }
    });

    $('body').on('change', '.column-views-select', function (e) {
        var target = $(this).find(":selected").attr('data-target');
        if (target.length > 0) {
            $('.transaction-common, .transaction-mobile-hidden').addClass('hidden-xs');
            $('.' + $(this).find(":selected").attr('data-target')).removeClass('hidden-xs');
            $('.frozen-column.transaction-label').removeClass('hidden-xs');
            $('.mobileView').removeClass('hidden-xs');
        } else {
            $('.mobileView').addClass('hidden-xs');
            $('.transaction-common, .transaction-label').addClass('hidden-xs');
        }
        if($(this).val() == '') {
          $('#columnViewsCount').html('0');
        } else {
          $('#columnViewsCount').html('1');
        }
    });

    $('body').on('click', '.column-views', function (e) {
        var sheet = $('.sheet-grid').attr('data-sheet-id');
        var hidden_transactions = [];
        var columnViewsCount = 0;
        $('.column-views').each(function (i, obj) {
            var checked = (this.checked ? "1" : "0");
            var target = $(this).attr('data-target');
            if (checked == 0) {
                $('.' + target).addClass('column-hidden');
                hidden_transactions[i] = target;
            } else {
                $('.' + target).removeClass('column-hidden');
            }

            if (checked == 1) {
              columnViewsCount = columnViewsCount + 1;
            }
            $('#columnViewsCount').html(columnViewsCount);
        });
        $.cookie('sheet_' + sheet + '_hidden_transactions', JSON.stringify(hidden_transactions));
        $.cookie('sheet_remember', remember_token);
    });

    $.fn.scrollEnd = function(callback, timeout) {
      $(this).scroll(function(){
        var $this = $(this);
        if ($this.data('scrollTimeout')) {
          clearTimeout($this.data('scrollTimeout'));
        }
        $this.data('scrollTimeout', setTimeout(callback,timeout));
      });
    };
    //.column-views

    var firstScrollTop = $(window).scrollTop();
    if ($('.frozen-column')[0]) {
        var firstScrollLeft = $('.frozen-column').first().offset().left;
    } else {
        var firstScrollLeft = '';
    }
//left-fixed
$(window).scroll(function(){
  if($('body').scrollLeft() == 0) {
      var scrollSelector = "html, body";
  } else {
      var scrollSelector = "body";
  }
  var tableOffset = $("#grid thead .colHeaders ").first().offset().top;
  if (firstScrollTop == $(window).scrollTop()) {
// horizontal
    $('#header-fixed').css('left', ($('#grid').offset().left - $(scrollSelector).scrollLeft()));
    var columnfixedtop = $('#column-fixed').css('top');
    columnfixedtop = columnfixedtop.replace('px', '');
    columnfixedtop = parseInt(columnfixedtop);
    if(columnfixedtop < 0) {
        columnfixedtop = 0;
    }
    $("#column-header-fixed").css('left', 0).css('top', columnfixedtop + 'px');
  } else {
//vertical
    $("#column-fixed").css('top', $('#grid thead').first().offset().top - $(scrollSelector).scrollTop());
    var headerfixedleft = $('#header-fixed').css('left');
    headerfixedleft = headerfixedleft.replace('px', '');
    headerfixedleft = parseInt(headerfixedleft);
    if(headerfixedleft < 0) {
        headerfixedleft = 0;
    }

$("#column-header-fixed").css('top', 0).css('left', headerfixedleft + 'px');

  }
});
$(window).scrollEnd(function(){
console.log('stopped scrolling');

        if($('body').scrollLeft() == 0) {
            var scrollSelector = "html, body";
        } else {
            var scrollSelector = "body";
        }


        var tableOffset = $("#grid thead .colHeaders ").first().offset().top;
        if (firstScrollTop == $(window).scrollTop()) {

            if ($('.datepicker.hasDatepicker')[0]) {
                $(".hasDatepicker").removeClass("hasDatepicker");
                $(".datepicker").datepicker("destroy");
            }

            fixedColumn = $("#column-fixed");
            fixedHeaderColumn = $("#column-header-fixed");

            $('.datepicker').datepicker("destroy").removeClass("hasDatepicker").removeAttr('id').datepicker({
                onSelect: function (dateText, obj) {
                    //$('.cell-active').val(dateText);
                    do_save($('.cell-active'), false);
                },
                dateFormat: 'mm/dd/y',
            });

            if ($(scrollSelector).scrollLeft() > firstScrollLeft) {
                console.log($('body').scrollTop());
                $("#column-fixed").css('left', 0).css('top', $('#grid tbody').first().offset().top - $(scrollSelector).scrollTop() - subtractor);
                fixedColumn.show();

                fixedHeaderColumn.show();
            } else {
                fixedColumn.hide();
                fixedHeaderColumn.hide();
            }
            console.log('horizontal scroll');
        } else {
            console.log('vertical scroll');

            if ($('.datepicker.hasDatepicker')[0]) {
                $(".hasDatepicker").removeClass("hasDatepicker");
                $(".datepicker").datepicker("destroy");
            }

            $('.datepicker').datepicker("destroy").removeClass("hasDatepicker").removeAttr('id').datepicker({
                onSelect: function (dateText, obj) {
                    //$('.cell-active').val(dateText);
                    do_save($('.cell-active'), false);
                },
                dateFormat: 'mm/dd/y',
            });


            var offset = $(this).scrollTop();
            if (offset >= tableOffset) {
              if($("#header-fixed").html() == '') {
                var fixedHeader = $("#header-fixed").html("").append($("#grid > colgroup").clone()).append($("#grid > thead").clone()).append($("#grid > tbody").clone());
                fixedHeader.find('.header-row.hot').remove();
                fixedHeader.find('tr.hot').remove();
              } else {
                var fixedHeader = $("#header-fixed");
              }

              fixedHeaderColumn = $("#column-header-fixed");

                fixedHeader.show();
                fixedHeaderColumn.show();
            } else if (offset < tableOffset) {
              if($("#header-fixed").length) {
                $("#header-fixed").html("").hide();
              }

                if($("#header-fixed").length) {
                  $("#column-header-fixed").hide();
                }

            }
            firstScrollTop = $(window).scrollTop();
        }
}, 500);
    //$(window).bind("scroll", function () {


    $('[data-toggle="tooltip"]').tooltip();
    var changingDate = false;
    $('.datepicker').datepicker({
        onChangeMonthYear: function (year, month, inst) {
            if (changingDate == false) {
                changingDate = true; //I set the date later which would call this
                //and cause infinite recursion, so use this flag to stop that
                if (year == 1899 || year == 1900) {
                    var now = new Date(); //what the picker would have had selected
                    //before clicking the forward/backward month button
                    if (year == 1899) //I clicked backward
                    {
                        now.setMonth(now.getMonth() - 1);
                    }
                    else if (year == 1900) //I clicked forward
                    {
                        now.setMonth(now.getMonth() + 1);
                    }
                    $(this).datepicker('setDate', now);
                }
                changingDate = false;
            }
        },
        onSelect: function (dateText, obj) {
            //$(this).val(dateText);
            //$(this).datepicker('setDate', dateText);
            do_save($(this), false);

        },
        dateFormat: 'mm/dd/y',
    });

    var substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            matches = [];

            substrRegex = new RegExp(q, 'i');

            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };

    $('input[data-field="escrow_officer"]').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'escrowOfficers',
            source: substringMatcher(escrow_officers)
        });

    $('input[data-field="loan_officer"]').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'escrowOfficers',
            source: substringMatcher(loan_officers)
        });

    var fixedColumn = $("#column-fixed").html("").append($("#grid > colgroup").clone()).append($("#grid > thead").clone()).append($("#grid > tbody").clone());
    $('#column-header-fixed').find('.hot-column').remove();
    $('#column-header-fixed').find('.header-cell').remove();

    var fixedHeaderColumn = $("#column-header-fixed").html("").append($("#grid > colgroup").clone()).append($("#grid > thead").clone()).append($("#grid > tbody").clone());
    $('#column-header-fixed').find('.hot').remove();
    $('#column-header-fixed').find('.header-cell').remove();

    fixedColumn.hide();
    fixedHeaderColumn.hide();

    if ($('.datepicker.hasDatepicker')[0]) {
        $(".hasDatepicker").removeClass("hasDatepicker");
        $(".datepicker").datepicker("destroy");
    }

    $('.datepicker').datepicker("destroy").removeClass("hasDatepicker").removeAttr('id').datepicker({
        onSelect: function (dateText, obj) {
            //$(this).val(dateText);
            do_save($(this), false);
        },
        dateFormat: 'mm/dd/y',
    });
})(jQuery);





if($('.activity-wrapper').length) {
    setInterval(function () {
        if (typeof updateActivity !== 'undefined' && $.isFunction(updateActivity)) {
            //updateActivity();
        }
    }, $('.activity-wrapper').attr('data-heartbeat'));
}

var updateActivity = function () {

    var element = $('.activity-wrapper');
    var _data = {};

    $.each(element.get(0).attributes, function (i, attrib) {
        _data[attrib.name.replace('data-', '')] = attrib.value;
    });

    _data['active_cell'] = active_cell_data();
console.log(_data);
    doAjax(ajaxUrl, _data, false);
}

var activity_poll = function(data) {
    console.log(data);
}

var active_cell_data = function() {
    _data = {};
    var element = $('.parent-active').children().first();
    if(element.length) {
        $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });
    }

    return _data;
}

var check_colors = function (element) {

    var type = element.attr('type');
    var colors = element.attr('data-colors');
    if (typeof colors !== typeof undefined && colors !== false) {

        var element_value = element.val();
        if (type == 'checkbox' && !element.is(":checked")) {
            element_value = '';
        }
        console.log('start');
        console.log(colors);

        if(colors.length == 1) {
            return;
        }
        var colors = jQuery.parseJSON(colors);
        try {
            var colors = jQuery.parseJSON(colors);
        }
        catch (err) {
            return;
        }
        console.log('stop');
        if(colors.length === 0) {
            var flag = true;
        } else {
            var flag = false;
            $.each(colors, function (key, value) {
                if (element_value == key) {
                    flag = true;
                    if (type == 'checkbox') {
                        element.parent().css('background-color', value);
                    } else {
                        element.css('background-color', value);
                    }
                }
            });
        }

        var bg_color = element.css('background-color');
        if (!flag && !bg_color) {
            if (type == 'checkbox') {
                element.parent().css('background-color', element.attr('data-default-color'));
            } else {
                element.css('background-color', element.attr('data-default-color'));
            }
        }
    }
}

var do_save = function (element, e) {
    if (element.attr('data-force-blur') == 'true') {
        element.removeAttr('data-force-blur');
        return;
    }

    var validInput = true;
    var value = element.val();
    if (e !== false && e.keyCode) {
        if (e.keyCode == 27) {
            valid = false;
        } else if (e.keyCode != 13) {
            return;
        }
    }

    switch (element.attr('data-input-type')) {
        case 'dagte':
            console.log(value);
            var comp = value.split('/');
            var y = parseInt(comp[2], 10);
            var m = parseInt(comp[1], 10);
            var d = parseInt(comp[1], 10);
            var date = new Date(y, m - 1, d);
            if (date.getFullYear() != y || date.getMonth() + 1 != m || date.getDate() != d) {
                validInput = false;
                console.log('notvalid');
            }
            break;
        case 'numdber':
            if (!$.isNumeric(value)) {
                validInput = false;
            }
            break;
        default:
    }

    var dataValueOld = element.attr('data-value-old');

    if (validInput && (element.val() != dataValueOld || element.attr('data-input-type') == 'Checkbox')) {
        element.siblings('.input-group-addon').children('.savingspinner').removeClass('hidden');
        var _data = {};
        _data['path'] = 'CompanySheet';
        _data['route'] = 'cell_update';
        $.each(element.get(0).attributes, function (i, attrib) {
            _data[attrib.name.replace('data-', '')] = attrib.value;
        });

        _data.value = element.val();
        if (!element.is(":checked") && element.attr('data-input-type') == 'Checkbox') {
            _data['value'] = '0';
        }
        doAjax(ajaxUrl, _data, false);
    } else {
        element.val(element.attr('data-value-old')).attr('data-force-blur', 'true').blur();
    }

    element.removeAttr('data-value-old');
    check_colors(element);
}

var switch_active_cell = function (element) {
    $(".datepicker").datepicker("hide");
    $('[data-editable="true"]').removeClass('cell-active');
    element.addClass('cell-active');

    $('.parent-active').removeClass('parent-active');
    element.parent().addClass('parent-active');

    element.focus();
}


var switch_active_cell_placeholder = function (element) {
    //return;
    $(".datepicker").datepicker("hide");
    $('[data-editable="true"]').removeClass('cell-active');
    element.addClass('cell-active');

    $('.parent-active').removeClass('parent-active');
    element.parent().addClass('parent-active');

}

var switch_active_cell_return = function (element) {
    $(".datepicker").datepicker("hide");
    $('[data-editable="true"]').removeClass('cell-active');
    if(element.hasClass('twitter-typeahead')) {
        element.find('.tt-input').addClass('cell-active');
    } else {
        element.addClass('cell-active');
    }

    $('.parent-active').removeClass('parent-active');
    if(element.parent().hasClass('twitter-typeahead')) {
        element.parent().parent().addClass('parent-active');
    } else {
        element.parent().addClass('parent-active');
    }

    if(element.hasClass('twitter-typeahead')) {
        element.find('.tt-input').focus();
    } else {
        element.focus();
    }
    //element.focus();
}

var edit_active_cell = function (element) {
    element.focus();
    element.val(element.val());
}

var clear_edit_active_cell = function (element) {
    element.focus();
    element.val('');
}

var contex_action = function () {
    $('#contex-modal').modal('show');
    //$('.context-color').colorpicker();
    if(typeof color_modal_palette !== 'undefined') {
        $('.context-color').spectrum({
            showPaletteOnly: true,
            showPalette:true,
            hideAfterPaletteSelect:true,
            //showInput: true,
            showInitial: true,
            showInput: true,
            preferredFormat: "hex",
            allowEmpty: true,
            palette: color_modal_palette
        });
    }

}
var cell_update = function () {
    $('#contex-modal').modal('hide');
}

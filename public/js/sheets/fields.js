(function($) {

	 $('body').on('click', '#cancelAddForm', function (e) {
		 $('#newfield-div').html();
		 $('#newfield-div').hide('slow');
		 $('#fields-div').show('slow');
		 $('#field-add-form')[0].reset();
	 });
		 
	 $('body').on('click', '#cancelEditForm', function (e) {		

		 $('#editfield-div').html('');
		 $('#editfield-div').hide('slow');
		 $('#fields-div').show('slow');
	 });
	 
	 $('body').on('click', '.deletefield', function (e) {
		 	
		 	if (confirm('Are you sure you want delete the field?')){
			
	 	        var element = $(this);		 	        
	 	        var _data = {};
	 	        $.each(element.get(0).attributes, function (i, attrib) {
	 	            _data[attrib.name.replace('data-', '')] = attrib.value;
	 	        });

	 	        _data.value = element.val();
	 	        doAjax(ajaxUrl, _data, false);
		 	        
			}	
			else{
				return false;
			}
		 	
	 });
	 
	 
    $('body').on('click', '#lfields', function (e) {    	

    	
    	$('.fieldtable').bootstrapTable({    	    
    	    url: ajaxUrl + 'sheetfields',   
    	    queryParams:{sheet: $(this).data('sheet')},
    	    pagination:false,
    	    reorderableRows:true,
    	    useRowAttrFunc:true,		           
    	    onReorderRow: function (table) {
    			reOrderFields(table);
    	    	//alert (JSON.stringify(table));	  
    		}
    	    
    	});    	
    });   

    
    $('body').on('click', '.delete-colorvalue', function (e) {
    	var row_number = $(this).data('row')
    	$('.row_'+row_number).remove();
    });    	
    

    $('body').on('submit', '.mform-add', function (e) {
        
    	e.preventDefault();      
    	var stop = false;
    	
        if( $('#type').val()=='Checkbox'){    		
    		$( ".colorvalue" ).each(function() {
    			
    			if( $(this).val()!='0' && $(this).val()!='1'){    				
    				alert('All values for the Background Colors by Value must be equal 0 or 1');    		  	
    				stop = true;    				  
    				return false;
    			}
    		});
    	}    
        
        if(stop==false){  
        	var element = $(this);
            var _data = {};
            var form_input = element.serialize();

            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });
            _data['form-input'] = form_input;
            doAjax(ajaxUrl, _data, false);
        }
        else{
        	return false;
        }
        
     });
    
  
    
    $('body').on('submit', '.mform-edit', function (e) {
        
    	e.preventDefault();      
    	var stop = false;
    	
        if( $('#eftype').val()=='Checkbox'){    		
    		$( ".colorvalue" ).each(function() {
    			
    			if( $(this).val()!='0' && $(this).val()!='1'){    				
    				alert('All values for the Background Colors by Value must be equal 0 or 1');    		  	
    				stop = true;    				  
    				return false;
    			}
    		});
    	}    
        
        if(stop==false){  
        	var element = $(this);
            var _data = {};
            var form_input = element.serialize();

            $.each(element.get(0).attributes, function (i, attrib) {
                _data[attrib.name.replace('data-', '')] = attrib.value;
            });
            _data['form-input'] = form_input;
            doAjax(ajaxUrl, _data, false);
        }
        else{
        	return false;
        }
        
     });
  
    
    
    
	 	
})(jQuery);


function checkValue(str){
	if(str.replace(/\s/g,"") != "" && isNumber(str))
		return true;	
}

function isNumber(o) {
    return typeof o === 'number' && isFinite(o);
}
	

function createField(){
	 //$('.color').colorpicker();
	$(".color").spectrum({
		showPaletteOnly: true,
		showPalette:true,
		hideAfterPaletteSelect:true,
		//showInput: true,
		showInitial: true,
		showInput: true,
		preferredFormat: "hex",
		palette: color_palette,
		allowEmpty: true
	});

	if(typeof color_palette !== 'undefined' && $.isArray(color_palette)) {
		$('.color-picker').spectrum({
			showPaletteOnly: true,
			showPalette: true,
			hideAfterPaletteSelect: true,
			//showInput: true,
			showInitial: true,
			showInput: true,
			preferredFormat: "hex",
			palette: color_palette,
			allowEmpty: true
		});
	}

	 $('body').on('change', '.ftype', function (e) {
		if( this.value=="Single Select" ){
			$('.sel-options-div').show();
			$('.select_options').prop('required',true);
		}
		else{
			$('.sel-options-div').hide();
			$('.select_options').prop('required',false);
		}		
	 });
	 
	 $('.forAllUsers').on('click', function(event) {
	    
        if( $(this).is(":checked") ){    
        	$('.users-chk').prop( "checked", false );
        	$('.users-chk').prop( "disabled", true );        	
        }
    	else{   
    	
    		$('.users-chk').prop( "disabled", false );
    	}        
    });	 
	$('#fields-div').hide('slow');
	$('#newfield-div').show('slow');
}


function addColorValueRecord(){
	//$('.color').colorpicker();
	$(".color").spectrum({
		showPaletteOnly: true,
		showPalette:true,
		hideAfterPaletteSelect:true,
		//showInput: true,
		showInitial: true,
		showInput: true,
		preferredFormat: "hex",
		palette: color_palette,
		allowEmpty: true
	});
}

function storeField(){
	 $('#newfield-div').html();
	 $('#field-add-form')[0].reset();	
	 $('#newfield-div').hide('slow');
	 $('#fields-div').show('slow');
	 refreshFieldTable($("input[name='sheet_id']").val());
}

var refreshFieldTable = function(tpl_id) {
    $('.fieldtable').bootstrapTable('refresh', {
        silent: true,
        url: ajaxUrl + 'sheetfields',
        query: {route: 'index', sheet: tpl_id}
    });
}


function editField(){
	
	 //$('.color').colorpicker();
	$(".color").spectrum({
		showPaletteOnly: true,
		showPalette:true,
		hideAfterPaletteSelect:true,
		//showInput: true,
		showInitial: true,
		showInput: true,
		preferredFormat: "hex",
		palette: color_palette,
		allowEmpty: true
	});
	if(typeof color_palette !== 'undefined' && $.isArray(color_palette)) {
		$('.color-picker').spectrum({
			showPaletteOnly: true,
			showPalette: true,
			hideAfterPaletteSelect: true,
			//showInput: true,
			showInitial: true,
			showInput: true,
			preferredFormat: "hex",
			palette: color_palette,
			allowEmpty: true
		});
	}
	 $('body').on('change', '.ftype', function (e) {
		if( this.value=="Single Select" ){
			$('.sel-options-div').show();
			$('.select_options').prop('required',true);
		}
		else{
			$('.sel-options-div').hide();
			$('.select_options').prop('required',false);
		}		
	 });
	 $('.forAllUsers').on('click', function(event) {
	    
        if( $(this).is(":checked") ){    
        	$('.users-chk').prop( "checked", false );
        	$('.users-chk').prop( "disabled", true );        	
        }
    	else{   
    	
    		$('.users-chk').prop( "disabled", false );
    	}        
    });	 
	 
	 
	$('#fields-div').hide('slow');
	$('#editfield-div').show('slow');
}

function updateField(){
	var tpl = $('.templateid').val() ;
	$('#fields-div').show('slow');
	$('#editfield-div').html();
	$('#editfield-div').hide('slow');
	refreshFieldTable(tpl);	
	
}




function deleteField(){
	 refreshFieldTable($("input[name='sheet_id']").val());
}


function reOrderFields(table){
	var _data = {};
	_data['table'] = table;
	_data['path'] = 'SheetFields';
	_data['route'] = 'ReOrder';	
	_data['_token'] = $("input[name='_token']").val();
	 doAjax(ajaxUrl, _data, false);	  
}

function ReOrder(){
	refreshFieldTable($('.sheet_id').val());
}




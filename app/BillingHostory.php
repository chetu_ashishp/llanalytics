<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
class BillingHostory extends Model {
    protected $table = 'billing_histories';
    
    public function setFormatedDate($date) {
  		  		
  		if(!empty($date))
  			return date('Y-m-d', strtotime($date));
  		else
  			return NULL;
  	}
    public static $invoice_lables = array(
			
			'invoice_id' =>" Invoice No",
            'invoice_date' =>"Invoice Date",
            'line_item' =>"Line Item",
            'total_line_item' =>"Total Line Item",
            'payment_made' =>"Payment Made",
            'transaction_id' =>"Transaction ID",
			'payments_or_adjustments' => "Payments or Adjustments" ,
	        'user_added_first_name' =>"Staff Member Purchased",
	        'status' =>"Status",
            'control' => 'Control'
	);
    public function subscription() {
		return $this->belongsTo('App\Subscription');
	}
}

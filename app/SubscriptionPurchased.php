<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
class SubscriptionPurchased extends Model {
    protected $table = 'subscription_purchased';
    
    public function setFormatedDate($date) {
  		  		
  		if(!empty($date))
  			return date('Y-m-d', strtotime($date));
  		else
  			return NULL;
  	}
    
     public function company() {
		return $this->belongsTo('App\Company');
	}
}

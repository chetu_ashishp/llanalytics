<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class EmailTemplate extends Model {
     protected $guarded = array();  // Important
	protected $table = 'email_templates';
	
	public static $rules_emailtemplate = array(
			'email_template_name' => 'required|unique:email_templates',
			'email_template_subject' => 'required',			
			'email_template_content' => 'required',
	);
		
	public static $client_lables = array(
			
			'email_template_name' =>"Client Number",
			'email_template_subject' => "Last Name" ,
			'email_template_content' => 'First Name',
	        'status' =>"Status"
	);
    public static $baseRules = array(
			'email_template_name' => 'required',
			'email_template_subject' => 'required',			
			'email_template_content' => 'required',
	);
    public static function validateUpdate($data, $id) {
		$updateRule = static::$baseRules;
		//$updateRule['email_template_name'] = 'required | email_templates | unique:email_templates,' . $id;
		//$updateRule['band'] = 'required | unique:musicians,band,' . $id;
		return Validator::make($data, $updateRule);
	}
  	
}
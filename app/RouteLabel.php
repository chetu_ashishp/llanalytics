<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteLabel extends Model {

    protected $guarded = array();
    protected $table = 'route_labels';
    public static $rules = array(
        'route'=>'required',
        'method'=>'required'
    );
    protected $fillable = array('route','method', 'label', 'description', 'public');

}

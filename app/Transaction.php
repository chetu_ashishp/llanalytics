<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

	protected $table = 'transactions';
	
	public static $rules = array(
			'start_date' => 'required|date_format:"m/d/Y"',
			'transaction_type' => 'required',
			'transaction_status' => 'required',
			'listing_date' => 'date_format:"m/d/Y"',
			'listing_expiration_date' => 'date_format:"m/d/Y"',
			'contract_date' => 'date_format:"m/d/Y"',
			
			'coe_date' => 'date_format:"m/d/Y"',						
	);
	
	public static $client_lables = array(
				
			'start_date' =>"Entry Date",
			'transaction_type' =>"Type",
			'transaction_status' =>"Status",
			'referral_source' =>"Source",
			'related_trans_id' =>"Related Transaction",
			
			'sales_price' 	=>"Sales Price",
			'bonus' 		=>"Bonus",			
			'agent_proportio' 		=>"Agent Proportion",
			'commission_rate' 		=>"Commission Rate",			
			'agent_split' 		=>"Agent Split",
			'agent_split2' 		=>"Agent Split 2",			
			'broker_flat_fee' 		=>"Broker Flat Fee",
			'assist_name' 		=>"Assist Name",
			'assist_fee' 		=>"Assist Fee",
			
			'gross_commission' 		=>"Gross Commission",
			'net_commission' 		=>"Net Commission",
			'final_net_commission' 		=>"Final Net Commission", 
			'sp_of_list' 		=>"SP as % of List",
			'sp_of_origin_list' 		=>"SP as % of Original List",			
			'dom_to_contract' 		=>"DOM to Contract",
			'dom_to_close' 		=>"DOM to Close",
			
			
			'mls1' 		=>"MLS # - 1",
			'mls2' 		=>"MLS # - 2",
			'listing_date' 		=>"Listing Date",
			'listing_expiration_date' 		=>"Listing Expiration Date",
			'origin_list_price' 		=>"Original List Price",
			'current_list_price' 		=>"Current List Price",
			'lockbox' 		=>"Lockbox #",
			
	
			'nickname' 			=>"Short Address",
			'property_house' 	=>"Property House #",
			'property_addr1' 	=>"Property Address Line 1",
			'property_addr2' 	=>"Property Address Line 2",
			'city' 				=>"City",
			'state' 			=>"State",
			'zip' 				=>"Zip",
			'subdivision' 		=>"Subdivision",
			'hoa' 				=>"HOA",
			
			
			'contract_date' 	=>"Contract Date",
			
			'coe_date' 			=>"COE Date",
			'other_agent_name' 	=>"Other Agent Name",
			'number_of_units' 	=>"Number Of Units",
			'lender' 			=>"Cash / Lender",
			'title_company' 	=>"Title Company",
			'escrow_officer' 	=>"Escrow Officer",
			'notes' 			=>"Notes"			
	);
	
	public function tstatus(){
		return $this->hasOne('App\CompanyDataOption', 'id', 'transaction_status');
	}
	
	public function ttype(){
		//echo $this->trunsaction_type;exit;
		return $this->hasOne('App\CompanyDataOption', 'id', 'transaction_type');
	}
	
	public function getFormatedDate($date='start_date') {
		if($this->{$date})
  			return date('m/d/Y', strtotime($this->{$date}));
		else 
			return '';	
  	}
	 
	public function setFormatedDate($date) {
	
		if(!empty($date))
			return date('Y-m-d', strtotime($date));
		else
			return NULL;
	}
	
}

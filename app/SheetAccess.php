<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SheetAccess extends Model {

	protected $table = 'sheet_accesses';

	public function users(){
		return $this->hasOne('App\User');
	}
	
	public function template(){
		return $this->hasOne('App\Template');
	}
}

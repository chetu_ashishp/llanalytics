<?php namespace App\Http\Middleware;

use Closure;
use Route;
use App\RolesMeta;
use Auth;
use Redirect;

class Permissions {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	
	public function handle($request, Closure $next)
    {
    	$user = Auth::user();
    	$role = isset($user->role_id) ? $user->role_id : null;
    	$methods = $request->route()->getMethods();
    	$uri = $request->route()->getUri();
    	    	
    	if (is_null($permission = RolesMeta::select(array('permission'))->where('role_id', '=', $role)->where('route', '=', $uri)->whereIn('method',  $methods)->first())){    	
    		return redirect('/')->with('error', 'You don\'t have permission to go this route');
    	}
    	
    	if ($permission->permission != 1) {    	    	
    		return redirect('/')->with('error', 'You don\'t have permission to go this route');;
    	}

        return $next($request);
    }	
	
	
}

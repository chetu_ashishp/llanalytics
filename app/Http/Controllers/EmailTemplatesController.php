<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use Event;
use App\EmailTemplate;
use DB;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use Route;


class EmailTemplatesController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
        $routepath = Route::getCurrentRoute()->getPath();
        if($routepath != 'emailtemplate/update/{etemplate}' && $routepath != 'emailtemplates/create' && $routepath != 'emailtemplate/store') {
            $something = new HomeController;
            $check = $something->authenticateUserRolePermissionURL($routepath);           
            if(!$check) {
               abort(404, 'Unauthorized action.');         
            }
        }
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the subscription.
	 *
	 * @return Response
	 */
	public function index() {
		$tableData = json_encode($this->show());
		return view('emailtemplate.index', compact('tableData'));		
	}
	
	
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($type=false) {
		$json = array();     
		$res = EmailTemplate::select(array('email_templates.id','email_templates.email_template_name', 'email_templates.email_template_subject', 'email_templates.status'))->where('status','!=','Deleted');
        $result = $res->get();
        if(isset($result) && count($result) > 0) {
            foreach($result as $rKey => $rVal) {
                $id = $result[$rKey]->id;
                $json[] = array('email_template_name' => $result[$rKey]->email_template_name,
                        'email_template_subject' => $result[$rKey]->email_template_subject,
                        'status' =>$result[$rKey]->status,					
                        'actions' => view('emailtemplate.actions', compact('id'))->render() );
            }
        }
		return $json;
	}
	
		
	
	/**
	 * Show the form for creating a new subscription.
	 *
	 * @return Response
	 */
	public function create($id = false) {                
       $form = 'Add';
      return view('emailtemplate.create', compact('form'));
	}

	/**
	 * Store a newly created subscription in table.
	 *
	 * @return Response
	 */
	public function storeemailtemplate() { 
        
		$addrs = array();
		$input = Request::all();
		$validator = Validator::make($input, EmailTemplate::$rules_emailtemplate); 
		unset($input["_token"]);
		
		if ($validator->passes()) {
		
			$input_fields = array(
						
                'email_template_name' ,
                'email_template_subject' ,
                'email_template_content' ,
                'status' ,
			);			
			$item = new EmailTemplate();
			foreach($input as $oKey => $oVal) {
	
				if(in_array($oKey, $input_fields)) {
					$item->$oKey = $oVal;
				}
			}			 
			$item->save();	
			return Redirect::to('emailtemplates');
		} else {
		
			return redirect()->back()->withInput()->withErrors($validator);
		}
	}
	
    
    /**
	 * Show the form for editing the subscription Package.
	 *
	 * @param  int  $item
	 * @return Response
	 */
	public function edit($id) {
        $form = "Edit";		
        if(isset($id) && !empty($id)) {
          $emailTemplateItem = EmailTemplate::select(array('email_templates.id', 'email_templates.email_template_name', 'email_templates.email_template_subject','email_templates.email_template_content','email_templates.status'))->where('id', '=', $id)->get();
          return view('emailtemplate.edit', compact('id','form','emailTemplateItem'));
        }
	}
	

    /**
	 * Update the subscription package.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function update($emailtemplate_id) {
	
        $output = Request::all();
        $validation = Validator::make($output, EmailTemplate::$rules_emailtemplate);
        $validation = EmailTemplate::validateUpdate($output, $emailtemplate_id);
	     if($validation->passes()) {
            unset($output["_token"]);
		
		
            $item = EmailTemplate::find($emailtemplate_id);
            $updateEmailTemplate = array(
               'email_template_name' => !empty($output["email_template_name"]) ? $output["email_template_name"] : '',
               'email_template_subject' => !empty($output["email_template_subject"]) ? $output["email_template_subject"] : '',
               'email_template_content' => !empty($output["email_template_content"]) ? $output["email_template_content"] : '',
               'status' => !empty($output["status"]) ? $output["status"] : ''
            );
                    
            foreach($updateEmailTemplate as $oKey => $oVal) {
               $item->$oKey = $oVal;				
            }
            $item->save();
            return Redirect::to('emailtemplates');
       } else {
			return redirect()->back()->withInput()->withErrors($validation);
		 }
	}
    
    public function removeCheck() {
		
		$id =  Request::input('id'); 		
		$token =  Request::input('_token');
		if (isKeyUsedAsForeignKey('EmailTemplate', $id)===false ) {
            return $this->ajax_construct(false, "deleteEmailTemplate({$id}, 'RemoveEmailTemplate', '{$token}'); ");
		} 
		else {
			$data['array'] = array('message' => array("This Eamil Template is in use and can't be deleted."));
			return array('success'=>false, "arr"=>$data);
		}
		
	}
  
    public function RemoveEmailTemplate() {
       
        $id =  Request::input('id');
		if(isset($id) && $id != '') {
			EmailTemplate::where('id', $id)->update(['status' => "Deleted"]);
		}
        return $this->ajax_construct();
    }
    
    /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {
        	      	
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
         
        
			if (Request::has('typeId')) {
            		$data = $this->$route(Request::input("typeId"));
			}
			
			elseif($route== 'deleteEmailTemplate'){
            	            
            	 $data = $this->$route(Request::input('deleteid'));
            }
			else{				
					$data = $this->$route();
			}
			
            if($route== 'show' || $route== 'deleteEmailTemplate') {            	
                return json_encode($data);
            }
            elseif($route== 'removeCheck' && isset($data["success"]) && $data["success"]==false){

            	return $this->json_error($data["arr"]);
            }
            	          
            
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            

            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('false' => true, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}
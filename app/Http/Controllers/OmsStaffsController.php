<?php namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Transaction;
use Request;
use Route;
use App\CompanyDataOption;
use App\DataOption;
use App\Client;
use App\DataType;
use Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use App\State;
use App\Address;
use App\CompanyDataType;
use App\Staff;
use App\User;
use App\Role;
use Mail;
use App\State as StateModel;
use App\Company as CompanyModel;
use Hash;
use App\EmailTemplate;

class OmsStaffsController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
        $routepath = Route::getCurrentRoute()->getPath();
        if($routepath != 'staff/update/{staff}' && $routepath != 'staff/storeStaff' && $routepath != 'omsstaffs/search') {
            $something = new HomeController;
            $check = $something->authenticateUserRolePermissionURL($routepath);
            if(!$check) {
               abort(404, 'Unauthorized action.');
            }
        }
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
        $tableData = json_encode($this->show());
		return view('cstaff.index', compact('tableData'));
	}
	
    /**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($type=false) {
        
       $json = array();
       $company_id = \Config::get('custom.omsStaffCompanyID'); //Internal Staff Company id       
       $result = User::OmsUserStaffData($company_id);
       $result = $result->get();
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			
			$json[] = array('first_name' => $result[$rKey]->first_name,
					'last_name' => $result[$rKey]->last_name,
					'email' => $result[$rKey]->email,
					'mobile_phone' => $result[$rKey]->mobile_phone,
					'role_id' => $result[$rKey]->role_name,
					'status' => $result[$rKey]->status,
					'actions' => view('cstaff.actions', compact('id'))->render() );
		}
		return $json;
	}
    
	/**
	 * Show the form for creating a new oms staff.
	 *
	 * @return Response
	 */
	public function create() {
            $form = 'Add';		
          	
          	$company_id = \Config::get('custom.omsStaffCompanyID'); //Internal Staff Company id
			$company = CompanyModel::find($company_id);
			$roles = Role::orderBy('role_name')->lists('role_name', 'id');
			return view('cstaff.create', compact('form', 'company','company_id','roles'));
	}
	
	/**
	 * Store a newly created staff.
	 *
	 * @return Response
	 */
	public function store () {
		
		$input = Request::all();
		
        $validation = Staff::validateOmsStore($input);    
		  unset($input["_token"]);
          if ($validation->passes() && !empty($input["company_id"])) {	
				
					$password = GUID();                   
					$passwordHash = Hash::make($password);
					$newStaff = array(
						'last_name' => !empty($input["lname"]) ? $input["lname"] : NULL,
						'first_name' => !empty($input["fname"]) ? $input["fname"] : NULL,
						'password' => $passwordHash,
						'status' => !empty($input["status"]) ? $input["status"] : NULL,
						'role_id' => !empty($input["role_id"]) ? $input["role_id"] : NULL,
						'mobile_phone' => !empty($input["primary_phone"]) ? $input["primary_phone"] : '',
						'alternate_phone' => !empty($input["alternate_phone"]) ? $input["alternate_phone"] : '',
						'name' => !empty($input["lname"]) ? $input["fname"]. ' '.$input["lname"] : NULL,
						'email' => !empty($input["email"]) ? $input["email"] : NULL,						
						'company_id' => \Config::get('custom.omsStaffCompanyID'), //Standard Administrator to update further
					 );
					$staffUser = new User();
					foreach($newStaff as $oKey => $oVal) {

						$staffUser->$oKey = $oVal;            
					}
					$userStaff = $staffUser->save();
					$userId = $staffUser->id;
					
				if(!empty($userId)) {
					$staffData = array(

						'user_id' => !empty($userId) ? $userId : NULL,
						'company_id' => \Config::get('custom.omsStaffCompanyID'), //Standard Administrator to update further
					);
					
					$staff = new Staff();
					foreach($staffData as $oKey => $oVal) {
						$staff->$oKey = $oVal;		
					}

					$itemuser = $staff->save();
                   
                    //mail functionality
                    if($itemuser) {
                       $data = array();
                       $data['first_name'] = !empty($input["fname"]) ? $input["fname"] : '';
                       $data['last_name'] = !empty($input["lname"]) ? $input["lname"] : '';
                       $data['name'] = $data['first_name'].' '. $data['last_name'];
                       //$data['company'] = !empty($input["company_name"]) ? $input["company_name"] : '';                    
                       $data['username'] = !empty($input["email"]) ? $input["email"] : '';
                       $data['password'] = !empty($password) ? $password : '';
                       //$data['toEmail'] = 'ashishp@chetu.com';
                       $data['toEmail'] = !empty($input["email"]) ? $input["email"] : 'ashishp@chetu.com';
                       $data['name'] = !empty($input["fname"]) ? $input["fname"] : '';
                       $data['OMSCompanyName'] = \Config::get('custom.OMSSupportPh');
                       $data['OMSSupportPh'] = \Config::get('custom.OMSSupportPh');
                       $data['OMSSupportEmail'] = \Config::get('custom.OMSSupportEmail');
                       $data['OMSCompanyLinkUrl'] = \Config::get('custom.OMSCompanyLinkUrl');
                       $emailTemplateItem = EmailTemplate::select(array('email_templates.id','email_templates.email_template_name','email_templates.email_template_subject', 'email_templates.email_template_content'))->where('email_template_name', '=', 'OMS internal Staff Creation')->get();
                       $data['email_template_subject'] = !empty($emailTemplateItem[0]['email_template_subject'])? $emailTemplateItem[0]['email_template_subject']: '';
                       $templateContent = !empty($emailTemplateItem[0]['email_template_content']) ? $emailTemplateItem[0]['email_template_content']: '';
                       if(!empty($templateContent)) {                       
                           if($template = getEmailFormat($templateContent, $data['name'],'',$data['OMSCompanyName'],$data['OMSSupportPh'],$data['OMSSupportEmail'],$data['OMSCompanyLinkUrl'],$data['username'],$data['password'])) {       
                         
                         Mail::raw($template, function($message) use ($template,$data) {
                            $message->from($data['OMSSupportEmail']);
                            $message->to($data['toEmail']);
                            $message->setBody($template, 'text/html');
                            $message->subject($data['email_template_subject']);
                            $message->sender($data['OMSSupportEmail'], 'OMS Solution');
                        });
                        //OMS internal Staff Creation to Admin
                       //$data['toOMSEmail'] = '';
                       $emailTemplateItem = EmailTemplate::select(array('email_templates.id','email_templates.email_template_name','email_templates.email_template_subject', 'email_templates.email_template_content'))->where('email_template_name', '=', 'OMS internal Staff Creation to Admin')->get();
                       $data['email_template_subject'] = !empty($emailTemplateItem[0]['email_template_subject'])? $emailTemplateItem[0]['email_template_subject']: '';
                       $templateContent = !empty($emailTemplateItem[0]['email_template_content']) ? $emailTemplateItem[0]['email_template_content']: '';
                       if(!empty($templateContent)) {                  
                            if($template = getEmailFormat($templateContent, $data['name'],'',$data['OMSCompanyName'],$data['OMSSupportPh'],$data['OMSSupportEmail'],$data['OMSCompanyLinkUrl'],$data['username'],$data['password'])) {       
                                Mail::raw($template, function($message) use ($template,$data) {
                                   $message->from($data['OMSSupportEmail']);
                                   $message->to($data['OMSSupportEmail']);
                                   $message->setBody($template, 'text/html');
                                   $message->subject($data['email_template_subject']);
                                   $message->sender($data['OMSSupportEmail'], 'OMS Solution');
                               });
                           }
                        }
                        
                     } 
                  }  
                    //
                 }
            }			
		return Redirect::to('staff');
			
		} else {
			return redirect()->back()->withInput()->withErrors($validation);
		}
	}	
	
	
	public function search() {
		$input = Request::input('params');
		parse_str($input, $output);
		$json = array();
		
		$company_id = \Config::get('custom.omsStaffCompanyID'); //Internal Staff Company id
		$res = User::select(array('users.id', 'users.first_name', 'users.last_name','users.mobile_phone','users.email','users.role_id', 'users.status'))->where('status', '!=', 'Deleted')->where('company_id', '=', $company_id);
	
		if(isset($output["company_number"]) && $output["company_number"]!='') {
			$filtername = $output["company_number"];
			$res->where(function ($query) use ($filtername) {
				$query->where('first_name', 'like',  '%'.$filtername.'%')
				->orWhere('last_name', 'like',  '%'.$filtername.'%')
				->orWhere('email', 'like',  '%'.$filtername.'%')
				->orWhere('mobile_phone', 'like',  '%'.$filtername.'%');
			});
		}		
		
		if(isset($output["status"]) && $output["status"]!='') {
			$res->where('status', $output["status"]);
		}
		$res->groupBy('users.id');
		$result = $res->get();
		$addr = array();
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			
			$json[] = array('first_name' => $result[$rKey]->first_name,
					'last_name' => $result[$rKey]->last_name,
					'email' => $result[$rKey]->email,
					'mobile_phone' => $result[$rKey]->mobile_phone,
					'role_id' => '',//$result[$rKey]->role_id,
					'status' => $result[$rKey]->status,
					'actions' => view('cstaff.actions', compact('id'))->render() );
		}
	
		$tableData = json_encode($json);
		
		$table =  view('cstaff.index', compact('tableData', 'id'));
		return json_encode($json);
		//return $this->ajax_construct($table, "initBootstrapTable(".$tableData."); ");
	}
	
    public function searchstaffnonactive() {
        
        $input = Request::input('params');   
		parse_str($input, $output);
      
       $json = array();
       $company_id = \Config::get('custom.omsStaffCompanyID'); //Internal Staff Company id
       $status = $output['staffstatus'];
       $result = User::OmsStaffActiveNonActive($company_id,$status);
       $result = $result->get();
	   foreach($result as $rKey => $rVal) {
            
            $id = $result[$rKey]->id;
			$is_deleted = $result[$rKey]->is_deleted;
            $status = $result[$rKey]->status;
            $status = $result[$rKey]->status;
            if($is_deleted == 1) $status = 'Deleted';
			$json[] = array('first_name' => $result[$rKey]->first_name,
					'last_name' => $result[$rKey]->last_name,
					'email' => $result[$rKey]->email,
					'mobile_phone' => $result[$rKey]->mobile_phone,
					'role_id' => $result[$rKey]->role_name,
					'status' => $status,
					'actions' => view('cstaff.searchactions', compact('id','is_deleted'))->render() );
		}
		return $json;
    }
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $client
	 * @return Response
	 */
	public function editOmsStaff($user_id) {
		
		$staffDetails = User::find($user_id);		
		$form = "Edit";
		$roles = Role::orderBy('role_name')->lists('role_name', 'id');
		return view('cstaff.edit', compact('user_id','form','staffDetails','roles') );
	}
	
		/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($staff) {
		
		$output = Request::all();
		$validation = Staff::validateOmsStaffUpdate($output,$staff);   
		 if ($validation->passes() && !empty($output["staff_id"])) {
			 
					$item = User::find($staff);
					 $newStaff = array(
						'last_name' => !empty($output["lname"]) ? $output["lname"] : NULL,
						'first_name' => !empty($output["fname"]) ? $output["fname"] : NULL,
						'status' => !empty($output["status"]) ? $output["status"] : NULL,
						'role_id' => !empty($output["role_id"]) ? $output["role_id"] : NULL,
						'mobile_phone' => !empty($output["primary_phone"]) ? $output["primary_phone"] : '',
						'alternate_phone' => !empty($output["alternate_phone"]) ? $output["alternate_phone"] : '',
						'name' => !empty($output["fname"]) ? $output["fname"]. ' '.$output["lname"] : NULL,
						'email' => !empty($output["email"]) ? $output["email"] : NULL
					 );
					 foreach($newStaff as $oKey => $oVal) {
						$item->$oKey = $oVal;				
					 }
					 $item->save();
					 $userId = $item->id;
					 return Redirect::to('staff');
		 } else {
			$data['array'] = array('message' => $validation->messages()->all());			
            return redirect()->back()->withInput()->withErrors($validation);
		 }
	}
	
	public function deleteOmsStaff() {
        
		$userId =  Request::input('id');
		if(isset($userId) && $userId != '') {
			//User::where('id', $userId)->update(['status' => "Deleted"]);
            User::where('id', $userId)->update(['is_deleted' => 1]);
		}
		return $this->ajax_construct();
	}
	
    public function resetomsstaff() {
      //die('resetomsstaff');
		$id =  Request::input('id');
		if(isset($id) && $id != '') {			
            User::where('id', $id)->update(['is_deleted' =>0]);
		}
		return $this->ajax_construct();
	}
    
	public function removeCheck() {
        
        $output = Request::all();
        $dataRoute = $output['route'];
        $dataPath =  $output['path'];
        if(isset($dataRoute) && isset($dataPath) && !empty($dataRoute) && !empty($dataPath)) {
            
            $something = new HomeController;
            $check = $something->authenticateUserRolePermission($dataPath,$dataRoute);
            if(!$check) {
                $data['array'] = array('message' => array("You dont have permission to perform this action."));
                return array('success'=>false, "arr"=>$data);
            }
            $id =  Request::input('item'); 		
            $token =  Request::input('_token'); 

           // if (isKeyUsedAsForeignKey('user', $id)==false ) {
            if(!iskeyUsedTable('users','parent_id',$id)) {
                return $this->ajax_construct(false, "RemoveOmsStaff({$id}, 'deleteOmsStaff', '{$token}'); ");			
            } 
            else {
                $data['array'] = array('message' => array("This OMS Staff is in use and can't be deleted."));
                return array('success'=>false, "arr"=>$data);
            }
        } else {
            $data['array'] = array('message' => array("Route is not defined."));
            return array('success'=>false, "arr"=>$data);
        }
            
	}
		/**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {
       	
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
         
        
			if (Request::has('typeId')) {
            		$data = $this->$route(Request::input("typeId"));
			}
			
			elseif($route== 'RemoveOmsStaff'){
            	            
            	 $data = $this->$route(Request::input('deleteid'));
            }
			else{				
					$data = $this->$route();
			}
			
            if($route== 'show' || $route== 'RemoveOmsStaff') {            	
                return json_encode($data);
            }
            elseif($route== 'removeCheck' && isset($data["success"]) && $data["success"]==false){
            	
            	return $this->json_error($data["arr"]);
            }
            	          
            
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            

            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
		}
}
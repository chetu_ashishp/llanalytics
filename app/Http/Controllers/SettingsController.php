<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use App\Settings;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Auth;
use Response;
use Route;
class SettingsController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{   
        $routepath = Route::getCurrentRoute()->getPath();
        if( $routepath != 'ajaxSettings') {
            $something = new HomeController;
            $check = $something->authenticateUserRolePermissionURL($routepath);           
            if(!$check) {
               abort(404, 'Unauthorized action.');         
            }
        }
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('settings.index', array());
	}


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function emailUpdate() { 

    	$input = Request::input('form-input');    	
    	parse_str($input, $output);
    	    	    	
        $validator = Validator::make($output, Settings::$rules_email);
           
        if ($validator->passes()) {
            foreach ($output as $key => $value) {

            	if ($key != '_token') {
                    if (Settings::where('key', '=', $key)->count() > 0)
                        Settings::where('key', '=', $key)->update(array('value' => $value));
                    else {
                        $settings=new Settings();
                        $settings->key=$key;
                        $settings->value=$value;
                        $settings->who_edited=  Auth::user()->id;
                        $settings->save();
                    }
                }                                
            }
            $data['alert'] = array( 'type'=> 'success', 'message' => 'Email settings were updated');
            return $this->json_success($data);
        }
        else{
        	$data['array'] = array( 'type'=> 'error', 'message' => $validator->messages()->toArray());          
        	return $this->json_error($data);
        }	        
    }
    
    public function json_success($data = false) {
    	return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
    }
    
    /**
     * JSON Error Handler
     *
     * @return JSON Response
     */
    public function json_error($data = false) {
    	return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
    }
    
    
    
    public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
    	$result = array();
    
    	if ($html) {
    		$result['html'] = $html->render();
    	}
    	if ($script) {
    		$result['script'] = $script;
    	}
    	if ($append) {
    		$result['append'] = $append;
    	}
    	if ($alert) {
    		$result['alert'] = $alert;
    	}
    	return $result;
    }
    
}

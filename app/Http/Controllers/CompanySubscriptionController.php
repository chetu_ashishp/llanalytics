<?php namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Session;
use Session;
//use App\custom;
use Request;
use App\CompanyDataOption;
use App\DataOption;
use App\Client;
use App\DataType;
use Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use App\State;
use App\Address;
use App\CompanyDataType;
use App\Staff;
use App\User;
use App\Subscription;
use App\SubscriptionPurchased;
use App\BillingHostory;
use App\EmailTemplate;
use App\State as StateModel;
use App\Company as CompanyModel;
use Hash;
use Mail;
use DB;
use Route;

class CompanySubscriptionController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
        
		$routepath = Route::getCurrentRoute()->getPath();
        if($routepath != 'ajaxCompanySubscribed' && $routepath != 'companysubscribedSearch') {
            $something = new HomeController;
            $check = $something->authenticateUserRolePermissionURL($routepath);           
            if(!$check) {   
               abort(404, 'Unauthorized action.');         
            }
        }
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		
        //$tableData = json_encode(array()); //json_encode($this->show()); 
        $tableData = json_encode($this->show());
							
		return view('companysubscription.index', compact('tableData'));
	}
    
    /**
	 * Display the subscribed company list resource.
	 *
	 * @return json array
	 */
	public function show($type = false) {
		    $json = array();
            $matchThese = ['subscribed' => 1, 'status' => 'Active', 'status' => 'Inactive'];
            $result = Company::select(array('companies.id', 'companies.company_number', 'companies.company_name', 'companies.company_type','companies.primary_phone','companies.email','companies.status','companies.fname','companies.lname'))->where('is_deleted', '!=', 1)->whereIn('status',  array('Active', 'Inactive'))->whereIn('subscribed',  array(1))->get();	  
			//echo '<pre/>';
           // print_r($result);
			if(isset($result) && count($result) > 0) {
				foreach($result as $rKey => $rVal) {
					$id = $result[$rKey]->id;
                    $company_status = $result[$rKey]->status;
					$arr[] = array(
						'company_number' => $result[$rKey]->company_number,
						'company_name' => $result[$rKey]->company_name,
						'email' => $result[$rKey]->email,
                        'primary_contact' =>$result[$rKey]->lname.', '.$result[$rKey]->fname,
                        'primary_phone' =>$result[$rKey]->primary_phone,
						'status' => $result[$rKey]->status,
						'actions' => view('companysubscription.actions', compact('id','company_status'))->render()
					);
				}            
			}   
         return $arr;
        /*$tableData = json_encode($arr);
		
		$table =  view('companysubscription.index', compact('tableData'));
		return $this->ajax_construct($table, "initBootstrapTable(".$tableData."); ");*/
	}
    
    public function approveCompanySubscription() {
        
         $company_id = Request::input('id');
         $company_status = Request::input('status');
       
         if(isset($company_id) && !empty($company_id) && $company_status == 'Active') {
               
            $companyUpdate =  Company::where('id', $company_id)->update(['status' => $company_status]);
            $companyUserUpdate =  User::where('company_id', $company_id)->update(['status' => $company_status]);
            if($companyUpdate && $company_status == 'Active') {
                
              
                 $output = Company::companyUserSubscribedData($company_id,'Active',1);               
                 $password = GUID();               
                 $passwordHash = Hash::make($password);
                 $updateCompanyUserPass =  User::where('company_id', $company_id)->update(['password' => $passwordHash]);
                 $data = array();
                 $data['first_name'] = !empty($output[0]->first_name) ? $output[0]->first_name : '';
                 $data['last_name'] = !empty($output[0]->last_name) ? $output[0]->last_name : '';
                 $data['name'] = $data['last_name'].', '.$data['first_name'];
                 $data['OMSCompanyName'] = !empty($output[0]->name) ? $output[0]->name : '';   
                 $data['OMSSupportPh'] = \Config::get('custom.OMSSupportPh');
                 $data['OMSSupportEmail'] = \Config::get('custom.OMSSupportEmail');
                 $data['company_name'] = !empty($output[0]->company_name) ? $output[0]->company_name : '';
                 //$data['company_name'] = !empty($output[0]->company_name) ? $output[0]->company_name : '';
                 $data['username'] = !empty($output[0]->email) ? $output[0]->email : '';
                 $data['password'] = $password;
                 //$email = 'ashishp@chetu.com'; // to change dynamic for company
                 $data['toEmail'] = !empty($output[0]->email) ? $output[0]->email : 'ashishp@chetu.com'; // email
                 $subject =  \Config::get('custom.companySubscriptioApprovalSubject');
                /*Mail::send('emails.subscription_approval_company', ['data' => $data], function($message) use ($email) {
                 $message->from(\Config::get('custom.OMSEmailFromAccount'), \Config::get('custom.companyName'))->to($email)->subject('Company Subscription!')->sender(\Config::get('custom.OMSEmailFromAccount'), $name = \Config::get('custom.companyName'));
                // $message->from('laurie@greenvalley123.com', 'OMS Solution')->to($email)->subject('Welcome To Oms!')->sender('laurie@greenvalley123.com', $name = 'OMS Solution');
               });*/
               
                $emailTemplateItem = EmailTemplate::select(array('email_templates.id','email_templates.email_template_name','email_templates.email_template_subject', 'email_templates.email_template_content'))->where('email_template_name', '=', 'Company Subscription Approval')->get();
                $data['email_template_subject'] = !empty($emailTemplateItem[0]['email_template_subject'])? $emailTemplateItem[0]['email_template_subject']: '';
                $templateContent = !empty($emailTemplateItem[0]['email_template_content']) ? $emailTemplateItem[0]['email_template_content']: '';
                if(!empty($templateContent)) {
                    if($template = getEmailFormat($templateContent, $data['name'],$data['company_name'],$data['OMSCompanyName'],$data['OMSSupportPh'],$data['OMSSupportEmail'],'',$data['username'],$data['password'])) {

                        /*Mail::raw($template, function($message) use ($template,$data) {
                            $message->from($data['OMSSupportEmail']);
                            $message->to($data['toEmail']);
                            $message->setBody($template, 'text/html');
                            $message->subject($data['email_template_subject']);
                            $message->sender($data['OMSSupportEmail'], 'OMS Solution');
                        });*/
                    }
                }
            }
         } elseif(isset($company_id) && !empty($company_id) && $company_status == 'Terminated') {
            // Company::where('id', $company_id)->update(['status' => "Deleted"]);
             //User::where('company_id', $company_id)->update(['status' => "Deleted"]);
             Company::where('id', $company_id)->update(['is_deleted' => 1]);
             User::where('company_id', $company_id)->update(['is_deleted' =>1]);
         }
         
        return $this->ajax_construct();
    }	
   
    public function search() {
        
        $input = Request::input('params');
		parse_str($input, $output);
      
        $json = array();
        $status = $output['staffstatus'];
        if($status == 'Active') {
            //$status = 'Active';
           // $result = Company::select(array('companies.id', 'companies.company_number', 'companies.company_name', 'companies.company_type','companies.primary_phone','companies.email','companies.status','companies.fname','companies.lname'))->where('status', '=', 'Active')->where('is_deleted', '=', 0)->whereIn('subscribed',  array(1))->get();
            $result = Company::select(array('companies.id', 'companies.company_number', 'companies.company_name', 'companies.company_type','companies.primary_phone','companies.email','companies.status','companies.fname','companies.lname','companies.is_deleted'))->where('is_deleted', '!=', 1)->whereIn('status',  array('Active', 'Inactive'))->whereIn('subscribed',  array(1))->get();
        } else {
            $result = Company::select(array('companies.id', 'companies.company_number', 'companies.company_name', 'companies.company_type','companies.primary_phone','companies.email','companies.status','companies.fname','companies.lname','companies.is_deleted'))->where('is_deleted', '=', 1)->whereIn('subscribed',  array(1))->get();
        }	
        if(isset($result) && count($result) > 0) {
            foreach($result as $rKey => $rVal) {
                $id = $result[$rKey]->id;
                $company_status = $result[$rKey]->status;
                $isDeleted = $result[$rKey]->is_deleted;
                
                if($isDeleted == 1) $staus = 'UnApproved';
                else $staus = $result[$rKey]->status;
                $json[] = array(
                    'company_number' => $result[$rKey]->company_number,
                    'company_name' => $result[$rKey]->company_name,
                    'email' => $result[$rKey]->email,
                    'primary_contact' =>$result[$rKey]->lname.', '.$result[$rKey]->fname,
                    'primary_phone' =>$result[$rKey]->primary_phone,
                    'status' => $staus,
                    'actions' => view('companysubscription.searchactions', compact('id','company_status','isDeleted'))->render()
                );
            }            
        }   
      return $json;
    }
    
    public function resetcompanyunapproved() {
        
        $company_id =  Request::input('id');
		if(isset($company_id) && $company_id != '') {
            //User::where('id', $id)->update(['is_deleted' =>0]);
             Company::where('id', $company_id)->update(['is_deleted' => 0]);
             User::where('company_id', $company_id)->update(['is_deleted' =>0]);
		}
		return $this->ajax_construct();
    }
    /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {

        	//echo 'dsfsdfsd';exit;  ajaxCompanySubscribed
            //echo  Request::input('route');        
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            //echo $route; 
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
           //  print_r($result['script']);
        
			            
			$data = $this->$route();
			
           // echo '<br/>  :data: ';
			//print_r($data);
			//die;
			
            if($route== 'approveCompanySubscription') {        	
                return json_encode($data);
            }
            elseif($route== 'approveCompanySubscription' && isset($data["success"]) && $data["success"]==false){

            	//var_dump($this->json_error($data["arr"]));exit;
            	
            	return $this->json_error($data["arr"]);
            }
            	          
            
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            

            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}
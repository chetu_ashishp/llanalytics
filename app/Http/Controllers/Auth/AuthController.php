<?php

namespace App\Http\Controllers\Auth;
//use Request;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\User;
use App\Company;
use App\DataOption;
use App\EmailTemplate;
use Auth;
use Redirect;
use Session;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Validator;
use Hash;
use Mail;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers;
 protected $request;
    /**
     * Create a new authentication controller instance.
     *
     * @param  Guard  $auth
     * @param  Registrar  $registrar
     * @return void
     */
    public function __construct(Guard $auth, Registrar $registrar) {

        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postLogin(Request $request) {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if ($this->auth->attempt($credentials, $request->has('remember'))) {
            if (Auth::user()->email === $request['email']) {
                $user_data = Auth::user();  //      echo $user_data->company_id; die;     
                $companyName = $this->getCompanyname($user_data->company_id);
                parse_str($companyName, $output);
                $companyName = !empty($companyName[0]->company_name) ? $companyName[0]->company_name : '';
               
                Session::put('role_id', $user_data->role_id);
                Session::put('company_id', $user_data->company_id);
                Session::put('company_name', $companyName);
                Session::put('user_id', $user_data->id);
                if($user_data->id != '' && $user_data->company_id != '') {
                    $companyLogin = $this->checkCompanyLogin($user_data->id,$user_data->company_id);
                }
                //var_dump($companyLogin); die;
                // if(Auth::user()->role_id==3)
             /*if($companyLogin && $companyLogin > 0)          
                return redirect("/companyportal");
                else
                return redirect()->intended($this->redirectPath());*/
                if(Auth::user()->role_id==3)
                return redirect("/companyportal");
                else
                return redirect()->intended($this->redirectPath());
            } else {
                Auth::logout();
            }
        }

        return redirect($this->loginPath())
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors([
                            'email' => $this->getFailedLoginMessage(),
        ]);
    }
    
    function getCompanyname($companyID) {
        $result = Company::select(array('companies.company_name'))->where('companies.id','=',$companyID)->get();
        return $result;
    }
    /**
     * Handle a login request to the application.
     *
     * @param  Request  $request
     * @return Response
     */
    public function companyAddSubscription(Request $request) {
      
        $referral_types = DataOption::where('type_id', 3)->orderBy('option_name')->lists('option_name', 'id');
		$companyList = DataOption::where('type_id', 2)->orderBy('option_name')->lists('option_name', 'id');	
       
        $tableData = json_encode(array()); //json_encode($this->show()); 
		return view('auth.companysubscribe', compact('tableData','referral_types','companyList'));
    }
    
    public function checkCompanyLogin($userID,$companyID) {
      
        $result = User::select(array('users.id','users.email','companies.id'))
		->join('companies', 'companies.id', '=', 'users.company_id')->where('users.company_id','!=',1)->where('users.company_id','=',$companyID)->where('users.id','=',$userID)->count();
        return $result;
        
    }
    
    public function companyAddSubscriptions($forminput) {
        
        $input = $forminput;
		parse_str($input, $output);
        $validator = Validator::make($output, [
            'company_name' => 'required|unique:companies',
            'company_type' => 'required',
			'fname' => 'required',
			'lname' => 'required',
			'email' => 'required|email|unique:users|unique:companies',
        ]);
         if ($validator->fails()) {
             
             $data['array'] = array('message' => $validator->messages()->all());
			return array('success'=>false, "errors"=>$data);
             
        } else {
       
         unset($output["_token"]);
         $output["status"] = 'Inactive';
         $output["subscribed"] = 1;
         $companyEntry = Company::create($output);
         $insertedCompanyId = $companyEntry->id;
      
         Company::where('id', $insertedCompanyId)->update(['company_number' => "VY{$insertedCompanyId}-".substr( time(),-4)]);
     
         if($companyEntry && isset($insertedCompanyId) && $insertedCompanyId != '') {              
            
                $password = GUID();   
                $passwordHash = Hash::make($password);         
                $newStaff = array(
                    'name' => !empty($output["company_name"]) ? $output["company_name"] : '',
                    'email' => !empty($output["email"]) ? $output["email"] : '',
                    'password' => $passwordHash,
                    'mobile_phone' => !empty($output["primary_phone"]) ? $output["primary_phone"] : '',
                    'status' => 'Inactive',
                    'role_id' => \Config::get('custom.role_id'), //Standard Administrator to update further
                    'company_id' => (int)$insertedCompanyId,
                    'primary_contact' => 1, //default Primary Contact
                    'first_name' => !empty($output["fname"]) ? $output["fname"] : '',
                    'last_name' => !empty($output["lname"]) ? $output["lname"] : ''
                );

                $staffUser = new User();
                foreach($newStaff as $oKey => $oVal) {

                    $staffUser->$oKey = $oVal;            
                }
               $usersave = $staffUser->save();
           
                $emailTemplateItem = EmailTemplate::select(array('email_templates.id','email_templates.email_template_name','email_templates.email_template_subject', 'email_templates.email_template_content'))->where('email_template_name', '=', 'Company Subscription')->get();
         
                $data = array();
                $data['name'] = !empty($output["fname"]) ? $output["fname"] : '';
                $data['company'] = !empty($output["company_name"]) ? $output["company_name"] : '';
                 $data['email_template_subject'] = !empty($emailTemplateItem[0]['email_template_subject'])? $emailTemplateItem[0]['email_template_subject']: '';
                 $email = 'ashishp@chetu.com';
                 $data['toEmail'] = !empty($output["email"]) ? $output["email"] : 'ashishp@chetu.com';
                 $data['OMSCompanyName'] = \Config::get('custom.OMSSupportPh');
                 $data['OMSSupportPh'] = \Config::get('custom.OMSSupportPh');
                 $data['OMSSupportEmail'] = \Config::get('custom.OMSSupportEmail');
                $templateContent = !empty($emailTemplateItem[0]['email_template_content']) ? $emailTemplateItem[0]['email_template_content']: '';
                if(!empty($templateContent)) {
                    if($template = getEmailFormat($templateContent, $data['name'],$data['company'],$data['OMSCompanyName'],$data['OMSSupportPh'],$data['OMSSupportEmail'])) {
                       
                        Mail::raw($template, function($message) use ($template,$data) {
                            $message->from($data['OMSSupportEmail']);
                            $message->to($data['toEmail']);
                            $message->setBody($template, 'text/html');
                            $message->subject($data['email_template_subject']);
                            $message->sender($data['OMSSupportEmail'], 'OMS Solution');
                        });
                    }
                }
                $emailTemplateadmin = EmailTemplate::select(array('email_templates.id','email_templates.email_template_subject', 'email_templates.email_template_content'))->where('email_template_name', '=', 'Company subscription to oms admin')->get();
              
                if(!empty($emailTemplateadmin)) {
                    $templateContent = !empty($emailTemplateadmin[0]['email_template_content']) ? $emailTemplateadmin[0]['email_template_content']: '';
                    if ($templateAdmin = getEmailFormat($templateContent, $data['name'],$data['company'],$data['OMSCompanyName'],$data['OMSSupportPh'],$data['OMSSupportEmail'])) {
                        $data['email_template_subject'] = !empty($emailTemplateItem[0]['email_template_subject'])? $emailTemplateItem[0]['email_template_subject']: '';
                        $data['toemail'] = 'ashishp@chetu.com';
                        $data['email_template_subject'] = !empty($emailTemplateadmin[0]['email_template_subject'])? $emailTemplateadmin[0]['email_template_subject']: '';
               
                        Mail::raw($templateAdmin, function($message) use ($templateAdmin,$data) {
                            $message->from($data['OMSSupportEmail']);
                            $message->to('ashishp@chetu.com');
                            $message->setBody($templateAdmin, 'text/html');
                            $message->subject($data['email_template_subject']);
                            $message->sender($data['OMSSupportEmail'], 'OMS Solution');
                        });
                    }
                }                
         }
         return $this->ajax_construct();
        } 
    }   
    
    /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax(Request $request) {
               	
            if (!$request['route']) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, $request['route'])) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = $request['route'];
            $result = array();
            $result['target'] = $request['target'];
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
           
			if ($request['typeId']) {
            		$data = $this->$route($request['typeId']);
			}			
			            
			$data = $this->$route($request['form-input']);
			
           if($route== 'companyAddSubscriptions' && isset($data["success"]) && $data["success"]==false){
                return $this->json_error($data["errors"]);
            }
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            $result['html'] = 'html';
            $result['redirect'] = '/auth/login';
            $result['alert'] = array(
                'type'=>'sucess',
                'message'=>'Company subscription completed'
            );
            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }

}

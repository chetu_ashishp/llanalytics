<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use App\CompanyDataOption;
use App\CompanyDataType;
use Route;

class CompanyToolsController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
           $routepath = Route::getCurrentRoute()->getPath();
       if($routepath != 'ajaxCompanytools') {
            $something = new HomeController;
            $check = $something->authenticateUserRolePermissionURL($routepath);           
            if(!$check) {
               abort(404, 'Unauthorized action.');         
            }
      }
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $create = $this->create();
        $tableData = json_encode($this->show());        
        $data_types = $create->data_types;
		return view('companydata.index', compact('create', 'tableData', 'data_types'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            $form = 'Add';
            $data_types = CompanyDataType::orderBy('type_name', 'asc')->lists('type_name', 'id');            
            return view('companydata.form', compact('data_types', 'form'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $input = Request::input('form-input');            
                    
            parse_str($input, $output);
            
            $input_fields = array(
                'type_id',
                'option_name',
            	'option_desc',
            	'option_status',
            	'considered_status'	            		
            );
           
            //var_dump($output);exit;
            
           if (!isset($output['considered_status'])){
           		$output['considered_status'] = "No";
           }      
           
           $item = new CompanyDataOption;
           foreach($output as $oKey => $oVal) {
                if(in_array($oKey, $input_fields)) {
                    $item->$oKey = $oVal;
                }
           }
           $item->save();
           return $this->ajax_construct(false, "resetToolsAddForm(); ");
	}


	
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($type=false)
	{
		    $json = array();
        
		    if($type){
            	$result = CompanyDataOption::select(array('company_data_options.id', 'company_data_options.option_name', 'company_data_options.option_desc', 'company_data_options.option_status' , 'company_data_types.type_name'))
            	->join('company_data_types', 'company_data_options.type_id', '=', 'company_data_types.id')->where('company_data_options.type_id',$type)
            	->get();
		    }
		    else{
		    	$result = CompanyDataOption::select(array('company_data_options.id', 'company_data_options.option_name', 'company_data_options.option_desc', 'company_data_options.option_status' , 'company_data_types.type_name'))
		    	->join('company_data_types', 'company_data_options.type_id', '=', 'company_data_types.id')
		    	->get();
		    }
			
            foreach($result as $rKey => $rVal) {
                $id = $result[$rKey]->id;
                $json[] = array(    'type' => $result[$rKey]->type_name,
                                    'name' => $result[$rKey]->option_name,
                					'desc' => str_limit($result[$rKey]->option_desc, 200),
                					'status' => $result[$rKey]->option_status,
                                    'actions' => view('companydata.actions', compact('id'))->render() );
            }
            
         // echo "<pre>";  var_dump($json);exit;            
            return $json;
	}

	
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function search($type)
	{
		$json = array();
	
			$result = CompanyDataOption::select(array('company_data_options.id', 'company_data_options.option_name', 'company_data_options.option_desc', 'company_data_options.option_status' , 'company_data_types.type_name'))
			->join('company_data_types', 'company_data_options.type_id', '=', 'company_data_types.id')->where('company_data_options.type_id',$type)
			->get();
		
			
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$json[] = array(    'type' => $result[$rKey]->type_name,
					'name' => $result[$rKey]->option_name,
					'desc' => $result[$rKey]->option_desc,
					'status' => $result[$rKey]->option_status,
					'actions' => view('companydata.actions', compact('id'))->render() );
		}
	
		
		  return $this->ajax_construct();
	}
	
	
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
            $form = 'Edit';
            
            $id = Request::input('item');
            
            $item = CompanyDataOption::find($id);
            
            $data_types = CompanyDataType::orderBy('type_name', 'asc')->lists('type_name', 'id');
            
            $html = view('companydata.form', compact('data_types', 'item', 'form'));
            
            return $this->ajax_construct($html);
	}
	
	public function removeCheck() {
		
        $output = Request::all();
        $dataRoute = $output['route'];
        $dataPath =  $output['path'];
        if(isset($dataRoute) && isset($dataPath) && !empty($dataRoute) && !empty($dataPath)) {
            
            $something = new HomeController;
            $check = $something->authenticateUserRolePermission($dataPath,$dataRoute);
            if(!$check) {
                $data['array'] = array('message' => array("You dont have permission to perform this action."));
                return array('success'=>false, "arr"=>$data);
            }
            $id =  Request::input('item'); 		
            $token =  Request::input('_token'); 
            $id =  Request::input('item'); 		
		
		//    dd(Helpers::isKeyUsedAsForeignKey('users', $student->user_id));
            if (!isKeyUsedAsForeignKey('company_data_options', $id) ) {						
                return $this->ajax_construct(false, "confirmRemove({$id}, 'option', 'deleteOption'); ");			
            } else{

                $data['array'] = array('message' => array("The Tool is in use in the system and cannot be deleted."));
                return array('success'=>false, "arr"=>$data);						
            }
        } else {
           $data['array'] = array('message' => array("Route is not defined."));
           return array('success'=>false, "arr"=>$data);
       }	
	}
	
	
	public function deleteOption($id) {
	
		CompanyDataOption::where('id', $id)->delete();
	
		$result = CompanyDataOption::select(array('company_data_options.id', 'company_data_options.option_name', 'company_data_options.option_desc', 'company_data_options.option_status' , 'company_data_types.type_name'))
		->join('company_data_types', 'company_data_options.type_id', '=', 'company_data_types.id')
		->get();
	
			
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$json[] = array(    'type' => $result[$rKey]->type_name,
					'name' => $result[$rKey]->option_name,
					'desc' => str_limit($result[$rKey]->option_desc, 200),
					'status' => $result[$rKey]->option_status,
					'actions' => view('companydata.actions', compact('id'))->render() );
		}
	
		// echo "<pre>";  var_dump($json);exit;
		return $json;
	}
	
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
            $input = Request::input('form-input');
            
            parse_str($input, $output);
            
            $input_fields = array(
                'type_id',
                'option_name',
            	'option_desc',
            	'option_status',   
            	'considered_status'
            );

	 	   if (!isset($output['considered_status'])){
           		$output['considered_status'] = "No";
           }
          
            
            $item = CompanyDataOption::find($output['id']);
            foreach($output as $oKey => $oVal) {
                if(in_array($oKey, $input_fields)) {
                    $item->$oKey = $oVal;
                }
            }
            $item->save();
            return $this->ajax_construct(false, "resetToolsEditForm(); ");
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
        /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {

        	//echo 'dsfsdfsd';exit;        	
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
         
        
			if (Request::has('typeId')) {
            		$data = $this->$route(Request::input("typeId"));
			}
			
			elseif($route== 'deleteOption'){
            	            
            	 $data = $this->$route(Request::input('deleteid'));
            }
			else{				
					$data = $this->$route();
			}
			
			
			
            if($route== 'show' || $route== 'deleteOption') {            	
                return json_encode($data);
            }
            elseif($route== 'removeCheck' && isset($data["success"]) && $data["success"]==false){

            	//var_dump($this->json_error($data["arr"]));exit;
            	
            	return $this->json_error($data["arr"]);
            }
            	          
            
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            

            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use App\DataOption;
use App\Subscription;
use Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use App\User;



class AddSubscriptionController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the subscription.
	 *
	 * @return Response
	 */
	public function index() {
		//$tableData = json_encode($this->show());
        $tableData = json_encode(array()); //json_encode($this->show()); 
		return view('addsubscription.index', compact('tableData'));		
	}
	
	
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($type=false) {
		$json = array();	
		//$result = Subscription::all()->where('subscription_status', '!=', 'Deleted');
        $res = Subscription::select(array('subscriptions.id','subscriptions.subscription_name', 'subscriptions.fee_type', 'subscriptions.cost_per_unit','subscriptions.billing_frequency','subscriptions.subscription_status'))->where('subscription_status', '!=', 'Deleted');
        $result = $res->get();
	
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$json[] = array(    'subscription_name' => $result[$rKey]->subscription_name,
					'fee_type' => $result[$rKey]->fee_type,
					'cost_per_unit' =>$result[$rKey]->cost_per_unit,
					'billing_frequency' => $result[$rKey]->billing_frequency,
					'subscription_status' => $result[$rKey]->subscription_status,					
					'actions' => view('subscription.actions', compact('id'))->render() );
		}			
		return $json;
	}
	
		
	
	/**
	 * Show the form for creating a new subscription.
	 *
	 * @return Response
	 */
	public function create($id = false) {
                
		$form = 'Add';
       // $billingFrequency = DataOption::where('type_id', 5)->where('option_status', '!=', 'Inactive')->orderBy('option_name')->lists('option_name', 'option_name');
		
		return view('subscription.create', compact('form'));
	}

	/**
	 * Store a newly created subscription in table.
	 *
	 * @return Response
	 */
	public function store() {
        
		$addrs = array();
		$input = Request::all();
		$validator = Validator::make($input, Subscription::$base_rules);		 
		unset($input["_token"]);
		
		if ($validator->passes()) {
		
			$input_fields = array(
						
					'subscription_name' ,
					'fee_type' ,
					'cost_per_unit' ,
					'billing_frequency',
					'subscription_status' ,
                    'subscription_users_allowed',
                    'subscription_desc',
                    'created_by'
			);	
			
			$item = new Subscription();	
           // $input["start_date"] = $item->setFormatedDate($input["start_date"]);
           // $input["end_date"] = $item->setFormatedDate($input["end_date"]);
			foreach($input as $oKey => $oVal) {
	
				if(in_array($oKey, $input_fields)) {
					$item->$oKey = $oVal;
				}
			}
			print_r($item); die; 
			$item->save();
			$insertedId = $item->id;	

			
			return Redirect::to('subscriptions');
		} else {
		
			return redirect()->back()->withInput()->withErrors($validator);
		}
	}
	
    
    /**
	 * Show the form for editing the subscription Package.
	 *
	 * @param  int  $item
	 * @return Response
	 */
	public function edit($subscription_id) {
        
		$form = "Edit";	
		
		$user_access = array();
		/*if($item->all_user_accsess!=1){
			$user_access = $item->accsess_users->lists("user_id");
		}*/
		//$temIncludeFields = $item->tplIncludedField->lists('field_name');
		$billingFrequency = DataOption::where('type_id', 5)->where('option_status', '!=', 'Inactive')->orderBy('option_name')->lists('option_name', 'option_name');
		$item = Subscription::where('subscription_status', 'Active')->where('id', $subscription_id)->get();
		return view('subscription.edit', compact('subscription_id','form', 'activeusers','user_access','item','billingFrequency'));
	}
	
	
	
    /**
	 * Update the subscription package.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function update($package_id) {
		//echo '$package_id: '.$package_id;
          //echo '<pre/>';
		$output = Request::all();
      //  print_r($output); die;
        $validation = Validator::make($output, Subscription::$base_rules);   
		 if ($validation->passes() && !empty($output["_token"])) {
			 
					$item = Subscription::find($package_id);
					 $updatePackage = array(
						'subscription_name' => !empty($output["subscription_name"]) ? $output["subscription_name"] : '',
						'fee_type' => !empty($output["fee_type"]) ? $output["fee_type"] : '',
						'cost_per_unit' => !empty($output["cost_per_unit"]) ? $output["cost_per_unit"] : '',
						'billing_frequency' => !empty($output["billing_frequency"]) ? $output["billing_frequency"] : '',
						'subscription_status' => !empty($output["subscription_status"]) ? $output["subscription_status"] : '',
						'subscription_users_allowed' => !empty($output["subscription_users_allowed"]) ? $output["subscription_users_allowed"] : '',
                         'subscription_desc' => !empty($output["subscription_desc"]) ? $output["subscription_desc"] : ''
					 );
                    
					 foreach($updatePackage as $oKey => $oVal) {
						$item->$oKey = $oVal;				
					 }
					 $item->save();
					 $subscriptionID = $item->id;
					 return Redirect::to('subscriptions');
		 } else {
			$data['array'] = array('message' => $validation->messages()->all());
          
			//print_r($validation);
			return redirect()->back()->withInput()->withErrors($validation);
		 }
	}
    
    public function deleteSubscription() {
        $id =  Request::input('id');
		//echo $id; die;
		if(isset($id) && $id != '') {
			Subscription::where('id', $id)->update(['subscription_status' => "Deleted"]);
		}
        return $this->ajax_construct();
    }
    
	public function removeCheck() {
		
		$id =  Request::input('item'); 
        $token =  Request::input('_token'); 
		//$Client = subscriptions::find($id);
		//    dd(Helpers::isKeyUsedAsForeignKey('users', $student->user_id));
        return array('errors' => 'true', 'message' => 'This Option is in use in a system');
		if (!isKeyUsedAsForeignKey('subscriptions', $id) ) {
			
			return $this->ajax_construct(false, "RemoveSubscription({$id}, 'deleteSubscription', '{$token}'); ");	
			
			//return array('errors' => FALSE);
		} else
			return array('errors' => 'true', 'message' => 'This Option is in use in a system');
	}
	

        /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {

               	
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
                 			
			$data = $this->$route();
			//print_r($data);
           // echo 'route: '.$route;
            if($route== 'show' || $route== 'RemoveSubscription') {	
                return json_encode($data);
            }
       
            if($route== 'removeCheck' && isset($data["success"]) && $data["success"]==false) {
				return $this->json_error($data["message"]);
			}
            
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            
           //var_dump($this->json_success($result));exit;
            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('false' => true, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}

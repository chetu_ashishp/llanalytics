<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CompanyNotification;
use Auth;
use Request;
use Session;
use App\CompanySheet;
use App\AccessedSheet;
use App\Permission;
use App\RolePermission;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{ 
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{            
		return view('home');
	}
	
	public function publicPortal()
	{
		return view('publicportal.home');
	}
	
	
	
	
	
	
	// Company portal methods
	
	public function companyPortal()
	{
		$sheets = $this->mostRecentSheets();		
		$tableData = $this->notifications();
		if($sheets){
			$sheets = array_chunk($sheets, 3);
		}
		
		return view('companyportal.home', compact('tableData','sheets'));
	}
	
	
	public function mostRecentSheets()
	{
		$arr = array();
		
	/*	Event::listen('illuminate.query', function($query, $params, $time, $conn)
		{
			dd(array($query, $params, $time, $conn));
		});*/
		
		$sheets = AccessedSheet::select(\DB::raw('company_sheets.*, MAX(accessed_sheets.created_at) as ld'))
		->leftJoin('company_sheets', 'accessed_sheets.sheet_id', '=', 'company_sheets.id')
		->where('accessed_sheets.user_id', Auth::user()->id)
		->orderBy('ld', 'DESC')->groupBy('accessed_sheets.sheet_id')->limit(5)->get();

			
		
		//var_dump($sheets->toArray());
		foreach($sheets as $sheet) {
			$arr[] = array(
					'id' => $sheet->id,
					'name' => $sheet->name,
					'purpose' => str_limit($sheet->purpose, 200),
					//'sheet_template' => empty($sheet->sheet_template) ? null : Template::where('id', $sheet->sheet_template)->pluck('name'),
					'status' => $sheet->status,					
			);			
		}	

		return $arr;
	}
		
	public function notifications()
	{
		$arr = array();
		$res = CompanyNotification::select(array('users.first_name as ufn', 	'users.last_name as uln',  'company_sheets_fields.field_name', 'company_sheets.name', 'clients.last_name as cln', 'clients.first_name as cfn',
				'company_notifications.*', 
				'transactions.mls1', 'transactions.nickname', 'transactions.property_house', 'transactions.property_addr1', 'transactions.property_addr2', 
				'ttt.option_name as transaction_type', 'tst.option_name as transaction_status'
				))-> 
		leftJoin('users', 'company_notifications.sender_id', '=', 'users.id')->
		leftJoin('company_sheets_fields', 'company_notifications.field_id', '=', 'company_sheets_fields.id')->
		leftJoin('company_sheets', 'company_sheets_fields.sheet_id', '=', 'company_sheets.id')->
		
		leftJoin('transactions', 'company_notifications.trans_id', '=', 'transactions.id')->
		leftJoin('clients', 'transactions.client_id', '=', 'clients.id')->
		
		leftJoin('company_data_options as ttt', 'ttt.id', '=', 'transactions.transaction_type')->
		leftJoin('company_data_options as tst', 'tst.id', '=', 'transactions.transaction_status')->
		where('user_id', Auth::user()->id)->orderBy("created_at", "DESC");
		
		
		if( Request::has('filter') && Request::input('filter')!='all' ){
			
			$res->where('company_notifications.type', Request::input('filter'));
		}
		elseif(!Request::has('filter')){
			
			$res->where('company_notifications.type', 'Incompleted');
		}
		
		$result = $res->get();
		
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$type = $result[$rKey]->type;
			
			$transaction = "Transaction Status: ".$result[$rKey]->transaction_status."<br>";
			$transaction .= "Transaction Type: ".$result[$rKey]->transaction_type."<br>";
			$transaction .= "MLS #: ".$result[$rKey]->mls1."<br>";
			$transaction .= "Short Address: ".$result[$rKey]->nickname."<br>";
			$transaction .= "Property House #: ".$result[$rKey]->property_house."<br>";
			$transaction .= "Property Address Line 1: ".$result[$rKey]->property_addr1."<br>";
			$transaction .= "Property Address Line 2: ".$result[$rKey]->property_addr2."<br>";
			
			$arr[] = array('sender' => $result[$rKey]->ufn." ".$result[$rKey]->uln,
					'time' => date('m/d/y h:i:s', strtotime($result[$rKey]->created_at)),
					'message' => str_limit($result[$rKey]->message, 100),
					'sheet' => $result[$rKey]->name,
					'field' => $result[$rKey]->field_name,
					'client' => $result[$rKey]->cfn." ".$result[$rKey]->cln,					
					'transaction' => $transaction,
					'status' => ($result[$rKey]->status=='Read'?'<span class="label label-default">Read</span>':'<span class="label label-danger">Unread</span>'),
					'actions' => view('companyportal.actions', compact('id', 'type'))->render() );
		}		
		return json_encode($arr);
	}
	
	public function view()
	{         
            $id = Request::input('item');            
            $data = CompanyNotification::select(array('users.first_name as ufn', 	'users.last_name as uln',  'company_sheets_fields.field_name', 'company_sheets.name', 'clients.last_name as cln', 'clients.first_name as cfn',
            		'company_notifications.*',
            		'transactions.mls1', 'transactions.nickname', 'transactions.property_house', 'transactions.property_addr1', 'transactions.property_addr2',
            		'ttt.option_name as transaction_type', 'tst.option_name as transaction_status'
            ))->
            leftJoin('users', 'company_notifications.sender_id', '=', 'users.id')->
            leftJoin('company_sheets_fields', 'company_notifications.field_id', '=', 'company_sheets_fields.id')->
            leftJoin('company_sheets', 'company_sheets_fields.sheet_id', '=', 'company_sheets.id')->
            
            leftJoin('transactions', 'company_notifications.trans_id', '=', 'transactions.id')->
            leftJoin('clients', 'transactions.client_id', '=', 'clients.id')->
            
            leftJoin('company_data_options as ttt', 'ttt.id', '=', 'transactions.transaction_type')->
            leftJoin('company_data_options as tst', 'tst.id', '=', 'transactions.transaction_status')->
            where('company_notifications.id', $id)->
            where('user_id', Auth::user()->id)->orderBy("created_at", "DESC")->first();
            
            $html = view('companyportal.alert', compact('data'));            
            return $this->ajax_construct($html);
		
	}
	
	
	public function setReadAlert()
	{
			
		$id =  Request::input("alertid");
		
		CompanyNotification::where('id', $id)->where('user_id', Auth::user()->id)->update(['status' => "Read"]);
		return $this->notifications();
				
	}
	
	public function completeAlert()
	{
			
		$id =  Request::input("alertid");		
		CompanyNotification::where('id', $id)->where('user_id', Auth::user()->id)->update(['type' => "Completed"]);
		return $this->notifications();
	
	}    
   
    public function validateajaxPermission() {
       // echo 'sdsa'; die;
        $data =  Request::all();      
        $sessionData = Session::all();
        //print_r($sessionData); die;
        if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
            $userData = getCurrentUserData($sessionData['user_id']);
            
            if($userData && $userData[0]->role_id != '') {
                
                $user_role_id = !empty($userData[0]->role_id) ? $userData[0]->role_id: '';
             
                $path = $data['path'];
                $route = !empty($data['route']) ? $data['route'] : '';
                $route = $path.'/'.$route;      
              
                $permission_id = Permission::select(array('permission_id'))->where('route', '=', $route)->get();
              
                if(count($permission_id) < 1) { 
                     $data['array'] = array('message' => array(""));
                     return array('success'=>true, "arr"=>$data);
                }
                $permission_id = !empty($permission_id[0]->permission_id) ? $permission_id[0]->permission_id : 0;
                //echo 'user_role_id--'.$permission_id;
                $permission_check = RolePermission::select(array('id'))->where('role_id', '=', $user_role_id)->where('permission_id', '=', $permission_id)->get(); 
                //echo '<pre/>';print_r($permission_check); echo count($permission_check);
                if(isset($permission_check) && count($permission_check) < 1) {
                    $data['array'] = array('message' => array("You dont have permission to perform this action."));
                   return array('success'=>false, "arr"=>$data);

                } else {
                     $data['array'] = array('message' => array(""));
                     return array('success'=>true, "arr"=>$data);
                }
            }
        }
    }
    
	public function authenticateUserRolePermission($routePath, $route) {
        
         $output = Request::all();       
         $sessionData = Session::all();
         
         if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
            $userData = getCurrentUserData($sessionData['user_id']);
            
            if($userData && $userData[0]->role_id != '') {
                
                $user_role_id = !empty($userData[0]->role_id) ? $userData[0]->role_id: '';
                
                       
                $route = $routePath.'/'.$route;

                $permission_id = Permission::select(array('permission_id'))->where('route', '=', $route)->get();
                $permission_id = !empty($permission_id[0]->permission_id) ? $permission_id[0]->permission_id : 0;
                $permission_check = RolePermission::select(array('id'))->where('role_id', '=', $user_role_id)->where('permission_id', '=', $permission_id)->get(); 

                if(isset($permission_check) && count($permission_check) < 1) {
                     $data['array'] = array('message' => array("You are Not Authorised to Access."));
                     return false;

                } else {

                    return true;
                }
            } else {
                    $data['array'] = array('message' => array("You are Not Authorised to Access This."));
                    return false;
            }
         } else {
                $data['array'] = array('message' => array("You are Not Authorised to Access .."));
                return false;
         } 
    }
    
    public function authenticateUserRolePermissionURL($routePath) {
       
        if(Request::input('route') == 'removeCheck') {
            $routePath = 'ajaxDataremove';
        }
         $sessionData = Session::all();
         
           if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
                $userData = getCurrentUserData($sessionData['user_id']);
            
            if($userData && $userData[0]->role_id != '') {
               // $user_role_id = $sessionData['role_id'];
                $user_role_id = !empty($userData[0]->role_id) ? $userData[0]->role_id: '';
                $route = $routePath;
                $permission_id = Permission::select(array('permission_id'))->where('route_url', '=', $route)->get();
                $permission_id = !empty($permission_id[0]->permission_id) ? $permission_id[0]->permission_id : 0;
                
                $permission_check = RolePermission::select(array('id'))->where('role_id', '=', $user_role_id)->where('permission_id', '=', $permission_id)->get(); 
                //echo count($permission_check);
                if(isset($permission_check) && count($permission_check) < 1) {
                     $data['array'] = array('message' => array("You are Not Authorised to Access."));
                     return false;

                } else {

                    return true;
                }
            } else {
                $data['array'] = array('message' => array("You are Not Authorised to Access This."));
                return false;
            }
        } else {
            $data['array'] = array('message' => array("You are Not Authorised to Access.."));
            return false;
        }
    }
	
	public function postAjax() {
	  //echo Request::input('route');
        
        $route_array = array('searchstaff','addColorValueRecord','fields','destroy', 'create_sheet_from', 'createField', 'editField', 'addTransaction', 'editTransaction', 'addStaff', 'editStaff', 'editOmsStaff', 'staff', 'show', 'getBillingsList', 'getSubscribedBillingsDetails', 'create', 'addsubscription', 'editcompany', 'removeCheck', 'templateview', 'viewdetails', 'templatecreate', 'edittemplate', 'deletetemplate', 'sheetview', 'sheetcreate', 'sheetaccess', 'sheeteditdetails', 'editsheet', 'removesheet', 'remove', 'createtemplatesheet', 'sheetfields', 'addoption', 'edit', 'reportsview', 'subscriptionscreate', 'subscriptionsedit', 'approveCompanySubscription', 'emailtemplatedit', 'rolepermissionassign', 'createrole', 'editroles', 'dataview', 'dataview', 'viewinvoice', 'addPaymentAdjustment');
        if(in_array(Request::input('route'), $route_array)) {
            $route = 'validateajaxPermission';
        } else {
       
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }
            $route = Request::input('route');
        }
		
		
		$result = array();
		$result['target'] = Request::input('target');
		$result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";	
		
		 $data = $this->$route();		
        
        if($route== 'validateajaxPermission' && isset($data["success"]) && $data["success"]==false) {
               
           return $this->json_error($data["arr"]);
        }       
		foreach ($data as $dKey => $dVal) {
			$result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
		}	
		// var_dump($this->json_success($result));exit;
		return $this->json_success($result);
	}
    //}
	/**
	 * JSON Success Handler
	 *
	 * @return JSON Response
	 */
	public function json_success($data = false) {
		return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
	}
	
	/**
	 * JSON Error Handler
	 *
	 * @return JSON Response
	 */
	public function json_error($data = false) {
		return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
	}
	
	public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
		$result = array();
	
		if ($html) {
			$result['html'] = $html->render();
		}
		if ($script) {
			$result['script'] = $script;
		}
		if ($append) {
			$result['append'] = $append;
		}
		if ($alert) {
			$result['alert'] = $alert;
		}
		return $result;
	}
	
	
}

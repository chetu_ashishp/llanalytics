<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Transaction;
use Illuminate\Support\Facades\Validator;
use Request;
use Symfony\Component\HttpFoundation\Response;
use App\Client;
use App\Company;
use App\Custom;
use App\Staff;
use App\User;
use App\Role;
use Event;
use Redirect;
use Hash;
use App\EmailTemplate;
use Mail;

class StaffsController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$tableData = json_encode(array()); //json_encode($this->show()); 
		return view('staff.index', compact('tableData'));
	}
		
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show()
	{
		$company_id = Request::input('item');		
		
		$arr = array();	
		
		$result = Staff::staffLists($company_id);		
		
		if($result && count($result) > 0) {
			foreach($result as $rKey => $rVal) {
				
				$id = $result[$rKey]->id;
				$arr[] = array(
						   
					'last_name' => $result[$rKey]->last_name,
					'first_name' => $result[$rKey]->first_name,						
					'title' => $result[$rKey]->name,
					'email' => $result[$rKey]->email,
					'primary_phone' =>  !empty($result[$rKey]->mobile_phone) ? $result[$rKey]->mobile_phone : '',
					'role_id' => !empty($result[$rKey]->role_name) ? $result[$rKey]->role_name : '',
					'primary_contact' => !empty($result[$rKey]->primary_contact) ? 'Yes' : 'No',
					'status' => $result[$rKey]->status,
					'actions' => view('staff.actions', compact('id', 'company_id'))->render()				 
					
				);
			}
		}
		
		$tableData = json_encode($arr);
		
		$table =  view('staff.index', compact('tableData', 'company_id'));
		return $this->ajax_construct($table, "initBootstrapTable(".$tableData."); ");
		
	}

	/**
	 * Show the form for adding the staff.
	 *
	 * @param  int  company_id
	 * @return Response
	 */

	function addStaff() {
        
		$output = Request::all();
        $dataRoute = $output['route'];
        $dataPath =  $output['path'];
        if(isset($dataRoute) && isset($dataPath) && !empty($dataRoute) && !empty($dataPath)) {
            
            $something = new HomeController;
            $check = $something->authenticateUserRolePermission($dataPath,$dataRoute);
            if(!$check) {
                $data['array'] = array('message' => array("You dont have permission to perform this action."));
                return array('success'=>false, "arr"=>$data);
            }          
            $company_id = Request::input("company_id"); 
            $company = Company::find($company_id);
            $form = "Add";
            $roles = Role::orderBy('role_name')->lists('role_name', 'id');
            $html = view('staff.create', compact('company_id', 'company','form','roles') );
            return $this->ajax_construct($html);
        
        } else {
            $data['array'] = array('message' => array("Route is not defined."));
            return array('success'=>false, "arr"=>$data);
        }
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeStaff() {
		
		$addrs = array();
		$input = Request::input("form-input");		
		parse_str($input, $output);		
		$validator = Validator::make($output, Staff::$rules);
		unset($output["_token"]);
		
		if ($validator->passes() && !empty($output["company_id"])) {
			
                    $password = GUID();
					$passwordHash = Hash::make($password);
					$newStaff = array(
						'last_name' => !empty($output["lname"]) ? $output["lname"] : NULL,
						'first_name' => !empty($output["fname"]) ? $output["fname"] : NULL,
						'password' => $passwordHash,
						'status' => !empty($output["status"]) ? $output["status"] : NULL,
						'role_id' => !empty($output["role_id"]) ? $output["role_id"] : '',
						'mobile_phone' => !empty($output["primary_phone"]) ? $output["primary_phone"] : '',
						'alternate_phone' => !empty($output["alternate_phone"]) ? $output["alternate_phone"] : '',
						'name' => !empty($output["title"]) ? $output["title"] : NULL,
						'email' => !empty($output["email"]) ? $output["email"] : NULL,
						'primary_contact' => !empty($output["primary_contact"]) ? $output["primary_contact"] : 0,// Primary staff member 1 or 0
						'company_id' => !empty($output["company_id"]) ? $output["company_id"] : NULL
					 );
				$staffUser = new User();
				if(isset($newStaff) && count($newStaff) > 0) {
					foreach($newStaff as $oKey => $oVal) {

						$staffUser->$oKey = $oVal;            
					}
					$staffUser->save();
					$userId = $staffUser->id;
					
					if(!empty($userId)) {
						$staffData = array(

							'user_id' => !empty($userId) ? $userId : NULL,
							'company_id' => !empty($output["company_id"]) ? $output["company_id"] : NULL,
							'linkedin_url' => !empty($output["linkedin_url"]) ? $output["linkedin_url"] : '',
							'facebook_url' => !empty($output["facebook_url"]) ? $output["facebook_url"] : '',
							'twitter_url' => !empty($output["twitter_url"]) ? $output["twitter_url"] : '',
							'googleplus_url' => !empty($output["googleplus_url"]) ? $output["googleplus_url"] : ''
						);

						$staff = new Staff();
						foreach($staffData as $oKey => $oVal) {
							$staff->$oKey = $oVal;		
						}

						$itemuser = $staff->save();
                        
                        if(!empty($itemuser) && !empty($userId)) {
							//Mail functionality
							
                            $data = array();
                            $data['name'] = !empty($output["fname"]) ? $output["fname"] : '';
                            $data['company'] = !empty($output["company_name"]) ? $output["company_name"] : '';                    
                            $data['username'] = !empty($output["email"]) ? $output["email"] : '';
                            $data['password'] = !empty($password) ? $password : '';
						
                            $data['toEmail'] = !empty($output["email"]) ? $output["email"] : 'ashishp@chetu.com';
                            $data['name'] = !empty($output["fname"]) ? $output["fname"] : '';
                            $data['OMSCompanyName'] = \Config::get('custom.OMSSupportPh');
                            $data['OMSSupportPh'] = \Config::get('custom.OMSSupportPh');
                            $data['OMSSupportEmail'] = \Config::get('custom.OMSSupportEmail');
                            $data['OMSCompanyLinkUrl'] = \Config::get('custom.OMSCompanyLinkUrl');
                            $emailTemplateItem = EmailTemplate::select(array('email_templates.id','email_templates.email_template_name','email_templates.email_template_subject', 'email_templates.email_template_content'))->where('email_template_name', '=', 'Company Staff Creation to Staff')->get();
                            $data['email_template_subject'] = !empty($emailTemplateItem[0]['email_template_subject'])? $emailTemplateItem[0]['email_template_subject']: '';
                            $templateContent = !empty($emailTemplateItem[0]['email_template_content']) ? $emailTemplateItem[0]['email_template_content']: '';
                            if(!empty($templateContent)) {
                                if($template = getEmailFormat($templateContent, $data['name'],$data['company'],$data['OMSCompanyName'],$data['OMSSupportPh'],$data['OMSSupportEmail'],$data['OMSCompanyLinkUrl'],$data['username'],$data['password'])) {

                                    Mail::raw($template, function($message) use ($template,$data) {
                                        $message->from($data['OMSSupportEmail']);
                                        $message->to($data['toEmail']);
                                        $message->setBody($template, 'text/html');
                                        $message->subject($data['email_template_subject']);
                                        $message->sender($data['OMSSupportEmail'], 'OMS Solution');
                                    });
                                }
                            }
						}
					}
				}
				return $this->ajax_construct();
			
		} else {
			$data['array'] = array('message' => $validator->messages()->all());
			return array('success'=>false, "errors"=>$data);
		}
	}
		
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $client
	 * @return Response
	 */
	public function editStaff() {
		
        $output = Request::all();
        $dataRoute = $output['route'];
        $dataPath =  $output['path'];
        if(isset($dataRoute) && isset($dataPath) && !empty($dataRoute) && !empty($dataPath)) {
            
            $something = new HomeController;
            $check = $something->authenticateUserRolePermission($dataPath,$dataRoute);
            if(!$check) {
                $data['array'] = array('message' => array("You dont have permission to perform this action."));
                return array('success'=>false, "arr"=>$data);
            } 
            $item = Staff::find(Request::input("user_id"));	
            $user_id = Request::input("user_id");
            $company_id = Request::input("company_id");
            $relatedtransid = "";

            $form = "Edit";
            $staffData = Staff::UserstaffData($company_id,$user_id);
            $roles = Role::orderBy('role_name')->lists('role_name', 'id');
            $html = view('staff.edit', compact('staffData','user_id', 'company_id','form','roles') );
            return $this->ajax_construct($html);
        } else {
            $data['array'] = array('message' => array("Route is not defined."));
            return array('success'=>false, "arr"=>$data);
        }
	}
	
	/**
	 * Show the form for update staff the specified resource.
	 *
	 * @param  int  $input
	 * @return Response
	 */
	public function updateStaff() {
		
		$input = Request::input("form-input");		
		parse_str($input, $output);
		$user_id = $output["id"];
		$company_id = $output["company_id"];
		$validator = Validator::make($output, Staff::$rules);
		 
		unset($output["_token"]);
		
		 if(isset($output["primary_contact"]) && $output["primary_contact"] == 1) {
			User::where('company_id', $output["company_id"])
					->where('primary_contact', 1)
					->update(['primary_contact' => 0]);
		 }
			$item = User::find($user_id);
					 $newStaff = array(
						'last_name' => !empty($output["lname"]) ? $output["lname"] : NULL,
						'first_name' => !empty($output["fname"]) ? $output["fname"] : NULL,
						'status' => !empty($output["status"]) ? $output["status"] : NULL,
						'role_id' => !empty($output["role_id"]) ? $output["role_id"] : NULL,
						'mobile_phone' => !empty($output["primary_phone"]) ? $output["primary_phone"] : '',
						'alternate_phone' => !empty($output["alternate_phone"]) ? $output["alternate_phone"] : '',
						'name' => !empty($output["title"]) ? $output["title"] : NULL,
						'email' => !empty($output["email"]) ? $output["email"] : NULL,
						'primary_contact' => !empty($output["primary_contact"]) ? $output["primary_contact"] : 0,// Primary staff member 1 or 0
						'company_id' => !empty($output["company_id"]) ? $output["company_id"] : NULL
					 );					
				if(isset($newStaff) && count($newStaff) > 0 && count($item) > 0) {
					foreach($newStaff as $oKey => $oVal) {
						$item->$oKey = $oVal;				
					}
					$item->save();
					$userId = $item->id;					
					
					if(!empty($userId)) {
						
						$staffItem = Staff::where('user_id' , '=', $user_id)->first();
						$staffData = array(
							'user_id' => !empty($userId) ? $userId : NULL,
							'company_id' => !empty($output["company_id"]) ? $output["company_id"] : NULL,
							'linkedin_url' => !empty($output["linkedin_url"]) ? $output["linkedin_url"] : '',
							'facebook_url' => !empty($output["facebook_url"]) ? $output["facebook_url"] : '',
							'twitter_url' => !empty($output["twitter_url"]) ? $output["twitter_url"] : '',
							'googleplus_url' => !empty($output["googleplus_url"]) ? $output["googleplus_url"] : ''
						);
						
						foreach($staffData as $oKey => $oVal) {
							$staffItem->$oKey = $oVal;		
						}

						$staffuser = $staffItem->save();
					}
				}
				return $this->ajax_construct();		
	
	}

	
	public function deleteStaff() {
        
		$id =  Request::input('id');
		if(isset($id) && $id != '') {
			//User::where('id', $id)->update(['status' => "Deleted"]);
            User::where('id', $id)->update(['is_deleted' =>1]);
		}
		return $this->ajax_construct();
	}
		
    public function searchstaff() {
      
        $input = Request::input('params');     
		parse_str($input, $output);     
        $company_id = $output['data-company_id'];
        $status = $output['staffstatus'];
		$json = array();
		
		$result = Staff::getstaffFilter($company_id,$status);
		if($result && count($result) > 0) {
			foreach($result as $rKey => $rVal) {
				
				$id = $result[$rKey]->id;
                $is_deleted = $result[$rKey]->is_deleted;
                $status = $result[$rKey]->status;
                if($is_deleted == 1) $status = 'Deleted';
               
				$json[] = array(						   
					'last_name' => $result[$rKey]->last_name,
					'first_name' => $result[$rKey]->first_name,						
					'title' => $result[$rKey]->name,
					'email' => $result[$rKey]->email,
					'primary_phone' =>  !empty($result[$rKey]->mobile_phone) ? $result[$rKey]->mobile_phone : '',
					'role_id' => !empty($result[$rKey]->role_name) ? $result[$rKey]->role_name : '',
					'primary_contact' => !empty($result[$rKey]->primary_contact) ? 'Yes' : 'No',
					'status' => $status,
					'actions' => view('staff.searchactions', compact('id', 'company_id','is_deleted'))->render()					
				);
			}
		}	
		return json_encode($json);
    }
	
	public function removeCheck() {
        
		$output = Request::all();
        $dataRoute = $output['route'];
        $dataPath =  $output['path'];
        $userId = $output['id'];
        
        if(isset($dataRoute) && isset($dataPath) && !empty($dataRoute) && !empty($dataPath)) {
            
            $something = new HomeController;
            $check = $something->authenticateUserRolePermission($dataPath,$dataRoute);
            if(!$check) {
                $data['array'] = array('message' => array("You dont have permission to perform this action."));
                return array('success'=>false, "arr"=>$data);
            }            
            $id =  Request::input('item');		
            $token =  Request::input('_token');
            
            if(checkPrimaryContact($userId) == true ) {
                $data['array'] = array('message' => array("This Staff is Primary Staff and cannot be Deleted."));
                return array('success'=>false, "arr"=>$data);
            }
            return $this->ajax_construct(false, "RemoveStaff({$id}, 'deleteStaff', '{$token}'); ");
            /*else if (isKeyUsedAsForeignKey('users', $id)==false ) {
                return $this->ajax_construct(false, "RemoveStaff({$id}, 'deleteStaff', '{$token}'); ");			
            }          
            else {
                $data['array'] = array('message' => array("This Staff is in use and can't be deleted."));
                return array('success'=>false, "arr"=>$data);
            }*/
		} else {
            $data['array'] = array('message' => array("Route is not defined."));
            return array('success'=>false, "arr"=>$data);
        }
	}	

	public function resetstaff() {
        
		$id =  Request::input('id');
		if(isset($id) && $id != '') {			
            User::where('id', $id)->update(['is_deleted' =>0]);
		}
		return $this->ajax_construct();
	}	
        /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {

          // echo $route = Request::input('route'); die;
          
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route'); 
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
        
            if (Request::has('typeId')) {
            	$data = $this->$route(Request::input("typeId"));
            }
            			
			elseif($route== 'deleteStaff'){
					
				$data = $this->$route(Request::input('deleteid'));
			}
			else{
				$data = $this->$route();
			}
			if(($route== 'addStaff' || $route == 'editStaff') && isset($data["success"]) && $data["success"]==false) {
                return $this->json_error($data["arr"]);
            }		
			if( ($route== 'addStaff' || $route== 'storeStaff' || $route== 'store' || $route=='updateStaff') && isset($data["success"]) && $data["success"]==false){
				return $this->json_error($data["errors"]);
			}				
					
			if($route== 'show') {
					$result['target'] ="#staffs";
			}
			
			
			if($route== 'removeCheck' && isset($data["success"]) && $data["success"]==false){
				return $this->json_error($data["arr"]);
			}
			
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}
<?php namespace App\Http\Controllers;

use App\CompanyNotification;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;

use App\User;
use App\CompanySheetField;
use App\CompanySheetFieldsAccess;
use App\CompanySheet;
use App\CompanySheetFieldBgColors;
use App\Custom;
use App\DataOption;
use App\DataType;
use App\CompanySheetFieldsTransactionTypeDefaults;
use Auth;
use App\Role;


class CompanySheetFieldsController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sheet_id = Request::input('sheet');
		print_r($sheet_id); die;
		$result = CompanySheet::find($sheet_id)->fields;
		//dd($result);		
		$arr = array();
        $fields_crosswalk = array_merge(CompanySheet::$transaction_fields, CompanySheet::$client_fields);
                
                
		foreach($result as $rKey => $rVal) {
		
			$id = $result[$rKey]->id;
            if(!array_key_exists($result[$rKey]->field_name, $fields_crosswalk)) {
            	
				$arr[] = array(
						'id'	=>   $result[$rKey]->id,
						'field_name' => $result[$rKey]->field_name,
						'type' => $result[$rKey]->type,
						'tooltip' => $result[$rKey]->tooltip,
						'status' => $result[$rKey]->status,
						'priority_order' => $result[$rKey]->priority_order,					
						'actions' => view('sheetfields.actions', compact('id', 'sheet_id'))->render() );
            }					
		}                
		return json_encode($arr);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function createField()
	{
		$sheet_id = Request::input("sheet_id");
		$company_id = 1;
		$activeusers = User::where('status', 'Active')->where('company_id', $company_id)->get();
		$parent_fields = CompanySheetField::where('parent_field', NULL)->where('sheet_id', $sheet_id)->orderBy('field_name')->get()->lists('field_name', 'id');
		$add_color_modal = view('sheetfields.includes.add_color_modal');

		$data = new \stdClass();
		$data->transaction_types = array_filter(companyOptionsByType('Transaction Type', 1));
		$data->color_options = DataOption::where('type_id', DataType::where('type_name', 'Color Option')->pluck('id'))->where('option_status', 'Active')->lists('option_name');

		$html = view('sheetfields.create', compact('data', 'sheet_id', 'activeusers', 'parent_fields', 'add_color_modal') );
		return $this->ajax_construct($html);
	}

	
	/**
	 * Add address record to the Client
	 *
	 * @return array
	 */
	public function addColorValueRecord(){
		
		$number = substr(time(), -4).rand(0, 100) ;
		$html = view('sheetfields.includes.field_colors', compact('number'));		
		$result = $this->ajax_construct($html, false, 'append');		
		return $result;
	}
	
	
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeField()
	{
		$addrs = array();
		$input = Request::input("form-input");
		
		parse_str($input, $output);
		//var_dump($output);exit;
		$validator = Validator::make($output, CompanySheetField::$rules);
		/*Event::listen('illuminate.query', function($query, $params, $time, $conn)
		 {
				dd(array($query, $params, $time, $conn));
				});*/
	
	
		unset($output["_token"]);
		if ($validator->passes()) { 
						
			$input_fields = array(			
					'field_name' ,
					'type' ,
					'select_options' ,
					'tooltip',
					'priority_order' ,
					'default_bg_color' ,
					'status',
					'sheet_id',
					'parent_field',
					'all_user_accsess'
			);
			
			if($output['parent_field']==0){
				unset($output['parent_field']);
			}
			
			$item = new CompanySheetField();	
			foreach($output as $oKey => $oVal) {
			
				if(in_array($oKey, $input_fields)) {
						$item->$oKey = $oVal;
				}
			}
			 
			$item->save();
			$insertedId = $item->id;

			$transaction_types = array_filter(companyOptionsByType('Transaction Type', 1));

			if (!empty($transaction_types) && !empty($item->id)) {
				$color_options = DataOption::where('type_id', DataType::where('type_name', 'Color Option')->pluck('id'))->where('option_status', 'Active')->lists('id', 'option_name');
				foreach($color_options as $cKey => $cVal) {
					$color_options[strtoupper($cKey)] = $color_options[strtolower($cKey)] = $cVal;
				}

				foreach ($transaction_types as $tKey => $tVal) {
					if (!empty($output['transaction_type_default_value'][$tKey]) || !empty($output['transaction_type_default_bg_color'][$tKey])) {
						$default = new CompanySheetFieldsTransactionTypeDefaults;
						$default->field_id = $item->id;
						$default->transaction_type = $tKey;
						$default->value = !empty($output['transaction_type_default_value'][$tKey]) ? $output['transaction_type_default_value'][$tKey] : null;
						$default->bg_color = !empty($color_options[$output['transaction_type_default_bg_color'][$tKey]]) ? $color_options[$output['transaction_type_default_bg_color'][$tKey]] : null;
						$default->save();
					}
				}
			}

			if(!isset($output["all_user_accsess"]) && isset($output["users"]) && !empty($output["users"])){
					
				foreach($output["users"] as $user) {
					$uObj = new CompanySheetFieldsAccess();
					$uObj->user_id =  $user;
					$uObj->field_id = $insertedId;
					$uObj->save();
				}
			}
			
			if(isset($output["colors"]) && !empty($output["colors"])){
				foreach($output["colors"] as $color) {
					$uObj = new CompanySheetFieldBgColors();
					$uObj->color=  $color["color"];
					$uObj->value =  $color["value"];
					$uObj->field_id = $insertedId;
					$uObj->save();
				}
			}
			
			return $this->ajax_construct();
		}	
		else{
			
			$data['array'] = array('message' => $validator->messages()->all());			
			return array('success'=>false, "errors"=>$data);
		}
	}

	
	function ReOrder(){
		
		$output = Request::input("table");
		//parse_str($input, $output);
		foreach($output as $curent_order=>$fields){
			$item = CompanySheetField::find($fields["id"]);
			$item->priority_order = $curent_order+1;			
			$item->save();
		}
		
		return $this->ajax_construct();
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editField()
	{
		$item = CompanySheetField::find(Request::input("id"));		
		$sheet_id = Request::input("sheet_id");		
		$company_id = 1;
		$activeusers = User::where('status', 'Active')->where('company_id', $company_id)->get();
		$parent_fields = CompanySheetField::where('parent_field', NULL)->where('sheet_id', $sheet_id)->orderBy('field_name')->get()->lists('field_name', 'id');

                $fields_crosswalk = array_merge(CompanySheet::$transaction_fields, CompanySheet::$client_fields);
                foreach($parent_fields as $pKey => $pVal) {
                        if(array_key_exists($pVal, $fields_crosswalk)) {
                            unset($parent_fields[$pKey]);
                        }
                }
		$user_access=array();
		if($item->all_user_accsess!=1){
			$user_access = $item->accsess_users->lists("user_id");
		}
		
		$color_array = $item->colors_values->toArray();

		$data = new \stdClass();
		$data->transaction_types = array_filter(companyOptionsByType('Transaction Type', 1));
		$data->color_options = DataOption::where('type_id', DataType::where('type_name', 'Color Option')->pluck('id'))->where('option_status', 'Active')->lists('option_name');

		$data->transaction_type_defaults = array();
		$data->sheet_type_defaults = CompanySheetFieldsTransactionTypeDefaults::where('field_id', $item->id)->get();

		foreach($data->sheet_type_defaults as $ditem) {
			$data->transaction_type_defaults[$ditem->transaction_type] = array(
				'value'         => $ditem->value,
				'bg_color'      => DataOption::where('id', $ditem->bg_color)->pluck('option_name'),
			);
		}

		$html = view('sheetfields.edit', compact('data', 'item', "sheet_id", "activeusers", "parent_fields", "user_access", 'color_array') );
		return $this->ajax_construct($html);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateField()	
	{
		$input = Request::input("form-input");
		parse_str($input, $output);
		
		$id = $output["id"];
		unset($output["id"]);
		
		
		$validator = Validator::make($output, CompanySheetField::$rules);
		$input_fields = array(
					
				'field_name' ,
				'type' ,
				'select_options' ,
				'tooltip',
				'priority_order' ,
				'default_bg_color' ,
				'status',
				'sheet_id',
				'parent_field',
				'all_user_accsess'
		);
		unset($output["_token"]);
		
		
		if ($validator->passes()) {

			if($output['parent_field']==0){
				$output['parent_field'] = NULL;
			}
			if(!isset($output["all_user_accsess"]) )
				$output["all_user_accsess"] = NULL;
						
			$item = CompanySheetField::find($id);						
			foreach($output as $oKey => $oVal) {
				if(in_array($oKey, $input_fields)) {
					$item->$oKey = $oVal;
				}	
			}
		
			$item->save();

			$transaction_types = array_filter(companyOptionsByType('Transaction Type', 1));
			CompanySheetFieldsTransactionTypeDefaults::where('field_id', $item->id)->delete();

			if (!empty($transaction_types) && !empty($item->id)) {
				$color_options = DataOption::where('type_id', DataType::where('type_name', 'Color Option')->pluck('id'))->where('option_status', 'Active')->lists('id', 'option_name');
				foreach($color_options as $cKey => $cVal) {
					$color_options[strtoupper($cKey)] = $color_options[strtolower($cKey)] = $cVal;
				}

				foreach ($transaction_types as $tKey => $tVal) {
					if (!empty($output['transaction_type_default_value'][$tKey]) || !empty($output['transaction_type_default_bg_color'][$tKey])) {
						$default = new CompanySheetFieldsTransactionTypeDefaults;
						$default->field_id = $item->id;
						$default->transaction_type = $tKey;
						$default->value = !empty($output['transaction_type_default_value'][$tKey]) ? $output['transaction_type_default_value'][$tKey] : null;
						$default->bg_color = !empty($color_options[$output['transaction_type_default_bg_color'][$tKey]]) ? $color_options[$output['transaction_type_default_bg_color'][$tKey]] : null;
						$default->save();
					}
				}
			}







			CompanySheetFieldsAccess::where('field_id', $item->id)->delete();
			// save access for the Field
			if(!$output["all_user_accsess"] && isset($output["users"]) && !empty($output["users"])){
					
				foreach($output["users"] as $user) {
					$uObj = new CompanySheetFieldsAccess();
					$uObj->user_id =  $user;
					$uObj->field_id = $item->id;
					$uObj->save();
				}
			}
			
			// save color/value for the Field
			CompanySheetFieldBgColors::where('field_id', $item->id)->delete();
			if(isset($output["colors"]) && !empty($output["colors"])){
				foreach($output["colors"] as $color) {
					$uObj = new CompanySheetFieldBgColors();
					$uObj->color=  $color["color"];
					$uObj->value =  $color["value"];
					$uObj->field_id = $item->id;
					$uObj->save();
				}
			}
			
			
			return $this->ajax_construct();
		}
		else{
			$data['array'] = array('message' => $validator->messages()->all());
			return array('success'=>false, "errors"=>$data);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteField()
	{	
		$id =  Request::input('id');

		$parent1 = CompanySheetField::where('parent_field', $id)->exists();
		$parent2 = Custom::where('field_id', $id)->exists();




		if(Role::isAllow(Auth::user()->role_id,'sheetfields/{sheetfields}','DELETE')) {
			CompanySheetField::where('parent_field', $id)->update(array('parent_field' => null));

			Custom::where('field_id', $id)->delete();
			CompanySheetFieldsTransactionTypeDefaults::where('field_id', $id)->delete();
			CompanyNotification::where('field_id', $id)->delete();
			CompanySheetField::where('id', $id)->delete();
			$script = "$('".'[data-id="'.$id.'"][data-route="deleteField"]'."').closest('tr').remove();";

			return $this->ajax_construct(false, $script);
		} else if (!$parent1 && !$parent2) {
			CompanySheetField::where('id', Request::input("id"))->delete();
			return $this->ajax_construct();
				
		} else{
				
			$data['array'] = array('message' => array("The Field is in use in the system and cannot be deleted."));
			return array('success'=>false, "errors"=>$data);
		}
		
		
		
		
	}
	
		
	/**
	 * Ajax handler
	 *
	 * @return JSON Response
	 */
	public function postAjax() {
	
		 
		 
		if (!Request::has('route')) {
			return $this->json_error(array('error' => 'Missing Parameters'));
		} elseif (!method_exists($this, Request::input('route'))) {
			return $this->json_error(array('error' => 'Invalid Parameters'));
		}
	
		
		$route = Request::input('route');
		$result = array();
		$result['target'] = Request::input('target');
		$result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
	
		$data = $this->$route();
	
		if($route== 'index') {
			return $data;
		}
			
		if( ($route== 'storeField' || $route=='updateField' || $route=='deleteField') && isset($data["success"]) && $data["success"]==false){
			return $this->json_error($data["errors"]);
		}
		
		foreach ($data as $dKey => $dVal) {
			$result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
		}	
		
		
		//var_dump($this->json_success($result));exit;
		return $this->json_success($result);
	}
	
	/**
	 * JSON Success Handler
	 *
	 * @return JSON Response
	 */
	public function json_success($data = false) {
		return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
	}
			
	/**
	 * JSON Error Handler
	 *
	 * @return JSON Response
	 */
	public function json_error($data = false) {
		return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
	}
	
	public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
		$result = array();
	
		if ($html) {
			$result['html'] = $html->render();
		}
		if ($script) {
			$result['script'] = $script;
		}
		if ($append) {
			$result['append'] = $append;
		}
		if ($alert) {
			$result['alert'] = $alert;
		}
		return $result;
	}
}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use App\RolePermission;
use Route;
use App\RolesMeta;
use App\RouteLabel;
use Validator;
use Input;
use Redirect;
use Request;

class RolesController extends Controller {

   // protected $layout = 'master';
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Request $request) {
       
       $routepath = Route::getCurrentRoute()->getPath();
       if($routepath != 'ajaxroles' && $routepath != 'roles/storePermission' && $routepath != 'roles/{roles}') {
            $something = new HomeController;
            $check = $something->authenticateUserRolePermissionURL($routepath);           
            if(!$check) {
               abort(404, 'Unauthorized action.');         
            }
       }
       $this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
    	    	
    	$result = Role::all();
    	$roles = array();
    	foreach($result as $rKey => $rVal) {
    		$id = $result[$rKey]->id;
    		$roles[] = array('name' => $result[$rKey]->role_name,
    				'desc' => $result[$rKey]->role_description,
                    'permission_level' => $result[$rKey]->permission_level,
    				'actions' => view('roles.actions', compact('id'))->render() );
    	}
    	
    	$tableData = json_encode($roles);
    	return view('roles.index', compact('tableData'));    	
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
    	$routes = Route::getRoutes();
    	$role = Role::find($id);
    	$route_array = array();
    	foreach ($routes as $route) {
    		$routes_array2 = array();
    		$uri = $route->uri();
    		$methods = $route->methods();
    		$methods = $methods[0];
    		$value_sql = RolesMeta::where('role_id', '=', $id)->where('route', '=', $uri)->where('method', '=', $methods)->where('permission', '=', 1)->first();
    		$label = RouteLabel::where('route', '=', $uri)->where('method', '=', $methods)->first();
    		if ($label == NULL) {
    			$routes_array2['label'] = false;
    			$routes_array2['description'] = false;
    			$routes_array2['uri'] = $uri;
    			$routes_array2['method'] = $methods;
    			$routes_array2['value'] = $value_sql == NULL ? FALSE : TRUE;
    			$routes_array2['read_only'] = '';
    			if ($methods == 'GET' and $uri == 'users/dashboard') {
    				$routes_array2['read_only'] = array('disabled' => 'disabled');
    				$routes_array2['value'] = TRUE;
    			}
    			$route_array[] = $routes_array2;
    		} elseif ($label->public != 1) {
    			$routes_array2['label'] = $label->label;
    			$routes_array2['description'] = $label->description;
    			$routes_array2['uri'] = $uri;
    			$routes_array2['method'] = $methods;
    			$routes_array2['value'] = $value_sql == NULL ? FALSE : TRUE;
    			$routes_array2['read_only'] = '';
    			if ($methods == 'GET' and $uri == 'users/dashboard') {
    				$routes_array2['read_only'] = array('disabled' => 'disabled');
    				$routes_array2['value'] = TRUE;
    			}
    			$route_array[] = $routes_array2;
    		}
    	}
    	
    	return view('roles.edit', compact('route_array', 'role'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
    	$role = Role::find($id);  	
    	$rules = array('role_name' => 'required',
            'role_description' => 'required',
            'permission_level' =>  'required|integer',
            //'permission_level' =>  'required|integer|unique:roles,permission_level,'.$id
        );
    	$validator = Validator::make(Input::all(), $rules);
    	if ($validator->passes()) {
    		$role->role_name = Input::get('role_name');
    		$role->role_description = Input::get('role_description');
            $role->permission_level = Input::get('permission_level'); 
    		$role->save();
    		/*RolesMeta::where('role_id', '=', $id)->update(array('permission' => '0'));
    		foreach (Input::get('routes') as $value) {
    			$value_array = explode(' ', $value);
    			$route = $value_array[1];
    			$method = $value_array[0];
    			$role_meta = RolesMeta::where('route', '=', $route)->where('method', '=', $method)->where('role_id', '=', $id)->first();
    			if (is_null($role_meta)) {
    				$role_meta_obj = new RolesMeta();
    				$role_meta_obj->role_id = $id;
    				$role_meta_obj->route = $route;
    				$role_meta_obj->method = $method;
    				$role_meta_obj->permission = 1;
    				$role_meta_obj->save();
    			} else {
    				$role_meta->role_id = $id;
    				$role_meta->route = $route;
    				$role_meta->method = $method;
    				$role_meta->permission = 1;
    				$role_meta->save();
    			}
    		}*/
    		return Redirect::action('RolesController@index');
    	} else {
    		return Redirect::back()->withInput()->withErrors($validator);
    	}
    }
    
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        
        $all_roles = Role::all();
        $routes = Route::getRoutes();
        $routes_array = array();
        foreach ($routes as $route) {
            $routes_array2 = array();
            $uri = $route->uri();
            $methods = $route->methods();
            $methods = $methods[0];
            $label = RouteLabel::where('route', '=', $uri)->where('method', '=', $methods)->where('public', '!=', 1)->first();
            if (is_null($label)) {
                $routes_array2['label'] = '';
                $routes_array2['description'] = '';
                $routes_array2['uri'] = $uri;
                $routes_array2['method'] = $methods;
            } else {
                $routes_array2['label'] = $label->label;
                $routes_array2['description'] = $label->description;
                $routes_array2['uri'] = $uri;
                $routes_array2['method'] = $methods;
            }
            $routes_array2['value'] = FALSE;
            if ($methods == 'GET' and $uri == 'users/dashboard') {
                $routes_array2['read_only'] = array('disabled' => 'disabled');
                $routes_array2['value'] = TRUE;
            }
            $routes_array[] = $routes_array2;
        }
      
        return view('roles.create', compact('routes_array','all_roles'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function assign() {
        
        $all_roles = Role::all()->lists('role_name', 'id');
        //$permissions = Permission::all()->orderBy("name","DESC");
        $permissions = Permission::orderBy('name', 'ASC')->get();
        $routes = Route::getRoutes();
        $routes_array = array();
        foreach ($routes as $route) {
            $routes_array2 = array();
            $uri = $route->uri();
            $methods = $route->methods();
            $methods = $methods[0];
            $label = RouteLabel::where('route', '=', $uri)->where('method', '=', $methods)->where('public', '!=', 1)->first();
            if (is_null($label)) {
                $routes_array2['label'] = '';
                $routes_array2['description'] = '';
                $routes_array2['uri'] = $uri;
                $routes_array2['method'] = $methods;
            } else {
                $routes_array2['label'] = $label->label;
                $routes_array2['description'] = $label->description;
                $routes_array2['uri'] = $uri;
                $routes_array2['method'] = $methods;
            }
            $routes_array2['value'] = FALSE;
            if ($methods == 'GET' and $uri == 'users/dashboard') {
                $routes_array2['read_only'] = array('disabled' => 'disabled');
                $routes_array2['value'] = TRUE;
            }
            $routes_array[] = $routes_array2;
        }     
        return view('roles.assign', compact('routes_array','all_roles','permissions'));
    }
    
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        
        $role = new Role();
        //$rules = array('role_name' => 'required', 'role_description' => 'required','permission_level' => 'required|integer');
        $rules = array('role_name' => 'required',
            'role_description' => 'required',
            'permission_level' =>  'required|integer',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            $role->role_name = Input::get('role_name');
            $role->role_description = Input::get('role_description');
            $role->	permission_level = Input::get('permission_level');   
            $role->save();
            $id = $role->id;           
            return Redirect::action('RolesController@index');
        } else {
            return Redirect::back()->withInput()->withErrors($validator);
        }
    }

   public function storePermissionRole() {
       
        $rules = array('role_name' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            $input = Input::all();
            $RolePermission = new RolePermission();
            if(isset($input['route']) && !empty($input['route'])) {            
                RolePermission::where('role_id', $input['role_name'])->delete();
                foreach($input['route'] as $key => $permissionID) {
                   $RolePermission = new RolePermission();
                    $RolePermission->role_id = $input['role_name'];
                    $RolePermission->permission_id = $permissionID;
                    $RolePermission->save();                   
                }
                return Redirect::to('roles');
            } else { 
                RolePermission::where('role_id', $input['role_name'])->delete();
                return Redirect::to('roles');
            }
        } else {
            return Redirect::back()->withInput()->withErrors($validator);
        }
   }  
   
   
   public function getRoles($role_id) {
       
      $rolesPermissionAssigned = RolePermission::where('role_id', $role_id)->lists('permission_id', 'permission_id');
    
      return $rolesPermissionAssigned;
   }
   
   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $role = Role::find($id);
        $role->delete($id);
        return Redirect::to('roles/');
    }
    
    /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {
        	
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
         
        
			if($route== 'getRoles') {
                $selectedSubscription =  Request::input('roleid');
                $data = $this->$route($selectedSubscription);
            }
			elseif($route== 'RemoveOmsStaff'){
            	            
            	 $data = $this->$route(Request::input('deleteid'));
            }
			
			
            if($route== 'show' || $route== 'getRoles') {            	
                return json_encode($data);
            }
            elseif($route== 'getRoles' && isset($data["success"]) && $data["success"]==false){
            	return $this->json_error($data["arr"]);
            }
            	          
            
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }            

            return $this->json_success($result);
        }

}

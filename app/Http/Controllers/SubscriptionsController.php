<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use App\DataOption;
use App\Subscription;
use App\BillingHostory;
use App\PaymentAdjustment;
use Event;
use DB;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use App\User;
use Route;


class SubscriptionsController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
        
       $routepath = Route::getCurrentRoute()->getPath();
       if($routepath != 'subscriptions/update/{subscription}' && $routepath != 'subscriptions/store' &&  $routepath !='ajaxSubscriptions' && $routepath != 'subscriptions/search') {
            $something = new HomeController;
            $check = $something->authenticateUserRolePermissionURL($routepath);           
            if(!$check) {
               abort(404, 'Unauthorized action.');         
            }
       }
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the subscription.
	 *
	 * @return Response
	 */
	public function index() {
		$tableData = json_encode($this->show());
       // $tableData = json_encode(array()); //json_encode($this->show()); 
		return view('subscription.index', compact('tableData'));		
	}
	
	
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($type=false) {
        
		$json = array();	
		//$result = Subscription::all()->where('subscription_status', '!=', 'Deleted');
        $res = Subscription::select(array('subscriptions.id','subscriptions.subscription_name', 'subscriptions.fee_type', 'subscriptions.cost_per_unit','subscriptions.billing_frequency','subscriptions.subscription_status'))->where('subscription_status', '=', 'Active')->where('is_deleted', '=', 0);
        $result = $res->get();
	
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$json[] = array('subscription_name' => $result[$rKey]->subscription_name,
					'fee_type' => $result[$rKey]->fee_type,
					'cost_per_unit' =>$result[$rKey]->cost_per_unit,
					'billing_frequency' => $result[$rKey]->billing_frequency,
					'subscription_status' => $result[$rKey]->subscription_status,					
					'actions' => view('subscription.actions', compact('id'))->render() );
		}			
		return $json;
	}
	
		
	
	/**
	 * Show the form for creating a new subscription.
	 *
	 * @return Response
	 */
	public function create($id = false) {
                
		$form = 'Add';
       $billingFrequency = DataOption::where('type_id', 5)->where('option_status', '!=', 'Inactive')->orderBy('option_name')->lists('option_name', 'option_name');
		
		return view('subscription.create', compact('form','billingFrequency'));
	}

	/**
	 * Store a newly created subscription in table.
	 *
	 * @return Response
	 */
	public function store() {
        
		$addrs = array();
		$input = Request::all();
		$validator = Validator::make($input, Subscription::$base_rules);		 
		unset($input["_token"]);
		
		if ($validator->passes()) {
		
			$input_fields = array(
						
					'subscription_name' ,
					'fee_type' ,
					'cost_per_unit' ,
					'billing_frequency',
					'subscription_status' ,
                    'subscription_desc',
                    'subscription_users_allowed',
                    'max_user_allowed',
                    'created_by'
			);	
			
			$item = new Subscription();	         
			foreach($input as $oKey => $oVal) {
	
				if(in_array($oKey, $input_fields)) {
					$item->$oKey = $oVal;
				}
			}
			 
			$item->save();
			$insertedId = $item->id;	

			
			return Redirect::to('subscriptions');
		} else {
		
			return redirect()->back()->withInput()->withErrors($validator);
		}
	}
	
    
    /**
	 * Show the form for editing the subscription Package.
	 *
	 * @param  int  $item
	 * @return Response
	 */
	public function edit($subscription_id) {
        
		$form = "Edit";	
		
		$user_access = array();	
		$billingFrequency = DataOption::where('type_id', 5)->where('option_status', '!=', 'Inactive')->orderBy('option_name')->lists('option_name', 'option_name');
		$item = Subscription::where('id', $subscription_id)->get();    
		return view('subscription.edit', compact('subscription_id','form','item','billingFrequency'));
	}
	

    /**
	 * Update the subscription package.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function update($package_id) {
	
		$output = Request::all();     
        $validation = Subscription::validateUpdate($output, $package_id);
     
		 if ($validation->passes() && !empty($output["_token"])) {
			 
					 $item = Subscription::find($package_id);
					 $updatePackage = array(
						'subscription_name' => !empty($output["subscription_name"]) ? $output["subscription_name"] : '',
						'fee_type' => !empty($output["fee_type"]) ? $output["fee_type"] : '',
						'cost_per_unit' => !empty($output["cost_per_unit"]) ? $output["cost_per_unit"] : '',
						'billing_frequency' => !empty($output["billing_frequency"]) ? $output["billing_frequency"] : '',
						'subscription_status' => !empty($output["subscription_status"]) ? $output["subscription_status"] : '',
						'subscription_desc' => !empty($output["subscription_desc"]) ? $output["subscription_desc"] : '',
                        'subscription_users_allowed' => !empty($output["subscription_users_allowed"]) ? $output["subscription_users_allowed"] : '',
                        'max_user_allowed' => !empty($output["max_user_allowed"]) ? $output["max_user_allowed"] : '',
					 );
                    
					 foreach($updatePackage as $oKey => $oVal) {
						$item->$oKey = $oVal;		
					 }
					 $item->save();
					 $subscriptionID = $item->id;
					 return Redirect::to('subscriptions');
		 } else {			
			return redirect()->back()->withInput()->withErrors($validation);
		 }
	}
    
    public function deleteSubscription() {
        
        $id =  Request::input('id');
		if(isset($id) && $id != '') {
			//Subscription::where('id', $id)->update(['subscription_status' => "Deleted"]);
            Subscription::where('id', $id)->update(['is_deleted' => 1]);
		}
        return $this->ajax_construct();
    }
    
    public function searchsubscription() {
        
        $input = Request::input('params');
		parse_str($input, $output);
      
        $json = array();
        $status = $output['subscriptionstatus'];
      
        if($status == 'Active') {
            $res = Subscription::select(array('subscriptions.id','subscriptions.subscription_name', 'subscriptions.fee_type', 'subscriptions.cost_per_unit','subscriptions.billing_frequency','subscriptions.subscription_status','subscriptions.is_deleted'))->where('subscription_status', '=', 'Active')->where('is_deleted', '=', 0);
        } else {
             //$res = Subscription::select(array('subscriptions.id','subscriptions.subscription_name', 'subscriptions.fee_type', 'subscriptions.cost_per_unit','subscriptions.billing_frequency','subscriptions.subscription_status','subscriptions.is_deleted'))->whereIn('subscription_status',  array('Active', 'Inactive'));
             
             $matchThese = ['subscription_status' => 'Active', 'is_deleted' => 1];
             $orThose = ['subscription_status' => 'InActive'];
             
             $res = Subscription::select(array('subscriptions.id','subscriptions.subscription_name', 'subscriptions.fee_type', 'subscriptions.cost_per_unit','subscriptions.billing_frequency','subscriptions.subscription_status','subscriptions.is_deleted'))->where($matchThese)->orWhere($orThose);
        }
            $result = $res->get();
	
            foreach($result as $rKey => $rVal) {
                $id = $result[$rKey]->id;
                $isDeleted = $result[$rKey]->is_deleted;
                $status = $result[$rKey]->subscription_status;
                if($isDeleted == 1) $status = 'Deleted';
                else $status = $result[$rKey]->subscription_status;
                $json[] = array('subscription_name' => $result[$rKey]->subscription_name,
                        'fee_type' => $result[$rKey]->fee_type,
                        'cost_per_unit' =>$result[$rKey]->cost_per_unit,
                        'billing_frequency' => $result[$rKey]->billing_frequency,
                        'subscription_status' => $status,					
                        'actions' => view('subscription.searchactions', compact('id','isDeleted','status'))->render() );
            }
        
		return $json;
    }
    
	public function removeCheck() {
		
		$id =  Request::input('item'); 
        $token =  Request::input('_token');
		
        if(!iskeyUsedTable('subscription_purchased','subscription_id',$id)) {
			return $this->ajax_construct(false, "RemoveSubscription({$id}, 'deleteSubscription', '{$token}'); ");			
		
		} else {
             $data['array'] = array('message' => array("This Subscription is used in this system and cannot be deleted"));
             return array('success'=>false, "arr"=>$data);
        }
	}
	
    public function getBillingsList() {
          
         $company_id = Request::input('item');
         $billings = DB::table('subscriptions')
            ->leftJoin('billing_histories', 'subscriptions.id', '=', 'billing_histories.subscription_id')
            ->select('billing_histories.*', 'subscriptions.subscription_name', 'subscriptions.fee_type')
            ->where('billing_histories.company_id', '=', $company_id)
            ->get();
         $arr = array();
        
         if($billings) {
			foreach($billings as $rKey => $rVal) {
                                
				$id = $billings[$rKey]->id;
				$arr[] = array(
					'billing_id' => $billings[$rKey]->id,	   
                    'subscription_name' => $billings[$rKey]->subscription_name,
                    'start_date' => getDateFormat($billings[$rKey]->start_date,0,10),//substr($billings[$rKey]->start_date,0,10),
                    'end_date' => getDateFormat($billings[$rKey]->end_date,0,10),//substr($billings[$rKey]->end_date,0,10),                 
                    'created_at' => $billings[$rKey]->created_at,
                    'user_added_first_name' => $billings[$rKey]->user_added_last_name.', '. $billings[$rKey]->user_added_first_name,
                    'action' => view('subscription.invoiceactions', compact('id','company_id'))->render()
                );
			}
		}
        
        $tableData = json_encode($arr);
		
		$table =  view('subscription.view', compact('tableData'));
		return $this->ajax_construct($table, "initBootstrapTable(".$tableData."); ");
    }
    
    public function getSubscribedBillingsDetails() {
        
        $company_id = Request::input('item');
         $billings = DB::table('subscriptions')
            ->leftJoin('billing_histories', 'subscriptions.id', '=', 'billing_histories.subscription_id')
            ->select('billing_histories.*', 'subscriptions.subscription_name', 'subscriptions.fee_type')
            ->where('billing_histories.company_id', '=', $company_id)
            ->get();
         $arr = array();
     
         if($billings) {
			foreach($billings as $rKey => $rVal) {                               
				$id = $billings[$rKey]->id;
				$arr[] = array(
						   
                    'invoice_id' => $billings[$rKey]->invoice_id,
                    'created_at' => substr($billings[$rKey]->created_at,0,10),
                    'total_amount' => $billings[$rKey]->total_amount,                 
                    'amount_due' => $billings[$rKey]->amount_due,
                    'item_purchased' => $billings[$rKey]->subscription_name,
                    'user_added_first_name' => $billings[$rKey]->user_added_last_name .', ' .$billings[$rKey]->user_added_first_name,
                    'status' => 'Paid',
                );
			}
		}
        
        $tableData = json_encode($arr);
		
		$table =  view('subscription.billing', compact('tableData'));
		return $this->ajax_construct($table, "initBootstrapTable(".$tableData."); ");
    }
    
    public function viewinvoice() {
        
        $user_id = Request::input("user_id");
		$company_id = Request::input("company_id");
        $billing_id = Request::input("id");
        $paymentAdjustmentList = array();
        $paymentAdjustmentListData = array();
        if(isset($billing_id) && $billing_id != '') {
            $billings = DB::table('subscriptions')
                ->leftJoin('billing_histories', 'subscriptions.id', '=', 'billing_histories.subscription_id')
                ->select('billing_histories.*', 'subscriptions.subscription_name', 'subscriptions.fee_type','subscriptions.billing_frequency')
                ->where('billing_histories.id', '=', $billing_id)
                ->get();
       
            if($billings) {
               foreach($billings as $rKey => $rVal) {

                   $id = $billings[$rKey]->id;
                   $arr[] = array(
                       'invoice_id' => $billings[$rKey]->invoice_id,	   
                       'invoice_date' => $billings[$rKey]->created_at,
                       'line_item' => '',
                       'total_line_item' => 1,                 
                       'payment_made' => 'N/A',
                       'transaction_id' => 'N/A',
                       'payments_or_adjustments' => 'Payment',
                       'user_added_first_name' => $billings[$rKey]->user_added_first_name.' '.$billings[$rKey]->user_added_last_name,
                       'status' => $billings[$rKey]->status,
                       'subscription_name' => $billings[$rKey]->subscription_name,
                       'subscription_desc' => $billings[$rKey]->subscription_name,
                       'total_amount' => $billings[$rKey]->total_amount,
                       'term' => $billings[$rKey]->billing_frequency,
                       'control' => view('subscription.addadjustmentaction', compact('id','company_id'))->render()
                   );

               }
           }
           
           $paymentAdjustmentquery =  PaymentAdjustment::select(array('payment_adjustment.id','payment_adjustment.billing_id','payment_adjustment.payment_adjustment_type', 'payment_adjustment.payment_adjustment_date', 'payment_adjustment.payment_adjustment_amount','payment_adjustment.payment_adjustment_reason','payment_adjustment.payment_adjustment_note','payment_adjustment.user_first_name','payment_adjustment.user_last_name','payment_adjustment.status'))->where('billing_id', '=', $billing_id)->where('status', '=', 'Active');
           $paymentAdjustmentList = $paymentAdjustmentquery->get();
          
           if(isset($paymentAdjustmentList) && !empty($paymentAdjustmentList)) {
               $PaymentAdjustment = new PaymentAdjustment();
                foreach($paymentAdjustmentList as $key => $value) {
                    $billingId = $paymentAdjustmentList[$key]['billing_id'];
                    $id = $paymentAdjustmentList[$key]['id'];
                    $paymentAdjustmentListData[$key]['payment_adjustment_date'] = $PaymentAdjustment->ConvertFormatedDate($paymentAdjustmentList[$key]['payment_adjustment_date']);
                    $paymentAdjustmentListData[$key]['payment_adjustment_amount'] = $paymentAdjustmentList[$key]['payment_adjustment_amount'];
                    $paymentAdjustmentListData[$key]['payment_adjustment_reason'] = $paymentAdjustmentList[$key]['payment_adjustment_reason'];
                    $paymentAdjustmentListData[$key]['payment_adjustment_note'] = $paymentAdjustmentList[$key]['payment_adjustment_note'];
                    $paymentAdjustmentListData[$key]['payment_adjustment_useradding'] = $paymentAdjustmentList[$key]['user_last_name'].' '.$paymentAdjustmentList[$key]['user_first_name'];
                    $paymentAdjustmentListData[$key]['status'] = view('subscription.deletepaymentadjustment', compact('id','billingId','company_id'))->render();
               }
             }          
        }
        $lables = BillingHostory::$invoice_lables;       
        $arr1 = array();
        $tableData = json_encode($arr1);
        $table =  view('subscription.invoice', compact('arr','billings','lables','paymentAdjustmentListData'));
        return $this->ajax_construct($table, "initBootstrapTable(".$tableData."); ");
    }
    
    public function addPaymentAdjustment() {
   
        $company_id = Request::input("company_id");
        $billing_id = Request::input("id");
        $arr1 = array();
        $currentDate = date('m/d/Y');      
        $tableData = json_encode($arr1);
        $table =  view('subscription.addadjustment', compact('arr','company_id','billing_id','currentDate'));
        return $this->ajax_construct($table, "initBootstrapTable(".$tableData."); ");
    }
    
    public function storepaymentadjustment() {
   
        $input = Request::input("form-input");		
		parse_str($input, $output);
        $data = Session::all();
        $userId = $data['login_82e5d2c56bdd0811318f0cf078b78bfc'];
        if(!empty($userId)) {
            $res = User::select(array('users.first_name','users.last_name'))->where('users.id', '=', $userId);
            $userName = $res->get(); 
        }
        
         $validator = Validator::make($output, PaymentAdjustment::$rules_client);
       
           if ($validator->passes()) {
             $input_fields = array(
					'billing_id' ,
                    'company_id' ,	
					'payment_adjustment_type' ,
					'payment_adjustment_date' ,
					'payment_adjustment_amount' ,
					'payment_adjustment_reason',
					'payment_adjustment_note'
			);			
			$PaymentAdjustment = new PaymentAdjustment();	
			foreach($output as $oKey => $oVal) {
	
				if(in_array($oKey, $input_fields)) {
					$PaymentAdjustment->$oKey = $oVal;
				}
			}
			
             $PaymentAdjustment['user_id'] = '';
             $PaymentAdjustment['status'] = 'Active';
             $PaymentAdjustment['user_first_name'] = !empty($userName[0]['first_name']) ? $userName[0]['first_name'] : '';
             $PaymentAdjustment['user_last_name'] = !empty($userName[0]['last_name']) ? $userName[0]['last_name'] : '';
             $PaymentAdjustment["payment_adjustment_date"] = $PaymentAdjustment->setFormatedDate($output["payment_adjustment_date"]);
			 $PaymentAdjustment->save();
            return $this->ajax_construct();
           
           } else {
               $data['array'] = array('message' => $validator->messages()->all());		
                return array('success'=>false, "data"=>$data);         
                
           } 
    }    
    
    public function removePaymentAdjustment() {
		
		$id =  Request::input('id');
        $token =  Request::input('_token'); 
		if (!isKeyUsedAsForeignKey('payment_adjustment', $id) ) {
            return $this->deletePaymentAdjustment();
		} else
			return array('errors' => 'true', 'message' => 'This Option is in use in a system');
	}
    
     public function deletePaymentAdjustment() {
         
        $id =  Request::input('id');
		if(isset($id) && $id != '') {
			PaymentAdjustment::where('id', $id)->update(['status' => "Deleted"]);
		}
        return $this->ajax_construct();
    }
    
    public function resetSubscriptionPackage() {
        
        $id =  Request::input('id');
		if(isset($id) && $id != '') {
             Subscription::where('id', $id)->update(['is_deleted' => 0]);
		}
		return $this->ajax_construct();
    }
        /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {
         
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
               			
			$data = $this->$route();
            if($route== 'show' || $route== 'RemoveSubscription' || $route == 'removePaymentAdjustment') {
                return json_encode($data);
            }
            if($route== 'storepaymentadjustment' && isset($data["success"]) && !$data["success"]) {
                
                return json_encode($data);
			}
            if($route== 'removeCheck' && isset($data["success"]) && $data["success"]==false) {
                //print_r($data["arr"]);
                //$data["arr"][0]['success']= false;
               // $data["arr"]['success'] = false;
                //$data['success'] = false;
                //  return json_encode($data);
                return $this->json_error($data["arr"]);
			}
            
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
          
            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('false' => true, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}

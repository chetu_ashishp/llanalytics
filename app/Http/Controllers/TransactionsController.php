<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Transaction;
use Illuminate\Support\Facades\Validator;
use Request;
use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpKernel\Client;
use App\Client;
use App\Company;
use App\Custom;

class TransactionsController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{		
	//	echo "fsdfsdfsd";exit;
	//        $create = $this->create();
      //  $tableData = json_encode($this->show());                                               
        //$data_types = $create->data_types;                
      //  $res = compact('create', 'tableData', 'data_types');
	//	return view('client.index', compact('create', 'tableData'));
	}
		
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show()
	{
		$client_id = Request::input('item');
		
		/** @ToDo
		 *
		 *  change Company id
		 */		
		$company_id = '1';		
		
		$arr = array();			
		$result = Transaction::select(array('transactions.id', 'transactions.start_date', 'transactions.transaction_type', 'transactions.transaction_status', 'transactions.mls1', 'transactions.property_house', 'transactions.property_addr1', 'transactions.coe_date'))			
			->where('transactions.client_id', $client_id)	
			->orderBy('start_date', 'desc')
			->get();
		
		if($result){
			foreach($result as $rKey => $rVal) {
								
				$id = $result[$rKey]->id;
				$arr[] = array(
						   
				'start_date' => $result[$rKey]->getFormatedDate('start_date'),
				'transaction_type' => $result[$rKey]->ttype["option_name"],						
				'mls1' => $result[$rKey]->mls1,
				'property_house' => $result[$rKey]->property_house,
				'property_addr1' =>  $result[$rKey]->property_addr1,
				'coe_date' => $result[$rKey]->getFormatedDate('coe_date'),
				'name' => $result[$rKey]->mls1,						
				'transaction_status' => $result[$rKey]->tstatus["option_name"],
				'actions' => view('transaction.actions', compact('id', 'company_id', 'client_id'))->render() );
			}
		}
		
		$tableData = json_encode($arr);
		
		$table =  view('transaction.index', compact('tableData', 'company_id', 'client_id'));
		return $this->ajax_construct($table, "initBootstrapTable(".$tableData."); ");
		
	}

	
	function autocompleate(){
//            \DB::enableQueryLog();
		$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND
		strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
		if (!$isAjax) {
			$user_error = 'Access denied - not an AJAX request...';
			trigger_error($user_error, E_USER_ERROR);
		}
		
		//•	Related Transaction (autosuggest list of all other transactions across the database 
		//	as $mls# - $propertyNickname - $startDate - $clientLastName, search on client name, mls #, nickname, only show “active” status transactions 
		//(e.g. in process, active, active completed, etc)		
		
		//	If above related transaction is selected on add, ask user “Prefill applicable fields from related transaction?” as a checkbox, if checked,
		// prefill all financial and contract fields with the data from the related transaction, user may still edit them)

		
		
		//CHANGED 3.3.	THIS will add Property House# to the Related Transaction autosuggest and remove start date.  
		//This will be changed to autosuggest a list based off the MLS#, Short Address (Nickname), Last Name, and Property House# when performing a search
		
		
		$term = trim($_GET['term']);
	
		$a_json = array();
		$a_json_row = array();
		
		$a_json_invalid = array(array("id" => "#", "value" => $term, "label" => "Only letters and digits are permitted..."));
		$json_invalid = json_encode($a_json_invalid);

		// SECURITY HOLE ***************************************************************
		// allow space, any unicode letter and digit, underscore and dash
//		if (preg_match("/[^\040\pL\pN_-]/u", $term)) {
//			print $json_invalid;
//			exit;
//		}
		// *****************************************************************************
	
		$parts = explode(' ', $term);
		$p = count($parts);
		
		/*
			\Event::listen('illuminate.query', function($query, $params, $time, $conn)
		{
			dd(array($query, $params, $time, $conn));
		});	
		*/
		
		$transactions = Transaction::select('transactions.*', 'clients.last_name')
		->join('clients', 'transactions.client_id', '=', 'clients.id')
		->leftjoin('company_data_options', 'transactions.transaction_status', '=', 'company_data_options.id');

		$transactions->where(function($query){	
			$query->where('company_data_options.considered_status', 'Yes');
		});
				
		$transactions->where(function($query) use($p,$parts){
			for ($i = 0; $i < $p; $i++) {
				$query->whereRaw("last_name LIKE '%" . $parts[$i] . "%'")
				->orWhere('transactions.mls1', 'LIKE', '%'. $parts[$i] .'%')
				->orWhere('transactions.mls2', 'LIKE', '%'. $parts[$i] .'%')
				->orWhere('transactions.nickname', 'LIKE', '%'. $parts[$i] .'%')			
				->orWhere('transactions.property_house', 'LIKE', '%'. $parts[$i] .'%');
			}				
		});	
			
		$all_trans = $transactions->get();

		//var_dump($all_trans);exit;
		/*$queries = \DB::getQueryLog();
		dd(last( $queries ));
		*/
		//dd(\DB::getQueryLog());

		
		foreach ($all_trans as $s) {
		
			$addition =!empty($s->mls1) ? $s->mls1.' - ' : '';
			$addition.=!empty($s->nickname) ? $s->nickname.' - ' : '';			
			$addition.=!empty($s->last_name) ? $s->last_name.' - ' : '';
			$addition.=!empty($s->property_house) ? $s->property_house : '';
			
			$a_json_row["id"] = $s->id;			
			$a_json_row["value"] = $addition;
			$a_json_row["label"] = $addition;
			$a_json_row['all'] = json_encode($s->toArray());		
			array_push($a_json, $a_json_row);
		}
		
	
		
		// highlight search results
		$a_json = apply_highlight($a_json, $parts);		
		$json = json_encode($a_json);		
		return $json;
	}
	
	
	/**
	 * Show the form for adding the specified resource.
	 *
	 * @param  int  $client
	 * @return Response
	 */

	function addTransaction(){
		
		$company_id = Request::input("company_id"); 
		$client_id = Request::input("client_id");
		
		$clientobj = Client::find($client_id);
		$company = Company::find($company_id);
		
		$html = view('transaction.create', compact('company_id', 'client_id', 'clientobj', 'company') );
		return $this->ajax_construct($html);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeTransaction()
	{
		$addrs = array();
		$input = Request::input("form-input");
		
		
	
		parse_str($input, $output);
		$validator = Validator::make($output, Transaction::$rules);
		/*Event::listen('illuminate.query', function($query, $params, $time, $conn)
		 {
				dd(array($query, $params, $time, $conn));
				});*/
	
		unset($output["_token"]);
		if ($validator->passes()) {
		
		
			$item = new Transaction();
			$output["start_date"] = !empty($output["start_date"])?$item->setFormatedDate($output["start_date"]):NULL;
			$output["listing_date"] = !empty($output["listing_date"])?$item->setFormatedDate($output["listing_date"]):NULL;
			$output["listing_expiration_date"] = !empty($output["listing_expiration_date"])?$item->setFormatedDate($output["listing_expiration_date"]):NULL;
			$output["contract_date"] = !empty($output["contract_date"])?$item->setFormatedDate($output["contract_date"]):NULL;
			
			$output["coe_date"] = !empty($output["coe_date"])?$item->setFormatedDate($output["coe_date"]):NULL;
			
			$output["state"] = !empty($output["state"])?$output["state"]:NULL;
			$output["lender"] = !empty($output["lender"])?$output["lender"]:NULL;
			$output["title_company"] = !empty($output["title_company"])?$output["title_company"]:NULL;

			$output["referral_source"] = !empty($output["referral_source"])?$output["referral_source"]:NULL;
			
			$output["city_id"] = !empty($output["city_id"])?$output["city_id"]:NULL;
			
			
			
			
			$output["sales_price"] =  str_replace(",", "", $output["sales_price"]);
			$output["bonus"] =  str_replace(",", "", $output["bonus"]);
			$output["agent_proportio"] =  str_replace(",", "", $output["agent_proportio"]);
			$output["commission_rate"] =  str_replace(",", "", $output["commission_rate"]);
			$output["agent_split"] =  str_replace(",", "", $output["agent_split"]);
			$output["agent_split2"] =  str_replace(",", "", $output["agent_split2"]);
			$output["broker_flat_fee"] =  str_replace(",", "", $output["broker_flat_fee"]);
			$output["assist_fee"] =  str_replace(",", "", $output["assist_fee"]);
			$output["gross_commission"] =  str_replace(",", "", $output["gross_commission"]);
			$output["net_commission"] =  str_replace(",", "", $output["net_commission"]);
			$output["final_net_commission"] =  str_replace(",", "", $output["final_net_commission"]);
			$output["sp_of_list"] =  str_replace(",", "", $output["sp_of_list"]);
			$output["sp_of_origin_list"] =  str_replace(",", "", $output["sp_of_origin_list"]);
			
			$output["origin_list_price"] =  str_replace(",", "", $output["origin_list_price"]);
			$output["current_list_price"] =  str_replace(",", "", $output["current_list_price"]);
				
			
			$output["related_trans_id"] = !empty($output["related_trans_id"]) && !empty($output["relatedtransid"])?$output["related_trans_id"]:NULL;
			unset($output["relatedtransid"]);
			
			foreach($output as $oKey => $oVal) {	
				$item->$oKey = $oVal;				
			}
			 
			$item->save();
			return $this->ajax_construct();
		}	
		else{
			$data['array'] = array('message' => $validator->messages()->all());			
			return array('success'=>false, "errors"=>$data);
		}
	}
	
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $client
	 * @return Response
	 */
	public function editTransaction()
	{
		$item = Transaction::find(Request::input("id"));		
		$company_id = Request::input("company_id");
		$client_id = Request::input("client_id");		
		$relatedtransid = "";
		
		if($item->related_trans_id){
			$rel_transaction = Transaction::find($item->related_trans_id);
			$relatedtransid =!empty($rel_transaction->mls1) ? $rel_transaction->mls1.' - ' : '';
			$relatedtransid .=!empty($rel_transaction->nickname) ? $rel_transaction->nickname.' - ' : '';
			$relatedtransid .=!empty($rel_transaction->start_date) ? $rel_transaction->getFormatedDate('start_date').' - ' : '';
			$relatedtransid .=!empty($rel_transaction->last_name) ? $rel_transaction->last_name : '';			
		}
		
		$clientobj = Client::find($client_id);		
		$html = view('transaction.edit', compact('item', "company_id", "client_id", "relatedtransid", "clientobj") );		
		return $this->ajax_construct($html);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateTransaction()
	{
		$input = Request::input("form-input");		
		parse_str($input, $output);
		
		$id = $output["id"];
		unset($output["id"]);
		
		$validator = Validator::make($output, Transaction::$rules);
		 
		unset($output["_token"]);
		
		
		
		//var_dump($input);exit;
		
		
		
		if ($validator->passes()) {
			
			$item = Transaction::find($id);
			  
			$output["start_date"] = !empty($output["start_date"])?$item->setFormatedDate($output["start_date"]):NULL;
			$output["listing_date"] = !empty($output["listing_date"])?$item->setFormatedDate($output["listing_date"]):NULL;
			$output["listing_expiration_date"] = !empty($output["listing_expiration_date"])?$item->setFormatedDate($output["listing_expiration_date"]):NULL;
			$output["contract_date"] = !empty($output["contract_date"])?$item->setFormatedDate($output["contract_date"]):NULL;
			
			$output["coe_date"] = !empty($output["coe_date"])?$item->setFormatedDate($output["coe_date"]):NULL;
			$output["referral_source"] = !empty($output["referral_source"])?$output["referral_source"]:NULL;
			$output["state"] = !empty($output["state"])?$output["state"]:NULL;
			
			$output["lender"] = !empty($output["lender"])?$output["lender"]:NULL;
			$output["title_company"] = !empty($output["title_company"])?$output["title_company"]:NULL;
			
			$output["city_id"] = !empty($output["city_id"])?$output["city_id"]:NULL;
			$output["sales_price"] =  str_replace(",", "", $output["sales_price"]);
			$output["bonus"] =  str_replace(",", "", $output["bonus"]);
			$output["agent_proportio"] =  str_replace(",", "", $output["agent_proportio"]);
			$output["commission_rate"] =  str_replace(",", "", $output["commission_rate"]);
			$output["agent_split"] =  str_replace(",", "", $output["agent_split"]);
			$output["agent_split2"] =  str_replace(",", "", $output["agent_split2"]);
			$output["broker_flat_fee"] =  str_replace(",", "", $output["broker_flat_fee"]);
			$output["assist_fee"] =  str_replace(",", "", $output["assist_fee"]);
			$output["gross_commission"] =  str_replace(",", "", $output["gross_commission"]);
			$output["net_commission"] =  str_replace(",", "", $output["net_commission"]);
			$output["final_net_commission"] =  str_replace(",", "", $output["final_net_commission"]);
			$output["sp_of_list"] =  str_replace(",", "", $output["sp_of_list"]);
			$output["sp_of_origin_list"] =  str_replace(",", "", $output["sp_of_origin_list"]);
			
			$output["origin_list_price"] =  str_replace(",", "", $output["origin_list_price"]);
			$output["current_list_price"] =  str_replace(",", "", $output["current_list_price"]);
			$output["related_trans_id"] = !empty($output["related_trans_id"]) && !empty($output["relatedtransid"])?$output["related_trans_id"]:NULL;
			
			unset($output["relatedtransid"]);
			
			foreach($output as $oKey => $oVal) {	
				$item->$oKey = $oVal;				
			}
			 
			$item->save();
			return $this->ajax_construct();
		}	
		else{
				$data['array'] = array('message' => $validator->messages()->all());			
				return array('success'=>false, "errors"=>$data);
			}
	}
	
	
	public function deleteTransaction(){
	
		$id =  Request::input('id');
		Transaction::where('id',$id)->delete();	
		return $this->ajax_construct();
	}	
		
	
	public function removeCheck() {
		
		$id =  Request::input('item'); 		
		$token =  Request::input('_token'); 	
		 //  var_dump(isKeyUsedAsForeignKey('transactions', $id)) ;exit;
		   
		    		    
		if (isKeyUsedAsForeignKey('transactions', $id)==false ) {						
			return $this->ajax_construct(false, "RemoveTransaction({$id}, 'deleteTransaction', '{$token}'); ");			
		} 
		else{
			$data['array'] = array('message' => array("This Transaction is in use and can't be deleted."));
			return array('success'=>false, "arr"=>$data);
		}
		
	}
	
	 
		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function destroy($id)
		{
			//
		}
		
		
        /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {

           
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');            
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
        
			

            if (Request::has('typeId')) {
            	$data = $this->$route(Request::input("typeId"));
            }
            			
			elseif($route== 'deleteTransaction'){
					
				$data = $this->$route(Request::input('deleteid'));
			}
			else{
				$data = $this->$route();
			}
					
					
					
			if( ($route== 'storeTransaction' || $route=='updateTransaction') && isset($data["success"]) && $data["success"]==false){
				return $this->json_error($data["errors"]);
			}					
					
			if($route== 'show') {
					$result['target'] ="#transactions";
			}
			
			
			if($route== 'removeCheck' && isset($data["success"]) && $data["success"]==false){
				return $this->json_error($data["arr"]);
			}
			/*elseif($route== 'deleteTransaction') {
				return json_encode($data);
			}*/
			
			
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            
         //  var_dump($this->json_success($result));exit;
            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}

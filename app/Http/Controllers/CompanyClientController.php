<?php namespace App\Http\Controllers;

use App\Company;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Transaction;
use Request;

use App\CompanyDataOption;
use App\Client;
use App\DataType;
use Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use App\State;
use App\Address;
use Session;
class CompanyClientController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */ 
	public function index() {
        
        $tableData = json_encode(array()); //json_encode($this->show()); 
		return view('companyportal.client.index', compact('tableData'));
	}   
		
	public function search() {
        
		$input = Request::input('params');
		parse_str($input, $output);	
		$json = array();
		/*Event::listen('illuminate.query', function($query, $params, $time, $conn)
		{
			dd(array($query, $params, $time, $conn));
		});*/
		//$company_id = \Config::get('custom.company_id');//1;
        $sessionData = Session::all();
        if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
             
            $userData = getCurrentUserData($sessionData['user_id']);
            
            if($userData && $userData[0]->company_id != '') {
                $company_id = $userData[0]->company_id;
             
                $res = Client::select(array('clients.id', 'clients.client_number', 'clients.last_name', 'clients.first_name', 'clients.email', 'clients.status'))->where('clients.company_id', $company_id)->where('clients.status','!=', 'Deleted');

                if($output["name"]!='') {
                    $filtername = $output["name"];				
                    $res->where(function ($query) use ($filtername) {
                        $query->where('last_name', 'like',  '%'.$filtername.'%')
                        ->orWhere('first_name', 'like',  '%'.$filtername.'%');
                    });
                }
		
                if($output["spouse_name"]!='') {	
                    $spouse = $output["spouse_name"];				
                    $res->where(function ($query) use ($spouse){
                        $query->where('spouse_fname', 'like',  '%'.$spouse.'%' )
                        ->orWhere('spouse_lname', 'like', '%'.$spouse.'%' );
                    });
                }
	
                if($output["email_staff"]!=''){
                    $res->where('email',  'like', '%'.$output["email_staff"].'%' );
                }

                if($output["status"]!=''){
                    $res->where('status', $output["status"]);
                }
		
                if($output["prop_addr"]!='') {

                    $res->leftjoin('transactions', 'transactions.client_id', '=', 'clients.id');
                    $res->leftjoin('states', 'transactions.state', '=', 'states.id');
                    $res->leftjoin('company_data_options', 'transactions.city_id', '=', 'company_data_options.id');
                    $res->leftjoin('company_data_options as tzip', 'transactions.zip', '=', 'tzip.id');

                    $propaddr = $output["prop_addr"];
                    $res->where(function ($query) use ($propaddr){
                        $query->where('property_house', 'like',  '%'.$propaddr.'%' )
                        ->orWhere('property_addr1', 'like', '%'.$propaddr.'%' )
                        ->orWhere('property_addr2', 'like', '%'.$propaddr.'%' )
                        ->orWhere('tzip.option_name', 'like', '%'.$propaddr.'%' )
                        ->orWhere('subdivision', 'like', '%'.$propaddr.'%' )
                        ->orWhere('states.state_name', 'like', '%'.$propaddr.'%' )
                        ->orWhere('states.state_abbr', 'like', '%'.$propaddr.'%' )
                        ->orWhere('company_data_options.option_name', 'like', '%'.$propaddr.'%' ) // city_id
                        ->orWhere('hoa', 'like', '%'.$propaddr.'%' );
                    }); //->orWhere('city', 'like', '%'.$propaddr.'%' )			
                }
			
                $res->groupBy('clients.id');
                $result = $res->get();

                foreach($result as $rKey => $rVal) {
                    $id = $result[$rKey]->id;

                    $cityname = $statename = "";
                    $addr = Address::select()->where('client_id', $result[$rKey]->id)->orderBy('addresses.created_at', 'asc')->first();
                    if($addr){


                        if($addr->city)
                            $cityname = $addr->city;


                        $state = $addr->cstate;

                        if($state)
                            $statename = $state->state_abbr;
                    }

                    $json[] = array('clnumber' => $result[$rKey]->client_number,
                            'clname' => $result[$rKey]->last_name.", ".$result[$rKey]->first_name,
                            'clctity' => $cityname, 
                            'clstate' => $statename,
                            'clemail' => $result[$rKey]->email,
                            'clqtrans' => count($result[$rKey]->transactions),
                            'clstatus' => $result[$rKey]->status,
                            'actions' => view('companyportal.client.actions', compact('id'))->render() );
                }
            }            
        }   
		return json_encode($json);
	}
	
	
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($type=false) {
        
		$json = array();
		//$company_id = \Config::get('custom.company_id');//1;
		$sessionData = Session::all();
        if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
             
            $userData = getCurrentUserData($sessionData['user_id']);
            
            if($userData && $userData[0]->company_id != '') {
                $company_id = $userData[0]->company_id;
                
            $result = Client::select(array('clients.id', 'clients.client_number', 'clients.last_name', 'clients.first_name', 'clients.email', 'clients.status'))->where('company_id', $company_id)->where('is_deleted',0)
            ->get();
		
            foreach($result as $rKey => $rVal) {
                $id = $result[$rKey]->id;

                $cityname = $statename = "";
                $addr = Address::where('client_id', $result[$rKey]->id)->orderBy('created_at', 'asc')->first();
                if($addr){


                    if($addr->city)
                        $cityname = $addr->city;

                    $state = $addr->cstate;

                    if($state)
                        $statename = $state->state_abbr;
                }				
                $json[] = array(    'clnumber' => $result[$rKey]->client_number,
                        'clname' => $result[$rKey]->last_name.", ".$result[$rKey]->first_name,
                        'clctity' => $cityname, 
                        'clstate' => $statename,
                        'clemail' => $result[$rKey]->email,
                        'clqtrans' => count($result[$rKey]->transactions),
                        'clstatus' => $result[$rKey]->status,
                        'actions' => view('client.actions', compact('id'))->render() );
            }
          }
        }    
		return $json;
	}
		

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
        
            $form = 'Add';
          	$referral_types = CompanyDataOption::where('type_id', 5)->orderBy('option_name')->lists('option_name', 'id');          	
          	$states = State::all()->lists('state_name','id');
          	$createadrr = array();     
          	
          	//$company_id = \Config::get('custom.company_id');
            $sessionData = Session::all();
            if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
                $userData = getCurrentUserData($sessionData['user_id']);
            
                if($userData && $userData[0]->company_id != '') {
                    $company_id = $userData[0]->company_id;
                    $company = Company::find($company_id);			
                    $createadrr = view('client.includes.addaddrmodal', compact('company_id', 'company','states') );
                }
            }
            return view('companyportal.client.create', compact('form', 'referral_types', 'createadrr'));
	}

	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($client) {
        
		$addrs = array();
   	   	$input = Request::all();
   	   	
   	   //	echo "<pre>"; var_dump($input);exit;
   	   	
       	$validator = Validator::make($input, Client::$rules_client);  
           
		   	unset($input["_token"]);
		   	
           	if ($validator->passes()) {
           	
	           	if(isset($input["type"])){
	           		$mail_addr_set = false;
	           		
		           	foreach($input["type"] as $key=>$adrr){
		           		
		           		$addrs[$key]["type"] = $adrr;
		           		$addrs[$key]["addr_line1"] = $input["addr_line1"][$key];
		           		$addrs[$key]["addr_line2"] = $input["addr_line2"][$key];
		           		$addrs[$key]["state_id"] = $input["state_id"][$key];
		           		
		           		if(!isset($input["zip"][$key]) || empty($input["zip"][$key])){
		           			$addrs[$key]["zip"] = NULL;
		           		}
		           		else{
		           			$addrs[$key]["zip"] = $input["zip"][$key];
		           			unset($input["zip"][$key]);
		           		}
		           		
		           		if(!isset($input['city'][$key]) || empty($input["city"][$key])){
		           			$addrs[$key]["city"] = NULL;
		           		}
		           		else{
		           			$addrs[$key]["city"] = $input["city"][$key];
		           			unset($input["city"][$key]);
		           		}
		           		
		           		if( isset($input["mailing_addr"][$key]) && $input["mailing_addr"][$key]=="Yes" && $mail_addr_set==false ){
		           			$mail_addr_set = true;
		           			$addrs[$key]["mailing_addr"] = $input["mailing_addr"][$key];
		           		}	           		
		           		
		           		unset($input["type"][$key]);           		
		           		unset($input["addr_line1"][$key]);
		           		unset($input["addr_line2"][$key]);		           		
		           		unset($input["state_id"][$key]);		           		
		           		unset($input["mailing_addr"][$key]);
		           	}
	           	}          	
          //	echo "<pre>"; var_dump($addrs);exit;
           	
           		$input_fields = array(
           			
           			'last_name' ,
           			'first_name' ,
           			'spouse_fname',
           			'spouse_lname' ,
           			'email' ,
           			'primary_phone',
           			'alternate_phone',
           			'mobile_phone',
           			'other_phone' ,
           			'date_met' ,
           			'referral_source',
           			'refsource_details',
           			'notes',
           			'website',
           			'linkedin_url',
           			'facebook_url',
           			'twitter_url',
           			'googleplus_url',
           			'status'           			           			
           			);           	
           		//echo '$input["date_met"]: '.$input["date_met"];
                 $item = new Client();
           		$input["date_met"] = $item->setFormatedDate($input["date_met"]);
           		//$input["date_met"] = '';
           		if(empty($input['referral_source']))           			
           			$input['referral_source'] = NULL;
           		
           		
	            $item = Client::find($client);
	            foreach($input as $oKey => $oVal) {
	            	
	                if(in_array($oKey, $input_fields)) {
	                    $item->$oKey = $oVal;	                
	                }
	            }
	                        
	            $item->save();	            
	        //  echo "<pre>";var_dump($addrs);exit;
	            foreach(Address::where('client_id', $client)->get() as $add){
					$add->delete();
				}
	            	            
	       // save addresses for the added Client     
	            if(!empty($addrs)){
	            	
	            	foreach($addrs as $addr) {	            	
	            		$addrObj = new Address();	            		
	            		foreach($addr as $aKey => $aVal) {	            			
	            			if($aKey!="state_id" || ($aKey=="state_id" && $aVal!="") )
	            				$addrObj->$aKey = $aVal;
	            		}	            		
	            		$addrObj->client_id = $client;
	            		$addrObj->save();
	            	}
	            }	            
	           	return Redirect::to('companyportal/client/details/'.$client);
	            //return redirect()->route('clientinfo', 'user' => $insertedId );
	            //return redirect()->action('App\Http\Controllers\ClientsController@details', 'user' => 1);
           }                      
           else{           	           	           	
           	 	return redirect()->back()->withInput()->withErrors($validator);
           }         
	}
	
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
        
         $sessionData = Session::all();
         if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
            $userData = getCurrentUserData($sessionData['user_id']);
            
                if($userData && $userData[0]->company_id != '') {
                    $company_id = $userData[0]->company_id;
                  
                $addrs = array();
                $input = Request::all();
                $validator = Validator::make($input, Client::$rules_client);  

                unset($input["_token"]);
                if ($validator->passes()) {

                 if(isset($input["type"])) {
                     $mail_addr_set = false;

                     foreach($input["type"] as $key=>$adrr) {

                         $addrs[$key]["type"] = $adrr;
                         $addrs[$key]["addr_line1"] = $input["addr_line1"][$key];
                         $addrs[$key]["addr_line2"] = $input["addr_line2"][$key];

                         if(!isset($input["zip"][$key]) || empty($input["zip"][$key])) {
                             $addrs[$key]["zip"] = NULL;
                         }	
                         else{
                             $addrs[$key]["zip"] = $input["zip"][$key];
                             unset($input["zip"][$key]);
                         }

                         if(!isset($input['city'][$key]) || empty($input["city"][$key])){
                             $addrs[$key]["city"] = NULL;
                         }
                         else{
                             $addrs[$key]["city"] = $input["city"][$key];
                             unset($input["city"][$key]);
                         }



                         $addrs[$key]["state_id"] = $input["state_id"][$key];	           		

                         if(isset($input["mailing_addr"][$key]) && $input["mailing_addr"][$key]=="Yes" && $mail_addr_set==false ){
                             $mail_addr_set = true;
                             $addrs[$key]["mailing_addr"] = $input["mailing_addr"][$key];
                         }	           		

                         unset($input["type"][$key]);           		
                         unset($input["addr_line1"][$key]);
                         unset($input["addr_line2"][$key]);	           		
                         unset($input["state_id"][$key]);	           		
                         unset($input["mailing_addr"][$key]);
                     }
                 }          	
                
                 $input_fields = array(

                         'client_number' ,
                         'last_name' ,
                         'first_name' ,
                         'spouse_fname',
                         'spouse_lname' ,
                         'email' ,
                         'primary_phone',
                         'alternate_phone',
                         'mobile_phone',
                         'other_phone' ,
                         'date_met' ,
                         'referral_source',
                         'refsource_details',
                         'notes',
                         'website',
                         'linkedin_url',
                         'facebook_url',
                         'twitter_url',
                         'googleplus_url',
                         'status'           			           			
                         );           	

                     $item = new Client();
                     $input["date_met"] = $item->setFormatedDate($input["date_met"]);
                     if(empty($input['referral_source']))
                         $input['referral_source'] = NULL;
                    
                     //$input['company_id'] = $company_id;
                     foreach($input as $oKey => $oVal) {

                         if(in_array($oKey, $input_fields)) {
                             $item->$oKey = $oVal;	                
                         }
                     }
                     $item['company_id'] = $company_id;                   
                     $item->save();
                     $insertedId = $item->id;
                    // var_dump($insertedId); die;
                     Client::where('id', $insertedId)->update(['client_number' => "VT{$insertedId}-".substr( time(),-4)]);	            

                // save addresses for the added Client     
                     if(!empty($addrs)) {

                         foreach($addrs as $addr) {

                             $addrObj = new Address();

                             foreach($addr as $aKey => $aVal) {

                                 if($aKey!="state_id" || ($aKey=="state_id" && $aVal!="") )
                                     $addrObj->$aKey = $aVal;
                             }

                             $addrObj->client_id = $insertedId;
                             $addrObj->save();
                         }
                     }	            
                    // return Redirect::to('companyportal/clients/details/'.$insertedId); 
                     return Redirect::to('companyportal/clients');
                }                      
                else {           	           	           	        	
                 return redirect()->back()->withInput()->withErrors($validator);
                }
             }
           } 
	}

	/**
	 * Show the details specified resource.
	 *
	 * @param  int  $client
	 * @return Response
	 */
	public function details($client) {
        //echo '<pre/>';
        //echo 'client'. $client;
		$addresses = $client->address; //->toArray();	
		$client_info = $client->toArray();		

		$lables = Client::$client_lables;		
		$client_info['date_met'] = $client->getFormatedDate('date_met');		
		$client_info['referral_source'] = companyOptionNameById($client_info['referral_source']);

		$data = array();
		$data['escrowOfficers'] = Transaction::where('escrow_officer', '!=', '')->groupBy('escrow_officer')->lists('escrow_officer');
		$data['loanOfficers'] = Transaction::where('loan_officer', '!=', '')->groupBy('loan_officer')->lists('loan_officer');


		return view('companyportal.client.details', compact('client', 'addresses', 'client_info', 'lables', 'data') );
	}	
	
	/**
	 * Add address record to the Client
	 * 
	 * @return array
	 */
	public function add_addr_record() {
		
		$input = Request::input('form-input');		
		parse_str($input, $output);		
		//var_dump($output);exit;	
        $result = '';
		$number = substr(time(), -4).rand(0, 100);
		
		$state = State::find($output["state_id"]);
		//$company_id = $output['company_id'];
        $sessionData = Session::all();
         if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
            $userData = getCurrentUserData($sessionData['user_id']);
            
                if($userData && $userData[0]->company_id != '') {
                    $company_id = $userData[0]->company_id;

                    if(!isset($output['zip']))
                        $output['zip'] = '';

                    if(!isset($output['city']))
                        $output['city'] = '';

                        $html = view('client.includes.clients_addresses', compact('number', 'output', 'state', 'company_id'));		
                        $result = $this->ajax_construct($html, "closeAddrModal();", 'append');		
                        $result["target"] = '#accordion';
              }
        }
		return $result;
	}
		
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $client
	 * @return Response
	 */
	public function edit($item) {
        
        $sessionData = Session::all();
        if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
            $userData = getCurrentUserData($sessionData['user_id']);
            
            if($userData && $userData[0]->company_id != '') {
                $company_id = $userData[0]->company_id;
                $addresses = array();
                //$addresses = $item->address; 	//obj addresses	
                $addresses = Address::select(array('addresses.type', 'addresses.addr_line1', 'addresses.addr_line2', 'addresses.zip', 'addresses.mailing_addr'))->where('client_id', $item)
                ->get();

                $form = "Edit";
                $referral_types = CompanyDataOption::where('type_id', 5)->orderBy('option_name')->lists('option_name', 'id');

                $item = Client::where('id', $item)->get();
                $something = new Client;
                $date_met = '';       
                if($item[0]['date_met'])
                $date_met = $something->getFormatedMetDate($item[0]->date_met);
      
                //$company_id = 1;
                $company = Company::find($company_id);
		
                $createadrr = view('client.includes.addaddrmodal', compact('company_id','company'));
                $addr = "";		
                if($addresses) {
                    $addr = "";
                    foreach($addresses as $output) {
                        $number = $output["id"];				
                        if($output["state_id"])
                            $state = State::find($output["state_id"])->toArray();
                        else 
                            $state = "";
                        $addr .= view('client.includes.clients_addresses', compact('number', 'output', 'state', 'company_id','company'));				
                    }
                }	
            }
        }    
		return view('companyportal.client.edit', compact('item', 'form','date_met', 'createadrr', 'referral_types','addr', 'company_id','company'));
	}
	
	
	public function deleteCompanyClient($id) {
        $clientId =  Request::input('id');       
		//Client::where('id', $id)->delete();		
		if(isset($clientId) && $clientId != '') {
			//Client::where('id', $clientId)->update(['status' => "Deleted"]);
            Client::where('id', $clientId)->update(['is_deleted' => 1]);
		}
        return $this->ajax_construct();
	}	
	
	public function removeCheck() {
		
		$id =  Request::input('item');
		$Client = Client::find($id);
		$token =  Request::input('_token');
        //$rr = isKeyUsedAsForeignKey('transactions', $id);     
		//if (!isKeyUsedAsForeignKey('transactions', $id) ) {
        if(!iskeyUsedTable('transactions','client_id',$id)) {
			return $this->ajax_construct(false, "RemoveCompanyClient({$id}, 'deleteCompanyClient', '{$token}'); ");
		} else {
            $data['array'] = array('message' => array("This Client is in use in a system and can't be deleted."));
			return array('success'=>false, "arr"=>$data);
        }
	}	

        /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {
        
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
         
        
            if($route == 'deleteCompanyClient') {
                $data = $this->$route(Request::input('item'));
            } else {
                $data = $this->$route();
            }
			
            if($route== 'show') {
                return json_encode($data);
            }            
            else if($route== 'removeCheck' && isset($data["success"]) && $data["success"]==false) {
               
                return $this->json_error($data["arr"]);
            }     
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            
           // var_dump($this->json_success($result));exit;
            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('false' => true, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}

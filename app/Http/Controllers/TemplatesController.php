<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use Route;
use App\Template;
use Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
//use App\State;
//use App\Address;
use App\User;
use App\TemplateIncluded;
use App\SheetAccess;
use App\Transaction;
use App\CompanySheet;
use App\TemplateField;
use App\CompanySheetField;
use App\CompanySheetAccess;


class TemplatesController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
        $routepath = Route::getCurrentRoute()->getPath();
        //echo $routepath; die;
        if($routepath != 'templates/update/{template}' && $routepath !='templates/store' && $routepath != 'ajaxTemplates') {
            $something = new HomeController;
            $check = $something->authenticateUserRolePermissionURL($routepath);           
            if(!$check) {
               abort(404, 'Unauthorized action.');         
            }
        }
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {	
		$tableData = json_encode($this->show());
		return view('template.index', compact('tableData'));		
	}
	
	
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($type=false) {
		$json = array();	
		$result = Template::all();
	
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$json[] = array(    'clnumber' => $result[$rKey]->client_number,
					'clname' => $result[$rKey]->name,
					'clpurpose' => str_limit($result[$rKey]->purpose, 200),
					'clcountsheets' => CompanySheet::where('sheet_template', $result[$rKey]->id)->count(),
                    'cldefaultsheets' => !empty($result[$rKey]->default_template) ? 'Yes' : '',
					'clstatus' => $result[$rKey]->status,	
					'actions' => view('template.actions', compact('id'))->render() );
		}			
		return $json;
	}
	
		
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id = false) {
                $data = new \stdClass();
                if($id) {
                    $data->item = CompanySheet::find($id);
                    if(isset($data->item->id)) {
                        $data->fields = CompanySheetField::where('sheet_id', $data->item->id)->lists('field_name');
                        $data->access = CompanySheetAccess::where('sheet_id', $data->item->id)->lists('user_id');
                    }
                }
                
		$form = 'Add';
		$company_id = 1;
		$activeusers = User::where('status', 'Active')->where('company_id', $company_id)->get();	
		return view('template.create', compact('form', 'activeusers', 'data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$addrs = array();
		$input = Request::all();
		$validator = Validator::make($input, Template::$rules_template);		 
		unset($input["_token"]);
		
		if ($validator->passes()) {
		
			$input_fields = array(
						
					'name' ,
					'purpose' ,
					'rows_to_freeze' ,
					'status',
					'all_user_accsess' ,
					'column_order' ,
					'freeze_first_column'					
			);	
			
			$item = new Template();			
			foreach($input as $oKey => $oVal) {
	
				if(in_array($oKey, $input_fields)) {
					$item->$oKey = $oVal;
				}
			}
			 
			$item->save();
			$insertedId = $item->id;
						
			// save addresses for the added Client
			if(isset($input["transaction_fileds"]) && !empty($input["transaction_fileds"])){
	
				foreach($input["transaction_fileds"] as $field) {
	
					$addrObj = new TemplateIncluded();		
					$addrObj->field_name =  $field;					
					$addrObj->template_id = $insertedId;
					$addrObj->save();					
				}
			}

			// save addresses for the added Client
			if(!isset($input["all_user_accsess"]) && isset($input["users"]) && !empty($input["users"])){
			
				foreach($input["users"] as $user) {	
					$uObj = new SheetAccess();
					$uObj->user_id =  $user;
					$uObj->template_id = $insertedId;
					$uObj->save();
				}
			}
			
			return Redirect::to('templates/details/'.$insertedId);			
		}
	
		else{
		
			return redirect()->back()->withInput()->withErrors($validator);
		}
	}
	
	/**
	 * Show the details specified resource.
	 *
	 * @param  int  $client
	 * @return Response
	 */
	public function details($template) {
	
		$temIncludeFields = $template->tplIncludedField->lists('field_name');		
		
		$user_access = array();
		if($template->all_user_accsess!=1){			
			$user_access = SheetAccess::select('name')->join('users', 'users.id', '=', 'sheet_accesses.user_id')->where('template_id', $template->id)->lists('name');
		}		
				
		$trans_labels = Transaction::$client_lables;
		$column_order = Template::$column_order;		
		return view('template.details', compact('template',  'temIncludeFields', 'trans_labels', 'column_order', 'user_access') );
	}
		
	
	
	function getTemplateFields($tpl_id) {
		
		$result = Template::find($tpl_id)->fields;
		$json = array();
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$json[] = array(   
					'name' => $result[$rKey]->field_name,
					'type' => $result[$rKey]->type,
					'tooltip' => $result[$rKey]->tooltip,
					'status' => $result[$rKey]->status,
					'priority_order' => $result[$rKey]->priority_order,					
					'actions' => view('fields.actions', compact('id'))->render() );
		}
		return $json;
		
		
	}
	
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $client
	 * @return Response
	 */
	public function edit($item)	{
		$form = "Edit";	
		$company_id = 1;
		
		$user_access = array();
		if($item->all_user_accsess!=1){
			$user_access = $item->accsess_users->lists("user_id");
		}
		$temIncludeFields = $item->tplIncludedField->lists('field_name');
		
		$activeusers = User::where('status', 'Active')->where('company_id', $company_id)->get();
		return view('template.edit', compact('item', 'form', 'activeusers','user_access', 'temIncludeFields'));
	}
	
	
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($tpl) {	
		$input = Request::all();
		$validator = Validator::make($input, Template::$rules_template);
		unset($input["_token"]);
				
		if ($validator->passes()) {
		
			$input_fields = array(
		
					'name' ,
					'purpose' ,
					'rows_to_freeze' ,
					'status',
					'all_user_accsess' ,
					'column_order' ,
					'freeze_first_column'
			);
				
			if(!isset($input["freeze_first_column"]) )
				$input["freeze_first_column"] = 0;
			
			if(!isset($input["all_user_accsess"]) ) 
				$input["all_user_accsess"] = 0;
			
			
			$item = Template::find($tpl->id);
			foreach($input as $oKey => $oVal) {
                if(in_array($oKey, $input_fields)) {

                	$item->$oKey = $oVal;	                
                }
	        }
	        	                        
	        $item->save();	
	        TemplateIncluded::where('template_id', $tpl->id)->delete();
	        
			// save addresses for the added Client
			if(isset($input["transaction_fileds"]) && !empty($input["transaction_fileds"])) {
		
				foreach($input["transaction_fileds"] as $field) {
		
					$addrObj = new TemplateIncluded();
					$addrObj->field_name =  $field;
					$addrObj->template_id = $tpl->id;
					$addrObj->save();
				}
			}
		
			SheetAccess::where('template_id', $tpl->id)->delete();
			// save addresses for the added Client
			if(!$input["all_user_accsess"] && isset($input["users"]) && !empty($input["users"])){
					
				foreach($input["users"] as $user) {
					$uObj = new SheetAccess();
					$uObj->user_id =  $user;
					$uObj->template_id = $tpl->id;
					$uObj->save();
				}
			}
				
			return Redirect::to('templates/details/'.$tpl->id);
		}
		
		else{			
			
			return redirect()->back()->withInput()->withErrors($validator);
		}
		      
	}
	

	

	public function deleteTemplate() {
	 	//Template::where('id', Request::input("value"))->delete();
        Template::where('id', Request::input('id'))->delete();
        //Template::where('id', Request::input('id'))->update(['status' => "Inactive"]);
		return $this->ajax_construct(false,  "redirectTo('templates'); ");
         	
	}	
	
	public function removeCheck() {
		
		//$id =  Request::input('item');	
		//$Client = Client::find($id);
		/*if (!isKeyUsedAsForeignKey('data_options', $id) ) {
			
			$html = view('data.form', compact('data_types', 'item', 'form'));
			
			return $this->ajax_construct(false, "removeConfirm({$id}, 'option'); ");
		} else
			return array('errors' => 'true', 'message' => 'This Option is in use in a system');*/
        $id =  Request::input('id');
        //echo 'id: '.$id; die;
        $token =  Request::input('_token');
        if(!iskeyUsedTable('company_sheets','sheet_template',$id)) {
            return $this->ajax_construct(false, "RemoveTemplate({$id}, 'deleteTemplate', '{$token}'); ");			
        } 
        else {
            $data['array'] = array('message' => array("This Template is in use and can't be deleted."));
            return array('success'=>false, "arr"=>$data);
        }
	}	

        /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {
               	
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
              				
			$data = $this->$route();			
			
            if($route== 'show') {            	
                return json_encode($data);
            }            
            elseif($route== 'removeCheck' && isset($data["success"]) && $data["success"]==false){
            	
            	return $this->json_error($data["arr"]);
            }
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            
            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('false' => true, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;

use App\User;
use App\TemplateField;
use App\FieldAccess;
use App\Template;
use App\FieldColorValue;
use Route;
use Session;

class FieldsController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $routepath = Route::getCurrentRoute()->getPath();;
		$this->middleware('auth');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{				 
		$template_id = Request::input('tpl');
		$result = Template::find($template_id)->fields;
		$arr = array();
		foreach($result as $rKey => $rVal) {
			$template_id = $template_id;
			$id = $result[$rKey]->id;
			$arr[] = array(
					'id'	=>   $result[$rKey]->id,
					'field_name' => $result[$rKey]->field_name,
					'type' => $result[$rKey]->type,
					'tooltip' => $result[$rKey]->tooltip,
					'status' => $result[$rKey]->status,
					'priority_order' => $result[$rKey]->priority_order,					
					'actions' => view('fields.actions', compact('id', 'template_id'))->render() );
					
		}
		return json_encode($arr);
	}

	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function createField()
	{
		$tpl_id = Request::input("template_id");
		$company_id = 1;
        $sessionData = Session::all();
        if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
             
            $userData = getCurrentUserData($sessionData['user_id']);
            
            if($userData && $userData[0]->company_id != '') {
               $company_id = $userData[0]->company_id;
            }
        }
		$activeusers = User::where('status', 'Active')->where('company_id', $company_id)->get();
		$parent_fields = TemplateField::where('parent_field', NULL)->where('template_id', $tpl_id)->orderBy('field_name')->get()->lists('field_name', 'id');
		$add_color_modal = view('fields.includes.add_color_modal');

		$html = view('fields.create', compact('tpl_id', 'activeusers', 'parent_fields', 'add_color_modal') );
		return $this->ajax_construct($html);
	}

	
	/**
	 * Add address record to the Client
	 *
	 * @return array
	 */
	public function addColorValueRecord(){
		
		$number = substr(time(), -4).rand(0, 100) ;
		$html = view('fields.includes.field_colors', compact('number'));		
		$result = $this->ajax_construct($html, false, 'append');		
		return $result;
	}	
	
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeField()
	{
		$addrs = array();
		$input = Request::input("form-input");
		
		parse_str($input, $output);
		//var_dump($output);exit;
		$validator = Validator::make($output, TemplateField::$rules);
		/*Event::listen('illuminate.query', function($query, $params, $time, $conn)
		 {
				dd(array($query, $params, $time, $conn));
				});*/
	
	
		unset($output["_token"]);
		if ($validator->passes()) { 
						
			$input_fields = array(			
					'field_name' ,
					'type' ,
					'select_options' ,
					'tooltip',
					'priority_order' ,
					'default_bg_color' ,
					'status',
					'template_id',
					'parent_field',
					'all_user_accsess'
			);
			
			if($output['parent_field']==0){
				unset($output['parent_field']);
			}
			
			$item = new TemplateField();	
			foreach($output as $oKey => $oVal) {
			
				if(in_array($oKey, $input_fields)) {
						$item->$oKey = $oVal;
				}
			}
			 
			$item->save();
			$insertedId = $item->id;
			
			if(!isset($output["all_user_accsess"]) && isset($output["users"]) && !empty($output["users"])){
					
				foreach($output["users"] as $user) {
					$uObj = new FieldAccess();
					$uObj->user_id =  $user;
					$uObj->field_id = $insertedId;
					$uObj->save();
				}
			}
			
			if(isset($output["colors"]) && !empty($output["colors"])){
				foreach($output["colors"] as $color) {
					$uObj = new FieldColorValue();
					$uObj->color=  $color["color"];
					$uObj->value =  $color["value"];
					$uObj->field_id = $insertedId;
					$uObj->save();
				}
			}
			
			return $this->ajax_construct();
		}	
		else{
			
			$data['array'] = array('message' => $validator->messages()->all());			
			return array('success'=>false, "errors"=>$data);
		}
	}

	
	function ReOrder(){
		
		$output = Request::input("table");
		//parse_str($input, $output);
		foreach($output as $curent_order=>$fields){
			$item = TemplateField::find($fields["id"]);
			$item->priority_order = $curent_order+1;			
			$item->save();
		}
		
		return $this->ajax_construct();
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editField()
	{
		$item = TemplateField::find(Request::input("id"));		
		$template_id = Request::input("template_id");		
		$company_id = 1;
        $sessionData = Session::all();
        if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
             
            $userData = getCurrentUserData($sessionData['user_id']);
            
            if($userData && $userData[0]->company_id != '') {
               $company_id = $userData[0]->company_id;
            }
        }
		$activeusers = User::where('status', 'Active')->where('company_id', $company_id)->get();
		$parent_fields = TemplateField::where('parent_field', NULL)->where('template_id', $template_id)->orderBy('field_name')->get()->lists('field_name', 'id');
		$user_access=array();
		if($item->all_user_accsess!=1){
			$user_access = $item->accsess_users->lists("user_id");
		}
		
		$color_array = $item->colors_values->toArray();		
	
		$html = view('fields.edit', compact('item', "template_id", "activeusers", "parent_fields", "user_access", 'color_array') );		
		return $this->ajax_construct($html);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateField()	
	{
		$input = Request::input("form-input");
		parse_str($input, $output);
		
		$id = $output["id"];
		unset($output["id"]);
		
		
		$validator = Validator::make($output, TemplateField::$rules);
		$input_fields = array(
					
				'field_name' ,
				'type' ,
				'select_options' ,
				'tooltip',
				'priority_order' ,
				'default_bg_color' ,
				'status',
				'template_id',
				'parent_field',
				'all_user_accsess'
		);
		unset($output["_token"]);
		
		
		if ($validator->passes()) {

			if($output['parent_field']==0){
				$output['parent_field'] = NULL;
			}
			if(!isset($output["all_user_accsess"]) )
				$output["all_user_accsess"] = NULL;
						
			$item = TemplateField::find($id);						
			foreach($output as $oKey => $oVal) {
				if(in_array($oKey, $input_fields)) {
					$item->$oKey = $oVal;
				}	
			}
		
			$item->save();
			
			FieldAccess::where('field_id', $item->id)->delete();
			// save access for the Field
			if(!$output["all_user_accsess"] && isset($output["users"]) && !empty($output["users"])){
					
				foreach($output["users"] as $user) {
					$uObj = new FieldAccess();
					$uObj->user_id =  $user;
					$uObj->field_id = $item->id;
					$uObj->save();
				}
			}
			
			// save color/value for the Field
			FieldColorValue::where('field_id', $item->id)->delete();
			if(isset($output["colors"]) && !empty($output["colors"])){
				foreach($output["colors"] as $color) {
					$uObj = new FieldColorValue();
					$uObj->color=  $color["color"];
					$uObj->value =  $color["value"];
					$uObj->field_id = $item->id;
					$uObj->save();
				}
			}
			
			
			return $this->ajax_construct();
		}
		else{
			$data['array'] = array('message' => $validator->messages()->all());
			return array('success'=>false, "errors"=>$data);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteField()
	{		
		$id =  Request::input('id');		
		$parent = TemplateField::where('parent_field', $id)->exists();
		if (!$parent) {				
			TemplateField::where('id', Request::input("id"))->delete();
			return $this->ajax_construct();
			
		} else{
			
			$data['array'] = array('message' => array("The Field is in use in the system and cannot be deleted."));
			return array('success'=>false, "errors"=>$data);
		}
		
		
		
		
	}
	
		
	/**
	 * Ajax handler
	 *
	 * @return JSON Response
	 */
	public function postAjax() {
	
		 
		 
		if (!Request::has('route')) {
			return $this->json_error(array('error' => 'Missing Parameters'));
		} elseif (!method_exists($this, Request::input('route'))) {
			return $this->json_error(array('error' => 'Invalid Parameters'));
		}
	
		
		$route = Request::input('route');
		$result = array();
		$result['target'] = Request::input('target');
		$result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
	
		$data = $this->$route();
	
		if($route== 'index') {
			return $data;
		}
			
		if( ($route== 'storeField' || $route=='updateField' || $route=='deleteField') && isset($data["success"]) && $data["success"]==false){
			return $this->json_error($data["errors"]);
		}
		
		foreach ($data as $dKey => $dVal) {
			$result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
		}	
		
		
		//var_dump($this->json_success($result));exit;
		return $this->json_success($result);
	}
	
	/**
	 * JSON Success Handler
	 *
	 * @return JSON Response
	 */
	public function json_success($data = false) {
		return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
	}
			
	/**
	 * JSON Error Handler
	 *
	 * @return JSON Response
	 */
	public function json_error($data = false) {
		return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
	}
	
	public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
		$result = array();
	
		if ($html) {
			$result['html'] = $html->render();
		}
		if ($script) {
			$result['script'] = $script;
		}
		if ($append) {
			$result['append'] = $append;
		}
		if ($alert) {
			$result['alert'] = $alert;
		}
		return $result;
	}
}

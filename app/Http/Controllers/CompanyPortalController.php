<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CompanyNotification;
use Auth;
use Request;
use Session;
use App\CompanySheet;
use App\AccessedSheet;
use App\Permission;
use App\Company;
use App\SubscriptionPurchased;
use App\Subscription;
use App\RolePermission;
use App\CompanyDataOption;
use App\State;
use App\State as StateModel;
use App\Company as CompanyModel;
use App\DataOption;
use Illuminate\Support\Facades\Validator;
use Redirect;

class CompanyPortalController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{ 
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()	{
        
		$sheets = $this->mostRecentSheets();		
		$tableData = $this->notifications();
        $companyData = $this->companyData();
		if($sheets) {
			$sheets = array_chunk($sheets, 3);
		}
        return view('companyportal.home', compact('tableData','companyData','sheets'));
	}
	
    
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
    public function companyData() {
        
        $sessionData = Session::all();        
        if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
            $userData = getCurrentUserData($sessionData['user_id']);
            
            if($userData && $userData[0]->company_id != '') {
                $companyId = $userData[0]->company_id;
            }
        }
        $result = Company::getCompanyList($companyId,'Active')->get();
        //echo '<pre/>'; print_r($result);
        	foreach($result as $rKey => $rVal) {
                $id = $result[$rKey]->id;
                $arr[] = array('company_id' => $result[$rKey]->id,
                            'company_number' => $result[$rKey]->company_number,
                            'company_name' => $result[$rKey]->company_name,
                            'primary_contact' => $result[$rKey]->last_name.', '.$result[$rKey]->first_name,
                            'city' => $result[$rKey]->city,
                            'state_id' => $result[$rKey]->state_id, //$statename,					
                            'subscription_name' => $result[$rKey]->subscription_name, //$statename,
                            'status' => $result[$rKey]->status,
                            'actions' => view('companyportal.companyportalactions', compact('id'))->render()
                );
           }		
		return json_encode($arr);
    }
    
    /**
	 * Show the details specified resource.
	 *
	 * @param  int  $client
	 * @return Response
	 */
	public function details($company_id) {
        
		$company = $refsource = $stateName = $companyList = $company_info = array();
		$chksubscriptionexpdate = false;
        
		$company = Company::select(array('companies.id','companies.company_number', 'companies.company_name', 'companies.company_type', 'companies.fname', 'companies.lname', 'companies.email', 'companies.company_title', 'companies.primary_phone', 'companies.alternate_phone', 'companies.addr_line1', 'companies.addr_line2', 'companies.city', 'companies.state_id', 'companies.country_id', 'companies.zip', 'companies.referral_source', 'companies.website', 'companies.linkedin_url', 'companies.facebook_url', 'companies.twitter_url', 'companies.googleplus_url', 'companies.status'))->where('id', $company_id)
		->get();     
        $SubscriptionPurchased = SubscriptionPurchased::select(array('subscription_id','start_date','end_date'))->where('company_id',$company_id)->orderBy('end_date', 'asc')->limit(1)->get();
        
        if(isset($SubscriptionPurchased) && count($SubscriptionPurchased) > 0 && $SubscriptionPurchased[0]['end_date'] != '') {
            
            $currentdate = date('Y-m-d');        
            $subsendDate = substr($SubscriptionPurchased[0]['end_date'],0,10);
            $chksubscriptionexpdate = checkDateExpire($SubscriptionPurchased[0]['end_date'],$currentdate);
        }
            $subscriptionList = Subscription::orderBy('subscription_name')->lists('subscription_name', 'id');
        
		if(isset($company) && count($company) > 0) {
            
			$refsource = CompanyDataOption::select('option_name')->where('id', $company[0]->referral_source)->first();
			$stateName = State::select('state_name')->where('id', $company[0]->state_id)->first();
			$companyList = DataOption::select('option_name')->where('id', $company[0]->company_type)->first();			
			$company_info = $company->toArray();
		}
        
		$lables = Company::$company_lables;
		return view('companyportal.details', compact('company_id','company','company_info','lables','stateName','refsource','companyList','subscriptionList','chksubscriptionexpdate') );
	}
	
    /**
    * This is function to view the edit form for the company on company portal level * 
    *
    * @param interger $id
    *
    * @return Void
    */
	public function edit($id) {
    
		$company_title = CompanyDataOption::where('type_id', 2)->orderBy('option_name')->lists('option_name', 'id');
		$states = StateModel::all()->lists('state_name','state_name');
		$companyList = DataOption::where('type_id', 2)->orderBy('option_name')->lists('option_name', 'id');
		$item = CompanyModel::find($id);
		$form = "Edit";
		$referral_types = DataOption::where('type_id', 3)->orderBy('option_name')->lists('option_name', 'id');
		return view('companyPortal.edit', compact('item', 'form', 'referral_types', 'company_id','company_title','companyList','states'));
	}
    
    /**
    * This is function to update the company details on company portal level * 
    *
    * @param interger $id
    *
    * @return Void / redirect
    */
    public function update($company) {
     
		$addrs = array();
   	   	$input = Request::all();
   	   	
       	$validator = Validator::make($input, Company::$rules_client);           
		   	unset($input["_token"]);         
           	$validation = Company::validateUpdate($input, $company);
             
	         if($validation->passes()) {
                 
           		$input_fields = array(
           			'company_name' ,
					'company_type' ,
           			'fname' ,
           			'lname' ,
           			'email' ,
					'company_title' ,
           			'primary_phone',
           			'alternate_phone',
           			'addr_line1',
           			'addr_line2' ,
           			'city' ,
           			'state_id',
           			'country_id',
           			'zip',
           			'referral_source',
					'website' ,
           			'linkedin_url',
           			'facebook_url',
           			'twitter_url',
           			'googleplus_url',
           			'status'         			           			
           			);           	
           		
           		
           		if(empty($input['referral_source']))           			
           			$input['referral_source'] = NULL;           		
           		
	            $item = CompanyModel::find($company);
				if(isset($input) && count($input) > 0) {
					foreach($input as $oKey => $oVal) {

						if(in_array($oKey, $input_fields)) {
							$item->$oKey = $oVal;	                
						}
					}
                    
				$item->save(); 
                
	           	return Redirect::to('companyportal/details/'.$company);
				} else {
						return Redirect::to('companyportal');
				}
           }                    
           else {
           	 	return redirect()->back()->withInput()->withErrors($validator);
           }         
	}
    
	public function publicPortal() {
		return view('publicportal.home');
	}
    
	// Company portal methods
	
	public function companyPortal() {
		$sheets = $this->mostRecentSheets();		
		$tableData = $this->notifications();
		if($sheets){
			$sheets = array_chunk($sheets, 3);
		}
		
		return view('companyportal.home', compact('tableData','sheets'));
	}
	
	
	public function mostRecentSheets() {
		$arr = array();
		
	/*	Event::listen('illuminate.query', function($query, $params, $time, $conn)
		{
			dd(array($query, $params, $time, $conn));
		});*/
		
		$sheets = AccessedSheet::select(\DB::raw('company_sheets.*, MAX(accessed_sheets.created_at) as ld'))
		->leftJoin('company_sheets', 'accessed_sheets.sheet_id', '=', 'company_sheets.id')
		->where('accessed_sheets.user_id', Auth::user()->id)
		->orderBy('ld', 'DESC')->groupBy('accessed_sheets.sheet_id')->limit(5)->get();

			
		
		//var_dump($sheets->toArray());
		foreach($sheets as $sheet) {
			$arr[] = array(
					'id' => $sheet->id,
					'name' => $sheet->name,
					'purpose' => str_limit($sheet->purpose, 200),
					//'sheet_template' => empty($sheet->sheet_template) ? null : Template::where('id', $sheet->sheet_template)->pluck('name'),
					'status' => $sheet->status,					
			);			
		}	

		return $arr;
	}
		
	public function notifications() {
		$arr = array();
		$res = CompanyNotification::select(array('users.first_name as ufn', 	'users.last_name as uln',  'company_sheets_fields.field_name', 'company_sheets.name', 'clients.last_name as cln', 'clients.first_name as cfn',
				'company_notifications.*', 
				'transactions.mls1', 'transactions.nickname', 'transactions.property_house', 'transactions.property_addr1', 'transactions.property_addr2', 
				'ttt.option_name as transaction_type', 'tst.option_name as transaction_status'
				))-> 
		leftJoin('users', 'company_notifications.sender_id', '=', 'users.id')->
		leftJoin('company_sheets_fields', 'company_notifications.field_id', '=', 'company_sheets_fields.id')->
		leftJoin('company_sheets', 'company_sheets_fields.sheet_id', '=', 'company_sheets.id')->
		
		leftJoin('transactions', 'company_notifications.trans_id', '=', 'transactions.id')->
		leftJoin('clients', 'transactions.client_id', '=', 'clients.id')->
		
		leftJoin('company_data_options as ttt', 'ttt.id', '=', 'transactions.transaction_type')->
		leftJoin('company_data_options as tst', 'tst.id', '=', 'transactions.transaction_status')->
		where('user_id', Auth::user()->id)->orderBy("created_at", "DESC");
		
		
		if( Request::has('filter') && Request::input('filter')!='all' ){
			
			$res->where('company_notifications.type', Request::input('filter'));
		}
		elseif(!Request::has('filter')){
			
			$res->where('company_notifications.type', 'Incompleted');
		}
		
		$result = $res->get();
		
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$type = $result[$rKey]->type;
			
			$transaction = "Transaction Status: ".$result[$rKey]->transaction_status."<br>";
			$transaction .= "Transaction Type: ".$result[$rKey]->transaction_type."<br>";
			$transaction .= "MLS #: ".$result[$rKey]->mls1."<br>";
			$transaction .= "Short Address: ".$result[$rKey]->nickname."<br>";
			$transaction .= "Property House #: ".$result[$rKey]->property_house."<br>";
			$transaction .= "Property Address Line 1: ".$result[$rKey]->property_addr1."<br>";
			$transaction .= "Property Address Line 2: ".$result[$rKey]->property_addr2."<br>";
			
			$arr[] = array('sender' => $result[$rKey]->ufn." ".$result[$rKey]->uln,
					'time' => date('m/d/y h:i:s', strtotime($result[$rKey]->created_at)),
					'message' => str_limit($result[$rKey]->message, 100),
					'sheet' => $result[$rKey]->name,
					'field' => $result[$rKey]->field_name,
					'client' => $result[$rKey]->cfn." ".$result[$rKey]->cln,					
					'transaction' => $transaction,
					'status' => ($result[$rKey]->status=='Read'?'<span class="label label-default">Read</span>':'<span class="label label-danger">Unread</span>'),
					'actions' => view('companyportal.actions', compact('id', 'type'))->render() );
		}		
		return json_encode($arr);
	}
	
	public function view() {         
            $id = Request::input('item');            
            $data = CompanyNotification::select(array('users.first_name as ufn', 	'users.last_name as uln',  'company_sheets_fields.field_name', 'company_sheets.name', 'clients.last_name as cln', 'clients.first_name as cfn',
            		'company_notifications.*',
            		'transactions.mls1', 'transactions.nickname', 'transactions.property_house', 'transactions.property_addr1', 'transactions.property_addr2',
            		'ttt.option_name as transaction_type', 'tst.option_name as transaction_status'
            ))->
            leftJoin('users', 'company_notifications.sender_id', '=', 'users.id')->
            leftJoin('company_sheets_fields', 'company_notifications.field_id', '=', 'company_sheets_fields.id')->
            leftJoin('company_sheets', 'company_sheets_fields.sheet_id', '=', 'company_sheets.id')->
            
            leftJoin('transactions', 'company_notifications.trans_id', '=', 'transactions.id')->
            leftJoin('clients', 'transactions.client_id', '=', 'clients.id')->
            
            leftJoin('company_data_options as ttt', 'ttt.id', '=', 'transactions.transaction_type')->
            leftJoin('company_data_options as tst', 'tst.id', '=', 'transactions.transaction_status')->
            where('company_notifications.id', $id)->
            where('user_id', Auth::user()->id)->orderBy("created_at", "DESC")->first();
            
            $html = view('companyportal.alert', compact('data'));            
            return $this->ajax_construct($html);
		
	}
	
	
	public function setReadAlert()
	{
			
		$id =  Request::input("alertid");
		
		CompanyNotification::where('id', $id)->where('user_id', Auth::user()->id)->update(['status' => "Read"]);
		return $this->notifications();
				
	}
	
	public function completeAlert()
	{
			
		$id =  Request::input("alertid");		
		CompanyNotification::where('id', $id)->where('user_id', Auth::user()->id)->update(['type' => "Completed"]);
		return $this->notifications();
	
	}   
	
	public function postAjax() {
	//echo $route = Request::input('route');
    //ec
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }
            $route = Request::input('route');
       
		
		
		$result = array();
		$result['target'] = Request::input('target');
		$result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";	
		
		 $data = $this->$route();			
		/*if($route== 'show') {
			return json_encode($data);
		}*/
        
        if($route== 'validateajaxPermission' && isset($data["success"]) && $data["success"]==false) {
               
           return $this->json_error($data["arr"]);
        }       
		foreach ($data as $dKey => $dVal) {
			$result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
		}		
		return $this->json_success($result);
	}
	
	/**
	 * JSON Success Handler
	 *
	 * @return JSON Response
	 */
	public function json_success($data = false) {
		return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
	}
	
	/**
	 * JSON Error Handler
	 *
	 * @return JSON Response
	 */
	public function json_error($data = false) {
		return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
	}
	
	public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
		$result = array();
	
		if ($html) {
			$result['html'] = $html->render();
		}
		if ($script) {
			$result['script'] = $script;
		}
		if ($append) {
			$result['append'] = $append;
		}
		if ($alert) {
			$result['alert'] = $alert;
		}
		return $result;
	}	
}
<?php namespace App\Http\Controllers;
use App;
use App\Company;
//use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Request;
use Route;
use App\CompanyDataOption;
use App\DataOption;
use App\Client;
use App\DataType;
use Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use App\State;
use App\Address;
use App\CompanyDataType;
use App\Staff;
use App\User;
use App\Subscription;
use App\SubscriptionPurchased;
use App\BillingHostory;
use App\EmailTemplate;
use App\Template;
use App\State as StateModel;
use App\Company as CompanyModel;
use Hash;
use Mail;
//use Illuminate\Http\Request;

class CompanyController extends Controller {    
	
    /**
    *  Create a new company controller instance and to check route permission 
    *
    * @param onject $request
    *
    * @return void
    */
	public function __construct(Request $request) {
       
        $routepath = Route::getCurrentRoute()->getPath();
        if($routepath != 'company/store' && $routepath != 'company/update/{company}' && $routepath != 'company/search') {
            $something = new HomeController;
            $check = $something->authenticateUserRolePermissionURL($routepath);        
            if(!$check) {
               abort(404, 'Unauthorized action.');         
            }
        }
		$this->middleware('auth');
	}        
	
    /**
    * Display a listing of company 
    *
    * @param void
    *
    * @return void
    */
	public function index() {
		
        $tableData = json_encode(array()); //json_encode($this->show()); 
							
		return view('company.index', compact('tableData'));
	}

    /**
     * Show the form for creating a new company.
     * 
     * @param void
     *
     * @return void
     */
	 public function create() {
		
		$form = 'Add';		   
		$referral_types = DataOption::where('type_id', 3)->orderBy('option_name')->lists('option_name', 'id');
		$companyList = DataOption::where('type_id', 2)->orderBy('option_name')->lists('option_name', 'id');			
		$states = StateModel::all()->lists('state_name','state_name');
		$company_types = CompanyDataType::all()->lists('type_name','id');
		$company_id = \Config::get('custom.company_id');
		$company = CompanyModel::find($company_id);
		$createadrr = view('company.includes.addaddrmodal', compact('company_id', 'company','states'));
		return view('company.create', compact('form', 'referral_types', 'createadrr','company_types','companyList'));			
	}

	/**
	 * Store a newly created company and staff in table.
     * 
     * @param void
	 *
	 * @return void
	 */
	public function store() {
        
   		   $addrs = array();
   		   $input = Request::all();
		  
           $validator = Validator::make($input, Company::$rules_client);  
           
		   unset($input["_token"]);
           if ($validator->passes()) {
               
             $sessionData = Session::all();
             //$user_id = $sessionData['user_id'];
             $user_id = !empty($sessionData['user_id']) ? $sessionData['user_id'] : '';
             //$user_id = !empty($user_id) ? $user_id : '';
             
           	$input_fields = array(   			
           			'company_name' ,
					'company_type' ,
           			'fname' ,
           			'lname' ,
           			'email' ,
					'company_title' ,
           			'primary_phone',
           			'alternate_phone',
           			'addr_line1',
           			'addr_line2' ,
           			'city' ,
           			'state_id',
           			'country_id',
           			'zip',
           			'referral_source',
					'website' ,
           			'linkedin_url',
           			'facebook_url',
           			'twitter_url',
           			'googleplus_url',
           			'status' ,
					'default_state'
           	);  
				 
	            $item = new Company();
	          
	            if(empty($input['referral_source']))
	            	$input['referral_source'] = NULL;
	            
				if(isset($input) && count($input) > 0) {
					
					foreach($input as $oKey => $oVal) {
						if(in_array($oKey, $input_fields)) {
							$item->$oKey = trim($oVal);	                
						}
					}
				           
					$item->save();
					$insertedId = $item->id;
					Company::where('id', $insertedId)->update(['company_number' => "VY{$insertedId}-".substr( time(),-4)]);

					if(isset($insertedId) && $insertedId != '') {	 

                        $password = GUID();
                        $passwordHash = Hash::make($password);
						$newStaff = array(
							'last_name' => !empty($input["lname"]) ? $input["lname"] : NULL,
							'first_name' => !empty($input["fname"]) ? $input["fname"] : NULL,
							'password' => $passwordHash,
							'status' => !empty($input["status"]) ? $input["status"] : NULL,
							//'role_id' => 2, // To Do Dynamic
							'role_id' => \Config::get('custom.company_super_admin_role_id'), //Company Super Administrator to update further
							'mobile_phone' => !empty($input["primary_phone"]) ? $input["primary_phone"] : '',
							'name' => !empty($input["company_name"]) ? $input["company_name"] : NULL,
							'email' => !empty($input["email"]) ? $input["email"] : NULL,
							'primary_contact' => \Config::get('custom.primary_contact'), //default Primary Contact 
							'company_id' => !empty($insertedId) ? $insertedId : ''
						 );

						$staffUser = new User();
						foreach($newStaff as $oKey => $oVal) {

							$staffUser->$oKey = $oVal;            
						}
                        $staffUser['parent_id'] = $user_id;
                      
						$staffUser->save();
						$userId = $staffUser->id;

						if(!empty($userId)) {
							$staffData = array(

								'user_id' => !empty($userId) ? $userId : NULL,
								'company_id' => !empty($insertedId) ? $insertedId : NULL,
								'linkedin_url' => !empty($input["linkedin_url"]) ? $input["linkedin_url"] : '',
								'facebook_url' => !empty($input["facebook_url"]) ? $input["facebook_url"] : '',
								'twitter_url' => !empty($input["twitter_url"]) ? $input["twitter_url"] : '',
								'googleplus_url' => !empty($input["googleplus_url"]) ? $input["googleplus_url"] : ''
							);
							$staff = new Staff();
							foreach($staffData as $oKey => $oVal) {
								$staff->$oKey = $oVal;		
							}
							$itemuser = $staff->save();							
						}
                        
						if(!empty($itemuser) && !empty($userId)) {
							//Mail functionality
							
							 $data = array();
                             $data['name'] = !empty($input["fname"]) ? $input["fname"] : '';
                             $data['company'] = !empty($input["company_name"]) ? $input["company_name"] : '';                    
                             $data['username'] = !empty($input["email"]) ? $input["email"] : '';
                             $data['password'] = !empty($password) ? $password : '';
						
                            $data['toEmail'] = !empty($input["email"]) ? $input["email"] : 'ashishp@chetu.com';
                            $data['name'] = !empty($input["fname"]) ? $input["fname"] : '';
                            $data['OMSCompanyName'] = \Config::get('custom.OMSSupportPh');
                            $data['OMSSupportPh'] = \Config::get('custom.OMSSupportPh');
                            $data['OMSSupportEmail'] = \Config::get('custom.OMSSupportEmail');
                            $data['OMSCompanyLinkUrl'] = \Config::get('custom.OMSCompanyLinkUrl');
                            $emailTemplateItem = EmailTemplate::select(array('email_templates.id','email_templates.email_template_name','email_templates.email_template_subject', 'email_templates.email_template_content'))->where('email_template_name', '=', 'Company Creation')->get();
                            $data['email_template_subject'] = !empty($emailTemplateItem[0]['email_template_subject'])? $emailTemplateItem[0]['email_template_subject']: '';
                            $templateContent = !empty($emailTemplateItem[0]['email_template_content']) ? $emailTemplateItem[0]['email_template_content']: '';
                            if(!empty($templateContent)) {
                                if($template = getEmailFormat($templateContent, $data['name'],$data['company'],$data['OMSCompanyName'],$data['OMSSupportPh'],$data['OMSSupportEmail'],$data['OMSCompanyLinkUrl'],$data['username'],$data['password'])) {

                                    Mail::raw($template, function($message) use ($template,$data) {
                                        $message->from($data['OMSSupportEmail']);
                                        $message->to($data['toEmail']);
                                        $message->setBody($template, 'text/html');
                                        $message->subject($data['email_template_subject']);
                                        $message->sender($data['OMSSupportEmail'], 'OMS Solution');
                                    });
                                }
                           }
					}
                        // functionality to create default template for current company
                            /*$defaultTemplateName = !empty($input["company_name"]) ? $input["company_name"] : '';
                            $defaultTemplateName = $defaultTemplateName.' '.'Default Template';
                            $defaultTemplateName = trim($defaultTemplateName);
                            
                            $newCompanyTemplate = array(
							'name' => $defaultTemplateName,
							'purpose' => 'Current files in escrow',
							'rows_to_freeze' => 1,
							'freeze_first_column' => 1,
							'status' => 'Active',
							'all_user_accsess' => 1,
							'column_order' => 'transactions.coe_date,asc',
							'company_id' => !empty($insertedId) ?$insertedId : '',
							'default_template' => 0, //default Primary Contact 
						 );

						$companyTemplate = new Template();
						foreach($newCompanyTemplate as $oKey => $oVal) {

							$companyTemplate->$oKey = $oVal; 
						}
                      
						$companyTemplate->save();*/
					}                    
					return Redirect::to('company/details/'.$insertedId);
                    
				} else {
					return Redirect::to('company');
				}
           }   
           else {
           	return redirect()->back()->withInput()->withErrors($validator);
           }           
	}
   
    
	/**
	 * Show the company details for specifiec company.
	 *
	 * @param  int  $company_id
     * 
	 * @return void / Response
	 */
	public function details($company_id) {
		
		$company = $refsource = $stateName = $companyList = $company_info = array();
		$chksubscriptionexpdate = false;
        
		$company = Company::select(array('companies.id','companies.company_number', 'companies.company_name', 'companies.company_type', 'companies.fname', 'companies.lname', 'companies.email', 'companies.company_title', 'companies.primary_phone', 'companies.alternate_phone', 'companies.addr_line1', 'companies.addr_line2', 'companies.city', 'companies.state_id', 'companies.country_id', 'companies.zip', 'companies.referral_source', 'companies.website', 'companies.linkedin_url', 'companies.facebook_url', 'companies.twitter_url', 'companies.googleplus_url', 'companies.status'))->where('id', $company_id)->where('status', '!=', 'Deleted')
		->get();
        $SubscriptionPurchased = SubscriptionPurchased::select(array('subscription_id','start_date','end_date'))->where('company_id',$company_id)->orderBy('end_date', 'asc')->limit(1)->get();
        
        if(isset($SubscriptionPurchased) && count($SubscriptionPurchased) > 0 && $SubscriptionPurchased[0]['end_date'] != '') {
            
            $currentdate = date('Y-m-d');  
            $subsendDate = substr($SubscriptionPurchased[0]['end_date'],0,10);
            $chksubscriptionexpdate = checkDateExpire($SubscriptionPurchased[0]['end_date'],$currentdate);
        }
            $subscriptionList = Subscription::orderBy('subscription_name')->where('subscription_status','Active')->lists('subscription_name', 'id');
        
		if(isset($company) && count($company) > 0) {
            
			$refsource = DataOption::select('option_name')->where('id', $company[0]->referral_source)->first();
			$stateName = State::select('state_name')->where('id', $company[0]->state_id)->first();
			$companyList = DataOption::select('option_name')->where('id', $company[0]->company_type)->first();			
			$company_info = $company->toArray();
		}
        
		$lables = Company::$company_lables;
		return view('company.details', compact('company_id','company','company_info','lables','stateName','refsource','companyList','subscriptionList','chksubscriptionexpdate') );
	}
	
	/**
	 * show function to show the type of company.
     * 
     * @param  int  $type
	 *
	 * @return json
	 */
	public function show($type = false) {
		    $json = array();
        
		    if($type) {
            	$result = CompanyDataOption::select(array('company_data_options.id', 'company_data_options.option_name', 'company_data_options.option_desc', 'company_data_options.option_status' , 'company_data_types.type_name'))
            	->join('company_data_types', 'company_data_options.type_id', '=', 'company_data_types.id')->where('company_data_options.type_id',$type)
            	->get();
		    }
		    else {
		    	$result = CompanyDataOption::select(array('company_data_options.id', 'company_data_options.option_name', 'company_data_options.option_desc', 'company_data_options.option_status' , 'company_data_types.type_name'))
		    	->join('company_data_types', 'company_data_options.type_id', '=', 'company_data_types.id')
		    	->get();
		    }
			
			if(isset($result) && count($result) > 0) {
				foreach($result as $rKey => $rVal) {
					$id = $result[$rKey]->id;
					$json[] = array(  
						'type' => $result[$rKey]->type_name,
						'name' => $result[$rKey]->option_name,
						'desc' => str_limit($result[$rKey]->option_desc, 200),
						'status' => $result[$rKey]->option_status,
						'actions' => view('companydata.actions', compact('id'))->render()
					);
				}            
			}   
            return $json;
	}

	/**
	 * search function to search the company.
     * 
     * @param  void
	 *
	 * @return json
	*/
	public function search() {
		
		$input = Request::input('params');
		parse_str($input, $output);
		$json = array();
	
		$res = Company::getAllCompanies('Active');
		if(isset($output["company_number"]) && $output["company_number"]!='') {
			$filtername = $output["company_number"];
			$res->where(function ($query) use ($filtername) {
				$query->where('company_number', 'like',  '%'.$filtername.'%')
				->orWhere('company_name', 'like',  '%'.$filtername.'%')
				->orWhere('fname', 'like',  '%'.$filtername.'%')
				->orWhere('lname', 'like',  '%'.$filtername.'%')
				->orWhere('state_id', 'like',  '%'.$filtername.'%')
				->orWhere('city', 'like',  '%'.$filtername.'%');
			});
		}		
		
		if(isset($output["status"]) && $output["status"]!='') {
			$res->where('status', $output["status"]);
		}
		//$res->where('is_deleted','!=', 1);
		$res->groupBy('companies.id');
		$result = $res->get();
        
		$addr = array();
		if(isset($result) && count($result) > 0) {
			foreach($result as $rKey => $rVal) {
				$id = $result[$rKey]->id;

				if($result[$rKey]->state_id!='') {
					$cityname = $statename = "";
					$addr = State::select('state_name')->where('id', $result[$rKey]->state_id)->first();

					if($addr) {
					$statename = $addr['state_name'];
					}
				} else {
					$statename = '';
				}				

				$json[] = array('company_number' => $result[$rKey]->company_number,
                            'company_name' => $result[$rKey]->company_name,
                            'primary_contact' => $result[$rKey]->lname.', '.$result[$rKey]->fname,
                            'city' => $result[$rKey]->city,
                            'state_id' => $result[$rKey]->state_id, //$statename,					
                            'subscription_name' => $result[$rKey]->subscription_name, //$statename,
                            'status' => $result[$rKey]->status,
                            'actions' => view('company.actions', compact('id'))->render()
               );
			}
		}
		return json_encode($json);
	}
	
	
	/**
	 * Show the edit form for editing the company.
	 *
	 * @param  int  $id
     * 
	 * @return void
	 */
	public function edit($id) {	
        
		$company_title = CompanyDataOption::where('type_id', 2)->orderBy('option_name')->lists('option_name', 'id');          	
		$states = StateModel::all()->lists('state_name','id');
		$companyList = DataOption::where('type_id', 2)->orderBy('option_name')->lists('option_name', 'id');
		$item = CompanyModel::find($id);
		$form = "Edit";
		$referral_types = DataOption::where('type_id', 3)->orderBy('option_name')->lists('option_name', 'id');
		return view('company.edit', compact('item', 'form', 'referral_types', 'company_id','company_title','companyList','states'));
	}
	
	/**
	 * function to check before delete the company.
	 *
	 * @param  void
     * 
	 * @return array
	 */
	public function removeCheck() {
		
		$id =  Request::input('item');       
		$token =  Request::input('_token');
		 		    
		if (iskeyUsedCompany('SubscriptionPurchased', $id)==false ) {
            
			return $this->ajax_construct(false, "RemoveCompany({$id}, 'deleteCompany', '{$token}'); ");			
		} 
		else {
			$data['array'] = array('message' => array("This Company is in use and can't be deleted."));
			return array('success'=>false, "arr"=>$data);
		}
		
	}
	
	/**
	 * function deleteOption to delete the company data option.
	 *
	 * @param  int id
     * 
	 * @return json
	*/
	public function deleteOption($id) {
		
		$json = array();
		CompanyDataOption::where('id', $id)->delete();
	
		$result = CompanyDataOption::select(array('company_data_options.id', 'company_data_options.option_name', 'company_data_options.option_desc', 'company_data_options.option_status' , 'company_data_types.type_name'))
		->join('company_data_types', 'company_data_options.type_id', '=', 'company_data_types.id')
		->get();
	
		if(isset($result) && count($result) > 0) {	
			foreach($result as $rKey => $rVal) {
				$id = $result[$rKey]->id;
				$json[] = array(  
						'type' => $result[$rKey]->type_name,
						'name' => $result[$rKey]->option_name,
						'desc' => str_limit($result[$rKey]->option_desc, 200),
						'status' => $result[$rKey]->option_status,
						'actions' => view('companydata.actions', compact('id'))->render()
				);
			}
		}
		return $json;
	}
	
    /**
	 * function deleteCompany to delete the company.
	 *
	 * @param  int id
     * 
	 * @return ajax
	*/
	public function deleteCompany() {
		
		$companyId =  Request::input('id');
		if(isset($companyId) && $companyId != '') {
			//Company::where('id', $companyId)->update(['status' => "Deleted"]);
            Company::where('id', $companyId)->update(['is_deleted' => 1]);
		}
		return $this->ajax_construct();
	}
    
	/**
	 * Update the specified company resource in table.
	 *
	 * @param  int  $company
     * 
	 * @return void
	*/
	public function update($company) {
        
		$addrs = array();
   	   	$input = Request::all();
   	   	
       	$validator = Validator::make($input, Company::$rules_client); 
           
		   	unset($input["_token"]);	   	
         
           	 $validation = Company::validateUpdate($input, $company);
	         if($validation->passes()) {
           		$input_fields = array(
           			'company_name' ,
					'company_type' ,
           			'fname' ,
           			'lname' ,
           			'email' ,
					'company_title' ,
           			'primary_phone',
           			'alternate_phone',
           			'addr_line1',
           			'addr_line2' ,
           			'city' ,
           			'state_id',
           			'country_id',
           			'zip',
           			'referral_source',
					'website' ,
           			'linkedin_url',
           			'facebook_url',
           			'twitter_url',
           			'googleplus_url',
           			'status'         			           			
           			);           	
           		
           		
           		if(empty($input['referral_source']))           			
           			$input['referral_source'] = NULL;
           		
           		
	            $item = CompanyModel::find($company);
				if(isset($input) && count($input) > 0) {
					foreach($input as $oKey => $oVal) {

						if(in_array($oKey, $input_fields)) {
							$item->$oKey = trim($oVal);
						}
					}				            
				$item->save();
                
	           	return Redirect::to('company/details/'.$company)->with('message', 'Succesfully Updated');
				} else {
						return Redirect::to('company');
				}
           }                      
           else {      	           	
           	 	return redirect()->back()->withInput()->withErrors($validator);
           }         
	}
	
    /**
	 * function to show the list subscription for a company.
	 *
	 * @param  int  $selectedSubscription
     * 
	 * @return json
	*/
    public function subscription($selectedSubscription) {
   
       $subscriptionData = Subscription::select(array('subscriptions.id', 'subscriptions.subscription_name', 'subscriptions.fee_type','subscriptions.cost_per_unit','subscriptions.billing_frequency', 'subscriptions.subscription_users_allowed'))->where('subscription_status', '=', 'Active')->where('id', '=', $selectedSubscription)->get();
      
       $json[] = array(
                'fee_type' => !empty($subscriptionData[0]->fee_type) ? $subscriptionData[0]->fee_type : '',
                'subscription_name' => !empty($subscriptionData[0]->subscription_name) ? $subscriptionData[0]->subscription_name : '',
                'cost_per_unit' => !empty($subscriptionData[0]->cost_per_unit) ? $subscriptionData[0]->cost_per_unit : '',
                'billing_frequency' => !empty($subscriptionData[0]->billing_frequency) ? $subscriptionData[0]->billing_frequency : '',
                'subscription_users_allowed' => !empty($subscriptionData[0]->subscription_users_allowed) ? $subscriptionData[0]->subscription_users_allowed : '',
                'subscription_desc' => !empty($subscriptionData[0]->subscription_desc) ? $subscriptionData[0]->subscription_desc : '',
        );
        return $json;
    }
    
     /**
	 * function to add the subscription package for a company.
	 *
	 * @param  int  $subs
     * 
	 * @return ajax
	*/
    public function purchaseSubscription($subs) {
        
         $input = Request::all();      
         $output2 = $input['form-input'];
         parse_str($output2, $formData);
         unset($formData['_token']);
         $data = Session::all();
         $userData = User::select(array('users.name', 'users.first_name', 'users.last_name'))->where('id', '=', $data['login_82e5d2c56bdd0811318f0cf078b78bfc'])->get();
       
         if(isset($formData['subscription_list']) && $formData['subscription_list'] !='' && $formData['company_id'] != '' && $formData["start_date"] != '' && $formData["end_date"] != '' && $formData["fee_type"] != '') {
            $Subscription = new SubscriptionPurchased();
            $Subscription['subscription_id'] = $formData['subscription_list'];
            $Subscription['company_id'] =  $formData['company_id'];
            $Subscription['user_id'] = ''; 
            $Subscription['start_date'] = $Subscription->setFormatedDate($formData["start_date"]);
            $Subscription['end_date'] = $Subscription->setFormatedDate($formData["end_date"]);     
            $Subscription->save();
            $subscriptionId = $Subscription->id;
            
           if(isset($subscriptionId) && $subscriptionId) {
               
                $invoive_id =  "SUBVY{$subscriptionId}-".substr( time(),-4);
                $billingHistory = array(
                'subscription_id' => !empty($formData['subscription_list']) ? $formData['subscription_list'] : '',
                'user_id' => !empty($data['login_82e5d2c56bdd0811318f0cf078b78bfc']) ? $data['login_82e5d2c56bdd0811318f0cf078b78bfc']: '',
                'company_id' => !empty($formData['company_id'])? $formData['company_id'] : '',
                'subscription_name' => !empty($formData['subscription_list']) ? $formData['subscription_list'] : '',
                'start_date' => $Subscription->setFormatedDate($formData["start_date"]), // To Do Dynamic
                'end_date' => $Subscription->setFormatedDate($formData["end_date"]), // To Do Dynamic
                'user_added_first_name' => !empty($userData[0]->first_name) ? $userData[0]->first_name : $userData[0]->name, //Standard Administrator to update further
                'user_added_last_name' => !empty($userData[0]->last_name) ? $userData[0]->last_name : '',
                'invoice_id' => !empty($invoive_id) ? $invoive_id : '',
                'total_amount' => !empty($formData["fee_type"]) ? $formData["fee_type"] : '',
                'amount_due' => '', //default Primary Contact 
                'item_purchased' => '',
                'status' => 'Active',
                'subscription_desc' => ''   
             );
                $BillingAdded = new BillingHostory();
                foreach($billingHistory as $oKey => $oVal) {

                    $BillingAdded->$oKey = $oVal;          
                }
                $BillingAdded->save();
                $billingId = $BillingAdded->id;
            }
       } else {
           $data['array'] = array('message' => array("Error: Cannot Subscribe Subscription Package, Please select all details! Please try Again."));
           return array('success'=>false, "arr"=>$data);
       }
        //return Redirect::to('company');
       return $this->ajax_construct();
    }
    
    
     /**
	 * function to show the list subscription histry for a company.
	 *
	 * @param  void
     * 
	 * @return ajax
	*/
    public function subscriptionhistory() {
        
       $company_id = Request::input('item');			
		
		$arr = array();	
		
		$result = Staff::staffLists($company_id);		
		
		if($result && count($result) > 0) {
			foreach($result as $rKey => $rVal) {
				
				$id = $result[$rKey]->id;
				$arr[] = array(
						   
					'last_name' => $result[$rKey]->last_name,
					'first_name' => $result[$rKey]->first_name,						
					'title' => $result[$rKey]->name,
					'email' => $result[$rKey]->email,
					'primary_phone' =>  !empty($result[$rKey]->mobile_phone) ? $result[$rKey]->mobile_phone : '',
					'role_id' => !empty($result[$rKey]->role_name) ? $result[$rKey]->role_name : '',
					'primary_contact' => !empty($result[$rKey]->primary_contact) ? 'Yes' : 'No',
					'status' => $result[$rKey]->status,
					'actions' => view('staff.actions', compact('id', 'company_id'))->render()				 
					
				);
			}
		}
		
		$tableData = json_encode($arr);
		
		$table =  view('staff.index', compact('tableData', 'company_id'));
		return $this->ajax_construct($table, "initBootstrapTable(".$tableData."); ");
    }
    
    
    /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {
        	      	
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
         
			if (Request::has('typeId')) {
            		$data = $this->$route(Request::input("typeId"));
			}
			
			elseif($route== 'deleteOption') {
            	            
            	 $data = $this->$route(Request::input('deleteid'));
            }
            elseif($route== 'subscription') {
                $selectedSubscription =  Request::input('ssubscription');
                $data = $this->$route($selectedSubscription);
            }
            elseif($route == 'purchaseSubscription') {
                $data = $this->$route(request::input('form-input'));                
            }
			else {          
					$data = $this->$route();
			}
			
            if($route== 'show' || $route== 'deleteOption' || $route== 'subscription'  || $route== 'subscriptionhistory') {           	
                return json_encode($data);
            }
            
            elseif(($route== 'removeCheck' || $route == 'purchaseSubscription') && isset($data["success"]) && $data["success"]==false) {
            	
            	return $this->json_error($data["arr"]);
            }            	          
            if(isset($data) && count($data) > 0) {
                foreach ($data as $dKey => $dVal) {
                    $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
                }
            }

            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}
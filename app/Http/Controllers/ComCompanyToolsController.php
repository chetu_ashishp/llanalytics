<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Request;
use App\CompanyDataOption;
use App\CompanyDataType;
use Event;
use Redirect;

class ComCompanyToolsController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $create = $this->create();
        $tableData = json_encode($this->show());        
        $data_types = $create->data_types;
		return view('companyportal.companydata.index', compact('create', 'tableData', 'data_types'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($type=false)
	{
		$json = array();
			
		$company_id = Auth::user()->company_id;
				
		if($type){
			$result = CompanyDataOption::select(array('company_data_options.id', 'company_data_options.option_name', 'company_data_options.option_desc', 'company_data_options.option_status' , 'company_data_types.type_name'))
			->join('company_data_types', 'company_data_options.type_id', '=', 'company_data_types.id')->where('company_data_options.type_id',$type)->where('company_id', $company_id)
			->get();
		}
		else{
			$result = CompanyDataOption::select(array('company_data_options.id', 'company_data_options.option_name', 'company_data_options.option_desc', 'company_data_options.option_status' , 'company_data_types.type_name'))
			->join('company_data_types', 'company_data_options.type_id', '=', 'company_data_types.id')->where('company_id', $company_id)
			->get();
		}
			
		
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$json[] = array(    'type' => $result[$rKey]->type_name,
					'name' => $result[$rKey]->option_name,
					'desc' => str_limit($result[$rKey]->option_desc, 200),
					'status' => $result[$rKey]->option_status,
					'actions' => view('companyportal.companydata.actions', compact('id'))->render() );
		}
	
		// echo "<pre>";  var_dump($json);exit;
		return $json;
	}
	
	
	
	
	
	
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            $form = 'Add';
            $data_types = CompanyDataType::orderBy('type_name', 'desc')->lists('type_name', 'id');            
            return view('companyportal.companydata.form', compact('data_types', 'form'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $input = Request::input('form-input');            
           // var_dump($input);exit;            
            parse_str($input, $output);
            
            $output['company_id'] = Auth::user()->company_id;
            
			 $input_fields = array(
                'type_id',
                'option_name',
            	'option_desc',
            	'option_status',
            	'considered_status'	            		
            );
           
            
           if (!isset($output['considered_status'])){
           		$output['considered_status'] = "No";
           }
         
                  

            $item = new CompanyDataOption;
            foreach($output as $oKey => $oVal) {
                if(in_array($oKey, $input_fields)) {
                    $item->$oKey = $oVal;
                }
            }
            $item->save();

            return $this->ajax_construct();
	}

	
		
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
            $form = 'Edit';            
            $id = Request::input('item');            
            $item = CompanyDataOption::find($id);            
            $data_types = CompanyDataType::orderBy('type_name', 'desc')->lists('type_name', 'id');            
            $html = view('companyportal.companydata.form', compact('data_types', 'item', 'form'));            
            return $this->ajax_construct($html);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
            $input = Request::input('form-input');            
            parse_str($input, $output);
            
			 $input_fields = array(
                'type_id',
                'option_name',
            	'option_desc',
            	'option_status',
            	'considered_status'	            		
            );
           
            
           if (!isset($output['considered_status'])){
           		$output['considered_status'] = "No";
           }
         
           
            $item = CompanyDataOption::find($output['id']);
            foreach($output as $oKey => $oVal) {
                if(in_array($oKey, $input_fields)) {
                    $item->$oKey = $oVal;
                }
            }
            $item->save();
            return $this->ajax_construct();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	
	
	public function removeCheck() {
	
		$id =  Request::input('item');
		$dataoption = CompanyDataOption::find($id);
		if (!isKeyUsedAsForeignKey('company_data_options', $id) ) {
	
			$html = view('companyportal.companydata.form', compact('data_types', 'item', 'form'));
			return $this->ajax_construct(false, "confirmRemove({$id}, 'option', 'deleteOption'); ");
		}
		else{
			//return array('success'=>false, 'data'=>array("array"=>array("message"=>array("The Option is in use in the system and cannot be deleted."))));
			
			 $data['array'] = array('message' => array("The Option is in use in the system and cannot be deleted."));
			 return array('success'=>false, "arr"=>$data);
			 
			
		}
	}
	
	public function deleteOption($id){
	
		CompanyDataOption::where('id', $id)->delete();
		
		$result = CompanyDataOption::select(array('company_data_options.id', 'company_data_options.option_name', 'company_data_options.option_desc', 'company_data_options.option_status' , 'company_data_types.type_name'))
		->join('company_data_types', 'company_data_options.type_id', '=', 'company_data_types.id')->get();
			
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$json[] = array(    'type' => $result[$rKey]->type_name,
					'name' => $result[$rKey]->option_name,
					'desc' => str_limit($result[$rKey]->option_desc, 200),
					'status' => $result[$rKey]->option_status,
					'actions' => view('companyportal.companydata.actions', compact('id'))->render()
			);
		}
		return $json;
	}
	
	
        /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {

        	//echo 'dsfsdfsd';exit;        	
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
         
        
			if (Request::has('typeId')) {
            		$data = $this->$route(Request::input("typeId"));
			}
			
			elseif($route== 'deleteOption'){
            	            
            	 $data = $this->$route(Request::input('deleteid'));
            }
			else{				
					$data = $this->$route();
			}
			
			
			if($route== 'removeCheck' && isset($data["success"]) && $data["success"]==false){					
				return $this->json_error($data["arr"]);
			}	
           	elseif($route== 'show' || $route== 'deleteOption') {            	
                return json_encode($data);
            }
            
                     
            
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            
          //  var_dump($result);exit;
            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}

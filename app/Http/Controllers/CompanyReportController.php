<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Request;
use Event;
use Redirect;

use App\CompanyDataType;
use App\CompanyDataOption;
use App\Client;
use App\DataType;
use App\State;
use App\Address;
use App\Transaction;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use Excel;

class CompanyReportController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('reports.index');
	}

	public function salesReport()
	{
		$filters = array(
			array(
				array("field" => "date_type", "label" => "Search By:", "type" => "radio", "options" => array("coe_date" => "COE Date", "contract_date" => "Contract Date"), "default" => "coe_date"),
				array("field" => "report_date", "label" => "", "type" => "date", "report_label" => "Report Date:"),
				array("field" => "grouping", "label" => "Grouping:", "type" => "radio", "options" => array("status" => "Status","referral" => "Referral Source","month" => "Month"), "default" => "status", "required" => true),
				array("field" => "report_format", "label" => "Report Format:", "type" => "radio", "options" => array("html" => "HTML","xls" => "Excel"), "default" => "html", "required" => true, "report_skip" => true),
			),
			$this->get_transaction_filters(),
			$this->get_optional_filters(),
		);
		
		$columns = array(
			array("field" => "transaction_status_option", "label" => "Status", "width" => 25),
			array("field" => "client_name", "label" => "Client", "width" => 25),
			array("field" => "transaction_type_option", "label" => "Type", "width" => 15),
			array("field" => "referral_source_option", "label" => "Referral<br/>Source", "width" => 15),
			array("field" => "sales_price", "label" => "Sales<br/>Price", "totaled" => true, "decimals" => 0, "align" => "right"),
			array("field" => "bonus", "label" => "Bonus", "totaled" => true, "decimals" => 2, "align" => "right"),
			array("field" => "agent_proportio", "label" => "Agent<br/>Proportion", "decimals" => 0, "after" => "%", "width" => 11, "align" => "right"),
			array("field" => "net_sales", "label" => "Net<br/>Sales", "as" => "(sales_price * agent_proportio)", "totaled" => true, "decimals" => 0, "align" => "right"),
			array("field" => "commission_rate", "label" => "Comm<br/>Rate", "decimals" => 1, "after" => "%", "width" => 7, "align" => "right"),
			array("field" => "gross_commission_calc", "label" => "Gross<br/>Comm", "totaled" => true, "decimals" => 2, "width" => 12, "align" => "right"),
			array("field" => "agent_split", "label" => "Agent's<br/>Split", "decimals" => 0, "after" => "%", "width" => 7.5, "align" => "right"),
			array("field" => "agent_split2", "label" => "Agent<br/>Split 2", "decimals" => 0, "after" => "%", "width" => 7.5, "align" => "right"),
			array("field" => "number_of_units", "label" => "Units", "totaled" => true, "decimals" => 2, "align" => "right"),
			array("field" => "broker_flat_fee", "label" => "Broker<br/>Flat Fee", "totaled" => true, "decimals" => 2, "align" => "right"),
			array("field" => "assist_fee", "label" => "Assist<br/>Fee", "totaled" => true, "decimals" => 2, "align" => "right"),
			array("field" => "final_net_commission", "label" => "Final Net<br/>Comm", "totaled" => true, "decimals" => 2, "width" => 12, "align" => "right"),
			array("field" => "contract_date", "label" => "Contract<br/>Date", "type" => "date", "width" => 12, "align" => "right"),
			array("field" => "coe_date", "label" => "COE<br/>Date", "type" => "date", "width" => 12, "align" => "right"),
		);
		
		$errors = array();
		
		$json = array();
		$xlsdata = array();
		$boldrows = array();
		$bluerows = array();
		
		if (!empty($_POST))
		{
			// Validate filters
			foreach ($filters[0] as $filter)
			{
				if (!empty($filter['required']) && empty($_POST[$filter['field']]))
				{
					$errors[] = "{$filter['label']} is required.";
				}
			}
		
			// Merge optional filters
			$opfilters = array();
			
			foreach ($filters as $filk => $filv)
			{
				if ($filk > 0)
				{
					foreach ($filv as $opfilter)
					$opfilters[] = $opfilter;
				}
			}			
		
			$query = Transaction::select(\DB::raw("transactions.*, CONCAT(clients.last_name,', ',clients.first_name) AS client_name, (sales_price * (agent_proportio/100)) as net_sales, gross_commission as gross_commission_calc, typeop.option_name as transaction_type_option, statusop.option_name as transaction_status_option, referralop.option_name as referral_source_option"))
				->leftJoin("clients","transactions.client_id","=","clients.id")
				->leftJoin(\DB::raw("company_data_options typeop"),"transactions.transaction_type","=","typeop.id")
				->leftJoin(\DB::raw("company_data_options statusop"),"transactions.transaction_status","=","statusop.id")
				->leftJoin(\DB::raw("company_data_options referralop"),"transactions.referral_source","=","referralop.id");
			$date_type = Request::input("date_type");
			
			$has_date = false;
			
			if (Request::has("report_date_from"))
			{
				$date_from = date("Y-m-d",strtotime(Request::input("report_date_from")));
				$query->where('transactions.'.$date_type,'>=', $date_from);
				$has_date = true;
			}
			if (Request::has("report_date_to"))
			{
				$date_to = date("Y-m-d",strtotime(Request::input("report_date_to")));
				$query->where('transactions.'.$date_type,'<=', $date_to);
				$has_date = true;
			}
			
			if ($has_date === false)
			{
				$errors[] = "You must enter a value for either the From Date or the To Date.";
			}
			
			if (empty($errors))
			{
				// Consider optional filters
				foreach ($opfilters as $opfilter)
				{
					if (Request::has($opfilter['field']))
					{
						$oparray = is_array($_POST[$opfilter['field']]) ? $_POST[$opfilter['field']] : array($_POST[$opfilter['field']]);
												
						$query->where(function($subquery) use($opfilter, $oparray) {
							foreach ($oparray as $opv)
								$subquery->orWhere("transactions.{$opfilter['field']}","=",$opv);
						});
					}
				}
				
				// Sort by chosen grouping
				$group_field = "";
				
				if (Request::input("grouping") == "status")
					$group_field = "transaction_status_option";
				else if (Request::input("grouping") == "referral")
					$group_field = "referral_source_option";
				else if (Request::input("grouping") == "month")
					$group_field = Request::input("date_type");
					
				$query->orderby($group_field, "asc");
				$query->orderby(Request::input("date_type"), "asc");
				$results = $query->get();

				$last_group = "";
				
				$totalrow = array("num" => 0, "rowtype" => "total");
				$grandtotalrow = array("num" => 0, "rowtype" => "grandtotal");
				
			
				foreach($results as $rKey => $rVal) {
					// If the grouping field is different from the last row,
					// make a summary row before continuing
					if ($group_field == "referral_source_option" && is_null($rVal->$group_field))
						$rVal->$group_field = "No Referral";
					
					if (
						(($group_field == "transaction_status_option" || $group_field == "referral_source_option") && $rVal->$group_field != $last_group) ||
						(($group_field != "transaction_status_option" && $group_field != "referral_source_option") && date("F, Y", strtotime($rVal->$group_field)) != $last_group)
					)
					{
						if (!empty($last_group))
						{
							$row_header = "";
						
							if ($group_field == "transaction_status_option" || $group_field == "referral_source_option")
							{
								$row_header = "Summary For {$last_group} ({$totalrow['num']})";
							}
							else 
							{
								$row_header = "{$last_group} ({$totalrow['num']} detail records)";
							}
							
							$totalrow['transaction_status_option'] = $row_header;
							$totalxls = array();
							
							foreach($columns as $col)
							{
								if (isset($col['decimals']))
								{
									if (isset($totalrow[$col['field']]))
										$totalrow[$col['field']] 		= number_format($totalrow[$col['field']],$col['decimals'],".",",");
								}
								
								// Set columns properly for Excel sheet
								$totalxls[$col['field']] = (isset($totalrow[$col['field']]) ? $totalrow[$col['field']] : null);
							}
							
							$json[] = $this->bold_row($totalrow);
							unset($totalrow);
						
							$boldrows[] = (sizeof($xlsdata));
							$bluerows[] = (sizeof($xlsdata));
							$xlsdata[] = $totalxls;
							unset($totalxls);
						}
						
						if ($group_field == "transaction_status_option")
						{
							$last_group = $rVal->$group_field;
						}
						else if ($group_field == "referral_source_option")
						{
							$last_group = (!empty($rVal->$group_field) ? $rVal->$group_field : "No Referral");
						}
						else
						{
							$last_group = date("F, Y", strtotime($rVal->$group_field));
						}
						
						$totalrow = array("num" => 0);
					}
				
					$colarray = array();
					
					foreach($columns as $col)
					{		
						$colarray[$col['field']] = $rVal->$col['field'];
						
						if (isset($col['decimals']))
							$colarray[$col['field']] = number_format($rVal->$col['field'],$col['decimals'],".","");
						
						if (!empty($col['totaled']) && !empty($colarray[$col['field']]))
						{
							if (empty($totalrow[$col['field']])) $totalrow[$col['field']] = 0;
							if (empty($grandtotalrow[$col['field']])) $grandtotalrow[$col['field']] = 0;
						
							$totalrow[$col['field']] += $colarray[$col['field']];
							$grandtotalrow[$col['field']] += $colarray[$col['field']];
						}
						
						if (isset($col['decimals']))
							$colarray[$col['field']] = number_format($rVal->$col['field'],$col['decimals'],".",",");
						
						if (isset($col['type']))
						{
							if ($col['type'] == "date")
								$colarray[$col['field']] = date("m/d/Y",strtotime($colarray[$col['field']]));
						}
						
						if (isset($col['after']))
							$colarray[$col['field']] .= $col['after'];
					}
									
					$totalrow['num']++;
					$grandtotalrow['num']++;
					
					$json[] = $colarray;
					$xlsdata[] = $colarray;
					unset($colarray);
				}
				
				// If this is the end of the results,
				// make one last summary row followed by a grand total row
				{
					$row_header = "";
						
					if ($group_field == "transaction_status_option" || $group_field == "referral_source_option")
					{
						$row_header = "Summary For {$last_group} ({$totalrow['num']})";
					}
					else 
					{
						$row_header = "{$last_group} ({$totalrow['num']} detail records)";
					}
				
					$totalrow['transaction_status_option'] = $row_header;
					$grandtotalrow['transaction_status_option'] = "Grand Total ({$grandtotalrow['num']} detail records)";
				
					$totalxls = array();
					$grandtotalxls = array();
				
					foreach($columns as $col)
					{					
						if (isset($col['decimals']))
						{
							if (isset($totalrow[$col['field']]))
								$totalrow[$col['field']] 		= number_format($totalrow[$col['field']],$col['decimals'],".",",");
								
							if (isset($grandtotalrow[$col['field']]))	
								$grandtotalrow[$col['field']] 	= number_format($grandtotalrow[$col['field']],$col['decimals'],".",",");
						}
						
						// Set columns properly for Excel sheet
						$totalxls[$col['field']] = (isset($totalrow[$col['field']]) ? $totalrow[$col['field']] : null);
						$grandtotalxls[$col['field']] = (isset($grandtotalrow[$col['field']]) ? $grandtotalrow[$col['field']] : null);
					}
					
					$json[] = $this->bold_row($totalrow);
					unset($totalrow);
					
					$boldrows[] = (sizeof($xlsdata));
					$bluerows[] = (sizeof($xlsdata));
					$xlsdata[] = $totalxls;
					unset($totalxls);
					
					$json[] = $this->bold_row($grandtotalrow);
					
					$boldrows[] = (sizeof($xlsdata));
					$xlsdata[] = $grandtotalxls;
				}
			}
		
			if ($_POST['report_format'] == "xls")
			{
			
				$report_header = $this->filter_display($filters, "Sales Report - ".date("m/d/Y H:i:s"));
				
				$headerrow = array();
			
				foreach ($columns as $col)
				{
					$headerrow[$col['field']] = $col['label'];
				}
								
				foreach ($headerrow as $xk => $xv)
				{
					$headerrow[$xk] = str_replace("<br/>","\n",$headerrow[$xk]);
				}
				
				$xlsdata = array_merge($report_header,array($headerrow),$xlsdata);
			
				$bold_spaces = sizeof($report_header) + sizeof(array($headerrow));
			
				Excel::create('Sales Report '.date("m-d-Y"), function($excel) use($xlsdata, $boldrows, $bluerows, $bold_spaces, $headerrow, $columns) {

					// Set the title
					$excel->setTitle('Sales Report - '.date("m/d/Y"));
	/*
					// Chain the setters
					$excel->setCreator('Maatwebsite')
						  ->setCompany('Maatwebsite');
					$excel->setDescription('A demonstration to change the file properties');			
	*/			
				$excel->sheet('Sales Report', function($sheet) use($xlsdata, $boldrows, $bluerows, $bold_spaces, $headerrow, $columns) {

					// Set layout options
					$sheet->setOrientation('landscape');
					$sheet->getPageSetup()->setFitToPage(true);
					$sheet->getPageSetup()->setFitToWidth(1);
					$sheet->getPageSetup()->setFitToHeight(0);

					$widthColumn = "A";
					$alignColumn = array();
					
					foreach ($columns as $col)
					{
						if (empty($col['width']))
							$col['width'] = 10;
							
						if (!empty($col['align']))
							$alignColumn[$widthColumn] = $col['align'];							
							
						$sheet->setWidth($widthColumn, $col['width']);						
						$widthColumn++;
					}
					
					$sheet->mergeCells("A1:{$widthColumn}1"); 
					$sheet->mergeCells("A2:{$widthColumn}2");
					$sheet->mergeCells("A3:{$widthColumn}3");
					$sheet->mergeCells("A4:{$widthColumn}4");
					
					$sheet->fromArray($xlsdata, null, 'A1', false, false);
					
					$first_letter = "A";
					$last_letter = chr((count($headerrow)-1) + ord("A"));
					$header_range = "{$first_letter}".$bold_spaces.":{$last_letter}".$bold_spaces;
					
					$sheet->getStyle($header_range)->getAlignment()->setWrapText(true);

					foreach ($bluerows as $brow)
					{
						$header_range = "{$first_letter}".($brow+$bold_spaces+1).":{$last_letter}".($brow+$bold_spaces+1);
						
						$sheet->getStyle($header_range)->applyFromArray(
							array(
								'fill' => array(
									'type' => \PHPExcel_Style_Fill::FILL_SOLID,
									'color' => array('rgb' => 'D1E4FC')
								)
							)
						);
					}
					
					$sheet->row($bold_spaces, function($row) {
							$row->setFontWeight('bold');
						});
					
					foreach ($boldrows as $brow)
					{
						$sheet->row(($brow+$bold_spaces+1), function($row) {
							$row->setFontWeight('bold');
						});
					}
					
					foreach ($alignColumn as $acol => $align)
					{
						$fromrow = $acol.($bold_spaces);
						$torow   = $acol.(sizeof($xlsdata));
						
						$sheet->getStyle("{$fromrow}:{$torow}")
						->getAlignment()->applyFromArray(
						array('horizontal' => 'right'));
					}
				});

				})->export('xls');
			}
		}

		//die(nl2br(print_r($xlsdata,true)));
		
		$tableData = json_encode($json);
		return view('reports.salesReport', compact('tableData'))->with(array("filters" => $filters, "columns" => $columns, "boldrows" => $boldrows, "bluerows" => $bluerows, "errs" => $errors));
	}
	
	public function trackRecordReport()
	{
		$filters = array(		
			array(
				array("field" => "date_type", "label" => "Search By:", "type" => "radio", "options" => array("coe_date" => "COE Date", "contract_date" => "Contract Date"), "default" => "coe_date"),
				array("field" => "report_date", "label" => "", "type" => "date", "report_label" => "Report Date:"),
				/*
				array("field" => "grouping", "label" => "Grouping", "type" => "radio", "options" => array("status" => "Status","referral" => "Referral Source","month" => "Month"), "default" => "status", "required" => true),
				*/
				array("field" => "report_format", "label" => "Report Format:", "type" => "radio", "options" => array("html" => "HTML","xls" => "Excel"), "default" => "html", "required" => true, "report_skip" => true),
			),
			$this->get_transaction_filters(),
			$this->get_optional_filters(),
		);
		
		$columns = array(
			array("field" => "client_name", "label" => "Client", "width" => 25),
			array("field" => "referral_source_option", "label" => "Referral<br/>Source", "width" => 18),
			array("field" => "subdivision", "label" => "Subdivision", "width" => 20),
			array("field" => "transaction_type_option", "label" => "Type", "width" => 12),
			array("field" => "transaction_status_option", "label" => "Status", "width" => 25),
			array("field" => "current_list_price", "label" => "Listing<br/>Price", "totaled" => true, "averaged" => true, "decimals" => 0, "width" => 10, "align" => "right"),
			array("field" => "sales_price", "label" => "Selling<br/>Price", "totaled" => true, "averaged" => true, "decimals" => 0, "width" => 10, "align" => "right"),
			array("field" => "listing_percentage", "label" => "% of<br/>List", "as" => "(sales_price * current_list_price)", "averaged" => true, "weighted" => true, "decimals" => 1, "after" => "%", "width" => 8, "align" => "right"),
			array("field" => "listing_date", "label" => "Listing<br/>Date", "type" => "date", "width" => 12, "align" => "right"),
			array("field" => "contract_date", "label" => "Contract<br/>Date", "type" => "date", "width" => 12, "align" => "right"),
			array("field" => "coe_date", "label" => "COE<br/>Date", "type" => "date", "width" => 12, "align" => "right"),
			array("field" => "dom_contract", "label" => "DOM to<br/>Contract", "as" => "DATEDIFF(contract_date,listing_date)", "averaged" => true, "decimals" => 0, "width" => 10, "align" => "right"),
			array("field" => "dom_close", "label" => "DOM to<br/>Close", "as" => "DATEDIFF(coe_date,listing_date)", "averaged" => true, "decimals" => 0, "width" => 10, "align" => "right"),
		);
		
		$errors = array();
		
		$json = array();
		$xlsdata = array();
		$boldrows = array();
		
		if (!empty($_POST))
		{
			// Validate filters
			foreach ($filters[0] as $filter)
			{
				if (!empty($filter['required']) && empty($_POST[$filter['field']]))
				{
					$errors[] = "{$filter['label']} is required.";
				}
			}
			
			// Merge optional filters
			$opfilters = array();
			
			foreach ($filters as $filk => $filv)
			{
				if ($filk > 0)
				{
					foreach ($filv as $opfilter)
					$opfilters[] = $opfilter;
				}
			}	
		
			$query = Transaction::select(\DB::raw("transactions.*, 
				CONCAT(clients.last_name,', ',clients.first_name) AS client_name, 
				((sales_price / current_list_price) * 100) as listing_percentage,
				DATEDIFF(contract_date,listing_date) as dom_contract,
				DATEDIFF(coe_date,listing_date) as dom_close,
				typeop.option_name as transaction_type_option, statusop.option_name as transaction_status_option,
				referralop.option_name as referral_source_option
				"
			))
				->leftJoin("clients","transactions.client_id","=","clients.id")
				->leftJoin(\DB::raw("company_data_options typeop"),"transactions.transaction_type","=","typeop.id")
				->leftJoin(\DB::raw("company_data_options statusop"),"transactions.transaction_status","=","statusop.id")
				->leftJoin(\DB::raw("company_data_options referralop"),"transactions.referral_source","=","referralop.id");
			$date_type = Request::input("date_type");
			
			$has_date = false;
			
			if (Request::has("report_date_from"))
			{
				$date_from = date("Y-m-d",strtotime(Request::input("report_date_from")));
				$query->where('transactions.'.$date_type,'>=', $date_from);
				$has_date = true;
			}
			if (Request::has("report_date_to"))
			{
				$date_to = date("Y-m-d",strtotime(Request::input("report_date_to")));
				$query->where('transactions.'.$date_type,'<=', $date_to);
				$has_date = true;
			}
			
			if ($has_date === false)
			{
				$errors[] = "You must enter a value for either the From Date or the To Date.";
			}
			
			if (empty($errors))
			{
				// Consider optional filters
				foreach ($opfilters as $opfilter)
				{
					if (Request::has($opfilter['field']))
					{
						$oparray = is_array($_POST[$opfilter['field']]) ? $_POST[$opfilter['field']] : array($_POST[$opfilter['field']]);
												
						$query->where(function($subquery) use($opfilter, $oparray) {
							foreach ($oparray as $opv)
								$subquery->orWhere("transactions.{$opfilter['field']}","=",$opv);
						});
					}
				}
				
				// Order by ascending COE date
				$query->orderBy("coe_date","asc");
				
				$results = $query->get();

				$sumrow = array();
				$totalrow = array();
				$averagerow = array();
				$weightedrow = array();
				
				// "Subdivision" column has total of transactions
				$totalrow['subdivision'] = 0;
				
				foreach($results as $rKey => $rVal) {
					$colarray = array();
					
					foreach($columns as $col)
					{
						$colarray[$col['field']] = $rVal->$col['field'];
						if (!empty($col['averaged']))
						{
							if (empty($sumrow[$col['field']]))
								$sumrow[$col['field']] = 0;
							
							$sumrow[$col['field']] += $rVal->$col['field'];
						}
						if (!empty($col['type']) && $col['type'] == "date" && !empty($rVal->$col['field']))
						{
							$colarray[$col['field']] = date("m/d/Y", strtotime($rVal->$col['field']));
						}
						
						if (isset($col['decimals']))
							$colarray[$col['field']] = number_format($rVal->$col['field'],$col['decimals'],".","");
							
						if (isset($col['after']))
							$colarray[$col['field']] .= $col['after'];
					}
					
					$totalrow['subdivision'] ++;
					
					$json[] = $colarray;
					$xlsdata[] = $colarray;
					
					unset($colarray);
				}
				
				// Write Total / Average / Weighted Average rows
				$numrows = sizeof($json);
				
				$totalrow['client_name'] = "Totals:";
				$averagerow['client_name'] = "Average:";
				$weightedrow['client_name'] = "Weighted Average:";
				
				$totalxls = array();
				$averagexls = array();
				$weightedxls = array();
				
				foreach($columns as $col)
				{
					if (!empty($col['totaled']))
					{
						$totalrow[$col['field']] = (!empty($sumrow[$col['field']]) ? $sumrow[$col['field']] : 0);
						if (isset($col['decimals']))
							$totalrow[$col['field']] = number_format($totalrow[$col['field']],$col['decimals'],".","");
						if (isset($col['after']))
							$totalrow[$col['field']] .= $col['after'];	
					}
					
					if (!empty($col['averaged']))
					{
						$averagerow[$col['field']] = (!empty($numrows)) ? ($sumrow[$col['field']] / $numrows) : 0;
						if (isset($col['decimals']))
							$averagerow[$col['field']] = number_format($averagerow[$col['field']],$col['decimals'],".","");
						if (isset($col['after']))
							$averagerow[$col['field']] .= $col['after'];
					}
					
					if (!empty($col['weighted']))
					{
						$weightedrow[$col['field']] = (!empty($sumrow['current_list_price'])) ? (($sumrow['sales_price'] / $sumrow['current_list_price']) * 100) : 0;
						if (isset($col['decimals']))
							$weightedrow[$col['field']] = number_format($weightedrow[$col['field']],$col['decimals'],".","");
						if (isset($col['after']))
							$weightedrow[$col['field']] .= $col['after'];
					}
					
					// Set columns properly for Excel sheet
					$totalxls[$col['field']] = (isset($totalrow[$col['field']]) ? $totalrow[$col['field']] : null);
					$averagexls[$col['field']] = (isset($averagerow[$col['field']]) ? $averagerow[$col['field']] : null);
					$weightedxls[$col['field']] = (isset($weightedrow[$col['field']]) ? $weightedrow[$col['field']] : null);
				}
				
				$boldrows[] = (sizeof($xlsdata));
				$xlsdata[] = $totalxls;
				$boldrows[] = (sizeof($xlsdata));
				$xlsdata[] = $averagexls;
				$boldrows[] = (sizeof($xlsdata));
				$xlsdata[] = $weightedxls;
				
				$json[] = $this->bold_row($totalrow);
				$json[] = $this->bold_row($averagerow);
				$json[] = $this->bold_row($weightedrow);
			}
			
			if ($_POST['report_format'] == "xls")
			{
			
				$report_header = $this->filter_display($filters, "Track Record Report - ".date("m/d/Y H:i:s"));
				
				$headerrow = array();
			
				foreach ($columns as $col)
				{
					$headerrow[$col['field']] = $col['label'];
				}
				
				foreach ($headerrow as $xk => $xv)
				{
					$headerrow[$xk] = str_replace("<br/>","\n",$headerrow[$xk]);
				}
				
				$xlsdata = array_merge($report_header,array($headerrow),$xlsdata);
			
				$bold_spaces = sizeof($report_header) + sizeof(array($headerrow));
			
				Excel::create('Track Record Report '.date("m-d-Y"), function($excel) use($xlsdata, $boldrows, $bold_spaces, $headerrow, $columns) {

					// Set the title
					$excel->setTitle('Track Record Report - '.date("m/d/Y"));
	
				$excel->sheet('Track Record Report', function($sheet) use($xlsdata, $boldrows, $bold_spaces, $headerrow, $columns) {

					// Set layout options
					$sheet->setOrientation('landscape');
					$sheet->getPageSetup()->setFitToPage(true);
					$sheet->getPageSetup()->setFitToWidth(1);
					$sheet->getPageSetup()->setFitToHeight(0);

					$sheet->fromArray($xlsdata, null, 'A1', false, false);

					$widthColumn = "A";
					$alignColumn = array();
					
					
					foreach ($columns as $col)
					{
						if (empty($col['width']))
							$col['width'] = 15;
							
						if (!empty($col['align']))
							$alignColumn[$widthColumn] = $col['align'];														
							
						$sheet->setWidth($widthColumn, $col['width']);						
						$widthColumn++;
					}
					
					$sheet->mergeCells("A1:{$widthColumn}1"); 
					$sheet->mergeCells("A2:{$widthColumn}2");
					$sheet->mergeCells("A3:{$widthColumn}3");
					$sheet->mergeCells("A4:{$widthColumn}4");
					
					$first_letter = "A";
					$last_letter = chr((count($headerrow)-1) + ord("A"));
					$header_range = "{$first_letter}".$bold_spaces.":{$last_letter}".$bold_spaces;
					
					$sheet->getStyle($header_range)->getAlignment()->setWrapText(true);
					
					$sheet->row($bold_spaces, function($row) {
							$row->setFontWeight('bold');
						});
					
					foreach ($boldrows as $brow)
					{
						$sheet->row(($brow+$bold_spaces+1), function($row) {
							$row->setFontWeight('bold');
						});
					}
					
					foreach ($alignColumn as $acol => $align)
					{
						$fromrow = $acol.($bold_spaces);
						$torow   = $acol.(sizeof($xlsdata));
						
						$sheet->getStyle("{$fromrow}:{$torow}")
						->getAlignment()->applyFromArray(
						array('horizontal' => 'right'));
					}		
				});

				})->export('xls');
			}			
		}
		
        $tableData = json_encode($json);
		return view('reports.trackRecordReport', compact('tableData'))->with(array("filters" => $filters, "boldrows" => $boldrows, "columns" => $columns, "errs" => $errors));	
	}
	
	public function salesStatsReport()
	{
		$filters = array(
			array(
				array("field" => "date_type", "label" => "Search By:", "type" => "radio", "options" => array("coe_date" => "COE Date", "contract_date" => "Contract Date"), "default" => "coe_date", "required" => true),
				array("field" => "report_date", "label" => "", "type" => "date", "report_label" => "Report Date:"),
				array("field" => "grouping", "label" => "Data View:", "type" => "radio", "options" => array("final_net_commission" => "Final Net Commission","number_of_units" => "Units",), "default" => "final_net_commission", "required" => true),
				array("field" => "report_format", "label" => "Report Format:", "type" => "radio", "options" => array("html" => "HTML","xls" => "Excel"), "default" => "html", "required" => true, "report_skip" => true),
			),
			$this->get_transaction_filters(),
			$this->get_optional_filters(),	
		);
		
		$columns = array(
			array("field" => "year", "label" => "Year", "width" => 25, "align" => "center"),
			array("field" => "01", "label" => "Jan", "align" => "right"),
			array("field" => "02", "label" => "Feb", "align" => "right"),
			array("field" => "03", "label" => "Mar", "align" => "right"),
			array("field" => "04", "label" => "Apr", "align" => "right"),
			array("field" => "05", "label" => "May", "align" => "right"),
			array("field" => "06", "label" => "Jun", "align" => "right"),
			array("field" => "07", "label" => "Jul", "align" => "right"),
			array("field" => "08", "label" => "Aug", "align" => "right"),
			array("field" => "09", "label" => "Sep", "align" => "right"),
			array("field" => "10", "label" => "Oct", "align" => "right"),
			array("field" => "11", "label" => "Nov", "align" => "right"),
			array("field" => "12", "label" => "Dec", "align" => "right"),
			array("field" => "total", "label" => "Total", "align" => "right"),
		);
		
		$errors = array();
		
		$json = array();
		$xlsdata = array();
		$boldrows = array();
		
		if (!empty($_POST))
		{
			// Validate filters
			foreach ($filters[0] as $filter)
			{
				if (!empty($filter['required']) && empty($_POST[$filter['field']]))
				{
					$errors[] = "{$filter['label']} is required.";
				}
			}
		
			// Merge optional filters
			$opfilters = array();
			
			foreach ($filters as $filk => $filv)
			{
				if ($filk > 0)
				{
					foreach ($filv as $opfilter)
					$opfilters[] = $opfilter;
				}
			}	
		
			$query = Transaction::select(\DB::raw("transactions.*"))
				->leftJoin("clients","transactions.client_id","=","clients.id");
			$date_type = Request::input("date_type");			
			
			$has_date = false;
			
			if (Request::has("report_date_from"))
			{
				$date_from = date("Y-m-d",strtotime(Request::input("report_date_from")));
				$query->where('transactions.'.$date_type,'>=', $date_from);
				$has_date = true;
			}
			if (Request::has("report_date_to"))
			{
				$date_to = date("Y-m-d",strtotime(Request::input("report_date_to")));
				$query->where('transactions.'.$date_type,'<=', $date_to);
				$has_date = true;
			}
			
			if ($has_date === false)
			{
				$errors[] = "You must enter a value for either the From Date or the To Date.";
			}
			
			if (empty($errors))
			{
				// Consider optional filters
				foreach ($opfilters as $opfilter)
				{
					if (Request::has($opfilter['field']))
					{
						$oparray = is_array($_POST[$opfilter['field']]) ? $_POST[$opfilter['field']] : array($_POST[$opfilter['field']]);
												
						$query->where(function($subquery) use($opfilter, $oparray) {
							foreach ($oparray as $opv)
								$subquery->orWhere("transactions.{$opfilter['field']}","=",$opv);
						});
					}
				}		
				/*
				Event::listen('illuminate.query', function($query, $params, $time, $conn)
				{
					dd(array($query, $params, $time, $conn));
				});
				
				*/
				$query->orderby($date_type, "asc");
				$results = $query->get();			
				
			//echo "<pre>"; var_dump($results->toArray());exit;
				
				
				// Sort results by month/year of selected date
				$sorted = array();
				
				$date_type = Request::input("date_type");
				$view_type = Request::input("grouping");			
				
				foreach($results as $rKey => $rVal) {	
					$year 	= date("Y", strtotime($rVal->$date_type));
					$month 	= date("m", strtotime($rVal->$date_type));
					
					if (empty($sorted[$year][$month]))
						$sorted[$year][$month] = 0;
					
					$sorted[$year][$month] += $rVal->$view_type;
				}
				
				// Get first and last year in the set
				$first_year = "";
				$last_year = "";
				
				foreach ($sorted as $sk => $sv)
				{
					if (empty($first_year) || $sk < $first_year)				
						$first_year = $sk;
					
					if (empty($last_year) || $sk > $last_year)				
						$last_year = $sk;
				}
				
				// Go through months of each year and sort out into result array
				$monthtotals = array("year" => "Total (Yr.) / Avg. (Mo.)", "total" => 0);
				
				foreach ($sorted as $year => $months)
				{
					$colarray = array();
					
					foreach ($columns as $cv)
					{
						if ($cv['field'] == "year")
							$colarray[$cv['field']] = $year;
						else
						{
							$colarray[$cv['field']] = 0;
							
							if (empty($monthtotals[$cv['field']]))
								$monthtotals[$cv['field']] = 0;
								
							if ($view_type == "number_of_units")
							{
								$colarray[$cv['field']] = number_format($colarray[$cv['field']],3);
								$monthtotals[$cv['field']] = number_format($monthtotals[$cv['field']],3);
							}
							else
							{
								$colarray[$cv['field']] = $colarray[$cv['field']];
								$monthtotals[$cv['field']] = $monthtotals[$cv['field']];
							}
						}
						
						if (isset($cv['decimals']))
							$colarray[$cv['field']] = number_format($rVal->$cv['field'],$cv['decimals'],".","");
							
						if (isset($cv['after']))
							$colarray[$cv['field']] .= $cv['after'];
					}
					
					// Fill in any missing months
					for ($calmonth = 1; $calmonth <= 12; $calmonth++)
					{
						if ($calmonth < 10)
							$calmonth = "0".$calmonth;
							
						if (empty($months[$calmonth]))
						{
							$months[$calmonth] = 0;
						}
					}

					foreach ($months as $month => $mdata)
					{
						// Set two decimal points in the case of units
						if ($view_type == "number_of_units")
							$mdata = number_format($mdata,2);
						else
							$mdata = $mdata;
					
						$colarray['total'] += $mdata;
						$colarray[$month] = number_format($mdata,2,".","");
						
						$monthtotals[$month] += $mdata;
					}
				
					$monthtotals['total'] += $colarray['total'];
					
					// Set two decimal points in the case of units
					if ($view_type == "number_of_units")
					{
						$colarray['total'] 		= number_format($colarray['total'],2);
						$monthtotals['total'] 	= number_format($monthtotals['total'],2);
						
						// On Excel view, go through the row and replace any zero values with an empty cell.
						if ($_POST['report_format'] == "xls")
						{
							foreach ($colarray as $ck => $colcell)
								if ($colcell == 0)
									$colarray[$ck] = "";
						}
					}
					else
					{
						$colarray['total'] 		= number_format($colarray['total'],2);
						$monthtotals['total'] 	= $monthtotals['total'];
						
						// On Excel view, go through the row and replace any empty cells with zero values.
						if ($_POST['report_format'] == "xls")
						{
							foreach ($colarray as $ck => $colcell)
								if ($colcell == 0)
									$colarray[$ck] = "";
						}
					}
					
					$json[] = $colarray;
					$xlsdata[] = $colarray;
					unset($colarray);
				}
				
				// Add row with month averages and grand total
				$numyears = sizeof($json);
				
				foreach ($monthtotals as $mk => $mv)
				{
					if ($mk != "total" && $mk != "year")
					{
						if ($monthtotals[$mk] != 0)
							$monthtotals[$mk] /= $numyears;	
					}
					
					if ($mk != "year")
					{
						// Set two decimal points in the case of units
						if ($view_type == "number_of_units")
						{
							// On Excel view, go through the row and replace any zero values with an empty cell.
							if ($_POST['report_format'] == "xls" && $mv == 0)
							{
								$monthtotals[$mk] = "";
							}
							else
							{
								$monthtotals[$mk] = number_format($monthtotals[$mk],2,".","");				
							}
						}
						// For Final Net Commission, set two decimals for HTML, zero for Excel
						if ($view_type == "final_net_commission")
						{
							if ($_POST['report_format'] == "xls")
							{
								$monthtotals[$mk] = ($monthtotals[$mk] == 0) ? "" : number_format($monthtotals[$mk],0,".","");	
							}
							else
							{
								$monthtotals[$mk] = number_format($monthtotals[$mk],2,".","");	
							}
						}
					}
				}
				
				$monthtotalxls = array();
				
				foreach($columns as $col)
				{		
					// Set columns properly for Excel sheet
					$monthtotalxls[$col['field']] = (isset($monthtotals[$col['field']]) ? $monthtotals[$col['field']] : null);
				}
				
				$boldrows[] = (sizeof($xlsdata));
				$xlsdata[] = $monthtotalxls;
				
				$monthtotals = $this->bold_row($monthtotals);
				$json[] = $monthtotals;
				/*
				if ($_POST['report_format'] == "html")
				{
					// Align values in the json array
					$aligncolumns = array();
					
					foreach ($columns as $ck => $cv)
					{
						if (!empty($cv['align']))
						{
							$aligncolumns[$cv['field']] = $cv['align'];
						}
					}
					
					foreach ($json as $jk => $jv)
					{
						foreach ($aligncolumns as $ak => $av)
						{
							$json[$jk][$ak] = "<div align=\"{$av}\">{$json[$jk][$ak]}</div>";
						}
					}
				}
				*/
			}
			
			if ($_POST['report_format'] == "xls")
			{
			
				$report_header = $this->filter_display($filters, "Sales Statistics Report - ".date("m/d/Y H:i:s"));
				
				$headerrow = array();
			
				foreach ($columns as $col)
				{
					$headerrow[$col['field']] = $col['label'];
				}
				
				foreach ($headerrow as $xk => $xv)
				{
					$headerrow[$xk] = str_replace("<br/>","\n",$headerrow[$xk]);
				}
				
				$xlsdata = array_merge($report_header,array($headerrow),$xlsdata);
			
				$bold_spaces = sizeof($report_header) + sizeof(array($headerrow));
			
				
				//echo "<pre>";var_dump($boldrows);exit;
				
				Excel::create('Sales Statistics Report '.date("m-d-Y"), function($excel) use($xlsdata, $boldrows, $bold_spaces, $headerrow, $columns) {

					// Set the title
					$excel->setTitle('Sales Statistics Report - '.date("m/d/Y"));
						
				$excel->sheet('Sales Statistics Report', function($sheet) use($xlsdata, $boldrows, $bold_spaces, $headerrow, $columns) {

					// Set layout options
					$sheet->setOrientation('landscape');
					$sheet->getPageSetup()->setFitToPage(true);
					$sheet->getPageSetup()->setFitToWidth(1);
					$sheet->getPageSetup()->setFitToHeight(0);

					$sheet->fromArray($xlsdata, null, 'A1', false, false);

					$widthColumn = "A";
					$alignColumn = array();
					
					foreach ($columns as $col)
					{
						if (empty($col['width']))
							$col['width'] = 10;
							
						if (!empty($col['align']))
							$alignColumn[$widthColumn] = $col['align'];															
							
						$sheet->setWidth($widthColumn, $col['width']);						
						$widthColumn++;
					}
					
					$sheet->mergeCells("A1:{$widthColumn}1"); 
					$sheet->mergeCells("A2:{$widthColumn}2");
					$sheet->mergeCells("A3:{$widthColumn}3");
					$sheet->mergeCells("A4:{$widthColumn}4");
					
					$first_letter = "A";
					$last_letter = chr((count($headerrow)-1) + ord("A"));
					$header_range = "{$first_letter}".$bold_spaces.":{$last_letter}".$bold_spaces;
					
					$sheet->getStyle($header_range)->getAlignment()->setWrapText(true);
					
					$sheet->row($bold_spaces, function($row) {
							$row->setFontWeight('bold');
						});
					
					foreach ($boldrows as $brow)
					{
						$sheet->row(($brow+$bold_spaces+1), function($row) {
							$row->setFontWeight('bold');
						});
					}
					
					foreach ($alignColumn as $acol => $align)
					{
						$fromrow = $acol.($bold_spaces);
						$torow   = $acol.(sizeof($xlsdata));
						
						$sheet->getStyle("{$fromrow}:{$torow}")
						->getAlignment()->applyFromArray(
						array('horizontal' => $align));
					}					
				});

				})->export('xls');
			}			
		}
		
        $tableData = json_encode($json);
		return view('reports.salesStatsReport', compact('tableData'))->with(array("filters" => $filters, "boldrows" => $boldrows, "columns" => $columns, "errs" => $errors));	
	}

	public function comparisonReport()
	{
		$filters = array(
			array(
				array("field" => "coe_date", "label" => "COE On or After Date:", "type" => "date", "report_label" => "COE Date:", "required" => true, "afterline" => "And"),
				array("field" => "contract_date", "label" => "Contract On or Before Date:", "type" => "date", "report_label" => "Contract Date:", "required" => true),
				//array("field" => "grouping", "label" => "Grouping:", "type" => "radio", "options" => array("status" => "Status","referral" => "Referral Source","month" => "Month"), "default" => "status", "required" => true),
				array("field" => "report_format", "label" => "Report Format:", "type" => "radio", "options" => array("html" => "HTML","xls" => "Excel"), "default" => "html", "required" => true, "report_skip" => true),
			),
			$this->get_transaction_filters(),
			//$this->get_optional_filters(),
		);
		
		$columns = array(
			array("field" => "transaction_status_option", "label" => "Status", "width" => 25),
			array("field" => "client_name", "label" => "Client", "width" => 25),
			array("field" => "transaction_type_option", "label" => "Type", "width" => 15),
			array("field" => "referral_source_option", "label" => "Referral<br/>Source", "width" => 15),
			array("field" => "sales_price", "label" => "Sales<br/>Price", "totaled" => true, "decimals" => 0, "align" => "right"),
			array("field" => "bonus", "label" => "Bonus", "totaled" => true, "decimals" => 2, "align" => "right"),
			array("field" => "agent_proportio", "label" => "Agent<br/>Proportion", "decimals" => 0, "after" => "%", "width" => 11, "align" => "right"),
			array("field" => "net_sales", "label" => "Net<br/>Sales", "as" => "(sales_price * agent_proportio)", "totaled" => true, "decimals" => 0, "align" => "right"),
			array("field" => "commission_rate", "label" => "Comm<br/>Rate", "decimals" => 1, "after" => "%", "width" => 7, "align" => "right"),
			array("field" => "gross_commission_calc", "label" => "Gross<br/>Comm", "totaled" => true, "decimals" => 2, "width" => 12, "align" => "right"),
			array("field" => "agent_split", "label" => "Agent's<br/>Split", "decimals" => 0, "after" => "%", "width" => 7.5, "align" => "right"),
			array("field" => "agent_split2", "label" => "Agent<br/>Split 2", "decimals" => 0, "after" => "%", "width" => 7.5, "align" => "right"),
			array("field" => "number_of_units", "label" => "Units", "totaled" => true, "decimals" => 2, "align" => "right"),
			array("field" => "broker_flat_fee", "label" => "Broker<br/>Flat Fee", "totaled" => true, "decimals" => 2, "align" => "right"),
			array("field" => "assist_fee", "label" => "Assist<br/>Fee", "totaled" => true, "decimals" => 2, "align" => "right"),
			array("field" => "final_net_commission", "label" => "Final Net<br/>Comm", "totaled" => true, "decimals" => 2, "width" => 12, "align" => "right"),
			array("field" => "contract_date", "label" => "Contract<br/>Date", "type" => "date", "width" => 12, "align" => "right"),
			array("field" => "coe_date", "label" => "COE<br/>Date", "type" => "date", "width" => 12, "align" => "right"),
		);
		
		$errors = array();
		
		$json = array();
		$xlsdata = array();
		$boldrows = array();
		$bluerows = array();
		
		if (!empty($_POST))
		{
			//die(nl2br(print_r($_POST,true)));
			// Validate filters
			foreach ($filters[0] as $filter)
			{
				if (!empty($filter['required']) && empty($_POST[$filter['field']]))
				{
					$errors[] = "{$filter['label']} is required.";
				}
			}
		
			// Merge optional filters
			$opfilters = array();
			
			foreach ($filters as $filk => $filv)
			{
				if ($filk > 0)
				{
					foreach ($filv as $opfilter)
					$opfilters[] = $opfilter;
				}
			}			
\DB::enableQueryLog();		
			$query = Transaction::select(\DB::raw("transactions.*, CONCAT(clients.last_name,', ',clients.first_name) AS client_name, (sales_price * (agent_proportio/100)) as net_sales, gross_commission as gross_commission_calc, typeop.option_name as transaction_type_option, statusop.option_name as transaction_status_option, referralop.option_name as referral_source_option"))
				->leftJoin("clients","transactions.client_id","=","clients.id")
				->leftJoin(\DB::raw("company_data_options typeop"),"transactions.transaction_type","=","typeop.id")
				->leftJoin(\DB::raw("company_data_options statusop"),"transactions.transaction_status","=","statusop.id")
				->leftJoin(\DB::raw("company_data_options referralop"),"transactions.referral_source","=","referralop.id");
			//$date_type = Request::input("date_type");
			
			
			$date_from = date("Y-m-d",strtotime(Request::input("coe_date")));
			$query->where('transactions.coe_date','>=', $date_from);
			
			$date_to = date("Y-m-d",strtotime(Request::input("contract_date")));
			$query->where('transactions.contract_date','<=', $date_to);
			
/*
			if (strtotime($date_from) > strtotime($date_to))
			{
				$errors[] = "COE Date must be earlier than Contract Date.";
			}
*/
			if (empty($errors))
			{
				// Consider optional filters
				foreach ($opfilters as $opfilter)
				{
					if (Request::has($opfilter['field']))
					{
						$oparray = is_array($_POST[$opfilter['field']]) ? $_POST[$opfilter['field']] : array($_POST[$opfilter['field']]);
												
						$query->where(function($subquery) use($opfilter, $oparray) {
							foreach ($oparray as $opv)
								$subquery->orWhere("transactions.{$opfilter['field']}","=",$opv);
						});
					}
				}
				
				// Sort by chosen grouping
				$group_field = "";
				
				/*if (Request::input("grouping") == "status")
					$group_field = "transaction_status_option";
				else if (Request::input("grouping") == "referral")
					$group_field = "referral_source_option";
				else if (Request::input("grouping") == "month")
					$group_field = Request::input("date_type");*/
					
				//$query->orderby($group_field, "asc");
				//$query->orderby(Request::input("date_type"), "asc");
				//$query->orderby("coe_date", "asc");
				//die(nl2br(print_r($query->get(),true)));
				$results = $query->get();
			
//print_r(\DB::getQueryLog());
//print nl2br(print_r($_POST,true));
//die(nl2br(print_r($results,true)));
				$last_group = "";
				
				$totalrow = array("num" => 0, "rowtype" => "total");
				$grandtotalrow = array("num" => 0, "rowtype" => "grandtotal");
				
			
				foreach($results as $rKey => $rVal) {
				//die(nl2br(print_r($rVal,true)));
					// If the grouping field is different from the last row,
					// make a summary row before continuing
					if ($group_field == "referral_source_option" && is_null($rVal->$group_field))
						$rVal->$group_field = "No Referral";
					
				/*	if (
						(($group_field == "transaction_status_option" || $group_field == "referral_source_option") && $rVal->$group_field != $last_group) ||
						(($group_field != "transaction_status_option" && $group_field != "referral_source_option") && date("F, Y", strtotime($rVal->$group_field)) != $last_group)
					)*/
					{					
						//if (!empty($last_group))
						{
						/*	$row_header = "";
						
							if ($group_field == "transaction_status_option" || $group_field == "referral_source_option")
							{
								$row_header = "Summary For {$last_group} ({$totalrow['num']})";
							}
							else 
							{
								$row_header = "{$last_group} ({$totalrow['num']} detail records)";
							}
							
							$totalrow['transaction_status_option'] = $row_header;*/
							$totalxls = array();
							
							foreach($columns as $col)
							{
								if (isset($col['decimals']))
								{
									if (isset($totalrow[$col['field']]))
										$totalrow[$col['field']] 		= number_format($totalrow[$col['field']],$col['decimals'],".",",");
								}
								
								// Set columns properly for Excel sheet
								$totalxls[$col['field']] = (isset($totalrow[$col['field']]) ? $totalrow[$col['field']] : null);
							}
							
							//$json[] = $this->bold_row($totalrow);
							unset($totalrow);
						
							//$boldrows[] = (sizeof($xlsdata));
							//$bluerows[] = (sizeof($xlsdata));
							//$xlsdata[] = $totalxls;
							unset($totalxls);
						}
						
						/*if ($group_field == "transaction_status_option")
						{
							$last_group = $rVal->$group_field;
						}
						else if ($group_field == "referral_source_option")
						{
							$last_group = (!empty($rVal->$group_field) ? $rVal->$group_field : "No Referral");
						}
						else
						{
							$last_group = date("F, Y", strtotime($rVal->$group_field));
						}*/
						
						$totalrow = array("num" => 0);
					}
				
					$colarray = array();
					
					foreach($columns as $col)
					{		
						$colarray[$col['field']] = $rVal->$col['field'];
						
						if (isset($col['decimals']))
							$colarray[$col['field']] = number_format($rVal->$col['field'],$col['decimals'],".","");
						
						if (!empty($col['totaled']) && !empty($colarray[$col['field']]))
						{
							if (empty($totalrow[$col['field']])) $totalrow[$col['field']] = 0;
							if (empty($grandtotalrow[$col['field']])) $grandtotalrow[$col['field']] = 0;
						
							$totalrow[$col['field']] += $colarray[$col['field']];
							$grandtotalrow[$col['field']] += $colarray[$col['field']];
						}
						
						if (isset($col['decimals']))
							$colarray[$col['field']] = number_format($rVal->$col['field'],$col['decimals'],".",",");
						
						if (isset($col['type']))
						{
							if ($col['type'] == "date")
								$colarray[$col['field']] = date("m/d/Y",strtotime($colarray[$col['field']]));
						}
						
						if (isset($col['after']))
							$colarray[$col['field']] .= $col['after'];
					}
									
					$totalrow['num']++;
					$grandtotalrow['num']++;
					
					$json[] = $colarray;
					$xlsdata[] = $colarray;
					unset($colarray);
				}
			//die(nl2br(print_r($json,true)));	
				// If this is the end of the results,
				// make one last summary row followed by a grand total row
				{
					$row_header = "";
						
					if ($group_field == "transaction_status_option" || $group_field == "referral_source_option")
					{
						$row_header = "Summary For {$last_group} ({$totalrow['num']})";
					}
					else 
					{
						$row_header = "{$last_group} ({$totalrow['num']} detail records)";
					}
				
					$totalrow['transaction_status_option'] = $row_header;
					$grandtotalrow['transaction_status_option'] = "Grand Total ({$grandtotalrow['num']} detail records)";
				
					$totalxls = array();
					$grandtotalxls = array();
				
					foreach($columns as $col)
					{					
						if (isset($col['decimals']))
						{
							if (isset($totalrow[$col['field']]))
								$totalrow[$col['field']] 		= number_format($totalrow[$col['field']],$col['decimals'],".",",");
								
							if (isset($grandtotalrow[$col['field']]))	
								$grandtotalrow[$col['field']] 	= number_format($grandtotalrow[$col['field']],$col['decimals'],".",",");
						}
						
						// Set columns properly for Excel sheet
						$totalxls[$col['field']] = (isset($totalrow[$col['field']]) ? $totalrow[$col['field']] : null);
						$grandtotalxls[$col['field']] = (isset($grandtotalrow[$col['field']]) ? $grandtotalrow[$col['field']] : null);
					}
					
					//$json[] = $this->bold_row($totalrow);
					unset($totalrow);
					
					$boldrows[] = (sizeof($xlsdata));
					//$bluerows[] = (sizeof($xlsdata));
					//$xlsdata[] = $totalxls;
					unset($totalxls);
					
					$json[] = $this->bold_row($grandtotalrow);
					
					$boldrows[] = (sizeof($xlsdata));
					$xlsdata[] = $grandtotalxls;
				}
			}
		
			if ($_POST['report_format'] == "xls")
			{
			
				$report_header = $this->filter_display($filters, "Comparison Report - ".date("m/d/Y H:i:s"));
				
				$headerrow = array();
			
				foreach ($columns as $col)
				{
					$headerrow[$col['field']] = $col['label'];
				}
								
				foreach ($headerrow as $xk => $xv)
				{
					$headerrow[$xk] = str_replace("<br/>","\n",$headerrow[$xk]);
				}
				
				$xlsdata = array_merge($report_header,array($headerrow),$xlsdata);
			
				$bold_spaces = sizeof($report_header) + sizeof(array($headerrow));
			
				Excel::create('Comparison Report '.date("m-d-Y"), function($excel) use($xlsdata, $boldrows, $bluerows, $bold_spaces, $headerrow, $columns) {

					// Set the title
					$excel->setTitle('Comparison Report - '.date("m/d/Y"));
	
				$excel->sheet('Comparison Report', function($sheet) use($xlsdata, $boldrows, $bluerows, $bold_spaces, $headerrow, $columns) {

					// Set layout options
					$sheet->setOrientation('landscape');
					$sheet->getPageSetup()->setFitToPage(true);
					$sheet->getPageSetup()->setFitToWidth(1);
					$sheet->getPageSetup()->setFitToHeight(0);

					$widthColumn = "A";
					$alignColumn = array();
					
					foreach ($columns as $col)
					{
						if (empty($col['width']))
							$col['width'] = 10;
							
						if (!empty($col['align']))
							$alignColumn[$widthColumn] = $col['align'];							
							
						$sheet->setWidth($widthColumn, $col['width']);						
						$widthColumn++;
					}
					
					$sheet->mergeCells("A1:{$widthColumn}1"); 
					$sheet->mergeCells("A2:{$widthColumn}2");
					$sheet->mergeCells("A3:{$widthColumn}3");
					$sheet->mergeCells("A4:{$widthColumn}4");
					
					$sheet->fromArray($xlsdata, null, 'A1', false, false);
					
					$first_letter = "A";
					$last_letter = chr((count($headerrow)-1) + ord("A"));
					$header_range = "{$first_letter}".$bold_spaces.":{$last_letter}".$bold_spaces;
					
					$sheet->getStyle($header_range)->getAlignment()->setWrapText(true);

					foreach ($bluerows as $brow)
					{
						$header_range = "{$first_letter}".($brow+$bold_spaces+1).":{$last_letter}".($brow+$bold_spaces+1);
						
						$sheet->getStyle($header_range)->applyFromArray(
							array(
								'fill' => array(
									'type' => \PHPExcel_Style_Fill::FILL_SOLID,
									'color' => array('rgb' => 'D1E4FC')
								)
							)
						);
					}
					
					$sheet->row($bold_spaces, function($row) {
							$row->setFontWeight('bold');
						});
					
					foreach ($boldrows as $brow)
					{
						$sheet->row(($brow+$bold_spaces+1), function($row) {
							$row->setFontWeight('bold');
						});
					}
					
					foreach ($alignColumn as $acol => $align)
					{
						$fromrow = $acol.($bold_spaces);
						$torow   = $acol.(sizeof($xlsdata));
						
						$sheet->getStyle("{$fromrow}:{$torow}")
						->getAlignment()->applyFromArray(
						array('horizontal' => 'right'));
					}
				});

				})->export('xls');
			}
		}

		//die(nl2br(print_r($xlsdata,true)));
		
		$tableData = json_encode($json);
		return view('reports.comparisonReport', compact('tableData'))->with(array("filters" => $filters, "columns" => $columns, "boldrows" => $boldrows, "bluerows" => $bluerows, "errs" => $errors));
	}
	
	
	function get_transaction_filters() {
		return array(
			array("field" => "transaction_type", "label" => "Transaction Type", "type" => "select", "options" => $this->get_datatypes("Transaction Type"), "default" => ""),
			array("field" => "transaction_status", "label" => "Transaction Status", "type" => "select", "multiple" => true, "options" => $this->get_datatypes("Transaction Status", true), "default" => ""),
		);
	}
	
	function get_optional_filters() {
		return array(
			array("field" => "referral_source", "label" => "Referral Source", "type" => "select", "multiple" => true, "options" => $this->get_datatypes("Referral Source", true), "default" => ""),
			array("field" => "lender", "label" => "Lender", "type" => "select", "options" => $this->get_datatypes("Lender"), "default" => ""),
			array("field" => "title_company", "label" => "Title Company", "type" => "select", "options" => $this->get_datatypes("Title Company"), "default" => ""),
		);
	}
	
	function get_datatypes($typename, $multiple=false) {
		$query = CompanyDataOption::select("company_data_options.id","company_data_options.option_name")
		->where("company_data_types.type_name","=",$typename)
		->join("company_data_types","company_data_options.type_id","=","company_data_types.id")
		->orderBy("company_data_options.option_name","asc")
		->get();
		
		$results = (!$multiple) ? array("" => "--Select--") : array();
		
		foreach ($query as $qv)
		{
			$results[$qv->id] = $qv->option_name;
		}
		
		return $results;
	}
	
	function bold_row($row_data)
	{
		/*foreach ($row_data as $rk => $rv)
		{
			$row_data[$rk] = "<b>{$row_data[$rk]}</b>";
		}*/
		
		return $row_data;
	}
	
	function filter_display($filters, $report_title)
	{
		$display = array();
		
		$display[] = array($report_title);
	
		foreach($filters as $filtergroup)
		{
			foreach ($filtergroup as $filter)
			{
				
				if (!empty($filter['report_skip']))
					continue;
					
				$filterdata = "";
				
				if ($filter['type'] == "date")
				{
					if ($filter['field'] == "coe_date")
						$filterdata .= "COE Dates On Or After {$_POST[$filter['field']]}";
					
					if ($filter['field'] == "contract_date")
						$filterdata .= "Contract Dates On Or BEfore {$_POST[$filter['field']]}";
				}
				else if ($filter['type'] == "radio")
				{
					if (!empty($filter['options']) && array_key_exists($_POST[$filter['field']],$filter['options']))
					{
						$filterdata .= $filter['options'][$_POST[$filter['field']]];
					}
				}
				else if ($filter['type'] == "select" && !empty($_POST[$filter['field']]))
				{
					if (is_array($_POST[$filter['field']]))
					{
						$oplist = array();
						foreach ($_POST[$filter['field']] as $op)
						{
							if (!empty($filter['options'][$op]))
							{
								$oplist[] = $filter['options'][$op];
							}
						}
						
						$filterdata .= implode(", ",$oplist);
					}
					else
					{
						if (!empty($filter['options'][$_POST[$filter['field']]]))
						{
							$filterdata .= $filter['options'][$_POST[$filter['field']]];
						}
					}
				}
				else
				{
					$filterdata .= (!empty($_POST[$filter['field']]));
				}
			/*
				if (empty($filterdata))
				{
					$filterdata = "N/A";
				}
			*/
				if (!empty($filter['report_label']))
					$filter['label'] = $filter['report_label'];
				
				if (!empty($filterdata))
				{
					$display[] = array($filter['label']." ".$filterdata);
				}
			}
		}
		
		$display[] = array(" ", " ");
		
		return $display;
	}
}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Http\Request;

class AuditTrailsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function audit($model) {
	
		
		//var_dump($model) ;exit;
		 
		$event_name = Event::firing();
		$class_name = explode(' ', $event_name);
		$type = $class_name[1];
		$data_after = array();
		$data_before = array();
		$model_array = $model->toArray();
		if (strpos($event_name, 'eloquent.updating') !== false and $type != 'AuditTrail') {
			$id = $model->id;
			$action = 'update';
			$dirty = $model->getDirty();
			foreach ($dirty as $key => $value) {
				$origin_value = $model->getOriginal($key);
				$data_before[$key] = $origin_value;
				$data_after[$key] = $value;
			}
			$trail = new AuditTrail();
			$trail->type_id = $id;
			$trail->type = $type;
			$trail->action = $action;
			$trail->before = serialize($data_before);
			$trail->after = serialize($data_after);
	
			$user_id = 0;
			$auth_user = Auth::user();
			if(!empty($auth_user) && isset($auth_user->id))
				$user_id = $auth_user->id;
			$trail->user_id = $user_id;
			$trail->save();
		} elseif (strpos($event_name, 'eloquent.saving') !== false and $type != 'AuditTrail' and !array_key_exists('id', $model_array)) {
			$action = 'save';
			$data_after = $model->toArray();
			$trail = new AuditTrail();
			$trail->type = $type;
			$trail->action = $action;
			$trail->after = serialize($data_after);
	
			$user_id = 0;
			$auth_user = Auth::user();
			if(!empty($auth_user) && isset($auth_user->id))
				$user_id = $auth_user->id;
			$trail->user_id = $user_id;
			$trail->save();
		} elseif (strpos($event_name, 'eloquent.deleting') !== false and $type != 'AuditTrail') {
			$id = $model->id;
			$action = "delete";
			$data_before = $model->toArray();
			$trail = new AuditTrail();
			$trail->type_id = $id;
			$trail->type = $type;
			$trail->action = $action;
			$trail->before = serialize($data_before);
	
			$user_id = 0;
			$auth_user = Auth::user();
			if(!empty($auth_user) && isset($auth_user->id))
				$user_id = $auth_user->id;
			$trail->user_id = $user_id;
			$trail->save();
		}
	}
}

<?php namespace App\Http\Controllers;


use App\CompanyDataOption;
use App\CompanyDataType;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Transaction;
use Auth;
use Request;
use Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use App\User;
use App\Client;
use App\Address;
use App\State;

use App\accessActions;
use App\accessClients;
use App\accessHowMetTable;
use App\accessPhoneNumbers;
use App\accessSales;
use App\accessSalesSource;
use App\accessSwitchboardItems;

class ImportController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
//die;
		$data = new \stdClass();
		$data->accessClients = accessClients::all();

		//Client::truncate();

		$client_crosswalk = array(
			//'client_number'		=> '',
			'last_name'			=> 'LastName',
			'first_name'		=> 'FirstName',
			'spouse_fname'		=> 'SpouseName',
			'spouse_lname'		=> '',
			'email'				=> '',
			'primary_phone'		=> '',
			'alternate_phone'	=> '',
			'mobile_phone'		=> '',
			'other_phone'		=> '',
			'refsource_details'	=> 'HowMet',
			'notes'				=> 'Other',
			'website'			=> '',
			'linkedin_url'		=> '',
			'facebook_url'		=> '',
			'twitter_url'		=> '',
			'googleplus_url'	=> '',
			//'status'			=> 'Status',
		);

		$address_crosswalk = array(
			'type'			=> '',
			'addr_line1'	=> 'Address1',
			'addr_line2'	=> '',
			'city'			=> 'City1',
			'zip'			=> 'Zip1',
		);

		$transactions_crosswalk = array(
			//'start_date'				=> '',
			//'transaction_status'		=> '',
			'related_trans_id'			=> '',
			'sales_price'				=> 'accesssales',
			'bonus'						=> 'Bonus',
			//'agent_proportio'			=> 'AgentSplit',
			//'commission_rate'			=> 'CommPct',
			//'agent_split'				=> 'BrokerPct',
			//'agent_Split2'				=> '',
			'broker_flat_fee'			=> '',
			'assist_name'				=> 'AssistName',
			'assist_fee'				=> 'AssistFee',
			'gross_commission'			=> '',
			'net_commission'			=> '',
			'final_net_commission'		=> '',
			'sp_of_list'				=> '',
			'sp_of_origin_list'			=> '',
			'dom_to_close'				=> '',
			'mls1'						=> '',
			'mls2'						=> '',
			//'listing_expiration_date'	=> '',
			'origin_list_price'			=> '',
			'current_list_price'		=> 'ListingPrice',
			'lockbox'					=> '',
			'nickname'					=> '',
			'property_house'			=> '',
			'property_addr1'			=> 'PropertyAddress',
			'property_addr2'			=> '',
			'city'						=> '',
			'zip'						=> '',
			'subdivision'				=> 'Subdivision',
			'hoa'						=> '',
			
			//'coe_date'					=> '',
			'other_agent_name'			=> '',
			//'lender'					=> '',
			//'title_company'				=> '',
			'escrow_officer'			=> '',
			//'notes'						=> 'Notes',
			//'client_id'					=> '',
			//'dom_to_contact'			=> '',
			'number_of_units'			=> 'NoOfUnits',



		);

		foreach($data->accessClients as $dKey => $dVal) {
			$item = $dVal;
			//print_r($item);die;
			$client = new Client();
			$client->date_met = $item->DateMet = (date('Y-m-d', strtotime($item->DateMet)) == '1970-01-01') || (date('Y-m-d', strtotime($item->DateMet)) == '0000-00-00') ? null : date('Y-m-d', strtotime($item->DateMet));

			$client->company_id = 1;
			$client->status = 'Active';
			$client->referral_source = CompanyDataOption::where('type_id', CompanyDataType::where('type_name', 'Referral Source')->pluck('id'))->where('option_name', $item->HowMetCat)->pluck('id');

			if(empty(trim($item->HowMetCat))) {
				$client->referral_source = null;
			} elseif(empty($client->referral_source)) {
				$referral_source = new CompanyDataOption();
				$referral_source->type_id = CompanyDataType::where('type_name', 'Referral Source')->pluck('id');
				$referral_source->option_name = !is_null($item->HowMetCat) ? $item->HowMetCat : '';
				$referral_source->option_status = 'Active';
				$referral_source->company_id = 1;
				$referral_source->considered_status = 'No';
				$referral_source->save();
				$client->referral_source = $referral_source->id;
			}

			$client->client_number = "IMP-".$item->ClientID;

			foreach($client_crosswalk as $cKey => $cVal) {
				if(!empty($cVal)) {
					$client->$cKey = !empty($item->$cVal) ? $item->$cVal : '';
				} else {
					$client->$cKey = '';
				}
			}

			$client->save();

			/*
			$address = new Address();
			$address->client_id = $client->id;
			$address->mailing_addr = $item->Mail1 == 1 ? 'Yes' : 'No';
			$address->state_id = !is_null($item->State1) ? State::where('state_abbr', $item->State1)->pluck('id') : null;
			foreach($address_crosswalk as $aKey => $aVal) {
				if(!empty($aVal)) {
					$address->$aKey = !empty($item->$aVal) ? $item->$aVal : '';
				} else {
					$address->$aKey = '';
				}
			}
			$address->save();
			*/

			$accessSales = accessSales::where('ClientID', $item->ClientID)->get();

			foreach($accessSales as $saleItem) {
				$transaction = new Transaction();
				$transaction->start_date = date('Y-m-d');
				$transaction->transaction_type = CompanyDataOption::where('type_id', CompanyDataType::where('type_name', 'Transaction Type')->pluck('id'))->where('option_name', $saleItem->Type)->pluck('id');
				if (is_null($transaction->transaction_type)) {
					$transaction_type = new CompanyDataOption();
					$transaction_type->type_id = CompanyDataType::where('type_name', 'Transaction Type')->pluck('id');
					$transaction_type->option_name = !is_null($saleItem->Type) ? $saleItem->Type : '';
					$transaction_type->option_status = 'Active';
					$transaction_type->company_id = 1;
					$transaction_type->considered_status = 'No';
					$transaction_type->save();

					$transaction->transaction_type = $transaction_type->id;
				}

				$transaction->referral_source = CompanyDataOption::where('type_id', CompanyDataType::where('type_name', 'Referral Source')->pluck('id'))->where('option_name', $saleItem->Source)->pluck('id');

				if (empty(trim($saleItem->Source))) {
					$transaction->referral_source = null;
				} elseif (empty($transaction->referral_source)) {
					$referral_source = new CompanyDataOption();
					$referral_source->type_id = CompanyDataType::where('type_name', 'Referral Source')->pluck('id');
					$referral_source->option_name = !is_null($saleItem->Source) ? $saleItem->Source : '';
					$referral_source->option_status = 'Active';
					$referral_source->company_id = 1;
					$referral_source->considered_status = 'No';
					$referral_source->save();
					$transaction->referral_source = $referral_source->id;
				}

				if ($saleItem->Closed == 1) {
					$transaction->transaction_status = CompanyDataOption::where('type_id', CompanyDataType::where('type_name', 'Transaction Status')->pluck('id'))->where('option_name', 'Closed')->pluck('id');
					if (empty($transaction->transaction_status)) {
						$transaction_status = new CompanyDataOption();
						$transaction_status->type_id = CompanyDataType::where('type_name', 'Transaction Status')->pluck('id');
						$transaction_status->option_name = 'Closed';
						$transaction_status->option_status = 'Active';
						$transaction_status->company_id = 1;
						$transaction_status->considered_status = 'No';
						$transaction_status->save();
						$transaction->transaction_status = $transaction_status->id;
					}
/*
					$transaction->transaction_status = CompanyDataOption::where('type_id', CompanyDataType::where('type_name', 'Transaction Status')->pluck('id'))->where('option_name', 'Transaction Complete')->pluck('id');
					if (empty($transaction->transaction_status)) {
						$transaction_status = new CompanyDataOption();
						$transaction_status->type_id = CompanyDataType::where('type_name', 'Transaction Status')->pluck('id');
						$transaction_status->option_name = 'Transaction Complete';
						$transaction_status->option_status = 'Active';
						$transaction_status->company_id = 1;
						$transaction_status->considered_status = 'No';
						$transaction_status->save();
						$transaction->transaction_status = $transaction_status->id;
					}
*/
				} elseif ($saleItem->InEscrow == 1) {
					$transaction->transaction_status = CompanyDataOption::where('type_id', CompanyDataType::where('type_name', 'Transaction Status')->pluck('id'))->where('option_name', 'In Escrow')->pluck('id');
					if (empty($transaction->transaction_status)) {
						$transaction_status = new CompanyDataOption();
						$transaction_status->type_id = CompanyDataType::where('type_name', 'Transaction Status')->pluck('id');
						$transaction_status->option_name = 'In Escrow';
						$transaction_status->option_status = 'Active';
						$transaction_status->company_id = 1;
						$transaction_status->considered_status = 'No';
						$transaction_status->save();
						$transaction->transaction_status = $transaction_status->id;
					}
				} elseif ($saleItem->Projected == 1) {
					$transaction->transaction_status = CompanyDataOption::where('type_id', CompanyDataType::where('type_name', 'Transaction Status')->pluck('id'))->where('option_name', 'Projected')->pluck('id');
					if (empty($transaction->transaction_status)) {
						$transaction_status = new CompanyDataOption();
						$transaction_status->type_id = CompanyDataType::where('type_name', 'Transaction Status')->pluck('id');
						$transaction_status->option_name = 'Projected';
						$transaction_status->option_status = 'Active';
						$transaction_status->company_id = 1;
						$transaction_status->considered_status = 'No';
						$transaction_status->save();
						$transaction->transaction_status = $transaction_status->id;
					}
				} else {
					$transaction->transaction_status = CompanyDataOption::where('type_id', CompanyDataType::where('type_name', 'Transaction Status')->pluck('id'))->where('option_name', 'Listing Active')->pluck('id');
				}

				$transaction->contract_date = date('Y-m-d', strtotime($saleItem->ContractDate));
				$transaction->contract_date = $transaction->contract_date == '1970-01-01' ? NULL : $transaction->contract_date;
				$transaction->coe_date = date('Y-m-d', strtotime($saleItem->ProjCloseDate));
				$transaction->coe_date = $transaction->coe_date == '1970-01-01' ? NULL : $transaction->coe_date;
				$transaction->listing_date = date('Y-m-d', strtotime($saleItem->ListingDate));
				$transaction->listing_date = $transaction->listing_date == '1970-01-01' ? null : $transaction->listing_date;

				$transaction->listing_expiration_date = null;
				$transaction->start_date = date('Y-m-d');

				$transaction->client_id = $client->id;
				$transaction->lender = !empty($saleItem->Lender) ? $saleItem->Lender : null;

				$transaction->lender = empty($saleItem->Lender) ? null : CompanyDataOption::where('type_id', CompanyDataType::where('type_name', 'Lender')->pluck('id'))->where('option_name', $saleItem->Lender)->pluck('id');

				if (!empty($saleItem->Lender) && empty($transaction->lender)) {
					$lender = new CompanyDataOption();
					$lender->type_id = CompanyDataType::where('type_name', 'Lender')->pluck('id');
					$lender->option_name = $saleItem->Lender;
					$lender->option_status = 'Active';
					$lender->company_id = 1;
					$lender->considered_status = 'No';
					$lender->save();
					$transaction->lender = $lender->id;
				}

				$transaction->title_company = empty($saleItem->TitleCo) ? null : CompanyDataOption::where('type_id', CompanyDataType::where('type_name', 'Title Company')->pluck('id'))->where('option_name', $saleItem->TitleCo)->pluck('id');
				if (!empty($saleItem->TitleCo) && empty($transaction->title_company)) {
					$title_company = new CompanyDataOption();
					$title_company->type_id = CompanyDataType::where('type_name', 'Title Company')->pluck('id');
					$title_company->option_name = $saleItem->TitleCo;
					$title_company->option_status = 'Active';
					$title_company->company_id = 1;
					$title_company->considered_status = 'No';
					$title_company->save();
					$transaction->title_company = $title_company->id;
				}

				//$transaction->dom_to_contract =  0;
				$transaction->notes = $saleItem->ID . " - " . $saleItem->Notes;
				//'agent_proportio'			=> 'AgentSplit',
				//'commission_rate'			=> 'CommPct',
				//'agent_split'				=> 'BrokerPct',
				//'agent_Split2'				=> '',
				$transaction->agent_proportio = $saleItem->AgentSplit * 100;
				$transaction->commission_rate = $saleItem->CommPct * 100;
				$transaction->agent_split = $saleItem->BrokerPct * 100;
				$transaction->agent_split2 = 94;

				foreach ($transactions_crosswalk as $tKey => $tVal) {
					if (!empty($tVal)) {
						$transaction->$tKey = !empty($saleItem->$tVal) ? $saleItem->$tVal : '';
					} else {
						$transaction->$tKey = '';
					}
				}

				$transaction->gross_commission = $transaction->sales_price * $transaction->agent_proportio * $transaction->commission_rate + $transaction->bonus;

				$transaction->save();

				$calculations = Transaction::find($transaction->id);
				$calculations->gross_commission = $calculations->sales_price * ($calculations->agent_proportio / 100) * ($calculations->commission_rate / 100)+ $transaction->bonus;

				$calculations->net_commission = $calculations->gross_commission * (($calculations->agent_split / 100) * ($calculations->agent_split2 / 100)) - $calculations->broker_flat_fee;
				$calculations->final_net_commission = $calculations->net_commission - $calculations->assist_fee;

				if (!empty($calculations->sales_price) && !empty($calculations->current_list_price) && $calculations->current_list_price != 0.00) {
					$calculations->sp_of_list = $calculations->sales_price / $calculations->current_list_price;
				}
				if (!empty($calculations->sales_price) && !empty($calculations->origin_list_price) && $calculations->origin_list_price != 0.00) {
					$calculations->sp_of_origin_list = $calculations->sales_price / $calculations->origin_list_price;
				}
				if(!is_null($calculations->contract_date) && !is_null($calculations->listing_date)) {
					$calculations->dom_to_contract = abs(strtotime($calculations->contract_date) - strtotime($calculations->listing_date));
					$calculations->dom_to_contract = floor($calculations->dom_to_contract/(60*60*24));
				}
				if(!is_null($calculations->coe_date) && !is_null($calculations->listing_date)) {
					$calculations->dom_to_close = abs(strtotime($calculations->coe_date) - strtotime($calculations->listing_date));
					$calculations->dom_to_close = floor($calculations->dom_to_close/(60*60*24));
				}
				$calculations->save();
			}

		}
		//$data->accessSales = accessSales::all();
		print_r($data);die;

		return view('import.' . __FUNCTION__, compact('data'));
	}
}

<?php

namespace App\Http\Controllers;

use App\DataOption;
use App\DataType;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Request;
use Event;
use Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use App\User;
use App\CompanySheet;
use App\CompanySheetField;
use App\CompanySheetAccess;
use App\CompanySheetType;
use App\CompanySheetStatus;
use App\Template;
use App\TemplateIncluded;
use App\SheetAccess;
use App\Client;
use App\Transaction;
use App\Custom;
use App\Settings;

use App\CompanySheetFieldBgColors;
use App\CompanySheetFieldsAccess;
use App\CompanyNotification;
use App\TemplateField;
use App\CompanyDataOption;
use App\CompanySheetTransactionTypeDefaults;
use App\CompanySheetFieldsTransactionTypeDefaults;
use App\AccessedSheet;
use DB;
use Session;

class CompanyPortalSheetController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $current_user = Auth::user();

        $data = new \stdClass();
        $data->tableData = array();

        $sheets = CompanySheet::select(array('company_sheets.*'))
            ->leftJoin('company_sheets_access', 'company_sheets.id', '=', 'company_sheets_access.sheet_id')
            ->where('company_sheets_access.user_id', $current_user->id)
            ->orWhere('company_sheets.all_user_accsess', 1)
            ->get();

        foreach ($sheets as $sheet) {
            $item = array(
                'name' => $sheet->name,
                'purpose' => str_limit($sheet->purpose, 200),
                'sheet_template' => empty($sheet->sheet_template) ? null : Template::where('id', $sheet->sheet_template)->pluck('name'),
                'status' => $sheet->status,
                'actions' => view('companyportal.sheets.actions', compact('sheet'))->render(),
            );
            array_push($data->tableData, $item);
        }

        $data->tableData = json_encode($data->tableData);

        return view('companyportal.sheets.' . __FUNCTION__, compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('companyportal.sheets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Request::input('form-input');
        parse_str($input, $data);

        $validator = Validator::make($data, CompanySheet::$rules_template[$data['sheet_type']]);

        if ($validator->passes()) {
            switch ($data['sheet_type']):
                case 'template':
                    $select_columns = array('purpose', 'rows_to_freeze', 'freeze_first_column', 'status', 'all_user_accsess', 'column_order');
                    $template = Template::select($select_columns)
                        ->where('id', $data['sheet_template'])
                        ->first();

                    foreach ($select_columns as $column) {
                        $data[$column] = $template->$column;
                    }

                    $template_fields = TemplateIncluded::where('template_id', $data['sheet_template'])->get();

                    foreach ($template_fields as $template_field) {
                        $data['transaction_fields'][] = $template_field->field_name;
                    }

                    $template_users = SheetAccess::where('template_id', $data['sheet_template'])->get();

                    foreach ($template_users as $template_user) {
                        $data['users'][] = $template_user->user_id;
                    }

                    $template_custom = TemplateField::where('template_id', $data['sheet_template'])->get();

                case 'scratch':
                    $input_fields = CompanySheet::$input_fields;

                    $item = new CompanySheet;
                    foreach ($data as $dKey => $dVal) {
                        if (in_array($dKey, $input_fields)) {
                            $item->$dKey = $dVal;
                        }
                    }
                    if ($data['sheet_type'] == 'template') {
                        $item->all_transaction_type = $item->all_transaction_status = 1;
                    }
                    $item->save();

                    $transaction_types = array_filter(companyOptionsByType('Transaction Type', 1));

                    if (!empty($transaction_types) && !empty($item->id)) {
                        $color_options = DataOption::where('type_id', DataType::where('type_name', 'Color Option')->pluck('id'))->where('option_status', 'Active')->lists('id', 'option_name');
                        foreach($color_options as $cKey => $cVal) {
                            $color_options[strtoupper($cKey)] = $color_options[strtolower($cKey)] = $cVal;
                        }

                        foreach ($transaction_types as $tKey => $tVal) {
                            if (!empty($data['transaction_type_default_value'][$tKey]) || !empty($data['transaction_type_default_bg_color'][$tKey]) || !empty($data['transaction_type_default_header_color'][$tKey])) {
                                $default = new CompanySheetTransactionTypeDefaults;
                                $default->sheet_id = $item->id;
                                $default->transaction_type = $tKey;
                                $default->value = !empty($data['transaction_type_default_value'][$tKey]) ? $data['transaction_type_default_value'][$tKey] : null;
                                $default->bg_color = !empty($color_options[$data['transaction_type_default_bg_color'][$tKey]]) ? $color_options[$data['transaction_type_default_bg_color'][$tKey]] : null;
                                $default->header_color = !empty($color_options[$data['transaction_type_default_header_color'][$tKey]]) ? $color_options[$data['transaction_type_default_header_color'][$tKey]] : null;
                                $default->save();
                            }
                        }
                    }

                    if (!empty($data['transaction_fields']) && !empty($item->id)) {
                        foreach ($data['transaction_fields'] as $transaction_field) {
                            $field = new CompanySheetField;
                            $field->sheet_id = $item->id;
                            $field->field_name = $transaction_field;
                            $field->save();
                        }
                    }

                    if (!empty($data['users']) && !empty($item->id)) {
                        foreach ($data['users'] as $user) {
                            $access = new CompanySheetAccess;
                            $access->sheet_id = $item->id;
                            $access->user_id = $user;
                            $access->save();
                        }
                    }

                    if (!empty($data['transaction_types']) && !empty($item->id)) {
                        foreach ($data['transaction_types'] as $transaction_type) {
                            $type = new CompanySheetType;
                            $type->sheet_id = $item->id;
                            $type->data_id = $transaction_type;
                            $type->save();
                        }
                    }

                    if (!empty($data['transaction_status']) && !empty($item->id)) {
                        foreach ($data['transaction_status'] as $transaction_status) {
                            $status = new CompanySheetStatus;
                            $status->sheet_id = $item->id;
                            $status->data_id = $transaction_status;
                            $status->save();
                        }
                    }

                    if (!empty($template_custom)) {
                        $template_crosswalk = array('field_name', 'type', 'select_options', 'tooltip', 'priority_order', 'default_bg_color', 'status', 'all_user_accsess');
                        foreach ($template_custom as $custom) {
                            $new = new CompanySheetField;
                            $new->sheet_id = $item->id;
                            foreach ($template_crosswalk as $cross) {
                                $new->$cross = $custom->$cross;
                            }
                            $new->save();
                        }
                    }

                    break;
            endswitch;

            return $this->ajax_construct(false, false, false, false, url('sheet/details/edit/' . $item->id));
        } else {
            return array('type' => 'error', 'message' => $validator->messages()->toArray());
        }
    }
    
    public function copyDuplicateSheets($id) {
        
        $sessionData = Session::all();     
        if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
            $userData = getCurrentUserData($sessionData['user_id']);
            
            if($userData && $userData[0]->company_id != '') {
                $companyId = $userData[0]->company_id;
            }
        }
        //echo 'id: '.$id; 
        //$emailTemplateItem = CompanySheet::select(array('email_templates.id','email_templates.email_template_name','email_templates.email_template_subject', 'email_templates.email_template_content'))->where('email_template_name', '=', 'OMS internal Staff Creation to Admin')->get();
        //$companySheet = CompanySheet::All()->where('id','=',$id);
        if(isset($companyId) && $companyId != '') {
            
            $companySheetList = DB::table('company_sheets')->where('id', $id)->first();
            $lastID = CompanySheet::all()->last()->id;
            $lastID = $lastID + 1;
           // echo '<pre/>';
            //print_r($companySheetList);
            //die;
            //INSERT INTO `orders` SELECT MAX(`order_id`)+1,`container_id`, `order_date`, `receive_date`, `timestamp` FROM `orders` WHERE `order_id` = 1
            $newSheet = new CompanySheet;
            //$new->sheet_id = $item->id;
            if(isset($companySheetList) && count($companySheetList) > 0) {
                foreach ($companySheetList as $cross => $val) {
                    $newSheet->$cross = $val;
                }
            
                $newSheet['id'] = '';
                $date = date('Ymd');
                $newSheet['name'] = $newSheet['name'].' New '.$date;
                $newSheet['company_id'] = $companyId;
                //print_r($newSheet); die;
                $newSheet->save();
               // echo $newSheetId = $newSheet->id;
                // $default = new CompanySheetTransactionTypeDefaults;
                $companySheetsFields = DB::table('company_sheets_fields')->where('sheet_id', $id)->get();
                 if(isset($companySheetsFields) && count($companySheetsFields) > 0) {
                   // print_r($companySheetsFields);
                    // echo '$lastID: '.$lastID;
                   
                    foreach($companySheetsFields as $key => $val) {

                           $default = new CompanySheetField;
                           $default->sheet_id = $lastID;
                           $default->field_name = $companySheetsFields[$key]->field_name;
                           $default->type = $companySheetsFields[$key]->type;
                           $default->select_options = $companySheetsFields[$key]->select_options;
                           $default->tooltip = $companySheetsFields[$key]->tooltip;
                           $default->priority_order = $companySheetsFields[$key]->priority_order;
                           $default->default_bg_color = $companySheetsFields[$key]->default_bg_color;
                           $default->status = $companySheetsFields[$key]->status;
                           $default->parent_field = $companySheetsFields[$key]->parent_field;
                           $default->all_user_accsess = $companySheetsFields[$key]->all_user_accsess;

                           $default->save();
                         // print_r($default);
                    }
                    
                  } 
                 //die('hrerr');
                $companySheetsTypesDefaults = DB::table('company_sheets_types_defaults')->where('sheet_id', $id)->get();
                if(isset($companySheetsTypesDefaults) && count($companySheetsTypesDefaults) > 0) {
                    foreach($companySheetsTypesDefaults as $key => $value) {

                         $companySheetDefault = new CompanySheetTransactionTypeDefaults;
                         $companySheetDefault->sheet_id = $lastID;
                         $companySheetDefault->transaction_type = $companySheetsTypesDefaults[$key]->transaction_type;
                         $companySheetDefault->value = $companySheetsTypesDefaults[$key]->value;
                         $companySheetDefault->bg_color = $companySheetsTypesDefaults[$key]->bg_color;
                         $companySheetDefault->header_color = $companySheetsTypesDefaults[$key]->header_color;
                         //print_r($companySheetsTypesDefaults);
                         $companySheetDefault->save();
                    }
                 } 
              
                // die;
                    $companySheetsAccess = DB::table('company_sheets_access')->where('sheet_id', $id)->get();
                    //print_r($companySheetsTypesDefaults);
                    if(isset($companySheetsAccess) && count($companySheetsAccess) > 0) {
                        foreach($companySheetsAccess as $key => $val) {

                               $SheetsAccess = new CompanySheetAccess;
                               $SheetsAccess->sheet_id = $lastID;
                               $SheetsAccess->user_id = $companySheetsAccess[$key]->user_id;                  
                               $SheetsAccess->save();
                              // print_r($SheetsAccess);
                        }
                    }   
                    
                    $companySheetsType = DB::table('company_sheets_types')->where('sheet_id', $id)->get();
                    if(isset($companySheetsType) && count($companySheetsType) > 0) {
                        //print_r($companySheetsTypesDefaults);
                        foreach($companySheetsType as $key => $val) {

                               $CompanySheetType = new CompanySheetType;
                               $CompanySheetType->sheet_id = $lastID;
                               $CompanySheetType->data_id = $companySheetsType[$key]->data_id;              
                               $CompanySheetType->save();
                               //print_r($CompanySheetType);
                        }
                    }
                        $companySheetsStatuses = DB::table('company_sheets_statuses')->where('sheet_id', $id)->get();
                        if(isset($companySheetsStatuses) && count($companySheetsStatuses) > 0) {
                            //print_r($companySheetsTypesDefaults);
                            foreach($companySheetsStatuses as $key => $val) {

                                   $CompanySheetStatuses = new CompanySheetStatus;
                                   $CompanySheetStatuses->sheet_id = $lastID;
                                   $CompanySheetStatuses->data_id = $companySheetsStatuses[$key]->data_id;              
                                   $CompanySheetStatuses->save();
                                  // print_r($CompanySheetStatuses);
                            }                    
                        }
                    
                  
                 
              
            }        
        }  
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        /*
         * Saving to access log
         */

        $as = new AccessedSheet();
        $as->user_id = Auth::user()->id;
        $as->sheet_id = $id;
        $as->save();
        /*
         * end Saving to access log
         */
        
        $data = new \stdClass();
        $data->auth = Auth::user();
        $data->sheet_id = $id;
        $data->sheet = CompanySheet::find($data->sheet_id);
        $data->rows = $data->options = new \stdClass();
        $data->gridData = array();
        $data->company_id = 1;
        $data->item = CompanySheet::find($id);
        $data->field_types = CompanySheet::$field_types;
        $data->transactions = array();
        $data->transaction_type_defaults = array();
        
        $sessionData = Session::all();  
        if(isset($sessionData['user_id']) && $sessionData['user_id'] != '') {
            $userData = getCurrentUserData($sessionData['user_id']);
            
            if($userData && $userData[0]->company_id != '') {
                $data->company_id = $userData[0]->company_id;
            }
        }
        $sheet_type_defaults = CompanySheetTransactionTypeDefaults::where('sheet_id', $id)->get();

        foreach($sheet_type_defaults as $sheet_type_default) {
            $data->transaction_type_defaults[$sheet_type_default->transaction_type] = array(
                'value'         => $sheet_type_default->value,
                'bg_color'      => DataOption::where('id', $sheet_type_default->bg_color)->pluck('option_name'),
                'header_color'  => DataOption::where('id', $sheet_type_default->header_color)->pluck('option_name'),
            );
        }

        $data->clients = Client::where('company_id', $data->company_id)
            ->where('status', 'Active')
            ->get();
        //echo '<pre/>';
        // print_r($data->clients); die;
        $transaction_types = empty($data->item->all_transaction_type) ? CompanySheetType::where('sheet_id', $data->item->id)->lists('data_id') : null;
        $transaction_status = empty($data->item->all_transaction_status) ? CompanySheetStatus::where('sheet_id', $data->item->id)->lists('data_id') : null;
        //print_r($transaction_status); die;
        //echo  ' $data->item->id : '.$data->item->id;
        $company_transaction_fields = CompanySheetField::where('sheet_id', $data->item->id)
            ->whereIn('field_name', array_keys(CompanySheet::$transaction_fields))
            ->lists('field_name');
        //print_r($company_transaction_fields); die;
        array_push($company_transaction_fields, 'id');
        // print_r($company_transaction_fields); die;
        $data->clients = Client::where('company_id', $data->company_id)
            ->where('status', 'Active')
            ->lists('id');
        
        if (count($data->clients) > 0) {
            if (!empty($data->item->column_order)) {
                $column_order = str_replace('transactions.', '', $data->item->column_order);
                $column_order = str_replace('clients.', '', $column_order);
                $column_order = explode(",", $column_order);
                $field_name = $column_order[0];
                $field_id = CompanySheetField::where('sheet_id', $data->item->id)
                    ->where('field_name', $column_order[0])
                    ->pluck('id');
            }
          // print_r($column_order); die;
            $transactions = Transaction::select(array_merge($company_transaction_fields, array('client_id', 'coe_date')))->whereIn('client_id', $data->clients);
            if (empty($data->item->all_transaction_type)) {
                $transactions = $transactions->whereIn('transaction_type', $transaction_types);
            }
            //print_r($transactions); die;
            if (empty($data->item->all_transaction_status)) {
                $transactions = $transactions->whereIn('transaction_status', $transaction_status);
            }

            $transactions = $transactions->get();
           
            if (count($transactions) > 0) {
                $contex_menu_items = array();
                foreach ($transactions as $tKey => $tVal) {

                    $transactions[$tKey]->client = Client::where('id', $tVal->client_id)->first();

                    $transactions[$tKey]->column_order = isset($field_id) && isset($transactions[$tKey]->$field_name) ? $transactions[$tKey]->$field_name : '';
                    if (isset($field_name) && $field_name == 'last_name') {
                        $transactions[$tKey]->column_order = $transactions[$tKey]->client->last_name;
                    } elseif (isset($field_name) && $field_name == 'coe_date') {
                        $transactions[$tKey]->column_order = $transactions[$tKey]->coe_date;
                    }

                    $custom = array();
                    $custom_fields = Custom::where('trans_id', $tVal->id)->get();
                    foreach ($custom_fields as $custom_field) {
                        $custom[$custom_field->field_id] = array(
                            'id' => $custom_field->id,
                            'field_value' => $custom_field->field_value,
                            'comment' => $custom_field->comment,
                            'color' => $custom_field->color,
                        );
                    }

                    if (empty($transactions[$tKey]->column_order)) {
                        $transactions[$tKey]->column_order = isset($field_id) && isset($custom[$field_id]) ? $custom[$field_id]['field_value'] : '';
                    }
                    $transactions[$tKey]->custom = $custom;
                    $transactions[$tKey]->transaction_type = Transaction::where('id', $tVal->id)->pluck('transaction_type');


                    $dates = array('start_date', 'contract_date', 'listing_date', 'listing_expiration_date', 'coe_date');
                    //foreach($dates as $date) {
                    //$transactions[$tKey]->$date = isset($transactions[$tKey]->$date) && $transactions[$tKey]->$date == "1970-01-01" ? null : $transactions[$tKey]->$date;
                    //}

                    $data->transactions[] = $transactions[$tKey];
                }
            }
        }

        //die;
        /*
          foreach($data->clients as $cKey => $cVal) {
          $transactions = Transaction::select($company_transaction_fields)->where('client_id', $cVal->id);

          if(empty($data->item->all_transaction_type)) {
          $transactions = $transactions->whereIn('transaction_type', $transaction_types);
          }
          if(empty($data->item->all_transaction_status)) {
          $transactions = $transactions->whereIn('transaction_status', $transaction_status);
          }

          $transactions = $transactions->get();

          if(count($transactions) > 0) {
          $contex_menu_items = array();
          foreach($transactions as $tKey => $tVal) {

          $custom = array();
          $custom_fields = Custom::where('trans_id', $tVal->id)->get();;
          foreach($custom_fields as $custom_field) {
          $custom[$custom_field->field_id] = array(
          'id'            => $custom_field->id,
          'field_value'   => $custom_field->field_value,
          'comment'       => $custom_field->comment,
          'color'         => $custom_field->color,
          );
          }
          $transactions[$tKey]->custom = $custom;
          $transactions[$tKey]->client = $data->clients[$cKey];

          $data->transactions[] = $transactions[$tKey];
          }

          $data->clients[$cKey]->transactions = $transactions;
          } else {
          unset($data->clients[$cKey]);
          }
          } */

       // print_r($data->transactions);die;
       //$data->transactions = array();
//print_r($data->clients[$cKey]);die;
        $data->sheet_field_defaults = array();
        $company_sheet_field_defaults =  CompanySheetFieldsTransactionTypeDefaults::whereIn('field_id', CompanySheetField::where('sheet_id', $data->item->id)->whereNull('parent_field')->lists('id'))->get();
        foreach($company_sheet_field_defaults as $fKey => $fVal) {
            $data->sheet_field_defaults[$fVal->transaction_type] = !isset($data->sheet_field_defaults[$fVal->transaction_type]) ? array() : $data->sheet_field_defaults[$fVal->transaction_type];
            $data->sheet_field_defaults[$fVal->transaction_type][$fVal->field_id]['value'] = $fVal->value;
            $data->sheet_field_defaults[$fVal->transaction_type][$fVal->field_id]['bg_color'] = DataOption::where('id', $fVal->bg_color)->pluck('option_name');
        }
        //print_r($data->sheet_field_defaults);die;
        //echo  $data->item->id;
        $company_sheet_fields = CompanySheetField::where('sheet_id', $data->item->id)
            ->whereNull('parent_field')
            ->orderBy('priority_order', 'asc')
            ->where('status', 'Active')
            ->get();

        $csf = array();
       // print_r($company_sheet_fields);die;

        foreach ($company_sheet_fields as $csfKey => $csfVal) {
            //$csf[] = $company_sheet_fields[$csfKey];
            $children = CompanySheetField::where('sheet_id', $data->item->id)
                ->where('parent_field', $csfVal->id)
                ->orderBy('priority_order', 'asc')
                ->where('status', 'Active')
                ->get();
             //print_r($children); die;
            $company_sheet_fields[$csfKey]->parent = true;

            $csf[] = $company_sheet_fields[$csfKey];

            foreach ($children as $child) {
                $child->parent = false;
                $csf[] = $child;
            }
        }

        $company_sheet_fields = $csf;
         
        $order_list = array_reverse( CompanySheet::$transaction_fields);

        foreach($order_list as $iKey => $iVal) {
            foreach ($company_sheet_fields as $csfKey => $csfVal) {
                if($csfVal->field_name == $iKey) {
                    $holding = $csfVal;
                    unset($company_sheet_fields[$csfKey]);
                    array_unshift($company_sheet_fields, $holding);
                }
            }
        }

        $fields_crosswalk = array_merge(CompanySheet::$transaction_fields, CompanySheet::$client_fields);
//        dd($csf);
      
        foreach ($company_sheet_fields as $company_sheet_field) {
            if (array_key_exists($company_sheet_field->field_name, CompanySheet::$client_fields)) {
//                dd($data->rows->client[$company_sheet_field->field_name] = array('name' => $fields_crosswalk[$company_sheet_field->field_name], 'parent' => $company_sheet_field->parent));
                $data->rows->client[$company_sheet_field->field_name] = array('name' => $fields_crosswalk[$company_sheet_field->field_name], 'parent' => $company_sheet_field->parent);
            } elseif (array_key_exists($company_sheet_field->field_name, CompanySheet::$transaction_fields)) {
                $data->rows->transaction[$company_sheet_field->field_name] = array('name' => $fields_crosswalk[$company_sheet_field->field_name], 'parent' => $company_sheet_field->parent);
            } else {
                $company_sheet_field->colors = CompanySheetFieldBgColors::where('field_id', $company_sheet_field->id)->lists('color', 'value');
                $company_sheet_field->access = CompanySheetFieldsAccess::where('field_id', $company_sheet_field->id)->lists('user_id');

                $contex_menu_items = array(
                    'add_comment' => array(
                        'show' => true, 'label' => 'Add Comment',
                    ),
                    'view_comment' => array(
                        'show' => true, 'label' => 'View Comment',
                    ),
                    'set_bg_color' => array(
                        'show' => true, 'label' => 'Set BG Color',
                    ),
                    'send_alert' => array(
                        'show' => true, 'label' => 'Send Alert',
                    ),
                );

                $company_sheet_field->contex_menu = "true";
                $company_sheet_field->contex_menu_items = json_encode($contex_menu_items);


                $data->rows->custom[$company_sheet_field->field_name] = $company_sheet_field;
            }
        }
        //print_r($data->rows->client); die;
//print_r($data->rows->transaction);die;

        foreach ($data->field_types as $fKey => $fVal) {
            if ($fVal == 'select') {
                if ($fKey == 'state') {
                    $data->options->$fKey = getStates();
                } else {
                    $data->options->$fKey = companyOptionsByType($fields_crosswalk[$fKey], $data->company_id);
                }
            }
        }
//dd($data->rows->transaction);
        if (!empty($column_order[1])) {
            if ($column_order[1] == 'asc') {
                usort($data->transactions, function ($a, $b) {
                    return strcmp(strtolower($a->column_order), strtolower($b->column_order));
                });
            } else {
                usort($data->transactions, function ($a, $b) {
                    return strcmp(strtolower($b->column_order), strtolower($a->column_order));
                });
            }
//            dd($data->transactions);
        }

        $data->escrowOfficers = Transaction::where('escrow_officer', '!=', '')->groupBy('escrow_officer')->lists('escrow_officer');
        $data->loanOfficers = Transaction::where('loan_officer', '!=', '')->groupBy('loan_officer')->lists('loan_officer');

        return view('companyportal.sheets.' . __FUNCTION__, compact('data'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $data = new \stdClass();
        $data->mode = 'Edit';
        $data->id = $id;

        $data = $this->scratch_data($data);

        $data->sheet = CompanySheet::find($id);
        $data->sheet_fields = CompanySheetField::where('sheet_id', $id)->lists('field_name');
        $data->sheet_access = CompanySheetAccess::where('sheet_id', $id)->lists('user_id');
        $data->sheet_types = CompanySheetType::where('sheet_id', $id)->lists('data_id');
        $data->sheet_statuses = CompanySheetStatus::where('sheet_id', $id)->lists('data_id');

        $data->transaction_type_defaults = array();
        $data->sheet_type_defaults = CompanySheetTransactionTypeDefaults::where('sheet_id', $id)->get();

        foreach($data->sheet_type_defaults as $item) {
            $data->transaction_type_defaults[$item->transaction_type] = array(
                'value'         => $item->value,
                'bg_color'      => DataOption::where('id', $item->bg_color)->pluck('option_name'),
                'header_color'  => DataOption::where('id', $item->header_color)->pluck('option_name'),
            );
        }
      // echo '<pre/>';
       // print_r($data);
        $data->html = view('companyportal.sheets.create_sheet_from.scratch', compact('data'))->render();
        
        return view('companyportal.sheets.' . __FUNCTION__, compact('data'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit_details($id)
    {
        $data = new \stdClass();
        $data->item = CompanySheet::find($id);
        $data->users = CompanySheetAccess::where('sheet_id', $data->item->id)->lists('user_id');
        $data->users_list = count($data->users) > 0 ? User::whereIn('id', $data->users)->lists('name') : array();
        $data->users_list = implode(', ', $data->users_list);
        $data->fields = CompanySheetField::where('sheet_id', $data->item->id)->lists('field_name');

        //var_dump($data->fields);exit;

        $data->fields = ucwords(str_replace('_', ' ', implode(', ', $data->fields)));
        return view('companyportal.sheets.' . __FUNCTION__, compact('data'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Request::input('form-input');
        parse_str($input, $data);

        $validator = Validator::make($data, CompanySheet::$rules_template[$data['sheet_type']]);

        if ($validator->passes()) {

            $input_fields = CompanySheet::$input_fields;

            $data['all_user_accsess'] = !empty($data['all_user_accsess']) ? $data['all_user_accsess'] : 0;
            $data['all_transaction_type'] = !empty($data['all_transaction_type']) ? $data['all_transaction_type'] : 0;
            $data['all_transaction_status'] = !empty($data['all_transaction_status']) ? $data['all_transaction_status'] : 0;
            $item = CompanySheet::find($data['id']);
            $item->freeze_first_column = '0';
            foreach ($data as $dKey => $dVal) {
                if (in_array($dKey, $input_fields)) {
                    $item->$dKey = $dVal;
                }
            }
            $item->save();

            $default_fields = array_keys(array_merge(CompanySheet::$client_fields, CompanySheet::$transaction_fields));
//                dd($default_fields);

            $transaction_types = array_filter(companyOptionsByType('Transaction Type', 1));
            CompanySheetTransactionTypeDefaults::where('sheet_id', $item->id)->delete();

            if (!empty($transaction_types) && !empty($item->id)) {
                $color_options = DataOption::where('type_id', DataType::where('type_name', 'Color Option')->pluck('id'))->where('option_status', 'Active')->lists('id', 'option_name');
                foreach($color_options as $cKey => $cVal) {
                    $color_options[strtoupper($cKey)] = $color_options[strtolower($cKey)] = $cVal;
                }

                foreach ($transaction_types as $tKey => $tVal) {
                    if (!empty($data['transaction_type_default_value'][$tKey]) || !empty($data['transaction_type_default_bg_color'][$tKey]) || !empty($data['transaction_type_default_header_color'][$tKey])) {
                        $default = new CompanySheetTransactionTypeDefaults;
                        $default->sheet_id = $item->id;
                        $default->transaction_type = $tKey;
                        $default->value = !empty($data['transaction_type_default_value'][$tKey]) ? $data['transaction_type_default_value'][$tKey] : null;
                        $default->bg_color = !empty($color_options[$data['transaction_type_default_bg_color'][$tKey]]) ? $color_options[$data['transaction_type_default_bg_color'][$tKey]] : null;
                        $default->header_color = !empty($color_options[$data['transaction_type_default_header_color'][$tKey]]) ? $color_options[$data['transaction_type_default_header_color'][$tKey]] : null;
                        $default->save();
                    }
                }
            }


            //CompanySheetField::where('sheet_id', $item->id)->whereIn('field_name', $default_fields)->delete();
            $existing_sheet_fields = CompanySheetField::where('sheet_id', $item->id)->whereIn('field_name', $default_fields)->lists('field_name', 'id');

            if (!empty($data['transaction_fields']) && !empty($item->id)) {
                foreach($existing_sheet_fields as $eKey => $eVal) {
                    if(!in_array($eVal, $data['transaction_fields'])) {
                        Custom::where('field_id', $eKey)->delete();
                        CompanySheetField::where('sheet_id', $item->id)->where('field_name', $eVal)->delete();
                    }
                    if(($key = array_search($eVal, $data['transaction_fields'])) !== false) {
                        unset($data['transaction_fields'][$key]);
                    }
                }

                foreach ($data['transaction_fields'] as $transaction_field) {
                    $field = new CompanySheetField;
                    $field->sheet_id = $item->id;
                    $field->field_name = $transaction_field;
                    $field->save();
                }
            } else {
                foreach($existing_sheet_fields as $eKey => $eVal) {
                    Custom::where('field_id', $eKey)->delete();
                }
                CompanySheetField::where('sheet_id', $item->id)->whereIn('field_name', $default_fields)->delete();
            }

            CompanySheetAccess::where('sheet_id', $item->id)->delete();

            if (!empty($data['users']) && !empty($item->id)) {
                foreach ($data['users'] as $user) {
                    $access = new CompanySheetAccess;
                    $access->sheet_id = $item->id;
                    $access->user_id = $user;
                    $access->save();
                }
            }

            CompanySheetType::where('sheet_id', $item->id)->delete();

            if (!empty($data['transaction_types']) && !empty($item->id)) {
                foreach ($data['transaction_types'] as $transaction_type) {
                    $type = new CompanySheetType;
                    $type->sheet_id = $item->id;
                    $type->data_id = $transaction_type;
                    $type->save();
                }
            }

            CompanySheetStatus::where('sheet_id', $item->id)->delete();

            if (!empty($data['transaction_status']) && !empty($item->id)) {
                foreach ($data['transaction_status'] as $transaction_status) {
                    $status = new CompanySheetStatus;
                    $status->sheet_id = $item->id;
                    $status->data_id = $transaction_status;
                    $status->save();
                }
            }

            return $this->ajax_construct(false, false, false, false, url('sheet/details/edit/' . $item->id));
        } else {
            return array('type' => 'error', 'message' => $validator->messages()->toArray());
        }
    }

    /**
     * Return the remove modal
     *
     * @param  int $id
     * @return Response
     */
    public function remove()
    {
        $data = new \stdClass();
        $data->item = Request::input('item');

        if ($this->can_user_delete($data->item)) {
            $html = view('companyportal.sheets.' . __FUNCTION__, compact('data'));
        } else {
            $html = view('companyportal.sheets.noop', compact('data'));
            $script = 'alert("You do not have permission to do this!");';
        }

        return $this->ajax_construct($html, !empty($script) ? $script : false);
    }

    public function contex_action()
    {
        $input = Request::all();

        $data = new \stdClass();
        $data->item = Custom::where('field_id', $input['field-id'])
            ->where('trans_id', $input['trans-id'])
            ->first();
        $data->trans_id = $input['trans-id'];
        $data->field_id = $input['field-id'];

        switch ($input['action']) {
            case 'Add Comment':
            case 'View Comment':
                $view = 'comment';
                break;
            case 'Set BG Color':
                $view = 'color';
                $data->current_color = $this->rgb2html($input['current-color']);
                $data->color_options = DataOption::where('type_id', DataType::where('type_name', 'Color Option')->pluck('id'))->where('option_status', 'Active')->lists('option_name');
                break;

            case 'Send Alert':
                $view = 'alert';
                $data->users = User::lists('name', 'id');
                break;
            default:
                $view = 'noop';
        }

        $html = view('companyportal.sheets.' . __FUNCTION__ . '.' . $view, compact('data'));


        return $this->ajax_construct($html, !empty($script) ? $script : false);
    }

    public function rgb2html($rgb)
    {
//        $r, $g=-1, $b=-1
        $rgb = str_ireplace('rgb(', '', $rgb);
        $rgb = str_ireplace(')', '', $rgb);
        $rgb = str_ireplace(' ', '', $rgb);
        $rgb = explode(',', $rgb);
        $r = $rgb[0];
        $g = $rgb[1];
        $b = $rgb[2];

        if (is_array($r) && sizeof($r) == 3)
            list($r, $g, $b) = $r;

        $r = intval($r); $g = intval($g);
        $b = intval($b);

        $r = dechex($r<0?0:($r>255?255:$r));
        $g = dechex($g<0?0:($g>255?255:$g));
        $b = dechex($b<0?0:($b>255?255:$b));

        $color = (strlen($r) < 2?'0':'').$r;
        $color .= (strlen($g) < 2?'0':'').$g;
        $color .= (strlen($b) < 2?'0':'').$b;
        return '#'.$color;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy()
    {
        $current_user = Auth::user();

        $data = new \stdClass();
        $data->item = Request::input('item');

        if ($this->can_user_delete($data->item)) {
            $fields = CompanySheetField::where('sheet_id', $data->item)->lists('id');
            foreach($fields as $field) {
                CompanyNotification::where('field_id', $field)->delete();
                Custom::where('field_id', $field)->delete();
                CompanySheetField::where('id', $field)->delete();
            }
            CompanySheet::where('id', $data->item)->delete();
        } else {
            $html = view('companyportal.sheets.noop', compact('data'));
            $script = 'alert("You do not have permission to do this!");';
        }

        return $this->ajax_construct(!empty($html) ? $html : false, !empty($script) ? $script : false, false, false, empty($html) && empty($script) ? url('sheets/') : false);
    }

    public function can_user_delete($sheet_id)
    {
        $current_user = Auth::user();

        $sheets = CompanySheet::select(array('company_sheets.*'))
            ->leftJoin('company_sheets_access', 'company_sheets.id', '=', 'company_sheets_access.sheet_id')
            ->where('company_sheets_access.user_id', $current_user->id)
            ->orWhere('company_sheets.all_user_accsess', 1)
            ->lists('id');

        return in_array($sheet_id, $sheets) ? true : false;
    }

    /**
     * Return the correct form for create new
     */
    public function create_sheet_from()
    {
        $current_user = Auth::user();

        $data = new \stdClass();
        $data->item = Request::input('value');

        switch ($data->item):
            case 'template':


                $res = Template::select(array('templates.*'))
                    ->leftJoin('sheet_accesses', 'templates.id', '=', 'sheet_accesses.template_id')
                    ->where('templates.status', 'Active')->orderBy('name');

                $cuser = $current_user->id;
                $res->where(function ($query) use ($cuser) {
                    $query->where('sheet_accesses.user_id', $cuser)->orWhere('templates.all_user_accsess', 1);
                });

                $data->templates = $res->lists('name', 'id');


                break;
            case 'scratch':
                $data = $this->scratch_data($data);
                break;
        endswitch;


        $html = view('companyportal.sheets.' . __FUNCTION__ . '.' . $data->item, compact('data'));

        return $this->ajax_construct($html);
    }

    /**
     * Return the scratch data
     */
    public function scratch_data($data)
    {


        $data->client_fields = CompanySheet::$client_fields;
        $data->transaction_fields = CompanySheet::$transaction_fields;
        $data->active_users = User::where('status', 'Active')
            ->where('company_id', Auth::user()->company_id)
            ->get();


        $data->transaction_types = array_filter(companyOptionsByType('Transaction Type', 1));
        $data->transaction_statuses = array_filter(companyOptionsByType('Transaction Status', 1));
        $data->color_options = DataOption::where('type_id', DataType::where('type_name', 'Color Option')->pluck('id'))->where('option_status', 'Active')->lists('option_name');
        $data->column_order = CompanySheet::$column_order;
        $data->status = CompanySheet::$status;

        return $data;
    }

    /**
     * Return the scratch data
     */
    public function cell_update()
    {
        $data = Request::all();
        $script = array();
        if (isset($data['input-type']) && $data['input-type'] == 'date') {
            $data['value'] = date('Y-m-d', strtotime($data['value']));
            if($data['value'] == '1970-01-01') {
                $data['value'] = null;
            }
        }

        switch ($data['cell-type']):
            case 'Transaction':
                $item = Transaction::find($data['item']);
                $item->$data['field'] = ($data['field'] == 'sales_price' ? str_ireplace(',', '', $data['value']) : $data['value']);
                $item->$data['field'] = ($data['field'] == 'current_list_price' ? str_ireplace(',', '', $data['value']) : $item->$data['field']);

                break;
            case 'Client':
                $item = Client::find($data['item']);
                $item->$data['field'] = $data['value'];
                break;
            case 'Custom':
                if (isset($data['contex-action'])) {
                    if ($data['contex-action'] == 'contex_form_alert') {
                        $current_user = Auth::user();
                        $item = new CompanyNotification;
                        $item->message = $data['value'];
                        $item->field_id = $data['field-id'];
                        $item->trans_id = $data['trans-id'];
                        $item->user_id = $data['user'];
                        $item->sender_id = $current_user->id;
                        $item->save();

                        if (isset($data['notify']) && $data['notify'] == 1) {
                            $to = User::find($data['user']);
                            $maildata = array();
                            $maildata['item'] = CompanyNotification::find($item->id);
                            $maildata['sender'] = User::find($item->sender_id);
                            $maildata['field'] = CompanySheetField::find($item->field_id);
                            $maildata['sheet'] = CompanySheet::find($maildata['field']->sheet_id);
                            $maildata['transaction'] = Transaction::find($item->trans_id);
                            $maildata['client'] = Client::find($maildata['transaction']->client_id);
                            $maildata['transactionstatus'] = CompanyDataOption::find($maildata['transaction']->transaction_status);
                            $maildata['transactiontype'] = CompanyDataOption::find($maildata['transaction']->transaction_type);


                            $mailer = app()['mailer'];

                            $to->from_settings = Settings::where('key','=','from_email_notice')->first()->value;


                            if(empty( $to->from_settings)){
                            	 $to->from_settings = "test@test.com";
                            }

                            $mailer->send('emails.notification', array('maildata' => $maildata), function ($message) use ($to) {
                                $message->from($to->from_settings);
                                $message->to($to->email, $to->name);
                                $message->subject('Notification');
                            });
                        }
                    } else {
                        $item = Custom::where('field_id', $data['field-id'])
                            ->where('trans_id', $data['trans-id'])
                            ->first();

                        $item = !empty($item) ? $item : new Custom;
                        $item->trans_id = $data['trans-id'];
                        $item->field_id = $data['field-id'];
                        $action = str_replace('contex_form_', '', $data['contex-action']);
                        $item->$action = trim($data['value']);
                        $field_type = CompanySheetField::find($item->field_id)->type;
                        if ($field_type == 'Checkbox')
                            $parent = '.parent()';
                        else
                            $parent = '';
                        if ($data['contex-action'] == 'contex_form_color') {
                            $script[] = "$('[data-trans-id=\"" . $data['trans-id'] . "\"][data-field-id=\"" . $data['field-id'] . "\"]')" . $parent . ".css('background-color', '" . trim($data['value']) . "');";
                        } else {
                            $script[] = "$('[data-trans-id=\"" . $data['trans-id'] . "\"][data-field-id=\"" . $data['field-id'] . "\"]').attr('data-" . $action . "', '" . trim($data['value']) . "');";
                        }
                        if ($data['contex-action'] == 'contex_form_comment' && !empty($data['value'])) {
                            $script[] = "$('[data-trans-id=\"" . $data['trans-id'] . "\"][data-field-id=\"" . $data['field-id'] . "\"]').attr('data-comment', '" . trim($data['value']) . "').siblings('.input-group-addon').find('.glyphicon-comment').parent().removeClass('hidden');";
                        } elseif ($data['contex-action'] == 'contex_form_comment') {
                            //remove
                            $script[] = "$('[data-trans-id=\"" . $data['trans-id'] . "\"][data-field-id=\"" . $data['field-id'] . "\"]').attr('data-comment', '" . trim($data['value']) . "').siblings('.input-group-addon').find('.glyphicon-comment').parent().addClass('hidden');";
                        }
                    }
                } else {
                    $item = !empty($data['item']) ? Custom::find($data['item']) : new Custom;
                    $item->trans_id = $data['trans-id'];
                    $item->field_id = $data['field-id'];
                    $item->field_value = is_null($data['value']) ? $data['value'] : trim($data['value']);
                }
                break;
            default:
        endswitch;

        if ($item) {
            if(isset($data['field']) && isset($item->$data['field'])) {
                $item->$data['field'] = is_null($item->$data['field']) ? $item->$data['field'] : trim($item->$data['field']);
            }

            $item->save();
            if (isset($data['contex-action'])) {

            } else {
                if ($data['cell-type'] == 'Custom') {
                    $script[] = "$('[data-input-type=\"" . $data['input-type'] . "\"][data-cell-type=\"" . $data['cell-type'] . "\"][data-trans-id=\"" . $data['trans-id'] . "\"][data-field-id=\"" . $data['field-id'] . "\"]').attr('data-item', '" . $item->id . "').siblings('.input-group-addon').children('.savingspinner').addClass('hidden');";
                    //$script[] = "$('[data-trans-id=\"" . $data['trans-id'] . "\"][data-field-id=\"" . $data['field-id'] . "\"]').attr('data-item', '" . $item->id . "').siblings('.input-group-addon').children('.savingspinner').addClass('hidden');";
                } else {
                    $script[] = "$('[data-input-type=\"" . $data['input-type'] . "\"][data-cell-type=\"" . $data['cell-type'] . "\"][data-field=\"" . $data['field'] . "\"][data-item=\"" . $data['item'] . "\"]').siblings('.input-group-addon').children('.savingspinner').addClass('hidden');";
                    //$script[] = "$('[data-field=\"" . $data['field'] . "\"][data-item=\"" . $data['item'] . "\"]').siblings('.input-group-addon').children('.savingspinner').addClass('hidden');";
                }
            }
        }

        if ($data['cell-type'] == 'Client' && in_array($data['field'], array('first_name', 'last_name'))) {
            $script[] = "$('[data-input-type=\"" . $data['input-type'] . "\"][data-cell-type=\"Client\"][data-field=\"" . $data['field'] . "\"][data-item=\"" . $data['item'] . "\"]').val('" . $data['value'] . "');";
        }

        //return color
        if($data['cell-type'] == 'Custom') {
            $current_field_value = Custom::where('field_id', $data['field-id'])->where('trans_id', $data['trans-id'])->pluck('field_value');
            $companySheetFieldBgColor = CompanySheetFieldBgColors::where('field_id', $data['field-id'])->where('value', $current_field_value)->pluck('color');
            $customs = Custom::where('field_id', $data['field-id'])->where('trans_id', $data['trans-id'])->pluck('color');
            $companySheetFieldTypeDefault = CompanySheetFieldsTransactionTypeDefaults::where('field_id', $data['field-id'])->where('transaction_type', Transaction::where('id', $data['trans-id'])->pluck('transaction_type'))->pluck('bg_color');
            $companySheetTypesDefaults = CompanySheetTransactionTypeDefaults::where('sheet_id', CompanySheetField::where('id', $data['field-id'])->pluck('sheet_id'))->where('transaction_type', Transaction::where('id', $data['trans-id'])->pluck('transaction_type'))->pluck('bg_color');

            if(!empty($companySheetFieldBgColor)){
                //Custom by Value Set in Edit Sheet
                $background_color = $companySheetFieldBgColor;
            } elseif(!empty($customs)) {
                //Custom - Set in Sheet
                $background_color = $customs;
            } elseif(!empty($companySheetFieldTypeDefault)) {
                //Field Default Set in Edit Sheet/Field
                $background_color = DataOption::where('id', $companySheetFieldTypeDefault)->pluck('option_name');
            } elseif(!empty($companySheetTypesDefaults)) {
                //Transaction Default Set in Edit Sheet/Transaction
                $background_color = DataOption::where('id', $companySheetTypesDefaults)->pluck('option_name');
            } else {
                //Default
                $background_color = CompanySheetField::where('id', $data['field-id'])->pluck('default_bg_color');
            }
            if(isset($data['input-type']) && isset($data['item'])) {
                $script[] = "$('[data-input-type=\"" . $data['input-type'] . "\"][data-cell-type=\"" . $data['cell-type'] . "\"][data-field-id=\"" . $data['field-id'] . "\"][data-trans-id=\"" . $data['trans-id'] . "\"][data-item=\"" . $data['item'] . "\"]').css('background-color', '".$background_color."');";
                $script[] = "$('[data-input-type=\"" . $data['input-type'] . "\"][data-cell-type=\"" . $data['cell-type'] . "\"][data-field-id=\"" . $data['field-id'] . "\"][data-trans-id=\"" . $data['trans-id'] . "\"][data-item=\"" . $data['item'] . "\"]').closest('td').css('background-color', '".$background_color."');";

            } else {
                $script[] = "$('[data-cell-type=\"" . $data['cell-type'] . "\"][data-field-id=\"" . $data['field-id'] . "\"][data-trans-id=\"" . $data['trans-id'] . "\"]').css('background-color', '".$background_color."');";
                $script[] = "$('[data-cell-type=\"" . $data['cell-type'] . "\"][data-field-id=\"" . $data['field-id'] . "\"][data-trans-id=\"" . $data['trans-id'] . "\"]').closest('td').css('background-color', '".$background_color."');";

            }

        }
//print_r($script);die;
        return $this->ajax_construct(false, implode('', $script));
    }

    /**
     * Activity Handler
     *
     */
    public function activity_poll($item = array())
    {
        $function_data = array('function' => __FUNCTION__, 'data' => array());

        $data = Request::all();


        //remove old active cells
        //remove active cell by sheet/user
        //store active cell by sheet/user

        //remove actions older than 30 seconds
        //check for new activities based off submitted last check timestamp
        //return new activities with new last check timestamp


        return $this->ajax_construct(false, false, false, false, false, array('function' => __FUNCTION__, 'data' => array()));
    }

    /**
     * Ajax handler
     *
     * @return JSON Response
     */
    public function postAjax()
    {
        //echo  $route = Request::input('route');
        if (!Request::has('route')) {
            return $this->json_error(array('error' => 'Missing Parameters'));
        } elseif (!method_exists($this, Request::input('route'))) {
            return $this->json_error(array('error' => 'Invalid Parameters'));
        }

        $route = Request::input('route');

        $result = array();
        $result['target'] = Request::input('target');

        $input = Request::input('form-input');
        parse_str($input, $output);

        $data = $this->$route($output);

        if ('show' == $route) {
            return json_encode($data);
        } elseif (isset($data['type']) && $data['type'] == 'error') {
            return $this->json_error($data);
        }

        if (!empty($data)) {
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
        }
        $result['script'] = !isset($result['script']) ? '' : $result['script'];
        $result['script'] .= "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "(" . (isset($result[$route]) ? json_encode($result[$route]) : null) . ");}";

        return $this->json_success($result);
    }

    /**
     * JSON Success Handler
     *
     * @return JSON Response
     */
    public function json_success($data = false)
    {
        return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
    }

    /**
     * JSON Error Handler
     *
     * @return JSON Response
     */
    public function json_error($data = false)
    {
        return empty($data) ? json_encode(array('success' => false)) : json_encode(array('false' => true, 'data' => $data));
    }

    /**
     * Response Handler
     *
     * @return Response
     */
    public function ajax_construct($html = false, $script = false, $append = false, $alert = false, $redirect = false, $functiondata = false)
    {
        $result = array();

        if ($html) {
            $result['html'] = $html->render();
        }
        if ($script) {
            $result['script'] = $script;
        }
        if ($append) {
            $result['append'] = $append;
        }
        if ($alert) {
            $result['alert'] = $alert;
        }
        if ($redirect) {
            $result['redirect'] = $redirect;
        }
        if ($functiondata && isset($functiondata['function']) && isset($functiondata['data'])) {
            $result[$functiondata['function']] = $functiondata['data'];
        }

        return $result;
    }

}

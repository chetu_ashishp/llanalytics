<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use App\DataOption;
use App\DataType;
use Route;

class DataOptionsController extends Controller {
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
           $routepath = Route::getCurrentRoute()->getPath();      
            $something = new HomeController;
            $check = $something->authenticateUserRolePermissionURL($routepath);           
            if(!$check) {
               abort(404, 'Unauthorized action.');         
            }
      
		$this->middleware('auth');
	}
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $create = $this->create();
        $tableData = json_encode($this->show());                                               
        $data_types = $create->data_types;
		return view('data.index', compact('create', 'tableData', 'data_types'));
	}

	
	public function datamain()
	{	
		return view('data.main', array());
	}	
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            $form = 'Add';
            $data_types = DataType::orderBy('type_name', 'asc')->lists('type_name', 'id');            
            return view('data.form', compact('data_types', 'form'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $input = Request::input('form-input');            
           // var_dump($input);exit;            
            parse_str($input, $output);
            
            
            
            $input_fields = array(
                'type_id',
                'option_name',
            	'option_desc',
            	'option_status'            		
            );

            $item = new DataOption;
            foreach($output as $oKey => $oVal) {
                if(in_array($oKey, $input_fields)) {
                    $item->$oKey = $oVal;
                }
            }
            $item->save();

            return $this->ajax_construct();
	}

	
	
	
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($type=false)
	{
		    $json = array();
        
		    if($type){
            	$result = DataOption::select(array('data_options.id', 'data_options.option_name', 'data_options.option_desc', 'data_options.option_status' , 'data_types.type_name'))
            	->join('data_types', 'data_options.type_id', '=', 'data_types.id')->where('data_options.type_id',$type)
            	->get();
		    }
		    else{
		    	$result = DataOption::select(array('data_options.id', 'data_options.option_name', 'data_options.option_desc', 'data_options.option_status' , 'data_types.type_name'))
		    	->join('data_types', 'data_options.type_id', '=', 'data_types.id')
		    	->get();
		    }
			
            foreach($result as $rKey => $rVal) {
                $id = $result[$rKey]->id;
                $json[] = array(    'type' => $result[$rKey]->type_name,
                                    'name' => $result[$rKey]->option_name,
                					'desc' => str_limit($result[$rKey]->option_desc, 200),
                					'status' => $result[$rKey]->option_status,
                                    'actions' => view('data.actions', compact('id'))->render() );
            }
            
         // echo "<pre>";  var_dump($json);exit;            
            return $json;
	}

	
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function search($type)
	{
		$json = array();
	
			$result = DataOption::select(array('data_options.id', 'data_options.option_name', 'data_options.option_desc', 'data_options.option_status' , 'data_types.type_name'))
			->join('data_types', 'data_options.type_id', '=', 'data_types.id')->where('data_options.type_id',$type)
			->get();
		
			
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$json[] = array(    'type' => $result[$rKey]->type_name,
					'name' => $result[$rKey]->option_name,
					'desc' => $result[$rKey]->option_desc,
					'status' => $result[$rKey]->option_status,
					'actions' => view('data.actions', compact('id'))->render() );
		}
	
		
		  return $this->ajax_construct();
	}
	
	
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
            $form = 'Edit';
            
            $id = Request::input('item');
            
            $item = DataOption::find($id);
            
            $data_types = DataType::orderBy('type_name', 'asc')->lists('type_name', 'id');
            
            $html = view('data.form', compact('data_types', 'item', 'form'));
            
            return $this->ajax_construct($html);
	}
	
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$input = Request::input('form-input');
	
		parse_str($input, $output);
	
		$input_fields = array(
				'type_id',
				'option_name',
				'option_desc',
				'option_status',
		);
	
		$item = DataOption::find($output['id']);
		foreach($output as $oKey => $oVal) {
			if(in_array($oKey, $input_fields)) {
				$item->$oKey = $oVal;
			}
		}
		$item->save();
		return $this->ajax_construct();
	}
	
	
	
	public function removeCheck() {
		
		$id =  Request::input('item'); 		
		//$dataoption = DataOption::find($id);
		//    dd(Helpers::isKeyUsedAsForeignKey('users', $student->user_id));
		if (!isKeyUsedAsForeignKey('data_options', $id) ) {
			
			//$html = view('data.form', compact('data_types', 'item', 'form'));			
			return $this->ajax_construct(false, "confirmRemove({$id}, 'option', 'deleteOption'); ");
			
			//return array('errors' => FALSE);
		} else{
			 $data['array'] = array('message' => array("The Option is in use in the system and cannot be deleted."));
			 return array('success'=>false, "arr"=>$data);
		}	 
	}
	
	
	public function deleteOption($id){
	
		//DataOption::where('id', $id)->delete();
	
		$result = DataOption::select(array('data_options.id', 'data_options.option_name', 'data_options.option_desc', 'data_options.option_status' , 'data_types.type_name'))
		->join('data_types', 'data_options.type_id', '=', 'data_types.id')
		->get();
	
		$json=array();
		foreach($result as $rKey => $rVal) {
			$id = $result[$rKey]->id;
			$json[] = array(    'type' => $result[$rKey]->type_name,
					'name' => $result[$rKey]->option_name,
					'desc' => str_limit($result[$rKey]->option_desc, 200),
					'status' => $result[$rKey]->option_status,
					'actions' => view('data.actions', compact('id'))->render() );
		}
	
		// echo "<pre>";  var_dump($json);exit;
		return $json;
	}
	
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
        /**
         * Ajax handler
         *
         * @return JSON Response
         */
        public function postAjax() {

        	//echo 'dsfsdfsd';exit;        	
            if (!Request::has('route')) {
                return $this->json_error(array('error' => 'Missing Parameters'));
            } elseif (!method_exists($this, Request::input('route'))) {
                return $this->json_error(array('error' => 'Invalid Parameters'));
            }

            $route = Request::input('route');
            
            $result = array();
            $result['target'] = Request::input('target');
            $result['script'] = "if (typeof " . $route . " !== 'undefined' && $.isFunction(" . $route . ")) { " . $route . "();}";
         
        
			if (Request::has('typeId')) {
            		$data = $this->$route(Request::input("typeId"));
			}
			
			elseif($route== 'deleteOption'){
            	            
            	 $data = $this->$route(Request::input('deleteid'));
            }
			else{				
					$data = $this->$route();
			}
			
			
			
			if($route== 'removeCheck' && isset($data["success"]) && $data["success"]==false){
				return $this->json_error($data["arr"]);
			}
			elseif($route== 'show' || $route== 'deleteOption') {            	
                return json_encode($data);
            }
            
                     
            
            foreach ($data as $dKey => $dVal) {
                $result[$dKey] = isset($result[$dKey]) ? $dVal . $result[$dKey] : $dVal;
            }
            
           // var_dump($this->json_success($result));exit;
            return $this->json_success($result);
        }

        /**
         * JSON Success Handler
         *
         * @return JSON Response
         */
        public function json_success($data = false) {
          return empty($data) ? json_encode(array('success' => true)) : json_encode(array('success' => true, 'data' => $data));
        }

        /**
         * JSON Error Handler
         *
         * @return JSON Response
         */
        public function json_error($data = false) {
          return empty($data) ? json_encode(array('success' => false)) : json_encode(array('success' => false, 'data' => $data));
        }

        public function ajax_construct($html = false, $script = false, $append = false, $alert = false) {
            $result = array();

            if ($html) {
                $result['html'] = $html->render();
            }
            if ($script) {
                $result['script'] = $script;
            }
            if ($append) {
                $result['append'] = $append;
            }
            if ($alert) {
                $result['alert'] = $alert;
            }
            return $result;
        }
}

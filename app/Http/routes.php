<?php

/*
 |--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController'
		]);

	Route::get('/', 'HomeController@index');
	
    Route::get('/company/subscription', 'Auth\AuthController@companyAddSubscription');
    Route::post('/company/subscription', 'Auth\AuthController@companyAddSubscription');
   /// Route::get('/company/addsubscription', 'Auth\AuthController@companyAddSubscriptions');
   // Route::post('/company/addsubscription', 'Auth\AuthController@companyAddSubscriptions');
    Route::get('ajaxCompanySubs', 'Auth\AuthController@postAjax');
	Route::post('ajaxCompanySubs', 'Auth\AuthController@postAjax');
    
    //Route::group(['middleware' => 'permissions'], function(){
	
	Route::get('home', 'HomeController@index');
	
	Route::get('data', 'DataOptionsController@index');
	Route::get('ajaxData', 'DataOptionsController@postAjax');
	Route::post('ajaxData', 'DataOptionsController@postAjax');
	Route::get('datamain', 'DataOptionsController@datamain');
	
	Route::controller('data', 'DataOptionsController');
	
	
	
	Route::post('ajaxSettings', 'SettingsController@emailUpdate');
	Route::get('settings', 'SettingsController@index');
	
	Route::get('search', 'ClientsController@search');
	Route::get('clients', array('as'=>'clients', 'uses' => 'ClientsController@index'));
	Route::get('ajaxClients', 'ClientsController@postAjax');
	Route::post('ajaxClients', 'ClientsController@postAjax');
	Route::get('clients/create', array('as' => 'newclient', 'uses' => 'ClientsController@create'));
	//Route::get('clients/create', array('as' => 'newclient', 'uses' => 'ClientsController@create'));
	//Route::get('clients/details/{client}', array('middleware' => 'permissions', 'permissions' => 'clients.details', 'as' => 'clients.details', 'uses' => 'ClientsController@details'));
	Route::post('clients/store', 'ClientsController@store');
	Route::get('clients/details/{client}', array('as' => 'clients.details', 'uses' => 'ClientsController@details'));
	Route::get('clients/destroy/{client}', array('as' => 'clients.destroy', 'uses' => 'ClientsController@delete'));
	Route::get('clients/edit/{client}', array('as' => 'client.edit', 'uses' => 'ClientsController@edit'));
	Route::put('clients/update/{client}', array('as' => 'client.update', 'uses' => 'ClientsController@update'));
	
	Route::get('templates', array('as' => 'templates', 'uses'=>'TemplatesController@index'));
	Route::get('ajaxTemplates', 'TemplatesController@postAjax');
	Route::post('ajaxTemplates', 'TemplatesController@postAjax');
	Route::get('templates/create', array('as' => 'newtemplate', 'uses' => 'TemplatesController@create'));
	Route::get('templates/create/{sheet}', array('as' => 'templates.create', 'uses' => 'TemplatesController@create'));
	Route::post('templates/store', 'TemplatesController@store');
	Route::get('templates/details/{template}', array('as' => 'templates.details', 'uses' => 'TemplatesController@details'));
	Route::get('templates/destroy/{template}', array('as' => 'templates.destroy', 'uses' => 'TemplatesController@delete'));
	Route::get('templates/edit/{template}', array('as' => 'templates.edit', 'uses' => 'TemplatesController@edit'));
	Route::put('templates/update/{template}', array('as' => 'templates.update', 'uses' => 'TemplatesController@update'));
	
    Route::get('sheets', array('as' => 'sheets', 'uses' => 'CompanySheetController@index'));
    Route::get('ajaxCompanySheet', 'CompanySheetController@postAjax');
    Route::post('ajaxCompanySheet', 'CompanySheetController@postAjax');
    Route::get('sheet/create', array('as' => 'createsheet', 'uses' => 'CompanySheetController@create'));
    Route::get('sheet/details/edit/{sheet}', array('as' => 'sheet.details.edit', 'uses' => 'CompanySheetController@edit_details'));
    Route::get('sheet/details/entry/{sheet}', array('as' => 'sheet.details.entry', 'uses' => 'CompanySheetController@show'));
    Route::get('sheet/edit/{sheet}', array('as' => 'sheet.edit', 'uses' => 'CompanySheetController@edit'));
    
    //Company portal sheets
    Route::get('companyportalsheets', array('as' => 'cpsheets', 'uses' => 'CompanyPortalSheetController@index'));
    Route::get('ajaxCompanyPortalSheet', 'CompanyPortalSheetController@postAjax');
    Route::post('ajaxCompanySheet', 'CompanyPortalSheetController@postAjax');
    Route::get('company/sheet/create', array('as' => 'createcompanysheet', 'uses' => 'CompanyPortalSheetController@create'));
    Route::get('company/sheet/createcopy/{sheet}', array('as' => 'createcopycompanysheet', 'uses' => 'CompanyPortalSheetController@copyDuplicateSheets'));
    Route::get('company/sheet/details/edit/{sheet}', array('as' => 'company.sheet.details.edit', 'uses' => 'CompanyPortalSheetController@edit_details'));
    Route::get('company/sheet/details/entry/{sheet}', array('as' => 'companysheet.details.entry', 'uses' => 'CompanyPortalSheetController@show'));
    Route::get('company/sheet/edit/{sheet}', array('as' => 'companysheet.edit', 'uses' => 'CompanyPortalSheetController@edit'));
    //

	Route::get('companytools', array('as' => 'companytools', 'uses' => 'CompanyToolsController@index'));
	Route::get('ajaxCompanytools', 'CompanyToolsController@postAjax');
	Route::post('ajaxCompanytools', 'CompanyToolsController@postAjax');
	Route::controller('companytools', 'CompanyToolsController');

	Route::get('import', array('as' => 'import', 'uses' => 'ImportController@index'));
	Route::get('ajaxImport', 'ImportController@postAjax');
	Route::post('ajaxImport', 'ImportController@postAjax');
	Route::resource('import', 'ImportController');
	
	Route::get('transactions', array('as' => 'transactions', 'uses' => 'TransactionsController@index'));
	Route::get('ajaxTransactions', 'TransactionsController@postAjax');
	Route::post('ajaxTransactions', 'TransactionsController@postAjax');
	Route::get('transactions/storeTransaction', array('as' => 'newtransaction', 'uses' => 'TransactionsController@storeTransaction'));
	Route::get('transactions/autocompleate', array('as' => 'transautocompleate', 'uses' => 'TransactionsController@autocompleate'));
	Route::resource('transactions', 'TransactionsController');
	
	// staff
	Route::get('staffs', array('as' => 'staffs', 'uses' => 'StaffsController@index'));
	Route::get('ajaxStaffs', 'StaffsController@postAjax');
	Route::post('ajaxStaffs', 'StaffsController@postAjax');
	Route::get('staffs/storeStaff', array('as' => 'newtransaction', 'uses' => 'StaffsController@storeStaff'));
   	Route::get('staffs/search', array('as' => 'sasearch', 'uses' => 'StaffsController@searchstaff'));
	Route::resource('staffs', 'StaffsController');
	
    // staff
	Route::get('roles', array('as' => 'roles', 'uses' => 'RolesController@index'));	
	Route::get('roles/assign', array('as' => 'roles.assign', 'uses' => 'RolesController@assign'));
    Route::get('roles/editpermissionassign/{id}', array('as' => 'roles.editpermission', 'uses' => 'RolesController@editpermissionassign'));
    Route::get('roles/storePermission', array('as' => 'roles.storePermission', 'uses' => 'RolesController@storePermissionRole'));
   // Route::get('transactions/storeTransaction', array('as' => 'newtransaction', 'uses' => 'RolesController@storepermissionrole'));
    Route::post('roles/storePermission', array('as' => 'roles.storePermission', 'uses' => 'RolesController@storePermissionRole'));
	Route::resource('roles', 'RolesController');
    
    //for add subcription package route 
    Route::get('AddSubscription', array('as' => 'staffs', 'uses' => 'AddSubscriptionController@index'));
	Route::get('ajaxAddSubscription', 'AddSubscriptionController@postAjax');
	Route::post('ajaxAddSubscription', 'AddSubscriptionController@postAjax');
	Route::get('AddSubscription/storeSubscription', array('as' => 'newSubscription', 'uses' => 'AddSubscriptionController@storeStaff'));
	Route::resource('AddSubscription', 'AddSubscriptionController');
    
    //Email template section
    Route::get('emailtemplates', array('as' => 'emailtemplates', 'uses' => 'EmailTemplatesController@index'));
	//Route::get('ajaxAddSubscription', 'AddSubscriptionController@postAjax');
	//Route::get('emailtemplate/edit/{template}', array('as' => 'emailtemplateedit', 'uses' => 'EmailTemplatesController@edit'));
    Route::post('ajaxEmailTemplate', 'EmailTemplatesController@postAjax');
    
    Route::get('emailtemplate/create', array('as' => 'newemailtemplate', 'uses' => 'EmailTemplatesController@create'));
    Route::get('emailtemplate/edit/{etemplate}', array('as' => 'emailtempalte.edit', 'uses' => 'EmailTemplatesController@edit'));
    //Route::get('emailtemplate/update/{etemplate}', array('as' => 'emailtempalte.update', 'uses' => 'EmailTemplatesController@update'));
    Route::post('emailtemplate/update/{etemplate}', array('as' => 'emailtempalte.update', 'uses' => 'EmailTemplatesController@update'));
    Route::get('emailtemplate/store', array('as' => 'storemailtemplate', 'uses' => 'EmailTemplatesController@storeemailtemplate'));
    Route::post('emailtemplate/store', array('as' => 'storemailtemplate', 'uses' => 'EmailTemplatesController@storeemailtemplate'));
	//Route::get('AddSubscription/storeSubscription', array('as' => 'newSubscription', 'uses' => 'AddSubscriptionController@storeStaff'));
	Route::resource('emailtemplates', 'EmailTemplatesController');
    
	// OMS staff
    Route::get('staff', array('as' => 'staff', 'uses'=>'OmsStaffsController@index'));
	Route::get('ajaxOmsStaff', 'OmsStaffsController@postAjax');
	Route::post('ajaxOmsStaff', 'OmsStaffsController@postAjax');
    //
	Route::get('staff', array('as' => 'staff', 'uses' => 'OmsStaffsController@index'));
	Route::get('staff/create', array('as' => 'newstaff', 'uses' => 'OmsStaffsController@create'));
	//Route::get('ajaxOmsStaff', 'OmsStaffsControllerr@postAjax');
	//Route::post('ajaxOmsStaff', 'OmsStaffsController@postAjax');
	Route::get('staff/storeStaff', 'OmsStaffsController@store');
	Route::post('staff/storeStaff', 'OmsStaffsController@store');
	Route::get('staff/search', array('as' => 'ssearch', 'uses' => 'OmsStaffsController@search'));
	Route::get('staff/edit/{staff}', array('as' => 'ostaff.edit', 'uses' => 'OmsStaffsController@editOmsStaff'));
	//Route::get('staff/update/{staff}',  'OmsStaffsController@update');
	Route::any('staff/update/{staff}', array('as' => 'staff.update', 'uses' => 'OmsStaffsController@update'));
	Route::get('omsstaffs/search', array('as' => 'sasearch', 'uses' => 'OmsStaffsController@searchstaffnonactive'));
	Route::get('staff/details/{staff}', array('as' => 'staff.details', 'uses' => 'OmsStaffsController@details'));
	Route::resource('staff', 'OmsStaffsController');

//  subscription route section
    Route::get('subscriptions', array('as' => 'subscriptions', 'uses'=>'SubscriptionsController@index'));
	Route::get('ajaxSubscriptions', 'SubscriptionsController@postAjax');
	Route::post('ajaxSubscriptions', 'SubscriptionsController@postAjax');
	Route::get('subscriptions/create', array('as' => 'newsubscription', 'uses' => 'SubscriptionsController@create'));
    Route::post('subscriptions/store', 'SubscriptionsController@store');
    Route::get('subscriptions/search', 'SubscriptionsController@searchsubscription');
    Route::get('subscriptions/edit/{subscription}', array('as' => 'subscriptions.edit', 'uses' => 'SubscriptionsController@edit'));
	Route::put('subscriptions/update/{subscription}', array('as' => 'subscriptions.update', 'uses' => 'SubscriptionsController@update'));
    
    #Route::post('subscriptions/storepaymentadjustment', 'SubscriptionsController@storepaymentadjustment');
    
    //company subscription section
    Route::get('companysubscribed', array('as' => 'csubscriptions', 'uses'=>'CompanySubscriptionController@index'));
    Route::get('companysubscribedSearch', 'CompanySubscriptionController@search');
    Route::post('ajaxCompanySubscribed', 'CompanySubscriptionController@postAjax');
    Route::resource('companysubscribed', 'CompanySubscriptionController');
    //company subscription section end here
    
//    
	Route::get('fields', array('as' => 'fields', 'uses' => 'FieldsController@index'));
	Route::get('ajaxFields', 'FieldsController@postAjax');
	Route::post('ajaxFields', 'FieldsController@postAjax');	
	Route::get('fields/create', array('as' => 'createtplfield', 'uses' => 'FieldsController@create'));
	Route::resource('fields', 'FieldsController');
	
	
	
	
	Route::get('sheetfields', array('as' => 'sheetfields', 'uses' => 'CompanySheetFieldsController@index'));
	Route::get('ajaxSheetFields', 'CompanySheetFieldsController@postAjax');
	Route::post('ajaxSheetFields', 'CompanySheetFieldsController@postAjax');
	Route::get('sheetfields/create', array('as' => 'createsheetfield', 'uses' => 'CompanySheetFieldsController@create'));
	Route::resource('sheetfields', 'CompanySheetFieldsController');
	
	
	
	//Public portal
	Route::get('publicportal', array('as' => 'publicPortal', 'uses' => 'HomeController@publicPortal'));
	
	// company Portal
	//Route::get('companyportal', array('as' => 'companyPortal', 'uses' => 'HomeController@companyPortal'));
    Route::get('companyportal', array('as' => 'company.Portal', 'uses' => 'CompanyPortalController@index'));
    Route::any('companyportal/edit/{company}', array('as' => 'companyPortal.edit', 'uses' => 'CompanyPortalController@edit'));
    Route::put('companyportal/update/{company}', array('as' => 'companyPortal.update', 'uses' => 'CompanyPortalController@update'));
	Route::get('companyportal/companytools', array('as' => 'cTools', 'uses' => 'ComCompanyToolsController@index'));
    Route::get('companyportal/details/{company}', array('as' => 'companyportal.details', 'uses' => 'CompanyPortalController@details'));
	Route::get('cCompanytools', array('as' => 'cCompanytools', 'uses' => 'ComCompanyToolsController@index'));
	Route::get('ajaxcCompanytools', 'ComCompanyToolsController@postAjax');
	Route::post('ajaxcCompanytools', 'ComCompanyToolsController@postAjax');

// Company Route Section
	
	Route::get('company','CompanyController@index');
	Route::get('company', array('as'=>'company', 'uses' => 'CompanyController@index'));
	Route::get('company/create', array('as' => 'newcompany', 'uses' => 'CompanyController@create'));
	Route::post('ajaxCompany', 'CompanyController@postAjax');
	Route::get('company/details/{company}', array('as' => 'company.details', 'uses' => 'CompanyController@details'));
	Route::get('company/search', array('as' => 'csearch', 'uses' => 'CompanyController@search'));
	Route::get('csearch', 'CompanyController@search');
	Route::get('company/edit/{company}', array('as' => 'company.edit', 'uses' => 'CompanyController@edit'));
	#Route::post('company/create', ['as' => 'newcompany_store', 'uses' => 'CompanyController@store']);
	Route::put('company/update/{company}', array('as' => 'company.update', 'uses' => 'CompanyController@update'));
	Route::post('company/store', 'CompanyController@store');
//		
// Routs section for company portal
	
	// Company portal client Section 
	//Route::get('companyportal/clients', array('as' => 'companyPortal', 'uses' => 'CompanyClientController@index'));
	Route::get('companyportal/clients', array('as' => 'Cpclient', 'uses' => 'CompanyClientController@index'));
	Route::get('companyportal/client/details/{client}', array('as' => 'cpclients.details', 'uses' => 'CompanyClientController@details'));
    Route::get('companyportal/client/edit/{company}', array('as' => 'companyclient.edit', 'uses' => 'CompanyClientController@edit'));
    Route::any('companyportal/client/update/{company}', array('as' => 'companyclient.update', 'uses' => 'CompanyClientController@update'));
    //Route::post('companyportal/client/update/{company}', array('as' => 'companyclient.update', 'uses' => 'CompanyClientController@update'));
	Route::get('ajaxCompanyPortalClient', 'CompanyClientController@postAjax');
	Route::post('ajaxCompanyPortalClient', 'CompanyClientController@postAjax');
    Route::get('companyClientSearch','CompanyClientController@search');
	Route::get('companyportal/client/create', array('as' => 'companynewclient', 'uses' => 'CompanyClientController@create'));
    Route::get('companyportal/clients/store', array('as' => 'companystoreclient', 'uses' => 'CompanyClientController@store'));
    Route::post('companyportal/clients/store', array('as' => 'companystoreclient', 'uses' => 'CompanyClientController@store'));
	Route::controller('companyportal/clients', 'CompanyClientController');
	
    // Comapny portal staff section routes
    Route::get('companyportal/staff', array('as' => 'Cpstaff', 'uses' => 'CompanyStaffController@index'));
    Route::get('companyportal/staffcreate', array('as' => 'newcpstaff', 'uses' => 'CompanyStaffController@create'));
    Route::get('companyportal/storeStaff', 'CompanyStaffController@storeCompanyPortalStaff');
	Route::post('companyportal/storeStaff', 'CompanyStaffController@storeCompanyPortalStaff');
    Route::get('companyportal/staff/edit/{staff}', array('as' => 'Cpstaff.edit', 'uses' => 'CompanyStaffController@editCompanyPortalStaff'));
    Route::any('companyportal/staff/update/{staff}', array('as' => 'Cpstaff.update', 'uses' => 'CompanyStaffController@updateStaff'));
    Route::get('ajaxCompanyPortalStaff', 'CompanyStaffController@postAjax');
	Route::post('ajaxCompanyPortalStaff', 'CompanyStaffController@postAjax');
    Route::controller('companyportal/staff', 'CompanyStaffController');
    
	// Company portal template Section 
	Route::get('companyportal/templates', array('as' => 'companyTemplate', 'uses' => 'CompanyTemplatesController@index'));
	#Route::get('companyportal/clients', array('as' => 'Cpclient', 'uses' => 'CompanyClientController@index'));
	Route::get('companyportal/templates/details/{template}', array('as' => 'cptemplates.details', 'uses' => 'CompanyTemplatesController@details'));
	Route::get('companyportal/templates/edit/{template}', array('as' => 'cptemplates.edit', 'uses' => 'CompanyTemplatesController@edit'));
    Route::get('companyportal/templates/create', array('as' => 'newcptemplate', 'uses' => 'CompanyTemplatesController@create'));
    Route::post('companyportal/templates/store', 'CompanyTemplatesController@store');
    Route::put('companyportal/templates/update/{template}', array('as' => 'cptemplates.update', 'uses' => 'CompanyTemplatesController@update'));
	#Route::get('companyportal/client/details/{client}', array('as' => 'cpclients.details', 'uses' => 'CompanyClientController@details'));
	#Route::get('ajaxcCompanytools', 'ComCompanyToolsController@postAjax');
	#Route::post('ajaxcCompanytools', 'ComCompanyToolsController@postAjax');
	#Route::get('companyportal/client/create', array('as' => 'companynewclient', 'uses' => 'CompanyClientController@create'));
	Route::controller('companyportal/templates', 'CompanyTemplatesController');
	
	// report section for company portal
	Route::get('companyportal/reports', array('as' => 'reports', 'uses' => 'CompanyReportController@index'));
	Route::get('reports/sales', 'ReportController@salesReport');
	Route::post('reports/sales', 'ReportController@salesReport');
	Route::get('reports/track_record', 'ReportController@trackRecordReport');
	Route::post('reports/track_record', 'ReportController@trackRecordReport');
	Route::get('reports/sales_stats', 'ReportController@salesStatsReport');
	Route::post('reports/sales_stats', 'ReportController@salesStatsReport');
	Route::get('reports/comparison', 'ReportController@comparisonReport');
	Route::post('reports/comparison', 'ReportController@comparisonReport');
	
// All Routs section for company portal end here
	
	Route::get('reports', array('as' => 'reports', 'uses' => 'ReportController@index'));
	Route::get('reports/sales', 'ReportController@salesReport');
	Route::post('reports/sales', 'ReportController@salesReport');
	Route::get('reports/track_record', 'ReportController@trackRecordReport');
	Route::post('reports/track_record', 'ReportController@trackRecordReport');
	Route::get('reports/sales_stats', 'ReportController@salesStatsReport');
	Route::post('reports/sales_stats', 'ReportController@salesStatsReport');
	Route::get('reports/comparison', 'ReportController@comparisonReport');
	Route::post('reports/comparison', 'ReportController@comparisonReport');
	//
    Route::get('validateajaxPermission', 'HomeController@postAjax');
	Route::post('validateajaxPermission', 'HomeController@postAjax');
   
    //
	Route::post('ajaxCompanyAlerts', 'HomeController@postAjax');
	Route::get('ajaxsetReadAlert', 'HomeController@setReadAlert');
	Route::get('ajaxFilterAlerts', 'HomeController@notifications');	
	Route::get('ajaxCompleteAlert', 'HomeController@completeAlert');
    
    Route::get('ajaxroles', 'RolesController@postAjax');
    Route::post('ajaxroles', 'RolesController@postAjax');
	Route::resource('roles', 'RolesController');
//});


Event::listen('eloquent.*: *', function($model){
	
	$event_name = Event::firing();
	$class_name = explode(' ', $event_name);
	$type = $class_name[1];
	$data_after = array();
	$data_before = array();
	$model_array = $model->toArray();
//	if($type=='App\Address'
//		&& $event_name!='eloquent.booting: App\Address'
//		&& $event_name!='eloquent.booted: App\Address'
//		&& $event_name!='eloquent.saving: App\Address'
//		&& $event_name!='eloquent.created: App\Address'
//		&& $event_name!='eloquent.saved: App\Address'
//		&& $event_name!='eloquent.creating: App\Address')
//		dd($event_name);
//	dd('h');
//	dd($type);
//	if($type-)
	if (strpos($event_name, 'eloquent.updating') !== false and $type != 'App\AuditTrail') {

		$id = $model->id;
		$action = 'update';
		$dirty = $model->getDirty();
		foreach ($dirty as $key => $value) {
			$origin_value = $model->getOriginal($key);
			$data_before[$key] = $origin_value;
			$data_after[$key] = $value;
		}
		$trail = new App\AuditTrail;
		$trail->type_id = $id;
		$trail->type = $type;
		$trail->action = $action;
		$trail->before = serialize($data_before);
		$trail->after = serialize($data_after);
	
		$user_id = 0;
		$auth_user = Auth::user();
		if(!empty($auth_user) && isset($auth_user->id))
			$user_id = $auth_user->id;
		$trail->user_id = $user_id;
		$trail->save();
	} elseif (strpos($event_name, 'eloquent.saving') !== false and $type != 'App\AuditTrail' and !array_key_exists('id', $model_array)) {
		$action = 'save';
		$data_after = $model->toArray();
		$trail = new App\AuditTrail;
		//dd($trail);
		$trail->type = $type;
		$trail->action = $action;
		$trail->after = serialize($data_after);
		$user_id = 0;
		$auth_user = Auth::user();
		if(!empty($auth_user) && isset($auth_user->id))
			$user_id = $auth_user->id;
		$trail->user_id = $user_id;
//		dd($trail);
//		dd('he');
		$trail->save();


	} elseif (strpos($event_name, 'eloquent.deleting') !== false and $type != 'App\AuditTrail') {
		$id = $model->id;
		$action = "delete";
		$data_before = $model->toArray();
		$trail = new App\AuditTrail;
		$trail->type_id = $id;
		$trail->type = $type;
		$trail->action = $action;
		$trail->before = serialize($data_before);
	
		$user_id = 0;
		$auth_user = Auth::user();
		if(!empty($auth_user) && isset($auth_user->id))
			$user_id = $auth_user->id;
		$trail->user_id = $user_id;
		$trail->save();
	}
	
});





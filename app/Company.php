<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Company extends Model {
	 //protected $fillable = array('type_id');
	 protected $guarded = array();  // Important
	 protected $table = 'companies';
	
	public static $rules_client = array(
			'company_name' => 'required|unique:companies|regex:/(^[A-Za-z0-9 ]+$)+/',
			'company_type' => 'required',
			'fname' => 'required',
			'lname' => 'required',
			'email' => 'required|email|unique:users|unique:companies',
	);
	
	public static $company_lables = array(
			
			'company_number' =>"Company Number",
			'company_name' => "Company Name" ,
			'company_type' => "Company Type" ,
			'fname' => 'Primary Contact - First Name',
			'lname' =>"Primary Contact - Last Name",
	        'email' =>"Primary Contact - Email",
			'company_title' =>"Primary contact - Title",
	        'primary_phone' =>"Primary contact - Primary Phone",
	        'alternate_phone' =>"Primary contact - Alternate Phone",
	        'addr_line1' => "Address Line 1",
			'addr_line2' => "Address Line 2",
	        'city' =>"City",
	        'state_id' =>"State",
			'country_id' =>"Country",
	        'zip' =>"Zip",
	        'referral_source' =>"How did you hear about OMS",
	        'website' =>"Website",
	        'linkedin_url' =>"LinkedIn URL",
	        'facebook_url' =>"Facebook URL",
	        'twitter_url' =>"Twitter URL",
	        'googleplus_url' =>"Google+ URL",
	        'status' =>"Status"
	);
	public static $baseRules = array(
			'company_name' => 'required | unique',
			'company_type' => 'required',
			'fname' => 'required',
			'lname' => 'required',
			'email' => 'email'
	);
	public static function validateUpdate($data, $id) {
		$updateRule = static::$baseRules;
       // $updateRule['company_name'] = 'required | unique |' . $id;
        //$updateRule['company_name'] = 'unique:companies,' . $id;
        $updateRule['company_name'] = 'required|unique:companies,company_name,' . $id;
		$updateRule['email'] = 'required | email | unique:companies,email,' . $id;
		//$updateRule['band'] = 'required | unique:musicians,band,' . $id;
		return Validator::make($data, $updateRule);
	}
    
	public function address() {
		return $this->hasMany('App\Address');
	}
	
	public function transactions() {
		return $this->hasMany('App\Transaction');
	}
	
	public function staff() {
		return $this->hasMany('App\Staff');
	}
	
    public function subscriptionPurchased() {
		return $this->hasMany('App\SubscriptionPurchased');
	}
    
	public function companydataoption() {
		return $this->belongsTo('App\CompanyDataOption');
	}
    
    public static function create(array $attributes) {
       // print_r($attributes); die;
     $model = new static($attributes);

     $model->save();

     return $model;
    }
    
   public static function companyUserSubscribedData($companyId,$status = 'Active',$subscribed = 0) {
		 
		$tt = \DB::table('companies')
        ->join('users', 'users.company_id', '=', 'companies.id')
        ->where('users.company_id', '=', $companyId)
		->where('companies.id', '=', $companyId)
       // ->where('companies.status', '=', $status)
       // ->where('users.status', '=', $status)
        ->where('companies.subscribed', '=', $subscribed)
        ->groupBy('companies.company_number')
        ->select('users.id','users.first_name','users.last_name','users.email', 'users.name','companies.company_number','companies.company_name')
        ->get();
		return $tt;
	 }
  public static function getAllCompanies($status='Active') {
       
		$allCompaniesList = \DB::table('companies','subscription_purchased','subscriptions')
         //->join('subscriptions', 'subscriptions.id', '=', 'subscription_purchased.subscription_id')
        //->join('subscription_purchased', 'subscription_purchased.company_id', '=', 'companies.id')
       ->leftJoin('subscription_purchased', 'subscription_purchased.company_id', '=', 'companies.id') // ['modified_by']
       ->leftJoin('subscriptions', 'subscriptions.id', '=', 'subscription_purchased.subscription_id') // ['created_by']
     
       // ->where('users.company_id', '=', $companyId)
		//->where('companies.id', '=', $companyId)
        ->where('companies.status', '!=', 'Deleted')
        ->where('companies.id', '!=', 1)
        ->where('companies.id', '!=', 2)
        ->where('companies.is_deleted','=', 0)
       // ->where('users.status', '=', $status)
       // ->where('companies.subscribed', '=', $subscribed)
       // ->groupBy('companies.company_number')
       // ->select('companies.id', 'companies.company_number', 'companies.company_name','companies.fname','companies.lname', 'companies.city', 'companies.state_id', 'companies.status');
        ->select('companies.id', 'companies.company_number', 'companies.company_name','companies.fname','companies.lname', 'companies.city', 'companies.state_id', 'companies.status','subscriptions.subscription_name');
        //->get();
		return $allCompaniesList;
  }   
  
  public static function getCompanyList($companyID, $status='Active') {
       
		$companiesList = \DB::table('companies','subscription_purchased','subscriptions')
      
       ->leftJoin('subscription_purchased', 'subscription_purchased.company_id', '=', 'companies.id') // ['modified_by']
       ->leftJoin('subscriptions', 'subscriptions.id', '=', 'subscription_purchased.subscription_id') // ['created_by']
       ->leftJoin('users', 'users.company_id', '=', 'companies.id')
       ->where('companies.status', '=', $status)
       ->where('companies.id', '=', $companyID)
       ->where('users.primary_contact', '=', 1)
       ->orderBy('companies.id', 'desc')
       ->limit(1)
       ->select('companies.id', 'companies.company_number', 'companies.company_name','users.first_name','users.last_name', 'companies.city', 'companies.state_id', 'companies.status','subscriptions.subscription_name');
        return $companiesList;
  }
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
class Subscription extends Model {
	 //protected $fillable = array('type_id');
	 //protected $guarded = array();  // Important
	 protected $table = 'subscriptions';
	
	public static $base_rules = array(
			'subscription_name' => 'required|unique:subscriptions',
			'fee_type' =>  array('required', 'regex:/^\d*(\.\d{2})?$/'),
			'cost_per_unit' =>'required|numeric|digits_between:1,3',
			//'billing_frequency' => 'required|numeric|digits_between:1,3',
            'billing_frequency' => 'required',
			'subscription_status' => 'required',
            'subscription_users_allowed' => 'required|numeric|digits_between:1,2',
            'max_user_allowed' => 'required|numeric|digits_between:1,2',
         //   'start_date' => 'date_format:m/d/Y',
            //'end_date' => 'date_format:m/d/Y|after:start_date',
	);
	
	public static $company_lables = array(
			
			'subscription_name' =>"Subscription Package Name",
			'fee_type' => "Fee Type (Flat Rate/Per User) " ,
			'cost_per_unit' => "Cost Per Unit" ,
			'billing_frequency' => 'Billing Frequency',
			'subscription_status' =>"Status",
	        //'start_date' =>"Start Date",
			//'end_date' =>"End Date",
	        'subscription_desc' =>"Subscription Desc"
	);
	public static $baseRules = array(
			'subscription_name' => 'required',
			'fee_type' =>  array('required', 'regex:/^\d*(\.\d{2})?$/'),
			'cost_per_unit' => array('required', 'regex:/^\d*(\.\d{2})?$/'),
			//'billing_frequency' => 'required|numeric|digits_between:1,3',
            'billing_frequency' => 'required',
			'subscription_status' => 'required',
            'subscription_users_allowed' => 'required|numeric|digits_between:1,2',
            'max_user_allowed' => 'required|numeric|digits_between:1,2',
	);
	public static function validateUpdate($data, $id)
	{
		$updateRule = static::$baseRules;
		$updateRule['subscription_name'] = 'required | unique:subscriptions,subscription_name,' . $id;
		//$updateRule['band'] = 'required | unique:musicians,band,' . $id;
		return Validator::make($data, $updateRule);
	}
    public function setFormatedDate($date) {
  		  		
  		if(!empty($date))
  			return date('Y-m-d', strtotime($date));
  		else
  			return NULL;
  	}
    public function getFormatedDate($date='date_met') {
		if($this->{$date})
  			return date('m/d/Y', strtotime($this->{$date}));
		else 
			return '';	
  	}
	public function billingHostory(){
		return $this->hasMany('App\BillingHostory');
	}

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySheetFieldsTransactionTypeDefaults extends Model {

    protected $table = 'company_sheets_fields_types_defaults';

}

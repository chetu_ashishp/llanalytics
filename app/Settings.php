<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'settings';
	
	public static $rules_email = array(
			'from_name_email_notice' => 'required',
			'from_email_notice' => 'required|email',
			'email_testing' => 'required',
			'testing_email' => 'email'
	);

}

<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {
    
	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
	public function Staff()
    {
        //return $this->hasOne('Staff');
		return $this->hasOne('App\Staff');
    }
	
	public function Role()
    {
        return $this->belongsTo('App\Role');
    }
	
    public function Company()
    {
        return $this->hasMany('App\Company');
    }
	public static function OmsUserStaffData($companyId,$filtername = '',$status = 'Active') {
        
		$query = \DB::table('users');
		$query->join('roles', 'roles.id', '=', 'users.role_id');
		$query->select('users.id','users.name','users.company_id', 'users.last_name', 'users.first_name', 'users.email','users.primary_contact','users.alternate_phone','users.mobile_phone','users.status','roles.role_name');
        $query->where('users.company_id', '=', $companyId);
		$query->where('users.status', '=',$status);
        $query->where('users.is_deleted', '=', '0');
		$query->orderBy('users.first_name','asc');
        
		return $query;
	 }
     
     public static function OmsStaffActiveNonActive($companyId, $status = 'Active') {
        if($status == 'Active') {
            
            $query = \DB::table('users');
            $query->join('roles', 'roles.id', '=', 'users.role_id');
            $query->select('users.id','users.name','users.company_id', 'users.last_name', 'users.first_name', 'users.email','users.primary_contact','users.alternate_phone','users.mobile_phone','users.status','users.is_deleted','roles.role_name');
            $query->where('users.company_id', '=', $companyId);
            $query->where('users.status', '=',$status);
            $query->where('users.is_deleted', '=', '0');
            $query->orderBy('users.first_name','asc');
        }
        elseif($status == 'NonActive') {
            
            $matchThese = ['users.status' => 'Active', 'users.is_deleted' => 1];
            $query = \DB::table('users');
            $query->join('roles', 'roles.id', '=', 'users.role_id');
            $query->select('users.id','users.name','users.company_id', 'users.last_name', 'users.first_name', 'users.email','users.primary_contact','users.alternate_phone','users.mobile_phone','users.status','users.is_deleted','roles.role_name');
            $query->where('users.company_id', '=', $companyId);
            $query->where('users.status', '!=', 'Active');          
            $query->orwhere($matchThese);
            $query->orderBy('users.first_name','asc');
        }
		return $query;
     }
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySheetField extends Model {

        protected $table = 'company_sheets_fields';
        
        
        public static $rules = array(
        		'field_name' => 'required',
        		'type' => 'required|in:Alphanumeric,Single Select,Date,Text Label-No Input,Checkbox',
        		'priority_order'=>	'integer',
        		'status' => 'required|in:Active,Inactive',
        		'select_options' => 'required_if:type,Single Select',
        		'parent_field'=>	'required'
        
        );
        public function accsess_users(){
        	return $this->hasMany('App\CompanySheetFieldsAccess', 'field_id');
        }
        
        public function colors_values(){
        	return $this->hasMany('App\CompanySheetFieldBgColors', 'field_id');
        }
}

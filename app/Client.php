<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {

	protected $table = 'clients';
	
	public static $rules_client = array(
			'last_name' => 'required',
			'first_name' => 'required',			
			'email' => 'email',			
			'date_met' => 'date_format:"m/d/Y"',			
			'website' =>'url',
			'linkedin_url' =>'url',
			'facebook_url' =>'url',
			'twitter_url' =>'url',
			'googleplus_url' =>'url',
	);
		
	public static $client_lables = array(
			
			'client_number' =>"Client Number",
			'last_name' => "Last Name" ,
			'first_name' => 'First Name',
			'spouse_fname' =>"Spouse First Name",
	        'spouse_lname' =>"Spouse Last Name  ",
	        'email' =>"Email",
	        'primary_phone' =>"Primary Phone",
	        'alternate_phone' =>"Alternate Phone",
	        'mobile_phone' =>"Mobile Phone",
	        'other_phone' =>"Other Phone",
			'addresses' => "Addresses",
	        'date_met' =>"Date Met",
	        'referral_source' =>"Client Source",
	        'refsource_details' =>"How did you hear details",
	        'notes' =>"Notes",
	        'website' =>"Website",
	        'linkedin_url' =>"LinkedIn URL",
	        'facebook_url' =>"Facebook URL",
	        'twitter_url' =>"Twitter URL",
	        'googleplus_url' =>"Google+ URL",
	        'status' =>"Status"
	);
		
	public function address(){
		return $this->hasMany('App\Address');
	}
	
	public function transactions(){
		return $this->hasMany('App\Transaction');
	}
		
	public function getFormatedDate($date='date_met') {
		if($this->{$date})
  			return date('m/d/Y', strtotime($this->{$date}));
		else 
			return '';	
  	}
    public function getFormatedMetDate($date='date_met') {
		if($date)
  			return date('m/d/Y', strtotime($date));
		else 
			return '';	
  	}
	
  	public function setFormatedDate($date) {
  		  		
  		if(!empty($date))
  			return date('Y-m-d', strtotime($date));
  		else
  			return NULL;
  	}  	
  	
}
<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RolesMeta extends Model {

    protected $guarded = array();
    protected $table = 'role_meta';
    public static $rules = array();

}

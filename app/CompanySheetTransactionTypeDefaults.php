<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySheetTransactionTypeDefaults extends Model {

    protected $table = 'company_sheets_types_defaults';

}

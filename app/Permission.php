<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Permission extends Model {

	protected $table = 'permissions';
	
	public static $rules = array(
			'name' => 'required',
			'route' => 'required',
	);
	public static $client_lables = array(
			
			'name' =>"Name",
			'email' => "Email" ,
			'password' => 'Password',
			'mobile_phone' =>"Mobile number",
	        'status' =>"Status",
	        'remember_token' =>"rembebert",
	        'role_id' =>"Role",
	        'company_id' =>"Company",
	        'primary_contact' =>"Primary Phone",
	        'first_name' =>"First Name",
			'last_name' => "Last Name"
	);
	
	
	public static $baseRules = array(
			'fname' => 'required',
			'lname' => 'required',
			'email' => 'required|email|unique:users',
			//'password' =>'required|confirmed',
			'role_id' =>'required',
	);	
	
}

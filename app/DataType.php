<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DataType extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'data_types';

}

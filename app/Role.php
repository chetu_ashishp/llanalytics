<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

    protected $guarded = array();
    protected $fillable = array('site_id', 'role_id', 'user_login', 'password', 'activated');
    public static $rules_new = array(
        'role_name' => 'required',
        'role_description' => 'required'
    );
    
    public static $customRules = array( 
			'role_name' => 'required',
			'role_description' => 'required',
            'permission_level' => 'required|integer|between:1,10'
	);
    
    static function isAllow($role_id,$route,$method='GET'){
        
        if(is_null(RolesMeta::where('role_id', '=', $role_id)->where('method', '=', $method)->where('route', '=', $route)->where('permission', '=', 1)->first()))
            return FALSE;
        else
            return TRUE;
    }
	public function user()
    {
       // return $this->belongsTo('App\User');
		 return $this->belongsTo('App\User')->select(array('id', 'role_id'));
    }

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyDataOption extends Model {

	protected $table = 'company_data_options';
	protected $fillable = array('type_id');

}

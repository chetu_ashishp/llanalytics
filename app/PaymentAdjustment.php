<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
class PaymentAdjustment extends Model {
    protected $table = 'payment_adjustment';
    
    public static $rules_client = array(
			'payment_adjustment_type' => 'required',
			'payment_adjustment_date' => 'required|date|date_format:"m/d/Y"',
			'payment_adjustment_amount' => array('required', 'regex:/^\d*(\.\d{2})?$/'),//array('required', 'regex:/^\d*(\.\d{2})?$/'),
			'payment_adjustment_reason' => 'required',
	);
    
    public function setFormatedDate($date) {
  		  		
  		if(!empty($date))
  			return date('Y-m-d', strtotime($date));
  		else
  			return NULL;
  	}
    
     public function ConvertFormatedDate($date) {
  		  		
  		if(!empty($date))
  			return date('m-d-Y', strtotime($date));
  		else
  			return NULL;
  	}
}

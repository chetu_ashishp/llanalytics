<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySheet extends Model {

        protected $table = 'company_sheets';
	
	public static $rules_template = array(
            'scratch'   => array(
                'name'              => 'required',			
                'purpose'           => 'required',	
                'rows_to_freeze'    => 'integer|required',
                'first_column_width'      => 'integer|required',
                'column_width'      => 'integer|required',
                'status'            => 'required',
                'column_order'      => 'required',
            ),
            'template'  => array(
                'name'        => 'required',
                'sheet_template'    => 'integer|required',
            ),
        );
    //    
	public static $transaction_fields = array(
            'coe_date'                  => 'COE Date',
            'contract_date'             => 'Contract Date',
            'current_list_price'        => 'Current List Price',
            'property_house'            => 'Property House #',
            'nickname'                  => 'Short Address',
            'subdivision'               => 'Subdivision',
            'property_addr1'            => 'Property Address Line 1',
            'city_id'                   => 'Property City',
            'state'                     => 'Property State',
            'zip'                       => 'Property ZIP',
            'sales_price'               => 'Sales Price',
            'other_agent_name'          => 'Other Agent',
            'other_company_name'        => 'Other Company',
            'escrow_officer'            => 'Escrow Officer',
            'title_company'             => 'Title Company',
            'lender'                    => 'Cash / Lender',
            'loan_officer'              => 'Loan Officer',
            'listing_date'              => 'Listing Date',
            'listing_expiration_date'   => 'Listing Expiration Date',
            'mls1'                      => 'MLS # - 1',
            'mls2'                      => 'MLS # - 2',
            'lockbox'                   => 'Lockbox #',
            'transaction_status'        => 'Transaction Status',
        );
        
        public static $client_fields = array(
            'last_name'     => 'Last Name',
            'first_name'    => 'First Name',
//            'full_name'    => 'Last Name, First Name',
        );
        
        public static $field_types = array(
            'last_name'                 => 'text',
            'first_name'                => 'text',
            'full_name'                 => 'text',
            'mls1'                      => 'text',
            'mls2'                      => 'text',
            'other_company_name'        => 'text',
            'loan_officer'              => 'text',
            'zip'                       => 'select',
            'contract_date'             => 'date',
            'coe_date'                  => 'date',
            'sales_price'               => 'number',
            'lender'                    => 'select',
            'title_company'             => 'select',
            'escrow_officer'            => 'text',
            'nickname'                  => 'text',
            'property_house'            => 'text',
            'property_addr1'            => 'text',
            'city_id'                   => 'select',
            'state'                     => 'select',
            'subdivision'               => 'text',
            'listing_date'              => 'date',
            'listing_expiration_date'   => 'date',
            'other_agent_name'          => 'text',
            'transaction_status'        => 'select',
            'lockbox'                   => 'text',
            'current_list_price'        => 'number',
        );
        
        public static $status = array(
            "Active"    => "Active",
            "Inactive"  => "Inactive"
        );

        public static $input_fields = array(
            'sheet_template',
            'name',
            'purpose',
            'rows_to_freeze',
            'freeze_first_column',
            'row_height',
            'first_column_width',
            'column_width',
            'status',
            'all_user_accsess',
            'column_order',
            'all_transaction_type',
            'all_transaction_status',
        );

    public static $column_order = array(
        ''=>'',
        'transactions.coe_date,desc' =>"COE - Earliest to Latest",
        'transactions.coe_date,asc' =>"COE - Latest to Earliest",
        'transactions.contract_date,desc' =>"Contract Date - Most Recent to Oldest",
        'transactions.contract_date,asc' =>"Contract Date - Oldest to Most Recent",
        'transactions.listing_date,desc' =>"Listing Date - Most Recent to Oldest",
        'transactions.listing_date,asc' =>"Listing Date - Oldest to Most Recent",
        'clients.last_name, asc' =>"Last Name - A-Z",
        'transactions.property_addr1,asc' =>"Address - Alphabetical",

        //'transactions.city,asc' =>"City - Alphabetical",
        //'transactions.subdivision,asc' =>"Sub-Division - Alphabetical",
    );

        public function fields(){
        	return $this->hasMany('App\CompanySheetField', 'sheet_id');
        }
        
}

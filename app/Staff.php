<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Staff extends Model {

	protected $table = 'staffs';
	
	public static $rules = array(
			//'user_id' => 'required',
			//'company_id' => 'required'
			'fname' => 'required',
			'lname' => 'required',
            'title' => 'required',			
			'email' => 'required|email|unique:users',
			//'password' =>'required|confirmed',
			'primary_phone' => 'required'
	);
	public static $client_lables = array(
			
			'name' =>"Name",
			'email' => "Email" ,
			'password' => 'Password',
			'mobile_phone' =>"Mobile number",
	        'status' =>"Status",
	        'remember_token' =>"rembebert",
	        'role_id' =>"Role",
	        'company_id' =>"Company",
	        'primary_contact' =>"Primary Phone",
	        'first_name' =>"First Name",
			'last_name' => "Last Name"
	);
	
	
	public static $baseRules = array(
			'fname' => 'required',
			'lname' => 'required',
			'email' => 'required|email|unique:users',
			//'password' =>'required|confirmed',
			'role_id' =>'required',
	);
	public static function validateOmsStore($data)
	{
		$updateRule = static::$baseRules;
		$updateRule['email'] = 'required | email | unique:users,email,';
		//$updateRule['band'] = 'required | unique:musicians,band,' . $id;
		return Validator::make($data, $updateRule);
	}
	
	public static function validateOmsStaffUpdate($data, $id)
	{
		$updateRule = static::$baseRules;
		$updateRule['email'] = 'required | email | unique:users,email,' . $id;
		//$updateRule['band'] = 'required | unique:musicians,band,' . $id;
		return Validator::make($data, $updateRule);
	}
	public function user()
    {
        return $this->hasOne('App\User');
    }
	
	public function company()
    {
        return $this->hasOne('App\Company');
    }
	
	public function getFormatedDate($date='start_date') {
		if($this->{$date})
  			return date('m/d/Y', strtotime($this->{$date}));
		else 
			return '';	
  	}
	 
	public function setFormatedDate($date) {
	
		if(!empty($date))
			return date('Y-m-d', strtotime($date));
		else
			return NULL;
	}
	
	public static function staffLists($companyId) {
		
		$tt = \DB::table('staffs')
        ->join('users', 'staffs.user_id', '=', 'users.id')
		 ->join('roles', 'roles.id', '=', 'users.role_id')		
        ->where('staffs.company_id', '=', $companyId)
		->where('users.status', '=', 'Active')
        ->where('users.is_deleted', '=', '0')
        //->groupBy('users.name')
        ->select('staffs.user_id', 'users.id','users.name', 'users.last_name', 'users.first_name', 'users.email','users.primary_contact','users.mobile_phone','users.primary_contact','users.status','roles.role_name')
        ->get();
		return $tt;
	 } 
	 
	 public static function UserstaffData($companyId,$userId) {
		 
		$tt = \DB::table('staffs')
        ->join('users', 'staffs.user_id', '=', 'users.id')
		 ->join('roles', 'roles.id', '=', 'users.role_id')		
        ->where('staffs.company_id', '=', $companyId)
		->where('users.id', '=', $userId)
        ->groupBy('users.name')
        ->select('staffs.user_id', 'users.id','users.name', 'users.last_name', 'users.first_name', 'users.email','users.primary_contact','users.alternate_phone','users.mobile_phone','users.status','users.role_id','roles.role_name','staffs.linkedin_url','staffs.facebook_url','staffs.twitter_url','staffs.googleplus_url')
        ->get();
		return $tt;
	 } 
	 
     public static function getstaffFilter($companyId,$status) {
         
        if($status == 'Active') {
            $tt = \DB::table('staffs')
            ->join('users', 'staffs.user_id', '=', 'users.id')
             ->join('roles', 'roles.id', '=', 'users.role_id')
            ->where('staffs.company_id', '=', $companyId)
            ->where('users.status', '=', 'Active')
            ->where('users.is_deleted', '=', '0')
            ->select('staffs.user_id', 'users.id','users.name', 'users.last_name', 'users.first_name', 'users.email','users.primary_contact','users.mobile_phone','users.primary_contact','users.status','users.is_deleted','roles.role_name')
            ->get();            
        }
        elseif($status == 'NonActive') {
            $matchThese = ['users.status' => 'Active', 'users.is_deleted' => 1];
            $tt = \DB::table('staffs')
            ->join('users', 'staffs.user_id', '=', 'users.id')
             ->join('roles', 'roles.id', '=', 'users.role_id')
            ->where('staffs.company_id', '=', $companyId)
            ->where('users.status', '!=', 'Active')
            //->where('users.status', '=', 'Active' && 'users.is_deleted' = 1)
            ->orwhere($matchThese)
           
            ->select('staffs.user_id', 'users.id','users.name', 'users.last_name', 'users.first_name', 'users.email','users.primary_contact','users.mobile_phone','users.primary_contact','users.status','users.is_deleted','roles.role_name')
            ->get();
            
            $status = '';
       }
        
        
		return $tt;
     }
	 
	
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model {

protected $table = 'templates';
	
	public static $rules_template = array(
			
			'name' => 'required',			
			'purpose' => 'required',	
			'rows_to_freeze' => 'integer|required',
			'status' => 'required',			
			'column_order' => 'required',
	);
	
	public static $column_order = array(
			''=>'',
			'transactions.property_addr1,asc' =>"Alphabetical by Property Address",
			'clients.last_name, asc' =>"Alphabetical by Client Last Name",
			
			'transactions.city,asc' =>"Alphabetical by Property City",
			'transactions.subdivision,asc' =>"Alphabetical by Property Sub-Division",
			
			'transactions.listing_date,asc' =>"Oldest to Newest by Listing Date",
			'transactions.listing_date,desc' =>"Newest to Oldest by Listing Date",
			
			'transactions.contract_date,asc' =>"Oldest to Newest by Contract Date",
			'transactions.contract_date,desc' =>"Newest to Oldest by Contract Date",
			
			'transactions.coe_date,asc' =>"Oldest to Newest by Escrow Close Date",
			'transactions.coe_date,desc' =>"Newest to Oldest by Escrow Close Date",		
	);
	
	
	public function tplIncludedField(){
		return $this->hasMany('App\TemplateIncluded');
	}
	
	public function accsess_users(){
		return $this->hasMany('App\SheetAccess'); 
	}
	 
	public function fields(){
		return $this->hasMany('App\TemplateField');
	}
	
	
	/*
	public static function boot()
	{
		parent::boot();
	
		// cause a delete of a product to cascade to children so they are also deleted
		static::deleted(function($template)
		{
			$template->tplIncludedField()->delete();
			$template->accsess_users()->delete();
		});
	}
	*/
	
	
	
	
}
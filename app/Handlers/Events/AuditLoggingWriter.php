<?php namespace App\Handlers\Events;

use App\Events\AuditLogging;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class AuditLoggingWriter {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  AuditLogging  $event
	 * @return void
	 */
	public function handle(AuditLogging $event)
	{
		//
	}

}

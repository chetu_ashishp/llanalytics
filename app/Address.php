<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {


	protected $table = 'addresses';
	
	public static $rules_client = array(
			'type' => 'required',			
			'zip' =>'integer'
	);

	
	public function cstate(){
		return $this->hasOne('App\State', 'id', 'state_id');
	}
	
	
	/*public function ccity(){
		return $this->hasOne('App\CompanyDataOption', 'id', 'city');
	}
	
	public function czip(){
		return $this->hasOne('App\CompanyDataOption', 'id', 'zip');
	}*/
	
	
}

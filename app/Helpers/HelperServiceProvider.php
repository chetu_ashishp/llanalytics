<?php 

use App\State;
use App\Country;
use App\CompanyDataOption;
use App\User;
use App\SubscriptionPurchased;
	
function isKeyUsedAsForeignKey($table, $key, $notIn = '') {
	
	$key = intval($key);
        if (preg_match('/^[-_a-z0-9]+$/i', $table) && $key > 0) {
            $tables_array = array();
            if (!empty($notIn) && is_array($notIn)) {
                $notInGlued = implode(', ', $notIn);
                $results = DB::select('SELECT table_name,column_name FROM information_schema.key_column_usage WHERE referenced_table_name = ? and table_name not in (?) and CONSTRAINT_SCHEMA=?', array($table, $notInGlued, DB::getConfig('database')));
            } else
                $results = DB::select('SELECT table_name,column_name FROM information_schema.key_column_usage WHERE referenced_table_name = ? and CONSTRAINT_SCHEMA=?', array($table, DB::getConfig('database')));
            
           // dd($results);
            
            foreach ($results as $value) {
                $count = DB::table($value->table_name)->where($value->column_name, '=', $key)->count();
                if ($count > 0)
                    $tables_array[] = $value->table_name;
                     array_push($tables_array, $value->table_name);//added chetu
            }
            
            if (empty($tables_array)){
            	return FALSE;
            }    
            else
                return $tables_array;
        }
        return false;
    }

function iskeyUsedCompany($table,$companyId) {
    
   $companyUsed = SubscriptionPurchased::select( array('subscription_purchased.id','subscription_purchased.subscription_id') )->where('subscription_purchased.company_id',$companyId)->count();
   if($companyUsed) {
         return true;
   }
   return false;
}

function iskeyUsedTable($table,$columnName,$value) {
    
  $chkItem = DB::table($table)->where($columnName, $value)->count();
   if($chkItem) {
         return true;
   }
   return false;
}

 function getStates() {
 	$states =  State::all()->lists('state_name','id'); 	
 	return array(''=>'')+$states;
 } 
 
 function getAllStates() {
 	$states =  State::all()->lists('state_name','state_name');
 	return array(''=>'')+$states;
 } 
 
 function getAllCompanies() {
 	$countries =  Country::all()->lists('country_name','country_name');
 	return array(''=>'')+$countries;
 } 
 
 function companyOptionsByType($type, $company_id) {
 	$states =  CompanyDataOption::select( array('company_data_options.option_name', 'company_data_options.id') )->leftJoin('company_data_types', 'company_data_types.id', '=', 'company_data_options.type_id')->where('type_name',$type)->where('company_id',$company_id)->orderBy('option_name')->get()->toArray();
 	
 	if(empty($states))	
 		return array();
 	
 	foreach($states as $v) {
 		$values[$v["id"]] = $v["option_name"] ; 
 	}  

 	if($type=='Lender' && in_array('Cash',$values) ){
 		
 		$key = array_search('Cash', $values);
 		unset($values[$key]); 
 		$first = array($key=>'Cash');
 		
 		$values =  $first+$values ; 		
 		
 	}
 		
 	return array(''=>'')+$values;
 }
 
  
 function companyOptionNameById($id){
 	if($id===0 || $id===NULL || $id=="")
 		return "";
 	else
 		return CompanyDataOption::where('id',$id)->pluck('option_name'); 	
 }
 
 
 function apply_highlight($a_json, $parts) {
 
 	$p = count($parts);
 	$rows = count($a_json);
 	
 	for ($row = 0; $row < $rows; $row++) {
 
 		$label = $a_json[$row]["label"];
 		$a_label_match = array();
 
 		for ($i = 0; $i < $p; $i++) {
 
 			$part_len = mb_strlen($parts[$i]);
 			$a_match_start = mb_stripos_all($label, $parts[$i]);
            
 			if($a_match_start){
	 			foreach ($a_match_start as $part_pos) {
	 
	 				$overlap = false;
	 				foreach ($a_label_match as $pos => $len) {
	 					if ($part_pos - $pos >= 0 && $part_pos - $pos < $len) {
	 						$overlap = true;
	 						break;
	 					}
	 				}
	 				if (!$overlap) {
	 					$a_label_match[$part_pos] = $part_len;
	 				}
	 			}
 			}
 				
 		}
  		if (count($a_label_match) > 0) {
 			ksort($a_label_match);
 
 			$label_highlight = '';
 			$start = 0;
 			$label_len = mb_strlen($label);
 
 			foreach ($a_label_match as $pos => $len) {
 				if ($pos - $start > 0) {
 					$no_highlight = mb_substr($label, $start, $pos - $start);
 					$label_highlight .= $no_highlight;
 				}
 				$highlight = '<span class="hl_results">' . mb_substr($label, $pos, $len) . '</span>';
 				$label_highlight .= $highlight;
 				$start = $pos + $len;
 			}
 
 			if ($label_len - $start > 0) {
 				$no_highlight = mb_substr($label, $start);
 				$label_highlight .= $no_highlight;
 			}
 
 			$a_json[$row]["label"] = $label_highlight;
 		}
 	}
 
 	return $a_json;
 }
 
 
 /**
  * mb_stripos all occurences
  * based on http://www.php.net/manual/en/function.strpos.php#87061
  *
  * Find all occurrences of a needle in a haystack
  *
  * @param string $haystack
  * @param string $needle
  * @return array or false
  */
 function mb_stripos_all($haystack, $needle) {
 
 	$s = 0;
 	$i = 0;
 
 	while (is_integer($i)) {
 
 		$i = mb_stripos($haystack, $needle, $s);
 
 		if (is_integer($i)) {
 			$aStrPos[] = $i;
 			$s = $i + mb_strlen($needle);
 		}
 	}
 
 	if (isset($aStrPos)) {
 		return $aStrPos;
 	} else {
 		return false;
 	}
 }
 
 function getEmailFormat($content = '',$registeredUserFirstLastName,$registeredCompanyName,$OMSCompanyName,$OMSSupportPh,$OMSSupportEmail,$omsurl = '',$username = '',$password = '') {
    
    $my_string = str_replace("((companyprimaryusername))","<b>".$registeredUserFirstLastName."</b>",$content);
    $my_string = str_replace("((companyname))","<b>".$registeredCompanyName."</b>",$my_string);
    $my_string = str_replace("((omscompanycontact))","<b>".$OMSSupportPh."</b>",$my_string);
    $my_string = str_replace("((omscompanyemail))","<b>".$OMSSupportEmail."</b>",$my_string);    
    $my_string = str_replace("((omslinkurl))","<b>".$omsurl."</b>",$my_string);
    $my_string = str_replace("((companyusername))","<b>".$username."</b>",$my_string);
    $my_string = str_replace("((companypassword))","<b>".$password."</b>",$my_string);
    $my_string = str_replace("((omsinternalstaffname))","<b>".$registeredUserFirstLastName."</b>",$my_string);
    $my_string = str_replace("((omsinternalstaffemail))","<b>".$username."</b>",$my_string);
    $my_string = str_replace("((omsinternalstaffpassword))","<b>".$password."</b>",$my_string);
    $my_string = str_replace("((companystaffusername))","<b>".$username."</b>",$my_string);
    $my_string = str_replace("((companystaffpassword))","<b>".$password."</b>",$my_string);
    $my_string = str_replace("((companystaffname))","<b>".$registeredUserFirstLastName."</b>",$my_string);    
    
    return $my_string;
 }
 
 /**
 * A slightly more readable, non-regex solution.
 */
function remove_if_trailing($haystack, $needle) {
    // The length of the needle as a negative number is where it would appear in the haystack
    $needle_position = strlen($needle) * -1;  

    // If the last N letters match $needle
    if (substr($haystack, $needle_position) == $needle) {
         // Then remove the last N letters from the string
         $haystack = substr($haystack, 0, $needle_position);
    }

    return $haystack;
}


function get_anything($string, $ending) {
    if (strpos($string, STARTING) !== 0)
    {
        return false;
    }
    if (substr($string, strlen($ending) * -1) != $ending)
    {
        return false;
    }
    $anything = substr($string, strlen(STARTING));
    $anything = substr_replace($anything, 'aaa', strlen($ending) * -1, -1);
    return $anything;
   // return true;
}

 function GUID() {
    if (function_exists('com_create_guid') === true) {
        return trim(com_create_guid(), '{}');
    }
    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
 }
 
 function checkDateExpire($mydate,$curdate) {
    $curdate = strtotime($curdate); // today
    $mydate = strtotime($mydate); // last date
   // echo 'curdate: '.$curdate. ' ------- '.'Lastdate: '.$mydate.'    ';
    if($curdate <= $mydate) {
        return true;
    }
    return false;
 }
 
 function checkPrimaryContact($userId) {
     
     $primaryUser = User::select( array('users.primary_contact') )->where('users.id',$userId)->where('primary_contact',1)->count();
   
     if($primaryUser == 1) {
         return true;
     }
     return false;
 }
 
 function getCurrentUserData($userId) {
     
      //$userData = User::select( array('users.role_id','users.company_id','companies.id') )->where('users.id',$userId)->get();
     // return $userData;
      $userData = DB::table('users')
            ->join('companies', 'users.company_id', '=', 'companies.id')           
            ->select('users.role_id', 'users.company_id', 'companies.company_name')
            ->where('users.id',$userId)           
            ->get();
       return $userData;
 }
 
 function getDateFormat($date) {
     $newDate = date('m-d-Y', strtotime($date));
     return $newDate;
 }
 
?>
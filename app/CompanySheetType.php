<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySheetType extends Model {

	protected $table = 'company_sheets_types';

}

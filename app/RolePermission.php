<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class RolePermission extends Model {

	protected $table = 'role_permissions';
	
	public static $rules = array(
			'fname' => 'required',
			'lname' => 'required',
            'title' => 'required',			
			'email' => 'required|email|unique:users',
			'primary_phone' => 'required'
	);
	public static $client_lables = array(
			
			'name' =>"Name",
			'email' => "Email" ,
			'password' => 'Password',
			'mobile_phone' =>"Mobile number",
	        'status' =>"Status",
	        'remember_token' =>"rembebert",
	        'role_id' =>"Role",
	        'company_id' =>"Company",
	        'primary_contact' =>"Primary Phone",
	        'first_name' =>"First Name",
			'last_name' => "Last Name"
	);
	
	
}

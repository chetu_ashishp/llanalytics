<?php
//use Illuminate\Support\Facades\Config;
return array(
  'company_id' => 1, // for default company
  'role_id' => 2, // for Standard Administrator
  'company_super_admin_role_id' => 1, // for Standard Administrator
  'primary_contact' => 1, // default 1 for primary staff of a company
  'omsStaffCompanyID' => 1, // default 1 for primary staff of a company
  'OMSSupportPh' => '000000000', // default 1 for primary staff of a company
  'OMSSupportEmail' => 'laurie@greenvalley123.com',
  'OMSEmailFromAccount' => 'laurie@greenvalley123.com',
  'companyName' => 'OMS Solution',
  'companySubscriptioApprovalSubject' => 'Company subscription Approval',
  'OMSCompanyLinkUrl' => 'www.oms.com',
  'my_arr_val' => array('1', '2', '3'),
);
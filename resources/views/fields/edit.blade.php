 	<div class="panel-body top20">
					<div class="container datalist">		    	
						<div class="jumbotron container">
						
							{!! Form::open( array('id' => 'field-edit-form', 'data-route' => 'updateField', 'class'=>'form-horizontal mform-edit',  'autocomplete'=>"off", 'data-path' => 'Fields', 'data-_token'=>csrf_token() )) !!}
							{!!Form::hidden('id', $item->id)!!}
							
							   
							<div class="form-group">
						    	<div class="col-sm-12">	     	
									{!! Form::button('Cancel', array('class'=>'btn   btn-default btn-sm  pull-right left10', 'id'=>'cancelEditForm'))!!}
									{!! Form::submit('Save', array('class'=>'btn  btn-info btn-sm  pull-right'))!!}
						    	</div>
							</div>
							   						
							<div class="row">					
								<div class="col-sm-6">
				               		{!!Form::hidden('template_id', $template_id, array('class'=>'templateid'))!!}
				               		
				               		<div class="form-group">
					                    {!!Form::label('field_name','Name', array('class'=>'col-sm-5 control-label required'))!!}
					                    <div class="col-sm-7">
					                    	{!!Form::text('field_name', $item->field_name, array('class'=>'form-control required', 'required'))!!}
					                	</div>
					            	</div>
						            	
					                <div class="form-group">
					                    {!!Form::label('type','Type', array('class'=>'col-sm-5 control-label required'))!!}
					                    <div class="col-sm-7">
					                    	{!!Form::select('type',  array(""=>"", "Alphanumeric"=>"Alphanumeric", "Checkbox"=>"Checkbox", "Date"=>"Date", "Single Select"=>"Single Select",  "Text Label-No Input"=>"Text Label-No Input") ,$item->type, array('class'=>'form-control ftype', 'required', 'id'=>'eftype'))!!}
					                	</div>
					            	</div> 					            			            	
					            	
					            	<div class="form-group sel-options-div" @if($item->type!='Single Select') style="display:none" @endif >
					                    {!!Form::label('select_options','Options for select', array('class'=>'col-sm-5 control-label required'))!!}
					                    <div class="col-sm-7">
					                    	@if($item->type!='Single Select')
					                    		{!!Form::textarea('select_options', $item->select_options , array('class'=>'form-control select_options', 'rows' => 5))!!}
					                    	@else
					                    		{!!Form::textarea('select_options', $item->select_options , array('class'=>'form-control select_options', 'rows' => 5, 'required'))!!}
					                    	@endif
					                    	
					                    	<span id="helpBlock" class="help-block">Please enter one “option” for the dropdown per line</span>
					                	</div>
					            	</div> 	
					            	 
					            	<div class="form-group">
					                    {!!Form::label('tooltip','Tooltip', array('class'=>'col-sm-5 control-label'))!!}
					                    <div class="col-sm-7">
					                    	{!!Form::text('tooltip', $item->tooltip, array('class'=>'form-control'))!!}
					                	</div>
					            	</div>		
					            	
					            	<div class="form-group">
					                    {!!Form::label('priority_order',' Current Priority Order', array('class'=>'col-sm-5 control-label'))!!}
					                    <div class="col-sm-7">
					                    	{!!Form::text('priority_order', $item->priority_order, array('class'=>'form-control', 'pattern'=>'[0-9]+'))!!}
					                	</div>
					            	</div>		
					            		
					            	
					            	<div class="form-group">
					                    {!!Form::label('default_bg_color',' Default Background Color for Field', array('class'=>'col-sm-5 control-label'))!!}
					                    <div class="col-sm-7">
					                    	{!!Form::text('default_bg_color', $item->default_bg_color, array('class'=>'form-control color'))!!}
					                	</div>
					            	</div>	
					            	
					            	<div class="form-group">
						                {!!Form::label('colorsbyvalue','Background Colors by Value', array('class'=>'col-sm-5 control-label'))!!}
						                <div class="col-sm-7">
						                 	{!!Form::button('<i class="fa fa-plus"></i> Add', array('class' => 'ajax btn btn-info btn-sm add-colorvalue', "data-path"=>"Fields", "data-route"=>"addColorValueRecord" , "data-target"=>"#colorsvalues-div" , "data-_token"=>csrf_token() ));!!}
						                 	<div class="top10">				                 	
						                 		<table id="colorsvalues-div">
						                 			@foreach($color_array as $color)
						                 				<tr class="row_{{$color["id"]}}">
															<td>{!!Form::text('colors['.$color["id"].'][value]', $color["value"], array('class'=>'form-control colorvalue','required' ))!!}</td>
															<td>{!!Form::text('colors['.$color["id"].'][color]', $color["color"], array('class'=>'form-control color','required', 'pattern'=>'^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$'))!!}</td>
															<td>{!!Form::button('<i class="glyphicon glyphicon-remove glyphicon-white"></i>', array('class'=>'btn-danger btn-xs delete-colorvalue', 'data-row'=>$color["id"]))!!}</td>
														</tr>
													@endforeach	
												</table>
											</div>										
						            	</div>	
					            	</div>
					            	        	
				            		<div class="form-group">
									    {!!Form::label('users','Users Who Can Access Field', array('class'=>'col-sm-5 control-label'))!!}
									     <div class="col-sm-7">
									     	  <div class="checkbox">
										        <label>
										          	{!!Form::checkbox('all_user_accsess', 1, $item->all_user_accsess, array('class' => 'forAllUsers'))!!} All 
										       </label>
										      </div>
											@foreach($activeusers as $user)
											      <div class="checkbox">
											        <label>
											        @if($item->all_user_accsess==1)
											          	{!!Form::checkbox('users[]', $user->id,  in_array($user->id,$user_access)?true:false, array('class' => 'users-chk', 'disabled'=>'disabled'))!!} {{$user->name}}
											        @else
											        	{!!Form::checkbox('users[]', $user->id, in_array($user->id,$user_access)?true:false, array('class' => 'users-chk'))!!} {{$user->name}}
											        @endif  	 
											       </label>
											      </div>
											    @endforeach   
										 </div>
									</div>	 
 
									<div class="form-group">
						                {!!Form::label('parent_field', 'Field Parent', array('class'=>'col-sm-5 control-label required'))!!}
						                <div class="col-sm-7">
						                 	{!!Form::select('parent_field',  array('0'=>'None - This is a Parent')+$parent_fields,  $item->parent_field, array('class'=>'form-control', 'required'))!!}
						               	</div>
						            </div>
							  			
				            		<div class="form-group">
						                {!!Form::label('status', 'Status', array('class'=>'col-sm-5 control-label required'))!!}
						                <div class="col-sm-7">
						                 	{!!Form::select('status',  array(""=>"", "Active"=>"Active", "Inactive"=>"Inactive"),  $item->status, array('class'=>'form-control', 'required'))!!}
						               	</div> 
						            </div>
							  		 			            	           		
					           	</div>
				           	</div>           	
				           
			           		<div class="form-group">
						    	<div class="col-sm-12">	     	
									{!! Form::button('Cancel', array('class'=>'btn   btn-default btn-sm  pull-right left10', 'id'=>'cancelEditForm'))!!}
									{!! Form::submit('Save', array('class'=>'btn  btn-info btn-sm  pull-right'))!!}
						    	</div>
							</div>
				          
				           				 
							{!! Form::close() !!}	
						</div>   		
					</div>    	
			  	</div>




				  		
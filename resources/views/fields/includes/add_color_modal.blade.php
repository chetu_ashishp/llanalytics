    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Color/value record</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-add form-add-modal" data-path="Fields" data-route="addColorValueRecord" data-_token="{!!csrf_token()!!}">
                  
                  <div class="container">
                  
                    	<div class="form-group">
			            	{!!Form::label('color','Color', array('class'=>'col-sm-5 control-label required'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('color', null, array('class'=>'form-control color', 'required'=>"required", 'pattern'=>'^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$' ))!!}
			                	</div>
			            </div>
			            
			            <div class="form-group">
			            	{!!Form::label('value','Value', array('class'=>'col-sm-5 control-label required'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('value', null, array('class'=>'form-control' , 'required'=>"required" ))!!}
			                	</div>
			            </div>
			          
                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-default"data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-info">Add</button>                        
                        </div>
                        
                  </div>
                       
                </form>
            </div>

        </div>
    </div>
<tr class="row_{{$number}}">
	<td>{!!Form::text('colors['.$number.'][value]', null, array('class'=>'form-control colorvalue','required' ))!!}</td>
	<td>{!!Form::text('colors['.$number.'][color]', null, array('class'=>'form-control color','required', 'pattern'=>'^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$'))!!}</td>
	<td>{!!Form::button('<i class="glyphicon glyphicon-remove glyphicon-white"></i>', array('class'=>'btn-danger btn-xs delete-colorvalue', 'data-row'=>$number))!!}</td>
</tr>
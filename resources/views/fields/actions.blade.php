{!! Form::button('Edit', array('class'=>'btn  btn-warning btn-xs ajax', 'data-id'=>$id, 'data-route' => 'editField', 'data-target' => '#editfield-div', 'data-path' => 'Fields', 'data-template_id'=>$template_id, 'data-_token'=>csrf_token() ))!!}
@if(App\Role::isAllow(Auth::user()->role_id,'fields/{fields}', 'DELETE'))
{!! Form::button('Delete', array('class'=>'btn  btn-danger btn-xs deletefield', 'data-id'=>$id,  'data-route' => 'deleteField', 'data-path' => 'Fields', 'data-_token'=>csrf_token() ))!!}
@endif

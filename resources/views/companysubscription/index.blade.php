@extends('app')
@section('style')
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
	<link href="{{ asset('/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}" rel="stylesheet">
@stop

@section('script')
<script>var tableData = {!! $tableData !!};</script>
<script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script> 
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="{{ asset('/js/typeahead.js-master/dist/typeahead.jquery.js') }}"></script>
<script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>   
<script src="{{ asset('/js/jquery.mask.js') }}"></script>   
<script src="{{ asset('/js/company/company.js') }}"></script>
 
@endsection

@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i>Company Subscribed </h4>	  			 
				  		</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">
                        {!! Form::open( array('staffsearch' => 'form', 'method' => 'put', 'files'=>true, 'class'=>'form-horizontal form-companysubscription-search',  'autocomplete'=>"off")) !!} 
				    	<div class="search-radio-btn pull-left">
                              <div class="input-field"> {!! Form::radio('staffstatus', 'Active', true, ['class' => 'left10 right10','id' => 'staffstatus']) !!} New / Approved</div>            
                              <div class="input-field"> {!! Form::radio('staffstatus', 'NonActive', null, ['class' => 'left10 right10','id' => 'staffstatus']) !!} UnApproved</div>
                              <div class="input-field"> {!! Form::submit('Search', array('class'=>'btn btn-xs btn-info', 'data-path'=>'Staffs','data-route' => 'searchstaff','data-_token'=>csrf_token()))!!}</div>
                        </div>
                        {!! Form::close() !!}
						<div id="list">
							 <table class="data-table" data-toggle="table" data-pagination="true" data-search="true" data-sort-name="clname" data-sort-order="asc" >
			                    <thead>
			                        <tr>
                                        <th data-field="company_number" data-sortable="true">Company Number</th>
			                            <th data-field="company_name" data-sortable="true">Company Name</th>                                        
                                         <th data-field="primary_contact" data-sortable="true">Primary Contact Name</th>
                                         <th data-field="email" data-sortable="true">Email</th>
			                            <th data-field="primary_phone" data-sortable="true">Contact(Phone No)</th>		                                                       
                                        <th data-field="status" data-sortable="true">Status</th>
			                            <th data-field="actions">Actions</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
						</div>
					</div>   	
				  </div>
			</div>
          
            </div>

        </div>
    </div>
    <input type="hidden" id="data-ajax" value="templates">
@endsection


<?php if($company_status == 'Active' && $isDeleted == 0) { ?>
{{'Approved'}}
<?php 
} else if($company_status != 'Active' && $company_status != 'Terminated' && $isDeleted == 0) { ?>
<button class="btn btn-warning btn-sm approveSubscribedCompany" data-id={{$id}} data-status="Active" data-show="Approve" data-route="approveCompanySubscription" data-path="CompanySubscribed" type="button" data-_token={{csrf_token()}}>
    Approve</button>
<button class="btn btn-warning btn-sm approveSubscribedCompany" data-id={{$id}} data-status="Terminated" data-show="UnApprove" data-route="approveCompanySubscription" data-path="CompanySubscribed" type="button" data-_token={{csrf_token()}}>
UnApprove</button>
<?php } else if($isDeleted) { ?>
<a href="#" id="" class="btn  btn-sm  btn-danger resetcompanyunapproved" data-id="{{$id}}" data-item="{{$id}}" data-route="resetcompanyunapproved" data-path="CompanySubscribed" data-_token="{{csrf_token()}}">ReCall</a>
<?php } ?>
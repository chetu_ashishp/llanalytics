@if($company_status!='Active' && $company_status!='Terminated')
<button class="btn btn-warning btn-sm approveSubscribedCompany" data-id={{$id}} data-status="Active" data-show="Approve" data-route="approveCompanySubscription" data-path="CompanySubscribed" type="button" data-_token={{csrf_token()}}>
    Approve</button>
<button class="btn btn-warning btn-sm approveSubscribedCompany" data-id={{$id}} data-status="Terminated" data-show="UnApprove" data-route="approveCompanySubscription" data-path="CompanySubscribed" type="button" data-_token={{csrf_token()}}>
UnApprove</button>
@else {{'Approved'}}
@endif
<button type="button" class="btn btn-warning ajax btn-sm dataedit" data-path="Data" data-route="edit" data-target=".modal-edit" data-_token="{{csrf_token()}}" data-item="{{ $id }}">Edit</button>

@if(App\Role::isAllow(Auth::user()->role_id,'data/option/{one?}/{two?}/{three?}/{four?}/{five?}', 'DELETE'))
	<button type="button" class="btn btn-danger ajax btn-sm datadelete" data-path="Data" data-route="removeCheck"  data-_token="{{csrf_token()}}" data-item="{{ $id }}">Delete</button>
@endif	
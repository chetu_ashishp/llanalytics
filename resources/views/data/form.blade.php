    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">{!!$form!!} Data Option</h4>
            </div>
            <div class="modal-body">
                <form class="form-{!!strtolower($form)!!}" data-path="Data" data-route="{!!$form == 'Add' ? 'store' : 'update'!!}" data-_token="{!!csrf_token()!!}">
                
                    @if(!empty($item->id))
                        {!! Form::hidden('id', $item->id) !!}
                    @endif
                   
                    <div class="form-group">
                        <label for="option_name" class="required">Option name</label>
                        {!! Form::text('option_name', !empty($item->option_name) ? $item->option_name : null, array('class' => 'form-control', 'required')) !!}
                    </div>
              
               		<div class="form-group">
                        <label for="type_id" class="required">Option type</label>
                        {!! Form::select('type_id', array('' => '') + $data_types, !empty($item->type_id) ? $item->type_id : null, array('class' => 'form-control', 'required', 'placeholder' => 'Data Type', 'data-_token'=>csrf_token() )) !!}
                    </div>
                    
                    <div class="form-group">
                        <label for="option_name">Option description</label>
                        {!! Form::textarea('option_desc', !empty($item->option_desc) ? $item->option_desc : null, array('class' => 'form-control')) !!}
                    </div>
                    
                    <div class="form-group" >
                        <label for="type_id" class="required">Option status</label>
                        {!! Form::select('option_status', array('Active' => 'Active', 'Inactive' => 'Inactive') , !empty($item->option_status) ? $item->option_status : 'Active', array('class' => 'form-control', 'required')) !!}
                    </div>
                    
                    <div class='row'>
                        <div class='col-md-12'>
                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-default"data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-info">{!!$form == 'Add' ? 'Save' : 'Update'!!}</button>                        
                        </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
     
 
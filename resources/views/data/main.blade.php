@extends('app')
@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">				
				<div class="panel-body">
				    <div class="container datalist">
						
						@if(App\Role::isAllow(Auth::user()->role_id,'data'))						
							{!!HTML::Link('data','Data Options', array('class'=>'list-group-item'))!!}
						@endif	
						
						@if(App\Role::isAllow(Auth::user()->role_id,'settings'))
							{!!HTML::Link('settings','Settings', array('class'=>'list-group-item'))!!}
						@endif
									    
					</div>   			
								
				</div>
		</div>
      </div>
   </div>
</div>
    
@endsection




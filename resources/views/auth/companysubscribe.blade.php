@extends('app')

@section('content')
<?php //$companyList = array(); 
//$referral_types = array(); ?>
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
			  <div class="panel-heading">		  	
					<div class="container">
						<h4 class="panel-title pull-left">Subscribe Company</h4>
						<a href="{{route('company')}}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>
					</div>		  	
			  </div>
		  
		  <div class="panel-body">
			<div class="container datalist">		    	
				<div class="jumbotron container">
					
					
               
                {!! Form::open( array('id' => 'staff-add-form', 'data-route' => 'companyAddSubscriptions', 'class'=>'form-horizontal form-add',  'autocomplete'=>"off", 'data-path' => 'CompanySubs', 'data-_token'=>csrf_token() )) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	     	
								<a href="{{route('company')}}" class="btn btn-default  btn-sm pull-right left10" role="button">Cancel</a>
								{!! Form::submit('Save', array('class'=>'btn btn-sm btn-info pull-right'))!!}							
					    	</div>
						</div>
		           </div>
		           
		           
					<div class="row">					
						<div class="col-sm-6">				                  	                  	
			               	
							<div class="form-group">
			                        {!!Form::label('company_name','Company Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('company_name', null, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>
							
			            	<div class="form-group">
			                        {!!Form::label('company_type','Company Type', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::select('company_type', array('' => '') + $companyList, null, array('class'=>'form-control  required', 'required'))!!}
									</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('fname','Primary Contact - First Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('fname', null, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>	            	            	
			            	
			            	<div class="form-group">
			                        {!!Form::label('lname','Primary Contact - Last Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('lname', null, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('email','Primary Contact - Email', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::email('email', null, array('class'=>'form-control'))!!}
									</div>
			            	</div>
			            	
							<div class="form-group">
				                {!!Form::label('company_title','Primary contact - Title', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">			                 	
									{!!Form::text('company_title', null, array('class'=>'form-control'))!!}
				               	</div>
				            </div> 	
							
			            	<div class="form-group">
			                        {!!Form::label('primary_phone','Primary contact - Phone', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('primary_phone', null, array('class'=>'form-control phoneUS' ))!!}
									</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('alternate_phone','Primary contact - Alternate Phone', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('alternate_phone', null, array('class'=>'form-control phoneUS' ))!!}
									</div>
			            	</div>       	   	
			            	<div class="form-group">
				                {!!Form::label('refsource_details','How did you hear about OMS', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('referral_source', array('' => '') + $referral_types, null, array('class'=>'form-control'))!!}
				               	</div>
				            </div> 			            	
			            		            	
			        	</div>
						
						<div class="col-sm-6">
							 <div class="form-group">
								{!!Form::label('addr_line1','Address Line 1', array('class'=>'col-sm-5 control-label'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('addr_line1', null, array('class'=>'form-control' ))!!}
			                	</div>
							</div>
							 <div class="form-group">
								{!!Form::label('addr_line2','Address Line 2', array('class'=>'col-sm-5 control-label'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('addr_line2', null, array('class'=>'form-control' ))!!}
			                	</div>
							</div>
							<div class="form-group">
								{!!Form::label('city','City', array('class'=>'col-sm-5 control-label'))!!}
								<div class="col-sm-7">		                    	
									{!!Form::text('city', null, array('class'=>'form-control'))!!}
								</div>
							</div>
							<div class="form-group">
								{!!Form::label('state_id','State', array('class'=>'col-sm-5 control-label'))!!}
								<div class="col-sm-7">
									{!!Form::select('state_id', getAllStates(), null, array('class'=>'form-control'))!!}
								</div>
							</div>
							<div class="form-group">
								{!!Form::label('country_id','Country', array('class'=>'col-sm-5 control-label'))!!}
								<div class="col-sm-7">
									{!!Form::select('country_id', getAllCompanies(), null, array('class'=>'form-control'))!!}
								</div>
							</div>
			            
							<div class="form-group">
								{!!Form::label('zip','Zip Code', array('class'=>'col-sm-5 control-label'))!!}
								<div class="col-sm-7">		                    	

										{!!Form::text('zip', null, array('class'=>'form-control', "pattern"=>"(\d{5}([\-]\d{4})?)"  ))!!}
								</div>
							</div>
				            		            
							<div class="form-group">
				                {!!Form::label('website','Website', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('website',  null, array('class'=>'form-control', 'placeholder'=>"https://website.com", 'id'=>'website'))!!}
				               	</div>
				            </div> 			            	
							
							
							<div class="form-group">
				                {!!Form::label('linkedin_url','LinkedIn URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('linkedin_url',  null, array('class'=>'form-control', 'placeholder'=>"https://linkedin.com", 'id'=>'linkedin_url'))!!}
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('facebook_url','Facebook URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('facebook_url',  null, array('class'=>'form-control', 'placeholder'=>"https://facebook.com", 'id'=>'facebook_url'))!!}
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('twitter_url','Twitter URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('twitter_url',  null, array('class'=>'form-control', 'placeholder'=>"https://twitter.com", 'id'=>'twitter_url'))!!}
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('googleplus_url','Google+ URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('googleplus_url',  null, array('class'=>'form-control', 'placeholder'=>"https://google.com", 'id'=>'googleplus_url'))!!}
				               	</div>
				            </div>
				        
				          <!--	<div class="form-group">
				                {!!Form::label('status', 'Status', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('status',  array("Active"=>"Active", "Inactive"=>"Inactive", 'Suspended'=>'Suspended', 'Terminated'=>'Terminated') ,"Active", array('class'=>'form-control'))!!}
				                 	
				               	</div>
				            </div>!-->
				            	           		
			           	</div>
		           	</div>
		 
		       	
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	     	
					    	
								<a href="{{route('company')}}" class="btn btn-default  btn-sm  pull-right left10" role="button">Cancel</a>
								{!! Form::submit('Save', array('class'=>'btn-sm btn btn-info pull-right'))!!}
																
					    	</div>
						</div>
		           </div>
            	{!! Form::close() !!}				 
					
				</div>   		
			</div>    	
		  </div>
		
      		</div>
		</div>
    </div>
</div>


@stop
<div class="modal fade" id="remove-sheet" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Remove Sheet</h4>
      </div>
      <div class="modal-body">
        <p>Removing this sheet will remove the sheet and ALL entry data within the sheet.  This action is permanent and cannot be reversed.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal" >Cancel</button>
        <button type="button" class="btn btn-danger ajax" data-path="CompanyPortalSheet" data-route="destroy" data-_token="{!!csrf_token()!!}" data-item="{!!$data->item!!}">Delete</button>
      </div>
    </div>
  </div>
</div>
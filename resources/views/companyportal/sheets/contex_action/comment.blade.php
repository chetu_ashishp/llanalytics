<div class="modal fade" id="contex-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Comment</h4>
      </div>
      <div class="modal-body">
        <p>
            <div class="form-group">
                <textarea class="form-control" max-length="255" rows="5" name="contex_form_comment">{!! isset($data->item->comment) ? $data->item->comment : null !!}</textarea>
            </div>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal" >Cancel</button>
        <button type="button" class="btn btn-danger contex_action" data-path="CompanySheet" data-route="cell_update" data-cell-type="Custom" data-contex-action="contex_form_comment" data-trans-id="{!! $data->trans_id !!}" data-field-id="{!! $data->field_id !!}" data-_token="{!!csrf_token()!!}">Save</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="contex-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Send Alert</h4>
      </div>
      <div class="modal-body">
        <p>
            <div class="form-group">
                {!!Form::label('contex_form_user','Staff: ', array('class'=>'control-label required'))!!}
                {!!Form::select('contex_form_user', array('' => '') + $data->users, null, array('class'=>'form-control'))!!}
            </div> 
        </p>
        <p>
            <div class="form-group">
                {!!Form::label('contex_form_alert','Alert Message: ', array('class'=>'control-label required'))!!}
                <textarea class="form-control" max-length="255" rows="5" name="contex_form_alert" required></textarea>
            </div>
        </p>
        <p>
            <div class="checkbox">
              <label><input type="checkbox" name="contex_form_mail" value="1">Notify user by email</label>
            </div>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal" >Close</button>
        <button type="button" class="btn btn-danger contex_action" data-path="CompanySheet" data-route="cell_update" data-cell-type="Custom" data-contex-action="contex_form_alert" data-trans-id="{!! $data->trans_id !!}" data-field-id="{!! $data->field_id !!}" data-_token="{!!csrf_token()!!}">Send Alert</button>
      </div>
    </div>
  </div>
</div>
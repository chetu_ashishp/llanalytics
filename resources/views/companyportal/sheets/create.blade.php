@extends('company')

@section('style')
	
@stop

@section('script')   
   <script src="{{ asset('/js/sheets/sheets.js') }}"></script>
@stop

@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">		  	
                    <div class="container">
                        Add Sheet
                    </div>		  	
                </div>
		  
                <div class="panel-body">
                    <div class="row">		    	
                        <div class="form-group">
                            {!!Form::label('create_sheet_from','Create Sheet from: ', array('class'=>'col-sm-5 control-label required'))!!}
                            <div class="col-sm-7">
                                {!!Form::select('create_sheet_from', array('' => '', 'scratch' => 'Scratch', 'template' => 'Template'), null, array('class'=>'form-control ajax', 'data-path' => 'CompanySheet', 'data-route' => 'create_sheet_from', 'data-_token' => csrf_token(), 'data-target' => '.create_sheet_form' ))!!}
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-12 create_sheet_form"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
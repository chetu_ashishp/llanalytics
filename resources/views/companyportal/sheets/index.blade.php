@extends('company')
@section('style')	
<link href="{{ asset('/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}" rel="stylesheet">
@stop
@section('script')
<script>var tableData = {!! !empty($data->tableData) ? $data->tableData : "[]" !!};</script>
<!-- Page-Level Plugin Scripts -->
   <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
   <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
   <script src="{{ asset('/js/sheets/sheets.js') }}"></script>
@endsection
@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
                    <div class="panel-heading">		  	
                        <div class="container">			
                            <div class="row">
                                <h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i> Sheets </h4>				  		  			 
                                <a href="{{route('createcompanysheet')}}" class="btn btn-primary  btn-sm pull-right sheetcreate" role="button" data-path="sheet" data-route="sheetcreate"  data-_token="{{ csrf_token() }}"><i class="fa fa-plus"></i> Add Sheet</a>				  		
                            </div>
                        </div>
                    </div>		
                    <div class="panel-body">
                        <div class="container datalist">
                            <div id="list">
                                <table class="data-table" data-toggle="table" data-pagination="true" data-search="true" data-sort-name="sheet_name" data-sort-order="asc" >
		                    <thead>
		                        <tr>
		                            <th data-field="name" data-sortable="true">Sheet Name</th>
		                            <th data-field="purpose" data-sortable="true">Sheet Purpose</th>
		                            <th data-field="sheet_template" data-sortable="true">Template Used</th>
		                            <th data-field="status" data-sortable="true">Status</th>		                                                       
		                            <th data-field="actions">Actions</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    </tbody>
		                </table>
                            </div>
                        </div>   	
                    </div>
                </div>
        </div>
    </div>
</div>
<input type="hidden" id="data-ajax" value="sheets">
@endsection

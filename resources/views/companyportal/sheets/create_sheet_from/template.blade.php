{!! Form::open( array('role' => 'form', 'class'=>'form-add', 'data-path' => 'CompanySheet', 'data-route' => 'store', 'data-_token' => csrf_token())) !!}
        {!!Form::input('hidden', 'sheet_type', 'template')!!}

        <div class="row">
            <div class="form-group" style="margin-top:20px;">
                <div class="col-sm-12">
                    <a href="{!! route('sheets') !!}" class="btn btn-danger pull-right left10" role="button">Cancel</a>
                    {!! Form::submit('Create', array('class'=>'btn btn-primary pull-right'))!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            {!!Form::label('name','Sheet Name', array('class'=>'control-label required'))!!}
            {!!Form::text('name', null, array('class'=>'form-control required', 'required'))!!}
        </div>

        <div class="form-group">
            {!!Form::label('sheet_template','Template', array('class'=>'control-label'))!!}
            {!!Form::select('sheet_template', array('' => '') + $data->templates, null, array('class'=>'form-control', 'required'=>'required'))!!}
        </div>

        <div class="row">
            <div class="form-group" style="margin-top:20px;">
                <div class="col-sm-12">
                    <a href="{!! route('sheets') !!}" class="btn btn-danger pull-right left10" role="button">Cancel</a>
                    {!! Form::submit('Create', array('class'=>'btn btn-primary pull-right'))!!}
                </div>
            </div>
        </div>

{!! Form::close() !!}
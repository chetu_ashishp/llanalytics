@extends('company')

@section('style')
		{{--<link href="{{ asset('/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">--}}
		<link href="{{ asset('/css/spectrum.css') }}" rel="stylesheet">
		<link href="{{ asset('/vendor/bootstrap-table/src/extensions/reorder-rows/bootstrap-table-reorder-rows.css') }}" rel="stylesheet">
@stop

@section('script')   
    <script>var tableData = {!! !empty($data->tableData) ? $data->tableData : "[]" !!};</script>
    <script src="{{ asset('/js/sheets/sheets.js') }}"></script>
    
     {{--<script src="{{ asset('/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>--}}
     <script src="{{ asset('/js/spectrum.js') }}"></script>
   	<script src="{{ asset('/js/sheets/fields.js') }}"></script>
   	<script src="{{ asset('/vendor/TableDnD/dist/jquery.tablednd.js') }}"></script>
	<script src="{{ asset('/vendor/bootstrap-table/src/extensions/reorder-rows/bootstrap-table-reorder-rows.js') }}"></script> 
	
    <!-- script src="{{ asset('/js/jquery.sortable.js') }}"></script -->
    <script>
        //$('.sortable').sortable();
//		$(document).ready(function(){
//
//		})
        </script>
        
@stop

@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
            <div role="tabpanel">
                <a href="{!!route('sheets') !!}" class="btn btn-default  btn-xs pull-right" role="button">Cancel</a>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#coredetails" aria-controls="coredetails" role="tab" data-toggle="tab">Core Details</a></li>
                    <li role="presentation"><a href="#fields" aria-controls="fields" class="sheetfields" role="tab" data-toggle="tab" id="lfields" data-sheet="{{$data->item->id}}" data-path="CompanySheet" data-route="sheetfields" data-_token="{!! csrf_token() !!}">Fields</a></li>
                </ul>
                <div class="tab-content top20">
                
                

                    <div role="tabpanel" class="tab-pane fade in active" id="coredetails">
                        <div class="row">
                        <div class="col-md-11 col-md-offset-1">
                     	<a href="{!!route('companysheet.details.entry', array('template' => $data->item->id)) !!}" class="btn btn-default sheetaccess" role='button' data-path="sheet" data-route="sheetaccess"  data-_token="{{ csrf_token() }}">Access Sheet</a>
                        <a href="{!!route('companysheet.edit', array('template' => $data->item->id)) !!}" class="btn btn-info editsheet" role="button" data-path="sheet" data-route="editsheet"  data-_token="{{ csrf_token() }}">Edit Sheet</a>
                        <a href="" class="btn btn-danger ajax2" id="removesheet" role="button" data-target="#modal-container" data-path="CompanySheet" data-route="remove" data-_token="{!! csrf_token() !!}" data-item="{!! $data->item->id !!}">Remove Sheet</a>
                        <a href="{!!route('createcopycompanysheet', array('sheet' => $data->item->id)) !!}" class="btn btn-warning createcopysheets" data-path="CompanySheet" data-route="copyDuplicateSheets" data-_token="{!! csrf_token() !!}" role="button">Create Copy of Sheet</a>
                                               
	                    <div class="row top20">
	                        <div class="col-lg-7">
                     	    		<div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Sheet Name</p></div>
			                        	<div class="col-lg-6"><p>{!! $data->item->name !!}</p></div>     
			                        </div>
			                        
			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Sheet Purpose: </p></div>
			                        	<div class="col-lg-6"><p>{!! $data->item->purpose !!}</p></div>     
			                        </div> 
			                        
			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Sheet Fields: </p></div>
			                        	<div class="col-lg-6"><p>{!! $data->fields !!}</p></div>     
			                        </div> 
			                        
			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label"># Rows to Freeze: </p></div>
			                        	<div class="col-lg-6"><p>{!! $data->item->rows_to_freeze !!}</p></div>     
			                        </div>

								<div class="row detail-cell">
									<div class="col-lg-6"><p class = "detail-cell-label">Default First Column Width: </p></div>
									<div class="col-lg-6"><p>{!! $data->item->first_column_width !!}</p></div>
								</div>

			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Default Column Width: </p></div>
			                        	<div class="col-lg-6"><p>{!! $data->item->column_width !!}</p></div>     
			                        </div>

			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Default row height: </p></div>
			                        	<div class="col-lg-6"><p>{!! $data->item->row_height !!}</p></div>
			                        </div>
			                        
			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Users Who Can Access Sheet:</p></div>
			                        	<div class="col-lg-6"><p>{!! $data->item->all_user_accsess == 1 ? 'All' : $data->users_list !!}</p></div>     
			                        </div> 
			                        
			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Status: </p></div>
			                        	<div class="col-lg-6"><p>{!! $data->item->status !!}</p></div>     
			                        </div>                         
	                        </div>
	                    </div>
	                </div>
	                </div>
	                </div>

	            
                   
                    <div role="tabpanel" class="tab-pane fade" id="fields">
                       	<div class="container top20">
	                        <div class="panel panel-info" id="fields-div">  			  		  			 
	                            <div class="panel-body">
	                            	<div class="container bottom20">
							        	<div class="row">
							        		{!! Form::button('<i class="fa fa-plus"></i> Add Field', array('class'=>'btn btn-xs  btn-info pull-right ajax',  'data-sheet_id'=>$data->item->id, 'data-route' => 'createField', 'data-target' => '#newfield-div', 'data-path' => 'SheetFields', 'data-_token'=>csrf_token()))!!}
							        	</div>		
							        </div>	        
								    <div class="container datalist">
		                               
		                                    <div id="list">
		                                        <table class="fieldtable" data-pagination="false" data-search="true" data-sort-name="priority_order" data-sort-order="asc" >
		                                            <thead>
		                                                <tr>
		                                                
		                                                	<th data-field="id" data-visible="false">Id</th>
		                                                    <th data-field="field_name" >Name</th>
		                                                    <th data-field="type" >Type</th>
		                                                    <th data-field="tooltip">Tooltip</th>
		                                                    <th data-field="priority_order" >Priority Order</th>		                                                       
		                                                    <th data-field="status" >Status</th>   
		                                                    <th data-field="actions">Actions</th>
		                                                </tr>
		                                            </thead>                                           
		                                        </table>
		                                    </div>
		                                </div> 
	                                  	
	                            </div>
	                        </div>                                                
	                        <div class="panel panel-info"  id="newfield-div" style="display:none"></div>
				 			<div class="panel panel-info"  id="editfield-div" style="display:none"></div>			 
	                    </div>
                    </div>
                </div>
            </div>
            
            
            
            
            

        </div>
    </div>
</div>
<div id="modal-container"></div>
<input type="hidden" id="data-ajax" value="sheets">
{!!Form::hidden('sheet_id', $data->item->id, array('class'=>'sheet_id'))!!}    
{!! Form::token() !!}
@stop
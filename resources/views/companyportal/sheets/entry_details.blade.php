@extends('company')

@section('style')
	
@stop

@section('script')   
    <script>var tableData = {!! !empty($data->tableData) ? $data->tableData : "[]" !!};</script>
    <script src="{{ asset('/js/sheets/sheets.js') }}"></script>
    <script src="{{ asset('/js/jquery.sortable.js') }}"></script>
    <script>
        $('.sortable').sortable();
        </script>
        
@stop

@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
            <div role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#coredetails" aria-controls="coredetails" role="tab" data-toggle="tab">Core Details</a></li>
                    <li role="presentation"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab">Fields</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="coredetails">
                        <a href="{!!route('sheet.edit', array('template' => $data->item->id)) !!}" class="btn btn-info" role="button">Edit Sheet</a>
                        <a href="" class="btn btn-danger ajax" role="button" data-target="#modal-container" data-path="CompanySheet" data-route="remove" data-_token="{!! csrf_token() !!}" data-item="{!! $data->item->id !!}">Remove Sheet</a>
                        <a href="" class="btn btn-warning" role="button">Create Template from Sheet </a>
SHEET DATA NEEDED HERE
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="fields">
                        <div class="panel">  			  		  			 
                            <div class="panel-body">
                                <div class="container">
                                    <a href="{{route('createsheet')}}" class="btn btn-primary  btn-sm pull-right" role="button"><i class="fa fa-plus"></i> Add Field</a>				  		
                                </div>
                                <div class="container datalist">
                                    <div id="list">
                                        <table class="data-table"  data-toggle="table" data-pagination="true" data-search="true" data-sort-name="sheet_name" data-sort-order="asc" >
                                            <thead>
                                                <tr>
                                                    <th data-field="name" data-sortable="true">Name</th>
                                                    <th data-field="type" data-sortable="true">Type</th>
                                                    <th data-field="tooltip" data-sortable="true">Tooltip</th>
                                                    <th data-field="priority-order" data-sortable="true">Priority Order</th>		                                                       
                                                    <th data-field="status" data-sortable="true">Status</th>		                                                       
                                                    <th data-field="actions">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody class="sortable">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>   	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            
            

        </div>
    </div>
</div>
<div id="modal-container"></div>
<input type="hidden" id="data-ajax" value="sheets">
@stop
@extends('company')

@section('style')
    <link href="{{ asset('/css/spectrum.css') }}" rel="stylesheet">
@stop

@section('script')
    <script src="{{ asset('/js/spectrum.js') }}"></script>
   <script src="{{ asset('/js/sheets/sheets.js') }}"></script>
@stop

@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">		  	
                    <div class="container">
                        Edit Sheet
                        <a href="{!!route('company.sheet.details.edit', array('template' => $data->id)) !!}" class="btn btn-default  btn-xs pull-right" role="button">Cancel</a>
                    </div>		  	
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 create_sheet_form">{!! $data->html !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
<button type="button" class="btn btn-default btn-xs ajax" data-path="CompanyAlerts" data-route="view" data-target=".modal-view" data-_token="{{csrf_token()}}" data-item="{{ $id }}">View</button>

@if($type=='Incompleted')
<button type="button" class="btn btn-success btn-xs setComplete" data-path="CompleteAlert"  data-_token="{{csrf_token()}}" data-item="{{ $id }}">Mark Complete</button>
@endif

@extends('company')

@section('script')
<script>var tableData = {!! $tableData !!};</script>
@endsection


@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i>Company Clients </h4>							
				  			<a href="{{route('companynewclient')}}" class="btn btn-info  btn-xs pull-right" role="button"><i class="fa fa-plus"></i> Add New Client</a>				  						  		
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">		    	
				    	<div class="jumbotron container">				    		
				    	
					    	 {!!Form::open(array('data-path' => 'Clients','data-route'=>'companyPortalSearch', 'class' => 'form-horizontal companyportal-form-search', 'data-_token'=>csrf_token()))!!}
				                  	<div class="col-sm-6">				                  	                  	
				                       <div class="form-group">
				                            {!!Form::label('name','Name', array('class'=>'col-sm-6 control-label'))!!}
				                            <div class="col-sm-6">
				                            	{!!Form::text('name', null, array('class'=>'form-control'))!!}
				                            </div>
				                       </div>
				                       <div class="form-group">
				                            {!!Form::label('spouse_name','Spouse Name', array('class'=>'col-sm-6 control-label'))!!}
				                            <div class="col-sm-6">
				                            	{!!Form::text('spouse_name', null, array('class'=>'form-control'))!!}
				                            </div>
				                       </div> 				                                           
				                       <div class="form-group">
				                            {!!Form::label('email_staff','Email Address', array('class'=>'col-sm-6 control-label'))!!}
				                            <div class="col-sm-6">
				                            	{!!Form::text('email_staff', null, array('class'=>'form-control'))!!}
				                            </div>
				                       </div>
				                  	</div>		
				                  				
				                  	<div class="col-sm-6">                  	
				                  	    <div class="form-group">
				                            {!!Form::label('prop_addr','Property Address', array('class'=>'col-sm-6 control-label'))!!}
				                            <div class="col-sm-6">
				                            	{!!Form::text('prop_addr', null, array('class'=>'form-control'))!!}
											</div>
				                        </div>				                        				                        
				                        <div class="form-group">
				                            {!!Form::label('status','Status', array('class'=>'col-sm-6 control-label'))!!}
				                            <div class="col-sm-6">				                            	
				                            	 {!! Form::select('status', array(''=>'', 'Active' => 'Active', 'Inactive' => 'Inactive', 'Suspended'=>'Suspended', 'Terminated'=>'Terminated'), null, array('class'=>'form-control')) !!}
											</div>	
										</div>					                                          
				                    	<div class="form-group">
									    	<div class="col-sm-12">
									       		{!! Form::submit('Search', array('class'=>'btn btn-xs btn-info pull-right'))!!}
									    	</div>
									  	</div>
				                  	</div>                  	
							{!!Form::close()!!}
						</div>		    				
						<div id="list">						
						 <table class="data-table" data-pagination="true">
		                    <thead>
		                        <tr>
		                            <th data-field="clnumber" data-sortable="true">Client Number</th>
		                            <th data-field="clname" data-sortable="true">Client Name</th>
		                            <th data-field="clctity" data-sortable="true">City</th>
		                            <th data-field="clstate" data-sortable="true">State</th>
		                            <th data-field="clemail" data-sortable="true">Email</th>
		                            <th data-field="clqtrans" data-sortable="true"># Transactions</th>
		                            <th data-field="clstatus" data-sortable="true">Status</th>                            
		                            <th data-field="actions">Actions</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    </tbody>
		                	</table>		                
						</div>
					</div>   	
				  </div>
				</div>          
            </div>
        </div>
    </div>

@endsection


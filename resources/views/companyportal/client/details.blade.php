@extends('company')

@section('style')
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
	<link href="{{ asset('/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}" rel="stylesheet">
@stop

@section('script')

<!-- Page-Level Plugin Scripts -->
   <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('/js/typeahead.js-master/dist/typeahead.jquery.js') }}"></script>
   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>   
    <script src="{{ asset('/js/jquery.mask.js') }}"></script>
   <script src="{{ asset('/js/clients/clients.js') }}"></script>
   <script src="{{ asset('/js/companyportal/client.js') }}"></script>
   
 <script src="{{ asset('/js/clients/jquery.formatCurrency-1.4.0.min.js') }}"></script>
 <script src="{{ asset('/js/clients/transactions.js') }}"></script>
 <script src="{{ asset('/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
 <script src="{{ asset('/vendor/925491/jquery.ui.autocomplete.html.js') }}"></script>
   <script>
	   var escrow_officers = {!! json_encode($data['escrowOfficers']) !!};
	   var loan_officers = {!! json_encode($data['loanOfficers']) !!};
   </script>
@stop

@section('content')

<div class="panel panel-info">
  <div class="panel-heading">		  	
		<div class="container">
			<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-info-sign"></i> Client Details </h4>
			<a href="{{route('clients')}}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>			
		</div>		  	
  </div>
  <div class="panel-body">    		
		<!-- Nav tabs -->		
		  <ul class="nav nav-tabs" role="tablist">
		        <li class="active"><a href="#core_details" data-toggle="tab">Core Details</a></li>
		        <li><a href="#transactions" id="ltransactions" class="ajax" data-path="Transactions" data-route="show" data-item="{{$client->id}}"  data-_token="{{csrf_token()}}" data-toggle="tab" >Transactions</a></li>
		  </ul>
			
		<!-- Tab panes -->
	    <div class="tab-content">
	        <div class="tab-pane active" id="core_details">
	            <div class="row">
	                 <div class="col-lg-8 col-lg-offset-1">
	                 	 <div class="row">
		                    <div class="top10 col-lg-7">
		                 
		                    	<a href="{{route('Cpclient')}}" class="btn btn-default  btn-sm pull-right left10" role="button">Cancel</a>
                                <a href="#" id="deleteCompanyClient" class="btn  btn-sm pull-right left10 btn-danger deleteCompany", data-id="{{$client->id}}" data-item="{{$client->id}}" data-route = "removeCheck" data-path = "CompanyPortalClient" data-_token = "{{csrf_token()}}">Delete</a>
		                        <a href="{{route('companyclient.edit', array('client' => $client->id)) }}" class="btn btn-warning btn-sm pull-right" role='button' >Edit</a>
		                    
		                    </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-lg-8 col-lg-offset-1">
	                    <div class="row">
	                        <div class="col-lg-7">	                            
	                            @foreach($lables as $key=>$value)
	                     	        @if( $key=='addresses')
	                     	    			<div class="row detail-cell">
				                                <div class="col-lg-6"><p class = "detail-cell-label"> {{$value}} </p></div> 
				                                <div class="col-lg-6">
				                                @if(isset($addresses) && !empty($addresses) )
				                                	  @foreach($addresses as $addr)				                                	  
				                                	  	<p>
					                                	  	<address>
															  <strong>{{$addr->type}}</strong><br>
																@if(!empty($addr->addr_line1))  {{$addr->addr_line1}} <br>@endif
																@if(!empty($addr->addr_line2))  {{$addr->addr_line2}} <br>@endif
																
																@if($addr->city)  {{$addr->city}}, @endif
																{{ isset($addr->state_id) ? App\State::find($addr->state_id)->state_abbr : ''}} 
																
																@if($addr->zip) 
																{{$addr->zip}}
																@endif
																
																
																 @if($addr->city || $addr->zip || !empty($addr->state_id))
																  <br>
																 @endif
																
																 Mailing Address? :  {{ isset($addr->mailing_addr) ? 'Yes' : 'No' }}						                     
															</address>
				                                	  	</p>
				                                	  			                                	  				                                	  
				                                	  @endforeach				                                
				                               	@else
				                                	<div class="col-lg-6"><p></p></div>
				                               	@endif
				                               	 </div>
				                               			                                
				                            </div>
	                     	    	@else
		                     	    		<div class="row detail-cell">
				                                <div class="col-lg-6"><p class = "detail-cell-label"> {{$value}} </p></div> 
				                                @if( ($key=='website' || $key=='linkedin_url' || $key=='facebook_url' || $key=='twitter_url' || $key=='googleplus_url') && !empty($client_info[$key]) )
				                                	<div class="col-lg-6"><p> {!!HTML::link($client_info[$key], $client_info[$key], array('target'=>'_blank'))!!} </p></div>
				                                @else
				                                	<div class="col-lg-6"><p>{{$client_info[$key]}}</p></div>
				                                @endif	
				                                
				                            </div>				                        
	                     	    	@endif                      	
	                            @endforeach
								
	                        </div>
	                    </div>
	                </div>
	            </div>
	            
	              <div class="row">
	                 <div class="col-lg-8 col-lg-offset-1">
	                 	 <div class="row">
		                    <div class="top10 col-lg-7">	
                              
		                    	<a href="{{route('Cpclient')}}" class="btn btn-default  btn-sm pull-right left10" role="button">Cancel</a>                               
		                        <a href="{{route('companyclient.edit', array('client' => $client->id)) }}" class="btn btn-warning btn-sm pull-right" role='button' >Edit</a>
                                
		                    
		                    </div>
	                    </div>
	                </div>
	            </div>
	            
	        </div>
	        <div class="tab-pane" id="transactions"></div>
	    </div>	    		
  </div>
</div>
@stop
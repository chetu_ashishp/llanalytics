    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add address record</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-add" data-path="Clients" data-route="add_addr_record" data-_token="{!!csrf_token()!!}">
                  
                  <div class="container">
                  
                    	<div class="form-group">
			            	{!!Form::label('type','Name', array('class'=>'col-sm-5 control-label required'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('type', null, array('class'=>'form-control', 'required'=>"required" ))!!}
			                	</div>
			            </div>
			            
			            <div class="form-group">
			            	{!!Form::label('addr_line1','Address Line 1', array('class'=>'col-sm-5 control-label'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('addr_line1', null, array('class'=>'form-control' ))!!}
			                	</div>
			            </div>
			            			            
			            <div class="form-group">
			            	{!!Form::label('addr_line2','Address Line 2', array('class'=>'col-sm-5 control-label'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('addr_line2', null, array('class'=>'form-control' ))!!}
			                	</div>
			            </div>
			       
			              
			           <div class="form-group">
		                    {!!Form::label('city','City', array('class'=>'col-sm-5 control-label'))!!}
		                    <div class="col-sm-7">
		                    	{!!Form::text('city', '', array('class'=>'form-control'))!!}
		                	</div>
		            	</div>
		            			            	
			            <div class="form-group">
			            	{!!Form::label('state_id','State', array('class'=>'col-sm-5 control-label'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::select('state_id', getStates(),  $company->default_state, array('class'=>'form-control' ))!!}
			                	</div>
			            </div>
			            			            
			            <div class="form-group">
			            	{!!Form::label('zip','Zip Code', array('class'=>'col-sm-5 control-label'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('zip', "", array('class'=>'form-control', "name"=>"zip", "pattern"=>"(\d{5}([\-]\d{4})?)"  ))!!}
			                	</div>
			            </div>
			            
			             <div class="form-group">
						    <div class="col-sm-offset-5 col-sm-7">
						      <div class="checkbox">
						        <label>
						          {!!Form::checkbox('mailing_addr', 'Yes' , false, array('class'=>'mailingaddr') )!!} Mailing Address?
						        </label>
						      </div>
						    </div>
						 </div>
							
                  		 {!!Form::hidden('company_id',  $company_id, array())!!}
                  		    
                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-default  btn-sm"data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-info btn-sm">Add</button>                        
                        </div>
                  </div>
                       
                </form>
            </div>

        </div>
    </div>
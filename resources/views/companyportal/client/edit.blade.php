@extends('company')

@section('style')
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
@stop

@section('script')

<?php 
   // echo '<pre/>';
    //print_r($item);
   // echo $item[0]->id;
      //      die;
?>
<!-- Page-Level Plugin Scripts -->
   <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>   
   <script src="{{ asset('/js/jquery.mask.js') }}"></script>
   <script src="{{ asset('/js/clients/clients.js') }}"></script>
    
@stop



@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
			  <div class="panel-heading">		  	
					<div class="container">
						<h4 class="panel-title pull-left">{{$form}} client</h4>
						<a href="{!!route('cpclients.details', array('client' =>$item[0]->id)) !!}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>		
					</div>		  	
			  </div>
		  
		  <div class="panel-body">
			<div class="container datalist">		    	
				<div class="jumbotron container">
					
					{!! Form::open( array('role' => 'form', 'method' => 'put', 'files'=>true, 'class'=>'form-horizontal',  'autocomplete'=>"off", 'url' => array('companyportal/client/update/'.$item[0]->id))) !!}
					
					<div class="form-group">
				    	<div class="col-sm-12">
				    		<a href="{!!route('cpclients.details', array('client' =>$item[0]->id)) !!}" class="btn btn-default   btn-sm pull-right left10" role="button">Cancel</a>
							{!! Form::submit('Save', array('class'=>'btn  btn-info pull-right btn-sm'))!!}
													
				    	</div>
					</div>
					
					
					<div class="row">					
						<div class="col-sm-6">				                  	                  	
			               	
			            	<div class="form-group">
			                        {!!Form::label('client_number','Client Number', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('client_number', $item[0]->client_number, array('class'=>'form-control required', 'disabled'))!!}
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('last_name','Last Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('last_name', $item[0]->last_name, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
			            	    
			            	<div class="form-group">
			                        {!!Form::label('first_name','First Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('first_name', $item[0]->first_name, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
			            				            	
			            	<div class="form-group">
			                        {!!Form::label('spouse_lname','Spouse Last Name', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('spouse_lname', $item[0]->spouse_lname, array('class'=>'form-control'))!!}
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('spouse_fname','Spouse First Name', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('spouse_fname', $item[0]->spouse_fname, array('class'=>'form-control'))!!}
			                	</div>
			            	</div>
			            	    			            	
			            	<div class="form-group">
			                        {!!Form::label('email','Email', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::email('email', $item[0]->email, array('class'=>'form-control'))!!}
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('primary_phone','Primary Phone', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('primary_phone', $item[0]->primary_phone, array('class'=>'form-control phoneUS' ))!!}
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('alternate_phone','Alternate Phone', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('alternate_phone', $item[0]->alternate_phone, array('class'=>'form-control phoneUS' ))!!}
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('mobile_phone','Mobile Phone', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('mobile_phone', $item[0]->mobile_phone, array('class'=>'form-control phoneUS' ))!!}
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('other_phone','Other Phone', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('other_phone', $item[0]->other_phone, array('class'=>'form-control phoneUS' ))!!}
			                	</div>
			            	</div>
			            				            	
			            	<div class="form-group">
				                {!!Form::label('addresses','Addresses', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::button('<i class="fa fa-plus"></i> Add address row', array('class' => 'btn btn-info btn-sm', 'data-target'=>".modal-add-addr", 'data-toggle'=>"modal" ));!!}
				                 	<div class="top10">				                 	
				                 		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				                 			@if($addr)
			                 					 {!!$addr!!}               			
				                 			@endif
										</div>
									</div>										
				            	</div>	
			            	</div>
			            	
			            	<div class="form-group">
				                {!!Form::label('date_met','Date Met', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('date_met', $date_met, array('class'=>'form-control datepicker', 'id'=>'datepicker', "pattern"=>"(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" ))!!}			                 	
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('referral_source','Client Source', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('referral_source', array('' => '') + $referral_types, $item[0]->referral_source, array('class'=>'form-control'))!!}
				               	</div>
				            </div> 	
			            			            	
			           	</div>
						
						<div class="col-sm-6">
							
							<div class="form-group">
				                {!!Form::label('refsource_details','How did you hear details', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::textarea('refsource_details',  $item[0]->refsource_details, array('class'=>'form-control', 'rows'=>'4'))!!}
				               	</div>
				            </div> 	
				            		            
				            	            	
							<div class="form-group">
				                {!!Form::label('notes','Notes', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::textarea('notes',  $item[0]->notes, array('class'=>'form-control', 'rows'=>'4'))!!}
				               	</div>
				            </div> 		
				            
							<div class="form-group">
				                {!!Form::label('website','Website', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('website',   $item[0]->website, array('class'=>'form-control', 'placeholder'=>"https://site.com"))!!}
				               	</div>
				            </div> 			            	
							
							
							<div class="form-group">
				                {!!Form::label('linkedin_url','LinkedIn URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('linkedin_url',   $item[0]->linkedin_url, array('class'=>'form-control', 'placeholder'=>"https://linkedin.com"))!!}
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('facebook_url','Facebook URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('facebook_url',   $item[0]->facebook_url, array('class'=>'form-control', 'placeholder'=>"https://facebook.com"))!!}
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('twitter_url','Twitter URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('twitter_url',   $item[0]->twitter_url, array('class'=>'form-control', 'placeholder'=>"https://twitter.com"))!!}
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('googleplus_url','Google+ URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('googleplus_url',   $item[0]->googleplus_url, array('class'=>'form-control', 'placeholder'=>"https://google.com" ))!!} 
				               	</div>
				            </div>
				        
				          	<div class="form-group">
				                {!!Form::label('status', 'Status', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('status',  array("Active"=>"Active", "Inactive"=>"Inactive", 'Suspended'=>'Suspended', 'Terminated'=>'Terminated') , $item[0]->status, array('class'=>'form-control'))!!}
				               	</div>
				            </div>  		
			           	</div>
		           	</div>
		           
		           
	           		<div class="form-group">
				    	<div class="col-sm-12">
				    		<a href="{!!route('cpclients.details', array('client' =>$item[0]->id)) !!}" class="btn btn-default   btn-sm pull-right left10" role="button">Cancel</a>			      	
							{!! Form::submit('Save', array('class'=>'btn  btn-info pull-right btn-sm'))!!}													
				    	</div>
					</div>
		           
		           				 
					{!! Form::close() !!}
				</div>   		
			</div>    	
		  </div>
		
      		</div>
		</div>
    </div>
</div>

<div class="modal fade modal-add-addr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">{!! $createadrr !!}</div>

@stop
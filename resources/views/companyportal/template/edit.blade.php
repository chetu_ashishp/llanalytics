@extends('company')

@section('style')
		
@stop

@section('script')   
   <script src="{{ asset('/js/templates/templates.js') }}"></script>
@stop



@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
			  <div class="panel-heading">		  	
					<div class="container">
						
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-notes"></i> {{$form}} template </h4>
				  			<a href="{!!route('cptemplates.details', array('template' =>$item->id)) !!}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>
					</div>		  	
			  </div>
		  
		  <div class="panel-body">
			<div class="container datalist">		    	
				<div class="jumbotron container">
					
					{!! Form::open( array('role' => 'form', 'method' => 'put', 'files'=>true, 'class'=>'form-horizontal', 'id'=>'edit-template', 'autocomplete'=>"off", 'url' => array('companyportal/templates/update/'.$item->id))) !!}
					      
					         			           	
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">
					    		<a href="{!!route('cptemplates.details', array('template' =>$item->id)) !!}" class="btn btn-default  btn-sm  pull-right left10" role="button">Back</a>	     	
								{!! Form::submit('Save', array('class'=>'btn  btn-info btn-sm  pull-right'))!!}
					    	</div>
						</div>
		           </div> 
		            
			       	<div class="row">					
						<div class="col-sm-6">				                  	                  	
			               		            	
			            	<div class="form-group">
			                        {!!Form::label('name','Template Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('name', $item->name, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('purpose','Template Purpose ', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::textarea('purpose',  $item->purpose, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
			            				  
			             
			      		            	 
			            	
			            	<div class="form-group">
			                        {!!Form::label('inc_first_name','Client Fields to Include', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	
				                    	 <div class="checkbox">
									        <label>
									          	{!!Form::checkbox('transaction_fileds[]', 'first_name', in_array('first_name', $temIncludeFields)?true:false, array('class' => 'atleastone'))!!} First Name
									        </label>
									     </div>  
									     <div class="checkbox"> 
									        <label>  	 
									          	{!!Form::checkbox('transaction_fileds[]', 'last_name',  in_array('last_name', $temIncludeFields)?true:false, array('class' => 'atleastone'))!!} Last Name
									        </label>
									     </div>
			                		</div>
			            	</div>
			            	
					
					 		  
					
							<div class="form-group">
							    {!!Form::label('mls1','Transaction Fields to Include', array('class'=>'col-sm-5 control-label'))!!}
							     <div class="col-sm-7"> 
							     
								      <div class="checkbox">
								        <label>
								          	{!!Form::checkbox('transaction_fileds[]', 'mls1',  in_array('mls1', $temIncludeFields)?true:false,array('class' => ' atleastone'))!!} MLS # - 1 
								       </label>
								      </div>
								       
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'mls2',  in_array('mls2', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} MLS # - 2
								        </label>
								      </div>
								      
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'contract_date',  in_array('contract_date', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Contract Date
								        </label>
								      </div>
									 <div class="checkbox">
										 <label>
											 {!!Form::checkbox('transaction_fileds[]', 'current_list_price' 	,  in_array('current_list_price', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Current List Price
										 </label>
									 </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'sales_price', in_array('sales_price', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Sales Price
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'lender',  in_array('lender', $temIncludeFields)?true:false,array('class' => ' atleastone'))!!} Cash / Lender
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'title_company',  in_array('title_company', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Title Company
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'escrow_officer',  in_array('escrow_officer', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Escrow Officer
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'nickname',  in_array('nickname', $temIncludeFields)?true:false,array('class' => ' atleastone'))!!} Short Address
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'property_house' ,  in_array('property_house', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Property House #
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'property_addr1',  in_array('property_addr1', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Property Address Line 1
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'city',  in_array('city', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Property City
								        </label>
								      </div>	
								      
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'state',  in_array('state', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Property State
								        </label>
								      </div>
								   
								      
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'subdivision' ,  in_array('subdivision', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Property Sub-division
								        </label>
								      </div>	
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 	'listing_date' ,  in_array('listing_date', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Listing Date
								        </label>
								      </div>	
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'listing_expiration_date' ,  in_array('listing_expiration_date', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Listing Expiration Date
								        </label>
								      </div>	
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'other_agent_name' ,  in_array('other_agent_name', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Other Agent
								        </label>
								      </div>
									 <div class="checkbox">
										 <label>
											 {!!Form::checkbox('transaction_fileds[]', 'lockbox' 	,  in_array('lockbox', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Lockbox #
										 </label>
									 </div>
		 							  <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'transaction_status',  in_array('transaction_status', $temIncludeFields)?true:false, array('class' => ' atleastone'))!!} Status
								        </label>
								      </div>


							    </div>
							</div>
				
							<div class="form-group">
			                    {!!Form::label('rows_to_freeze','# Rows to Freeze ', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::select('rows_to_freeze', range(0, 12), $item->rows_to_freeze, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
							  	
							<div class="form-group">
							    <div class="col-sm-offset-5 col-sm-7">
							      <div class="checkbox">
							        <label>
							          	{!!Form::checkbox('freeze_first_column', 1, $item->freeze_first_column, array('class' => ''))!!} Freeze First Column							          					          								          	
							        </label>
							      </div>
							    </div>
							</div> 	
																	  							
							<div class="form-group">
							    {!!Form::label('mls1','Users Who Can Access Sheet', array('class'=>'col-sm-5 control-label'))!!}
							     <div class="col-sm-7">
							     	  <div class="checkbox">
								        <label>
								          	{!!Form::checkbox('all_user_accsess', 1, $item->all_user_accsess, array('class' => 'forAllUsers'))!!} All 
								       </label>
								      </div>
							     	@foreach($activeusers as $user)
								      <div class="checkbox">
								        <label>
								        @if($item->all_user_accsess==1)
								          	{!!Form::checkbox('users[]', $user->id,  in_array($user->id,$user_access)?true:false, array('class' => 'users-chk', 'disabled'=>'disabled'))!!} {{$user->name}}
								        @else
								        	{!!Form::checkbox('users[]', $user->id, in_array($user->id,$user_access)?true:false, array('class' => 'users-chk'))!!} {{$user->name}}
								        @endif  	 
								       </label>
								      </div>
								    @endforeach  
								 </div>
							</div>	      
																		
							<div class="form-group">
			                    {!!Form::label('column_order','Column Order', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::select('column_order', App\Template::$column_order, $item->column_order, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>	
																  	
							<div class="form-group">
				                {!!Form::label('status', 'Status', array('class'=>'col-sm-5 control-label required'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('status',  array("Active"=>"Active", "Inactive"=>"Inactive") , $item->status, array('class'=>'form-control', 'required'))!!}
				               	</div>
				            </div>
							  		            	           		
			           	</div>
		           	</div>
		           			
		           			
		           			           	
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">
					    		<a href="{!!route('cptemplates.details', array('template' =>$item->id)) !!}" class="btn btn-default  btn-sm  pull-right left10" role="button">Back</a>	     	
								{!! Form::submit('Save', array('class'=>'btn  btn-info btn-sm  pull-right'))!!}
					    	</div>
						</div>
		           </div> 
					{!! Form::close() !!}	
				</div>   		
			
		
      		</div>
		</div>
    </div>
</div>



@stop
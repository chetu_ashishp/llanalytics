@extends('company')

@section('script')
<script>var tableData = {!! $tableData !!};</script>
@stop


@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i> Templates </h4>				  		  			 
				  			<!--<a href="{{route('newcptemplate')}}" class="btn btn-info  btn-sm pull-right" role="button"><i class="fa fa-plus"></i> Add New Template</a>!-->
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">
						<div id="list">
							 <table class="data-table" data-toggle="table" data-pagination="true" data-search="true" data-sort-name="clname" data-sort-order="asc" >
			                    <thead>
			                        <tr>
			                            <th data-field="clname" data-sortable="true">Template Name</th>
			                            <th data-field="clpurpose" data-sortable="true">Template Purpose</th>
			                            <th data-field="clcountsheets" data-sortable="true"># Sheets Using Template</th>
			                            <th data-field="clstatus" data-sortable="true">Status</th>		                                                       
			                            <th data-field="actions">Actions</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
						</div>
					</div>   	
				  </div>
			</div>
          
            </div>

        </div>
    </div>
    <input type="hidden" id="data-ajax" value="templates">
@endsection


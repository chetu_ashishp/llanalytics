@extends('company')
<?php //echo '<pre/>';print_r($companyList); ?>
@section('style')
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
@stop
@section('script')
<!-- Page-Level Plugin Scripts -->
   <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>   
   <script src="{{ asset('/js/jquery.mask.js') }}"></script>
   <script src="{{ asset('/js/clients/clients.js') }}"></script>    
@stop
@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
			  <div class="panel-heading">		  	
					<div class="container">
						<h4 class="panel-title pull-left">{{$form}} company</h4>
						<a href="{!!route('companyportal.details', array('company' =>$item->id)) !!}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>		
					</div>		  	
			  </div>
		  
		  <div class="panel-body">
			<div class="container datalist">		    	
				<div class="jumbotron container">
					
					{!! Form::open( array('role' => 'form', 'method' => 'put', 'files'=>true, 'class'=>'form-horizontal',  'autocomplete'=>"off", 'url' => array('companyportal/update/'.$item->id))) !!}
					
					<div class="form-group">
				    	<div class="col-sm-12">
				    		<a href="{!!route('companyportal.details', array('company' =>$item->id)) !!}" class="btn btn-default   btn-sm pull-right left10" role="button">Cancel</a>			      	
							{!! Form::submit('Save', array('class'=>'btn  btn-info pull-right btn-sm'))!!}
													
				    	</div>
					</div>					
					
					<div class="row">					
						<div class="col-sm-6">				                  	                  	
			               	<div class="form-group">
			                        {!!Form::label('company_number','Company Number', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('company_number', $item->company_number, array('class'=>'form-control', 'disabled'))!!}
									</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('company_name','Company Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('company_name', $item->company_name, array('class'=>'form-control'))!!}
									</div>
			            	</div>
							
			            	<div class="form-group">
			                        {!!Form::label('company_type','Company Type', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::select('company_type', array('' => '') + $companyList, $item->company_type, array('class'=>'form-control'))!!}
										
									</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('fname','Primary Contact - First Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('fname', $item->fname, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>	            	            	
			            	
			            	<div class="form-group">
			                        {!!Form::label('lname','Primary Contact - Last Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('lname', $item->lname, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('email','Primary Contact Email', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::email('email', $item->email, array('class'=>'form-control'))!!}
			                	</div>
			            	</div>
			            	
							<div class="form-group">
				                {!!Form::label('company_title','Primary Contact Title', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
									{!!Form::text('company_title', $item->company_title, array('class'=>'form-control'))!!}
				               	</div>
				            </div> 	
							
			            	<div class="form-group">
			                        {!!Form::label('primary_phone','Primary Contact - Primary Phone', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('primary_phone', $item->primary_phone, array('class'=>'form-control phoneUS  required', 'required' ))!!}
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('alternate_phone','Primary Contact - Alternate Phone', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('alternate_phone',  $item->alternate_phone, array('class'=>'form-control phoneUS' ))!!}
			                	</div>
			            	</div>       	   	
			            	<div class="form-group">
				                {!!Form::label('refsource_details','How did you hear about OMS', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	
									{!!Form::select('referral_source', array('' => '') + $referral_types, $item->referral_source, array('class'=>'form-control'))!!}
				               	</div>
				            </div> 			            	
			            		            	
			        	</div>
						
						<div class="col-sm-6">
							 <div class="form-group">
								{!!Form::label('addr_line1','Address Line 1', array('class'=>'col-sm-5 control-label'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('addr_line1', $item->addr_line1, array('class'=>'form-control' ))!!}
			                	</div>
							</div>
							 <div class="form-group">
								{!!Form::label('addr_line2','Address Line 2', array('class'=>'col-sm-5 control-label'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('addr_line2', $item->addr_line2, array('class'=>'form-control' ))!!}
			                	</div>
							</div>
							<div class="form-group">
								{!!Form::label('city','City', array('class'=>'col-sm-5 control-label'))!!}
								<div class="col-sm-7">		                    	
									{!!Form::text('city', $item->city, array('class'=>'form-control'))!!}
								</div>
							</div>
							<div class="form-group">
								{!!Form::label('state_id','State', array('class'=>'col-sm-5 control-label'))!!}
								<div class="col-sm-7">
									{!!Form::select('state_id', getAllStates(), $item->state_id, array('class'=>'form-control'))!!}
								</div>
							</div>
                             <div class="form-group">
								{!!Form::label('country_id','Country', array('class'=>'col-sm-5 control-label'))!!}
								<div class="col-sm-7">
									{!!Form::select('country_id', getAllCompanies(), $item->country_id, array('class'=>'form-control'))!!}
								</div>
							</div>
							<div class="form-group">
								{!!Form::label('zip','Zip Code', array('class'=>'col-sm-5 control-label'))!!}
								<div class="col-sm-7">		                    	

										{!!Form::text('zip', $item->zip, array('class'=>'form-control', "pattern"=>"(\d{5}([\-]\d{4})?)"  ))!!}
								</div>
							</div>
				            		            
							<div class="form-group">
				                {!!Form::label('website','Website', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('website',  $item->website, array('class'=>'form-control', 'placeholder'=>"https://website.com", 'id'=>'website'))!!}
				               	</div>
				            </div> 			            	
							
							
							<div class="form-group">
				                {!!Form::label('linkedin_url','LinkedIn URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('linkedin_url',  $item->linkedin_url, array('class'=>'form-control', 'placeholder'=>"https://linkedin.com", 'id'=>'linkedin_url'))!!}
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('facebook_url','Facebook URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('facebook_url',  $item->facebook_url, array('class'=>'form-control', 'placeholder'=>"https://facebook.com", 'id'=>'facebook_url'))!!}
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('twitter_url','Twitter URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('twitter_url',  $item->twitter_url, array('class'=>'form-control', 'placeholder'=>"https://twitter.com", 'id'=>'twitter_url'))!!}
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('googleplus_url','Google+ URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('googleplus_url',  $item->googleplus_url, array('class'=>'form-control', 'placeholder'=>"https://google.com", 'id'=>'googleplus_url'))!!}
				               	</div>
				            </div>
				        
				          	<div class="form-group">
				                {!!Form::label('status', 'Status', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('status',  array("Active"=>"Active", "Inactive"=>"Inactive", 'Suspended'=>'Suspended', 'Terminated'=>'Terminated') ,$item->status, array('class'=>'form-control'))!!}
				                 	
				               	</div>
				            </div>
				            	           		
			           	</div>
		           	</div>
		           
		           
	           		<div class="form-group">
				    	<div class="col-sm-12">
				    		<a href="{!!route('companyportal.details', array('company' =>$item->id)) !!}" class="btn btn-default   btn-sm pull-right left10" role="button">Cancel</a>			      	
							{!! Form::submit('Save', array('class'=>'btn  btn-info pull-right btn-sm'))!!}													
				    	</div>
					</div>
		           
		           				 
					{!! Form::close() !!}	
				</div>   		
			</div>    	
		  </div>
		
      		</div>
		</div>
    </div>
</div>

@stop
@extends('company')

@section('script')
<script>var tableData = {!! $tableData !!};</script>

<script src="{{ asset('/js/company.js') }}"></script>
@endsection

@section('content')
<?php 
if(isset($companyData) && $companyData != '') {
    $companyData = json_decode($companyData);
    $company_number = !empty($companyData[0]->company_number) ? $companyData[0]->company_number: '';
    $company_name = !empty($companyData[0]->company_name) ? $companyData[0]->company_name: '';
    $primary_contact = !empty($companyData[0]->primary_contact) ? $companyData[0]->primary_contact: '';
    $city = !empty($companyData[0]->city) ? $companyData[0]->city: '';
    $state_id = !empty($companyData[0]->state_id) ? $companyData[0]->state_id: 'N/A';
    $subscription_name = !empty($companyData[0]->subscription_name) ? $companyData[0]->subscription_name: 'N/A';
    $status = !empty($companyData[0]->status) ? $companyData[0]->status: '';
}
?>
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="container">			
                    <div class="row">
                       	<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i> Company Portal</h4>
                    </div>
                </div>
            </div>		
            <div class="panel-body">
                <div class="container datalist">
                    <div id="list">						
						 <table class="data-table22" cellpadding="10" cellspacing="10" border="0" width="100%" >
		                    <thead>
		                       <tr style="border-collapse: collapse">
                                    <th><div class="compantportal" >Company Number</div></div></th>
                                    <th><div  class="compantportal" >Company Name</div></th>
                                    <th><div  class="compantportal" >Primary Contact</div></th>
                                    <th><div  class="compantportal" >City</div></th>
                                    <th><div  class="compantportal" >State</div></th>
                                    <th><div  class="compantportal" >Subscription Level</div></th>
                                    <th><div  class="compantportal" >Status</div></th>
                                    <th><div  class="compantportal" >Actions</div></th>
                                </tr>
                             </thead>
		                    <tbody>
                               <tr style="border-collapse: collapse">
                                   <td><div class="compantportal" >{!! $company_number !!}</div></td>
                                   <td><div class="compantportal" >{!! $company_name !!}</div></td>
                                   <td><div class="compantportal" >{!! $primary_contact !!}</div></td>
                                   <td><div class="compantportal" >{!! $city !!}</div></td>
                                   <td><div class="compantportal" >{!! $state_id !!}</div></td>
                                   <td><div class="compantportal" >{!! $subscription_name !!}</div></td>
                                   <td><div class="compantportal" >{!! $status !!}</div></td>
                                   <td><div class="compantportal" ><a href="{!!route('companyportal.details', array('company' =>$companyData[0]->company_id)) !!}" class="btn btn-info btn-xs" role='button' >View details</a></div></td>
                               </tr>		                   
		                    </tbody>
		                	</table>		                
						</div>
                </div>			  
            </div>				
        </div>			
        
        <div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-table"></i> The most recent sheets  </h4>				  			
															  		
						</div>
				  	</div>
				</div>		
			
				<div class="panel-body">
					<div class="container datalist">
			    
					    @foreach($sheets as $key=>$sheet)
					    	<div class="row">
					    
						    	@foreach($sheet as $column)
						        	<div class="col-sm-4 col-md-4">
										<div class="thumbnail tile tile-medium tile-teal">
											<a href="/sheet/details/entry/{!!$column["id"]!!}">
											
												{!! $column["name"]!!} <br>	
												{!! $column["purpose"]!!} <br>
												{!! $column["status"]!!} <br>
											</a>
										</div>
									</div>									
								@endforeach
							</div>	
							
						@endforeach	
						
					</div>
				</div>
				     
       </div>   
       
        
        
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-envelope"></i> Notifications </h4>				  			
							<div class="col-lg-2 pull-right">						
								 {!! Form::select('filter_alerts_type', array('all' => 'All', 'Completed' => 'Complete', 'Incompleted' => 'Incomplete') , 'Incompleted', array('class' => 'form-control input-sm alertfilter' , 'data-path'=>'Data',  'data-route'=>'show',   "autocomplete"=>"off", 'data-_token'=>csrf_token())) !!}
							</div>								  		
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">
				    
						<div id="list">
						  	  <table class="data-table" dataSrc="tableData2" data-toggle="table" data-pagination="true" data-search="true" data-sort-name="name" data-sort-order="asc" >
			                    <thead>
			                        <tr>
			                            <th data-field="sender" data-sortable="true">Sender</th>
			                            <th data-field="time" data-sortable="true">Date/Time Sent</th>
			                            <th data-field="message" data-sortable="true">Message</th>
			                            
			                            <th data-field="sheet" data-sortable="true">Sheet Name</th>
			                            <th data-field="field" data-sortable="true">Field Name</th>
			                            <th data-field="client" data-sortable="true">Client</th>
			                            <th data-field="transaction" data-sortable="true">Transaction Info</th>
			                            
			                            <th data-field="status" data-sortable="true">Status</th>
			                            <th data-field="actions">Actions</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
						</div>
					</div>   	
				  </div>
			</div>
      </div>
   </div>
</div>


 <div class="modal modal-view "></div>
 
@endsection

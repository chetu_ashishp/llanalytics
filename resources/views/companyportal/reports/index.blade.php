@extends('app')

@section('script')

@endsection

@section('content')

<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-notes-2"></i> Reports </h4>							  		
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">
				    
						<div id="list">
						  	  <table class="data-table" data-toggle="table" data-pagination="true" data-search="true" data-sort-name="name" data-sort-order="asc" >
			                    <thead>
			                        <tr>
			                            <th data-field="name" data-sortable="true">Report Name</th>			                            			                            
			                        </tr>
			                    </thead>
			                    <tbody>
									<tr data-index="0">
										<td style=""><a href="companyportal/reports/sales">Sales Report</a></td>
									</tr>
									<tr data-index="1">
										<td style=""><a href="/reports/track_record">Track Record Report</a></td>
									</tr>
									<tr data-index="2">
										<td style=""><a href="/reports/sales_stats">Sales Statistics Report</a></td>
									</tr>								
									<tr data-index="3">
										<td style=""><a href="/reports/comparison">Comparison Report</a></td>
									</tr>								
			                    </tbody>
			                </table>
						</div>
					</div>   	
				  </div>
		</div>
      </div>
   </div>
</div>

<input type="hidden" id="data-ajax" value="Reports">
@endsection

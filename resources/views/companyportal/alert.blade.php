    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Notification</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" data-path="Companytools" data-_token="{!!csrf_token()!!}">
                
                
                    @if(!empty( $data["id"]))
                        {!! Form::hidden('id', $data["id"],  array('class'=>'alertid') ) !!}
                    @endif
                                   
                    <div class="form-group">
                        <label class="col-sm-3 control-label ">Transaction Status</label>
						<div class="col-sm-9">
							<p class="form-control-static">{!! $data["transaction_status"] !!}</p>
						</div>
                    </div>
              
              		<div class="form-group">
                        <label class="col-sm-3 control-label">Transaction Type</label>
						<div class="col-sm-9">
							<p class="form-control-static">{!! $data["transaction_type"] !!}</p>
						</div>
                    </div>
              
              		<div class="form-group">
                        <label class="col-sm-3 control-label">MLS #</label>
						<div class="col-sm-9">
							<p class="form-control-static">{!! $data["mls1"] !!}</p>
						</div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Short Address</label>
						<div class="col-sm-9">
							<p class="form-control-static">{!! $data["nickname"] !!}</p>
						</div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Property House #</label>
						<div class="col-sm-9">
							<p class="form-control-static">{!! $data["property_house"] !!}</p>
						</div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Property Address Line 1</label>
						<div class="col-sm-9">
							<p class="form-control-static">{!! $data["property_addr1"] !!}</p>
						</div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Property Address Line 2</label>
						<div class="col-sm-9">
							<p class="form-control-static">{!! $data["property_addr2"] !!}</p>
						</div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Message</label>
						<div class="col-sm-9">
							<p class="form-control-static">{!! $data["message"] !!}</p>
						</div>
                    </div>
                    
                 
                    <div class='row'>
                        <div class='col-md-12'>
                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                                    
                        </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
     
     
     
     
     
     

@extends('app')

@section('content')
<div class="container">
<div class="row">
    <div class="col-md-4">
        <div class="btn-group btn-group-justified">
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="panel panel-info">
         <div class="panel-heading">		  	
					<div class="container">
						<h4 class="panel-title pull-left"> Edit Role</h4>
						<a href="{{route('roles')}}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>
					</div>	  	
			  </div>
        <div id="content">
            <div id="details">                       
                {!! Form::model($role, array('role' => 'form', 'method' => 'put', 'route' => array('roles.update', $role->id))) !!}
                <div class="form-group"  style="clear:both; margin: 5px 0px 20px 0px">
                       {!!Form::label('role_name','Name', array('class'=>'col-sm-4 control-label text-right'))!!}
                        <div class="col-sm-4 " style="margin-top:-5px;">
                          {!! Form::text('role_name', $role->role_name, array('class' => 'form-control','required'=>'required')) !!}
                        </div>
                </div>
                 <div class="form-group"  style="clear:both; margin: 5px 0px 20px 0px">
                       
                        <div class="col-sm-7">
                           
                        </div>
                </div>
                <div class="form-group"  style="clear:both;">                    
                          {!!Form::label('role_description','Role Description', array('class'=>'col-sm-4 control-label text-right'))!!}
                        <div class="col-sm-4" style="margin-top:-5px;">
                          {!! Form::text('role_description', $role->role_description, array('class' => 'form-control')) !!}
                        </div>
                </div>
                <div class="form-group"  style="clear:both; margin: 5px 0px 20px 0px">
                       
                        <div class="col-sm-7">
                           
                        </div>
                </div>
                 <div class="form-group"  style="clear:both;">
                        {!!Form::label('permission_level','Permission Level', array('class'=>'col-sm-4 control-label text-right'))!!}
                        <div class="col-sm-4" style="margin-top:-5px;">
                           {!! Form::text('permission_level', $role->permission_level, array('class' => 'form-control','placeholder'=>'Permission Level')) !!}
                        </div>
                </div>
                <div class="form-group"  style="clear:both; margin: 5px 0px 20px 0px">
                       
                        <div class="col-sm-7">
                           
                        </div>
                </div>
            </div>
          </div>
        
       
            <div class="form-group">
                <div class="col-sm-5 col-md-4 col-md-offset-4">            
               {!! Form::button('Save', array('type' => 'submit', 'class' => 'btn btn-sm btn-info')) !!}
                  <a href="{{route('roles')}}" class="btn btn-default  btn-sm  left10 " role="button">Cancel</a>
                {!! Form::close() !!}
                </div>
            </div>
     
            <div class="form-group"  style="clear:both; margin: 5px 0px 20px 0px">                       
                <div class="col-sm-7"></div>
            </div>
     </div>       
    </div>
</div>

@stop

@section('scripts')
<script>
    $('#myTab a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    })
</script>
@stop
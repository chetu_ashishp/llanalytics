@extends('app')
@section('style')
<link href="{{ asset('/css/custom.css') }}" rel="stylesheet">	
	<link href="{{ asset('/vendor/bootstrap-table/dist/bootstrap-table.css') }}" rel="stylesheet">	
    <link href="{{ asset('/vendor/bootstrap-table-filter/src/bootstrap-table-filter.css') }}" rel="stylesheet">	
     <link href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">	
@stop
@section('script')
<script src="{{ asset('/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script> 

<script src="{{ asset('/js/jquery.mask.js') }}"></script>
<script src="{{ asset('/js/role.js') }}"></script>   
<script>
$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click
       // alert('selecctall');
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
   
});
</script>
@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i> Assign Permission </h4>				  			
                            <a href="{{route('roles')}}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>
                        </div>
				  	</div>
				</div>
                 {!! Form::open( array('role' => 'form', 'method' => 'post', 'class'=>'form-horizontal',  'autocomplete'=>"off", 'url' => array('roles/storePermission'),'data_token' => 'saafaf')) !!}
				  <div class="panel-body">
				    <div class="container datalist">
						 <div class="col-md-12 form-group">
                        {!!Form::label('role_name', 'Select Roles', array('class'=>'col-sm-3 control-label'))!!}
                        <div class="col-sm-5 pd-right0" style="margin-top: -5px;">
                            {!!Form::select('role_name', array('' => 'Select Role') + $all_roles, 'Super Admin', array('class'=>'form-control role_name required', 'required','data-path' => 'roles','data-route' => 'getRoles', 'data-_token'=>csrf_token()))!!}

                        </div>
                    </div>
					
               <div class="form-group col-md-12 routelist hide">
                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
				{!!Form::label('Roles', 'Assign Permissions', array('class'=>'col-sm-3 role_assign control-label'))!!}
				  <div class="checkbox route-assign-list col-sm-5" style="margin-top:0;">
					<div class="row">
					   <div class="col-md-12" style="margin-bottom: 10px;">
							<label>

							   {!! Form::checkbox('all', 'all', null, ['class' => 'field','id' => 'selecctall']) !!}Select All
							</label>
					   </div>              
					@foreach($permissions as $key => $route)
					<div class="col-md-12">
						<label>
						  
						   {!! Form::checkbox('route[]', $route->permission_id, '', ['class' => 'checkbox1','id' => $route->permission_id]) !!}
							 {!!$route->name!!}
						</label>
				   </div>               
				   @endforeach             
				   
				   
					</div>
				  </div>
                <div class="row">
				 <div class="checkbox col-sm-7 col-md-offset-3">
                     
					{!! Form::button('Save', array('type' => 'submit', 'class' => 'btn btn-info style_save')) !!}
                     <a href="{{route('roles')}}" class="btn btn-default  btn-sm  left10" role="button">Cancel</a>
					{!! Form::close() !!}
				 </div>
                </div>
            </div> 
           </div>
				 </div>
			</div>          
        </div>
   </div>
</div>
@endsection
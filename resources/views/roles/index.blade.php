@extends('app')

@section('script')
<script>var tableData = {!! $tableData !!};</script>
 <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
 <script src="{{ asset('/js/typeahead.js-master/dist/typeahead.jquery.js') }}"></script>  
 <script src="{{ asset('/js/role.js') }}"></script>
@endsection


@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i> Roles </h4>
				  			<a href="{{route('roles.create') }}" class="btn btn-info  btn-sm pull-right left10 createrole" role="button" data-path="roles" data-route="createrole" data-_token="{{ csrf_token() }}"><i class="fa fa-plus"></i> Add New Role</a>
                            <a href="{{route('roles.assign') }}" class="btn btn-info  btn-sm pull-right rolepermissionassign" data-path="roles" data-route="rolepermissionassign" data-_token="{{ csrf_token() }}" role="button"><i class="fa fa-plus"></i> Assign Role & Permission</a>
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">
						<div id="list">
							 <table class="data-table" data-toggle="table" data-pagination="true" data-search="true" data-sort-name="name" data-sort-order="asc" >
			                    <thead>
			                        <tr>
			                            <th data-field="name" data-sortable="true">Name</th>
			                            <th data-field="desc" data-sortable="true">Description</th>
                                        <th data-field="permission_level" data-sortable="true">Permission Level</th>
			                            <th data-field="actions">Actions</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>                            
						</div>
					</div>   	
				 </div>
			</div>          
        </div>
   </div>
</div>
@endsection
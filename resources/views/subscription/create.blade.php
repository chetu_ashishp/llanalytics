@extends('app')



@section('script')


<!-- Page-Level Plugin Scripts -->
   <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.mask.js') }}"></script>   
    <script src="{{ asset('/js/inputmask.js') }}"></script> 
    <script src="{{ asset('/js/jquery.inputmask.js') }}"></script>  
   <script src="{{ asset('/js/subscription/subscription.js') }}"></script>
  
@stop



@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
			  <div class="panel-heading">		  	
					<div class="container">
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-notes"></i> {{$form}} subscription </h4>
                            <a href="{!! isset($data->item->id) ? route('sheet.details.edit', array('sheet' => $data->item->id)) : route('subscriptions') !!}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>
						</div>
					</div>
			  </div>
		  
		  <div class="panel-body">
			<div class="container datalist">		    	
				<div class="jumbotron container">
							           
					{!! Form::open( array('role' => 'form', 'method' => 'post', 'files'=>true, 'class'=>'form-horizontal', 'id'=>'add-template',  'autocomplete'=>"off", 'url' => array('subscriptions/store'))) !!}
					
					 			
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	   
					    	 	<a href="{!! isset($data->item->id) ? route('subscriptions.details.edit', array('sheet' => $data->item->id)) : route('subscriptions') !!}" class="btn btn-default  btn-sm  pull-right left10" role="button">Back</a>  	
								{!! Form::submit('Save', array('class'=>'btn  btn-info  btn-sm  pull-right '))!!}								
					    	</div>
						</div>
		           </div>
		           			
		           			
		           			
					<div class="row">					
						<div class="col-sm-6">				                  	                  	
			               		            	
			            	<div class="form-group">
			                        {!!Form::label('subscription_name','Subscription Package Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">			                    	
                                    {!!Form::text('subscription_name', null, array('class'=>'form-control required', 'required','placeholder' => 'Enter Subscription Package Name'))!!}
			                	</div>
			            	</div>
			            	         	     	
					
				
							<div class="form-group">
			                    {!!Form::label('fee_type','Base Rate', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::text('fee_type', null, array('class'=>'form-control feetype required', 'required','placeholder' => 'Enter Total Subscription Price,  ex: 100.00' ))!!}
			                	</div>
			            	</div>
                            
							 <div class="form-group">
			                    {!!Form::label('billing_frequency','Billing Frequency ', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">	                    	
                                    {!!Form::select('billing_frequency',  array("" => "") + $billingFrequency, 'Monthly', array('class'=>'form-control  required', 'required'))!!}
                                    
			                	</div>
			            	</div>									  							
															
                            <div class="form-group">
				                {!!Form::label('subscription_status', 'Status', array('class'=>'col-sm-5 control-label required'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('subscription_status',  array("Active"=>"Active", "Inactive"=>"Inactive") ,isset($data->item->status) ? $data->item->status : "Active", array('class'=>'form-control', 'required'))!!}
				               	</div>
				            </div>
                            
                            <div class="form-group">
				                {!!Form::label('subscription_users_allowed', '# of Users Included in Base Rate', array('class'=>'col-sm-5 control-label required'))!!}
				                <div class="col-sm-7">
                                    {!!Form::text('subscription_users_allowed', null, array('class'=>'form-control costunit required', 'required','placeholder' => 'Enter Number of user for this subscription'))!!}
				               	</div>
				            </div>
							  		            	           		
			           	</div>
		          
		           	<div class="col-sm-6">
							<div class="form-group">
			                    {!!Form::label('cost_per_unit','Rate for each Additional User', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::text('cost_per_unit', null, array('class'=>'form-control costunit required', 'required' ,'placeholder' => 'Enter Additional Cost per User Added'))!!}
			                	</div>
			            	</div>
                           <div class="form-group">
			                    {!!Form::label('max_user_allowed','Maximum Users Allowed', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::text('max_user_allowed', null, array('class'=>'form-control costunit required', 'required' ,'placeholder' => 'Enter Max User Allowed for this Subscription'))!!}
			                	</div>
			            	</div>
                            <div class="form-group">
			                        {!!Form::label('subscription_desc','Subscription Details ', array('class'=>'col-sm-5  control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::textarea('subscription_desc', null, ['size' => '42x7'], array('class'=>'form-control'))!!}
			                	</div>
			            	</div> 
                    </div>    
                    </div>    
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	   
					    	 	<a href="{!! isset($data->item->id) ? route('sheet.details.edit', array('sheet' => $data->item->id)) : route('subscriptions') !!}" class="btn btn-default  btn-sm  pull-right left10" role="button">Back</a>  	
								{!! Form::submit('Save', array('class'=>'btn  btn-info  btn-sm  pull-right '))!!}								
					    	</div>
						</div>
		           </div>
		           				 
					{!! Form::close() !!}	
				</div>   		
			</div>    	
		  </div>
		
      		</div>
		</div>
    </div>
</div>

@stop
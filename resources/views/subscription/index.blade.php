@extends('app')
@section('style')
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
	<link href="{{ asset('/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}" rel="stylesheet">
@stop
@section('script')
<script>var tableData = {!! $tableData !!};</script>
<script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('/js/typeahead.js-master/dist/typeahead.jquery.js') }}"></script>
   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>   
    <script src="{{ asset('/js/jquery.mask.js') }}"></script>
  

   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.mask.js') }}"></script>   
    <script src="{{ asset('/js/inputmask.js') }}"></script> 
    <script src="{{ asset('/js/jquery.inputmask.js') }}"></script>  
    <script src="{{ asset('/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('/js/subscription/subscription.js') }}"></script> 
 
 
@endsection

@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i> Subscriptions </h4> 		  			 
				  			<a href="{{route('newsubscription')}}" class="btn btn-info  btn-sm pull-right subscriptionscreate" role="button" data-path="subscriptions" data-route="subscriptionscreate"  data-_token="{{ csrf_token() }}"><i class="fa fa-plus"></i> Add New Subscriptions</a>				  		
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">
                        {!! Form::open( array('subscriptionsearch' => 'form', 'method' => 'put', 'files'=>true, 'class'=>'form-horizontal form-subscription-search',  'autocomplete'=>"off")) !!} 
				    	<div class="search-radio-btn pull-left">
                              <div class="input-field"> {!! Form::radio('subscriptionstatus', 'Active', true, ['class' => 'left10 right10','id' => 'subscriptionstatus']) !!} Active</div>       
                              <div class="input-field"> {!! Form::radio('subscriptionstatus', 'NonActive', null, ['class' => 'left10 right10','id' => 'subscriptionstatus']) !!} NonActive</div>
                              <div class="input-field">  {!! Form::submit('Search', array('class'=>'btn btn-xs btn-info', 'data-path'=>'Subscriptions','data-route' => 'searchsubscription','data-_token'=>csrf_token()))!!}</div>
                        </div>
                        {!! Form::close() !!}
						<div id="list">
							 <table class="data-table" data-toggle="table" data-pagination="true" data-search="true" data-sort-name="clname" data-sort-order="asc" >
			                    <thead>
			                        <tr>
			                            <th data-field="subscription_name" data-sortable="true">Subscription Name</th>
			                            <th data-field="fee_type" data-sortable="true">Fee Type</th>
			                            <th data-field="cost_per_unit" data-sortable="true"># Cost Per Unit</th>
			                            <th data-field="billing_frequency" data-sortable="true">Billing Frequency</th>		                                                       
                                        <th data-field="subscription_status" data-sortable="true">Status</th>
			                            <th data-field="actions">Actions</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
						</div>
					</div>   	
				  </div>
			</div>
          
            </div>

        </div>
    </div>
    <input type="hidden" id="data-ajax" value="templates">
@endsection


@section('style')
 
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
@stop
@section('script')

<!-- Page-Level Plugin Scripts -->
  
     
@stop 	 
<div class="panel-body">
			<div class="container datalist">
				<div class="row">    
					<div class="col-md-12">
						<div class="panel panel-info">
						  <div class="panel-heading">		  	
								<div class="container">
									<h4 class="panel-title pull-left">Add Payment/Adjustment</h4>
									
								</div>		  	
						  </div>
						</div>
					</div>
				</div>
				<div class="jumbotron container">
					
					
					
					{!! Form::open( array('id' => 'add_paymentadjustment', 'data-route' => 'storepaymentadjustment', 'class'=>'form-horizontal form-add',  'autocomplete'=>"off", 'data-path' => 'Subscriptions', 'data-_token'=>csrf_token() )) !!}
                    {!!Form::hidden('billing_id', $billing_id)!!}
                   	{!!Form::hidden('company_id', $company_id)!!}
                    {!!Form::hidden('user_id', '')!!}
					<div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	     	
								{!! Form::button('Cancel', array('class'=>'btn  btn-sm btn-default pull-right left10', 'id'=>'cancelPaymentAdjustment'))!!}
								{!! Form::submit('Save', array('class'=>'btn btn-sm btn-info pull-right'))!!}							
					    	</div>
						</div>
		           </div>
		           
		           
					<div class="row">					
						<div class="col-sm-6">				                  	                  	
			              
							<div class="form-group">
			                        {!!Form::label('payment_adjustment_type','Type', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										<!--{!!Form::text('payment_adjustment', null, array('class'=>'form-control required', 'required'))!!}!-->
                                        {!!Form::select('payment_adjustment_type',  array("Payment"=>"Payment", "Adjustment"=>"Adjustment") ,"Payment", array('class'=>'form-control'))!!}
									</div>
			            	</div>	            	            	
			            	
			            	<div class="form-group">
			                         {!!Form::label('payment_adjustment_date','Date', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('payment_adjustment_date', $currentDate, array('class'=>'form-control datepicker', 'id'=>'datepicker', "pattern"=>"(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d", 'required' ))!!}
									</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('payment_adjustment_amount','Amount', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('payment_adjustment_amount', null, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('payment_adjustment_reason','Reason', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
                                        {!!Form::text('payment_adjustment_reason', null, array('class'=>'form-control required', 'required'))!!}
                                    </div>
			            	</div>
			       </div>
						
						<div class="col-sm-4">
							 <div class="form-group">
			                        {!!Form::label('payment_adjustment_note','Note ', array('class'=>'col-sm-5  control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::textarea('payment_adjustment_note', null, ['size' => '42x7'], array('class'=>'form-control'))!!}
			                	</div>
			            	</div> 
						      		
			           	</div>
		           	</div>
		 
		       	
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	     	
								{!! Form::button('Cancel', array('class'=>'btn  btn-sm btn-default pull-right left10', 'id'=>'cancelPaymentAdjustment'))!!}
								{!! Form::submit('Save', array('class'=>'btn-sm btn btn-info pull-right','data-company_id'=>$company_id))!!}
																
					    	</div>
						</div>
		           </div>
		           				 
					{!! Form::close() !!}	
				</div>   		
			</div>    	
		  </div>
  </div>		
@extends('app')

@section('style')
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
@stop

@section('script')


<!-- Page-Level Plugin Scripts -->
   <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.mask.js') }}"></script>   
    <script src="{{ asset('/js/inputmask.js') }}"></script> 
    <script src="{{ asset('/js/jquery.inputmask.js') }}"></script>  
   <script src="{{ asset('/js/subscription/subscription.js') }}"></script>
  
@stop


<?php //echo '<pre/>'; print_r($item); die;
$subscription_id = !empty($item[0]->id) ? $item[0]->id: '';
$subscription_name = !empty($item[0]->subscription_name) ? $item[0]->subscription_name: ''; 
$fee_type = !empty($item[0]->fee_type) ? $item[0]->fee_type: ''; 
$cost_per_unit = !empty($item[0]->cost_per_unit) ? $item[0]->cost_per_unit: ''; 
$billing_frequency = !empty($item[0]->billing_frequency) ? $item[0]->billing_frequency: ''; 
$subscription_status = !empty($item[0]->subscription_status) ? $item[0]->subscription_status: ''; 
$start_date = !empty($item[0]->start_date) ? $item[0]->start_date: ''; 
$end_date = !empty($item[0]->end_date) ? $item[0]->end_date: '';
$total_sub_user = !empty($item[0]->subscription_users_allowed) ? $item[0]->subscription_users_allowed: ''; 
$subscription_desc = !empty($item[0]->subscription_desc) ? $item[0]->subscription_desc: '';
$max_user =  !empty($item[0]->max_user_allowed) ? $item[0]->max_user_allowed: '';

?>

@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
			  <div class="panel-heading">		  	
					<div class="container">
						
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-notes"></i> {{$form}} subscription </h4>
                            <a href="{!!route('subscriptions') !!}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>
					</div>		  	
			  </div>
		  
		 <div class="panel-body">
			<div class="container datalist">		    	
				<div class="jumbotron container">							           
					{!! Form::open( array('role' => 'form', 'method' => 'put', 'files'=>true, 'class'=>'form-horizontal', 'id'=>'edit-template', 'autocomplete'=>"off", 'url' => array('subscriptions/update/'.$subscription_id))) !!}
                    <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">
					    		<a href="{!!route('subscriptions') !!}" class="btn btn-default  btn-sm  pull-right left10" role="button">Back</a>	     	
								{!! Form::submit('Save', array('class'=>'btn  btn-info btn-sm  pull-right'))!!}
					    	</div>
						</div>
		           </div> 
                   <div class="row">					
						<div class="col-sm-6">				                  	                  	
			               		            	
			            	<div class="form-group">
			                        {!!Form::label('subscription_name','Subscription Package Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">			                    	
                                    {!!Form::text('subscription_name', $subscription_name, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
			            	<div class="form-group">
			                    {!!Form::label('fee_type','Fee Type (Subscription Price)', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::text('fee_type', $fee_type, array('class'=>'form-control feetype required', 'required'))!!}
			                	</div>
			            	</div>
                            
							<div class="form-group">
			                    {!!Form::label('billing_frequency','Billing Frequency ', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::select('billing_frequency', array('' => '') + $billingFrequency, $billing_frequency, array('class'=>'form-control required', 'required'))!!}
                                     <!--{!!Form::text('billing_frequency', $billing_frequency, array('class'=>'form-control required', 'required'))!!}!-->
                                </div>
			            	</div>									  							
															
                            <div class="form-group">
				                {!!Form::label('subscription_status', 'Status', array('class'=>'col-sm-5 control-label required'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('subscription_status',  array("Active"=>"Active", "Inactive"=>"Inactive") ,$subscription_status, array('class'=>'form-control', 'required'))!!}
				               	</div>
				            </div>
                            <div class="form-group">
                              {!!Form::label('subscription_users_allowed', 'No of User Allowed', array('class'=>'col-sm-5 control-label required'))!!}
                              <div class="col-sm-7">
                                  {!!Form::text('subscription_users_allowed', $total_sub_user, array('class'=>'form-control costunit required', 'required'))!!}
                              </div>
                          </div>  		            	           		
			           	</div>
		          
		           	<div class="col-sm-6">
							<div class="form-group">
			                    {!!Form::label('cost_per_unit','Cost Per Unit / User', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::text('cost_per_unit', $cost_per_unit, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
                             <div class="form-group">
			                    {!!Form::label('max_user_allowed','Max User Allowed', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::text('max_user_allowed', $max_user, array('class'=>'form-control costunit required', 'required' ,'placeholder' => 'Enter Max User Allowed for this Subscription'))!!}
			                	</div>
			            	</div>                            
                            <div class="form-group">
			                        {!!Form::label('subscription_desc','Package Details ', array('class'=>'col-sm-5  control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::textarea('subscription_desc', $subscription_desc, ['size' => '40x5'], array('class'=>'form-control'))!!}
			                	</div>
			            	</div> 
                    </div>    
                    </div>    
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	   
					    	 	<a href="{!! isset($data->item->id) ? route('sheet.details.edit', array('sheet' => $data->item->id)) : route('subscriptions') !!}" class="btn btn-default  btn-sm  pull-right left10" role="button">Back</a>  	
								{!! Form::submit('Save', array('class'=>'btn  btn-info  btn-sm  pull-right '))!!}								
					    	</div>
						</div>
		           </div>
		           				 
					{!! Form::close() !!}	
				</div>   		
			</div>    	
		  </div
    </div>
</div>



@stop
@extends('innertab')

@section('script')
 
<script>
var tableData = {!! $tableData !!};
</script>
@endsection

@section('content')
<div class="container top20">
    <div class="row">    
        <div class="col-md-12">
	       	
	        <div class="panel panel-info" id="transactions-div">
	            				
	        	<div class="panel-body">	        	
	        				        
				    <div class="container datalist">
						<div id="list">
							 <table class="data-table" data-toggle="table" data-pagination="true" data-sort-name="start_date" data-sort-order="desc" >
			                    <thead>
			                        <tr>
			                            <th data-field="invoice_id" data-sortable="true">Invoice Id</th>
			                            <th data-field="created_at" data-sortable="true">Invoice Date</th>
                                        <th data-field="total_amount" data-sortable="true">Total Amount</th>                                   
                                        <th data-field="amount_due" data-sortable="true">Amount Due</th>
                                        <th data-field="item_purchased" data-sortable="true">Item(s) Purchased</th>
                                        <th data-field="user_added_first_name" data-sortable="true">Staff Who Purchased</th>
                                        <th data-field="status" data-sortable="true">Status</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    </tbody> 
			                </table>		                
						</div>
					</div>   	
				</div>
			</div>
									
			<div class="panel panel-info"  id="newtransaction-div" style="display:none">				
			 	
      		</div>      		
			 
			 <div class="panel panel-info"  id="edittransaction-div" style="display:none"></div>    
		</div>
	</div>
</div>
<input type="hidden" id="data-ajax" value="Transactions">
@endsection


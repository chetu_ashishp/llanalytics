@extends('app')
@section('style')
    <link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}" rel="stylesheet">
@stop

@section('script')

<!-- Page-Level Plugin Scripts -->

<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
  
    <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
 
    <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>   
    <script src="{{ asset('/js/jquery.mask.js') }}"></script>   
    <script src="{{ asset('/js/company/company.js') }}"></script> 
   
 <script src="{{ asset('/js/clients/jquery.formatCurrency-1.4.0.min.js') }}"></script>
 <script src="{{ asset('/js/company/staff.js') }}"></script> 
 <script src="{{ asset('/vendor/925491/jquery.ui.autocomplete.html.js') }}"></script>
   <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
@stop
@section('content')

<div class="panel panel-info">
  <div class="panel-heading">		  	
		<div class="container">
			<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-info-sign"></i> Company:  {{$company_info[0]['company_name']}} </h4>
			<a href="{{$company_info[0]['id']}}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>			
		</div>		  	
  </div>
  <div class="panel-body"> 		
		<!-- Nav tabs -->		
		  <ul class="nav nav-tabs" role="tablist">
		        <li class="active"><a href="#core_details" data-toggle="tab">Core Details</a></li>
		        <li><a href="#staffs" id="lstaff" class="ajax companystaff" data-path="Staffs" data-route="show" data-item="{{ $company_info[0]['id'] }}"  data-_token="{{csrf_token()}}" data-toggle="tab" >Staff Details</a></li>
               <!-- <li><a href="#subscriptionHistory" id="sstaff" class="ajax" data-path="Company" data-route="subscriptionhistory" data-target="#subscriptionHistory" data-item="{{ $company_info[0]['id'] }}"  data-_token="{{csrf_token()}}" data-toggle="tab" >Subscription History</a></li>!--->
                <li><a href="#subscriptionHistory" id="subscription_history" class="ajax" data-path="Subscriptions" data-target="#subscriptionHistory" data-route="getBillingsList" data-item="{{ $company_info[0]['id'] }}"  data-_token="{{csrf_token()}}" data-toggle="tab" >Subscription History</a></li>
                <li><a href="#billingHistory" id="billing_history" class="ajax" data-path="Subscriptions" data-target="#billingHistory" data-route="getSubscribedBillingsDetails" data-item="{{ $company_info[0]['id'] }}"  data-_token="{{csrf_token()}}" data-toggle="tab" >Billing History</a></li>
          </ul>
			
		<!-- Tab panes -->
	    <div class="tab-content">
	        <div class="tab-pane active" id="core_details">
	            <div class="row">
	                 <div class="col-lg-8 col-lg-offset-1">
	                 	 <div class="row">
		                    <div class="top10 col-lg-7">
		                    
		                    	<a href="{{route('company')}}" class="btn btn-default  btn-sm pull-right left10" role="button" data-route ="companycancel" data-path="Company" data-_token="{{csrf_token()}}">Cancel</a>								
								<a href="#" id="deleteCompanye" class="btn  btn-sm pull-right left10 btn-danger deleteCompany", data-id="{{$company_info[0]['id']}}" data-item="{{$company_info[0]['id']}}" data-route ="removeCheck" data-path="Company" data-_token="{{csrf_token()}}">Delete</a>
                                <a href="{{route('company.edit', array('company' => $company_info[0]['id'])) }}" class="btn editcompany btn-warning btn-sm pull-right left10" role='button' data-path="Company" data-route="editcompany" data-_token="{{ csrf_token() }}">Edit</a>
                                <?php if($chksubscriptionexpdate === false) { ?>
                                    <a href="#" class="btn btn-primary company_add_subscription btn-sm pull-right left10" role="button" data-toggle="modal" data-path="Company" data-route="addsubscription" data-_token="{{ csrf_token() }}" data-target="#myModalHorizontal">Add Subscription</a>
                                <?php }else { ?>
                                   <a href="#" class="btn btn-primary btn-sm pull-right left10" role="button">Subscription is Active</a> 
                               <?php } ?>
		                    </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-lg-8 col-lg-offset-1">
	                    <div class="row">
	                        <div class="col-lg-7">	                            
	                            @foreach($lables as $key=>$value)
	                     	        
									<div class="row detail-cell">
										<div class="col-lg-6"><p class = "detail-cell-label"> {{$value}} </p></div> 
									   @if($key=='referral_source')
										<div class="col-lg-6"><p>{{$refsource['option_name']}}</p></div>												
										@elseif($key=='company_type')
										<div class="col-lg-6"><p>{{$companyList['option_name']}}</p></div>												
										@else

											<div class="col-lg-6"><p>{{$company_info[0][$key]}}</p></div>

										  @endif
									</div>				                        
	                     	    	                    	
	                            @endforeach
								
	                        </div>
	                    </div>
	                </div>
	            </div>
	            
	              <div class="row">
	                 <div class="col-lg-8 col-lg-offset-1">
	                 	 <div class="row">
		                    <div class="top10 col-lg-7">	
		                   
		                    	<a href="{{route('company')}}" class="btn btn-default  btn-sm pull-right left10" role="button">Cancel</a>	                        
		                       
		                       <a href="{{route('company.edit', array('company' => $company_info[0]['id'])) }}" class="btn editcompany btn-warning btn-sm pull-right left10" role='button' data-path="Company" data-route="editcompany" data-_token="{{ csrf_token() }}">Edit</a>
		                    </div>
	                    </div>
	                </div>
	            </div>
	            
	        </div>
	        <div class="tab-pane" id="staffs"></div>
            <div class="tab-pane" id="subscriptionHistory"></div>
            <div class="tab-pane" id="billingHistory"></div>
	    </div>
	    
	    		
  </div>
    
    <!-- Model for add subscription !-->
    <div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" 
                       data-dismiss="modal">
                           <span aria-hidden="true">&times;</span>
                           <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Subscribe Subscription Package
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">

                    {!! Form::open( array('id' => 'subscription-purchase-form', 'data-route' => 'purchaseSubscription', 'class'=>'form-horizontal form-add',  'autocomplete'=>"off", 'data-path' => 'Company', 'data-route' => 'purchaseSubscription', 'data-_token'=>csrf_token() )) !!}
                      {!!Form::hidden('company_id', $company_info[0]['id'])!!}
                      <div class="form-group">
                        <label  class="col-sm-3 control-label"
                                  for="inputEmail3">Subscription</label>
                        <div class="col-sm-9">
                            {!!Form::select('subscription_list',  array("" => "") + $subscriptionList, null, array('class'=>'form-control subscriptionList required','id' => 'subscription_list', 'required', 'data-path' => 'Company', 'data-_token'=>csrf_token(), 'data-route' => 'subscription', 'width' => 'auto'))!!}
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label"
                              for="inputPassword3" >Fee Type</label>
                        <div class="col-sm-9">
                            {!!Form::text('fee_type', null, array('class'=>'form-control-custom required', 'id' => 'fee_type', 'required readonly'))!!}                            
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="col-sm-3 control-label"
                              for="inputPassword3" >Cost Per Unit</label>
                        <div class="col-sm-9">
                            {!!Form::text('cost_per_unit', null, array('class'=>'form-control-custom required', 'id' => 'cost_per_unit', 'required readonly'))!!}                            
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="col-sm-3 control-label" 
                              for="inputPassword3" >Billing Frequency</label>
                        <div class="col-sm-9">
                            {!!Form::text('billing_frequency', null, array('class'=>'form-control-custom  required', 'id' => 'billing_frequency', 'required readonly'))!!}                            
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="col-sm-3 control-label" 
                              for="inputPassword3">No of User Allowed</label>
                        <div class="col-sm-9">
                            {!!Form::text('subscription_users_allowed', null, array('class'=>'form-control-custom  required', 'id' => 'subscription_users_allowed', 'required readonly'))!!}                            
                        </div>
                      </div> 
                       <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputPassword3" >Start Date</label>
                        <div class="col-sm-9">
                            {!!Form::text('start_date', null, array('class'=>'form-control-custom startdate ', 'id'=>'datepicker', "pattern"=>"(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d", 'size'=>'35', 'width' => '96', 'required' ))!!}
                        </div>
                      </div>  
                        <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputPassword3" >End Date</label>
                        <div class="col-sm-9">
                            {!!Form::text('end_date', null, array('class'=>'form-control-custom endDate', 'id'=>'datepicker', "pattern"=>"(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d", 'size'=>'35', 'width' => '96', 'required readonly' ))!!}
                        </div>
                      </div> 
                      
                     <!-- <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-default">Sign in</button>
                        </div>
                      </div>!-->
                    {!! Form::close() !!}
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                                Close
                    </button>
                    {!! Form::submit('Subscribe Package', array('class'=>'btn  btn-info  btn-sm  pull-right ','id' => 'subscribe_package'))!!}
                </div>
            </div>
        </div>
    </div>
</div>    
@stop
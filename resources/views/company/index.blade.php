@extends('app')
@section('style')
	
	<link href="{{ asset('/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}" rel="stylesheet">
@stop

@section('script')
<script>var tableData = {!! $tableData !!};</script>


<!-- Page-Level Plugin Scripts -->
   
 <script src="{{ asset('/js/jquery.mask.js') }}"></script>    
 <script src="{{ asset('/js/company/staff.js') }}"></script>
 <script src="{{ asset('/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
 <script src="{{ asset('/vendor/925491/jquery.ui.autocomplete.html.js') }}"></script>
  
@endsection

@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i> Companies </h4>							
				  			<a href="{{route('newcompany')}}" class="btn btn-info companycreate btn-xs pull-right" data-path="company" data-route="create" data-_token={{ csrf_token() }} role="button"><i class="fa fa-plus"></i> Add New Company</a>				  						  		
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">	    	
				    	<div class="jumbotron container">				    		
				    	
					    	 {!!Form::open(array('data-path' => 'Company','data-route'=>'search', 'class' => 'form-horizontal form-company-search', 'data-_token'=>csrf_token()))!!}
				                  	<div class="col-sm-5">				                  	                  	
				                       <div class="form-group">
				                            {!!Form::label('company_number','Search', array('class'=>'col-sm-4 control-label'))!!}
				                            <div class="col-sm-8">
				                            	{!!Form::text('company_number', null, array('class'=>'form-control'))!!}
				                            </div>
				                       </div>				                                           
				                     </div>		
				                  	 <div class="col-sm-3">                  	   				                        				                        
				                        <div class="form-group">
				                            {!!Form::label('status','Status', array('class'=>'col-sm-4 control-label'))!!}
				                            <div class="col-sm-8">				                            	
				                            	 {!! Form::select('status', array(''=>'', 'Active' => 'Active', 'Inactive' => 'Inactive', 'Suspended'=>'Suspended', 'Terminated'=>'Terminated'), null, array('class'=>'form-control')) !!}
											</div>	
										</div>	                	
				                  	</div> 
									<div class="col-sm-2">			                  	                  	
                                        {!! Form::submit('Search', array('class'=>'btn btn-xs btn-info pull-left mar-top7 companysearch', 'data-path'=>'Company', 'data-path'=>'search', 'data-_token'=>csrf_token()))!!}	                                          
				                  	</div>	
							{!!Form::close()!!}
						</div>		    				
						<div id="list">						
						 <table class="data-table apply_dataTable" data-toggle="table" data-pagination="true" data-search="true" data-label="bus" data-sort-name="clname" data-sort-order="asc" data-page-length="100" data-page-list="[25, 50, 100, 200]" data-page-size="100">
                             <thead>
		                        <tr>
		                            <th data-field="company_number" data-sortable="true">Company Number</th>
		                            <th data-field="company_name" data-sortable="true">Company Name</th>
									<th data-field="primary_contact" data-sortable="true">Primary Contact</th>
		                            <th data-field="city" data-sortable="true">City</th>
		                            <th data-field="state_id" data-sortable="true">State</th>		                            
		                            <th data-field="subscription_name" data-sortable="true"># Subscription Level</th>
		                            <th data-field="status" data-sortable="true">Status</th>                            
		                            <th data-field="actions">Actions</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    </tbody>
		                	</table>		                
						</div>
					</div>   	
				  </div>
				</div>          
            </div>
        </div>
    </div>

@endsection
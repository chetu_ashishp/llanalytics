@extends('app')
@section('script') 
 <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.mask.js') }}"></script>   
   <script src="{{ asset('/js/templates/edittemplate.js') }}"></script>
   <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script> <script type="text/javascript">
    //<![CDATA[
            bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
      //]]>
   </script>
@stop
@section('content')
<?php //echo '<pre/>'; print_r($emailTemplateItem); die;
    
    $email_template_id = isset($emailTemplateItem[0]->id) ? $emailTemplateItem[0]->id : '';
	$email_template_name = isset($emailTemplateItem[0]->email_template_name) ? $emailTemplateItem[0]->email_template_name : '';    
	$email_template_subject  = isset($emailTemplateItem[0]->email_template_subject) ? $emailTemplateItem[0]->email_template_subject : '';    
	$email_template_content = isset($emailTemplateItem[0]->email_template_content) ? $emailTemplateItem[0]->email_template_content : '';    
	$status = isset($emailTemplateItem[0]->status) ? $emailTemplateItem[0]->status : '';  
?>	
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
			  <div class="panel-heading">		  	
					<div class="container">
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-notes"></i> {{$form}} Email Template </h4>
                            <a href="{!! isset($data->item->id) ? route('emailtemplates', array('emailtemplates' => $data->item->id)) : route('emailtemplates') !!}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>
						</div>
					</div>
			  </div>
		  
		  <div class="panel-body">
			<div class="container datalist">		    	
				<div class="jumbotron container">
			
                    {!! Form::open( array('role' => 'form', 'method' => 'post', 'files'=>true, 'class'=>'form-horizontal',  'autocomplete'=>"off", 'url' => array('emailtemplate/update/'.$id))) !!}
					{!!Form::hidden('id', $id)!!}
                    <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">
					    	 	<a href="{!! isset($data->item->id) ? route('emailtemplates', array('emailtemplates' => $data->item->id)) : route('emailtemplates') !!}" class="btn btn-default  btn-sm  pull-right left10" role="button">Back</a>  	
								{!! Form::submit('Save', array('class'=>'btn  btn-info  btn-sm  pull-right '))!!}								
					    	</div>
						</div>
		           </div>
		           			
		           			
		           			
					<div class="row">					
						<div class="col-sm-12">				                  	                  	
			               		            	
			            	<div class="form-group">
			                        {!!Form::label('email_template_name','Email Template Name', array('class'=>'col-sm-3 control-label required'))!!}
			                        <div class="col-sm-5">
			                    	{!!Form::text('email_template_name', $email_template_name, array('class'=>'form-control required ','readonly' => 'true', 'required'))!!}
			                	</div>
                                    
			            	</div>
                           
                           
                            <div class="form-group">
			                        {!!Form::label('email_template_subject','Email Template Subject', array('class'=>'col-sm-3 control-label required'))!!}
			                        <div class="col-sm-5">
			                    	{!!Form::text('email_template_subject', $email_template_subject, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
			            	
                            <div class="form-group">
			                        {!!Form::label('email_template_name','Email Template Body', array('class'=>'col-sm-3 control-label required'))!!}
			                        <div class="col-sm-7 white-bg">
                                        <?php $text = "(( name ))"; ?>
			                    	 <textarea name="email_template_content" class="required" style="width: 100%; background: #fff;">{!!$email_template_content !!}</textarea>
			                	</div>
			            	</div>
                             <div class="form-group">
                                 {!!Form::label('','', array('class'=>'col-sm-3 control-label'))!!}
			                    <div class="col-sm-5">
			                    	<?php echo 'Note:<br/>                                      
                                        ((companyname)) = Company Name <br/>
                                        ((companyemail)) = Company Email ID<br/>
                                        ((companyusername)) = Company username<br/>
                                        ((companypassword)) = Company Password<br/>
                                        ((companyprimaryusername)) = Company First Name Last Name<br/>
                                        ((omscompanyemail)) = OMS Contact Email ID<br/>
                                        ((omscompanycontact)) = OMS Contact Number<br/>
                                        ((omsinternalstaffname)) = OMS Internal Staff Firstname Last Name<br/>
                                        ((omsinternalstaffemail)) = OMS Internal Staff Email ID / User Name<br/>
                                        ((omsinternalstaffpassword)) = OMS Internal Staff Password <br/>
                                        ((omsinternalstaffpassword)) = OMS Primary Staff Password<br/>
                                        ((companystaffname)) = Company Secondary Staff Creation<br/>
                                        ((companystaffusername)) = Company Secondary Staff Username/Email<br/>
                                        ((companystaffpassword)) = Company Secondary Staff Password Generated / Created<br/>';
                                    ?>
			                	</div>
                                    
			            	</div>
                          <div class="form-group">
				                {!!Form::label('status', 'Status', array('class'=>'col-sm-3 control-label required'))!!}
				                <div class="col-sm-2">
				                 	{!!Form::select('status',  array("Active"=>"Active", "Inactive"=>"Inactive") ,$status, array('class'=>'form-control', 'required'))!!}
				               	</div>
				            </div>
							  		            	           		
			           	</div>
		           	</div>
		           			
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	   
					    	 	<a href="{!! isset($data->item->id) ? route('emailtemplates', array('emailtemplates' => $data->item->id)) : route('emailtemplates') !!}" class="btn btn-default  btn-sm  pull-right left10" role="button">Back</a>  	
								{!! Form::submit('Save', array('class'=>'btn  btn-info  btn-sm  pull-right '))!!}								
					    	</div>
						</div>
		           </div>
		           				 
					{!! Form::close() !!}	
				</div>   		
			</div>    	
		  </div>
		
      		</div>
		</div>
    </div>
</div>

@stop
@extends('app')
@section('style')
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
	<link href="{{ asset('/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}" rel="stylesheet">
@stop

@section('script')
<script>var tableData = {!! $tableData !!};</script>


<!-- Page-Level Plugin Scripts -->
   <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('/js/typeahead.js-master/dist/typeahead.jquery.js') }}"></script>
   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>   
    <script src="{{ asset('/js/jquery.mask.js') }}"></script>
  
   
 <script src="{{ asset('/js/clients/jquery.formatCurrency-1.4.0.min.js') }}"></script>
 <script src="{{ asset('/js/company/staff.js') }}"></script>
 <script src="{{ asset('/js/templates/edittemplate.js') }}"></script>
 <script src="{{ asset('/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
 <script src="{{ asset('/vendor/925491/jquery.ui.autocomplete.html.js') }}"></script>
 
@endsection

@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i> Email Template </h4>				  		  			 
				  							  		
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">
						<div id="list">
							 <table class="data-table break-all-td" data-toggle="table" data-pagination="true" data-search="true" data-sort-name="clname" data-sort-order="asc" >
			                    <thead>
			                        <tr>
			                            <th data-field="email_template_name" data-sortable="true">Template Name</th>
			                            <th data-field="email_template_subject" class="col-xs-6" data-sortable="true">Template Subject</th>
			                            <th data-field="status" data-sortable="true">Status</th>	                                                       
			                            <th data-field="actions">Actions</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
						</div>
					</div>   	
				  </div>
			</div>
            <div class="panel panel-info"  id="newtransaction-div" style="display:none">				
			 	
      		</div> 
             <div class="panel panel-info"  id="editemail-template-div" style="display:none"></div>
            </div>

        </div>
    </div>
    <input type="hidden" id="data-ajax" value="templates">
@endsection
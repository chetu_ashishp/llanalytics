@extends('app')

@section('script')
<script>var tableData = {!! $tableData !!};</script>
<script src="{{ asset('/js/dataoptions/dataoptions.js') }}"></script>
@endsection

@section('content')

<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-wrench"></i> Company Tools </h4>
				  			<button type="button" class="btn btn-info  btn-sm pull-right addcompany_tool_option" data-toggle="modal" data-target=".modal-add" data-path="companytools" data-route="addoption" data-_token="{!! csrf_token() !!}"><i class="fa fa-plus"></i> Add Option</button>
							<div class="col-lg-2 pull-right">						
								 {!! Form::select('filter_data_type', array('' => '') + $data_types, '', array('class' => 'form-control input-sm' , 'data-path'=>'Companytools',  'data-route'=>'show', 'id'=>'filter_data_type', 'data-_token'=>csrf_token())) !!}
							</div>								  		
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">
				    
						<div id="list">
						  	  <table class="data-table" data-toggle="table" data-pagination="true" data-search="true" data-sort-name="name" data-sort-order="asc" >
			                    <thead>
			                        <tr>
			                            <th data-field="name" data-sortable="true">Name</th>
			                            <th data-field="type" data-sortable="true">Type</th>
			                            <th data-field="desc" data-sortable="true">Description</th>
			                            <th data-field="status" data-sortable="true">Status</th>
			                            <th data-field="actions">Actions</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
						</div>
					</div>   	
				  </div>
		</div>
      </div>
   </div>
</div>
    
    

<div class="modal modal-add">{!! $create !!}</div>

<div class="modal modal-edit"></div>

<input type="hidden" id="data-ajax" value="Companytools">
@endsection

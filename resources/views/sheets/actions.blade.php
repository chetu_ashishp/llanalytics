<a href="{!!route('sheet.details.entry', array('template' => $sheet->id)) !!}" class="btn btn-default btn-xs sheetaccess" role='button' data-path="sheet" data-route="sheetaccess"  data-_token="{{ csrf_token() }}">Access</a>
<a href="{!!route('sheet.details.edit', array('template' => $sheet->id)) !!}" class="btn btn-default btn-xs sheeteditdetails" role='button' data-path="sheet" data-route="sheeteditdetails"  data-_token="{{ csrf_token() }}">Edit Sheet Details</a>


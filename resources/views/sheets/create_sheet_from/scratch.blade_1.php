{!! Form::open( array('role' => 'form', 'class'=>'form-add', 'data-path' => 'CompanySheet', 'data-route' => !empty($data->mode) && $data->mode == 'Edit' ? 'update' : 'store', 'data-_token' => csrf_token())) !!}
        {!!Form::input('hidden', 'sheet_type', 'scratch')!!}
        {!!Form::input('hidden', 'id', !empty($data->sheet->id) ? $data->sheet->id : null )!!}
        
        <div class="form-group">
            {!!Form::label('name','Sheet Name', array('class'=>'control-label required'))!!}
            {!!Form::text('name', !empty($data->sheet->name) ? $data->sheet->name : null, array('class'=>'form-control required', 'required'))!!}
        </div>
			            	
        <div class="form-group">
            {!!Form::label('purpose','Sheet Purpose ', array('class'=>'control-label required'))!!}
            {!!Form::textarea('purpose', !empty($data->sheet->purpose) ? $data->sheet->purpose : null, array('class'=>'form-control required', 'required'))!!}
        </div>

        @if(count($data->client_fields) > 0)
        <div class="form-group">
            {!!Form::label('inc_first_name','Client Fields to Include', array('class'=>'control-label'))!!}
            @foreach($data->client_fields as $cKey => $cVal)
                <div class="checkbox">
                    <label>
                        {!!Form::checkbox('transaction_fields[]', $cKey, !empty($data->sheet_fields) && in_array($cKey, $data->sheet_fields) ? true : false,array('class' => 'oneclient', 'required' => 'required'))!!} {!! $cVal !!}
                    </label>
                </div>  
            @endforeach
        </div>
        @endif
        
        @if(count($data->transaction_fields) > 0)
        <div class="form-group">
            {!!Form::label('mls1','Transaction Fields to Include', array('class'=>'control-label'))!!}
            @foreach($data->transaction_fields as $cKey => $cVal)
                <div class="checkbox">
                    <label>
                        {!!Form::checkbox('transaction_fields[]', $cKey, !empty($data->sheet_fields) && in_array($cKey, $data->sheet_fields) ? true : false,array('class' => 'atleastone'))!!} {!! $cVal !!}
                    </label>
                </div>
            @endforeach
        </div>
        @endif
				
        <div class="form-group">
            {!!Form::label('rows_to_freeze','# Rows to Freeze ', array('class'=>'control-label required'))!!}
            {!!Form::select('rows_to_freeze', range(0, 12), !empty($data->sheet->rows_to_freeze) ? $data->sheet->rows_to_freeze : 0, array('class'=>'form-control required', 'required' => 'required'))!!}
        </div>
							  	
        <div class="form-group">
            <div class="checkbox">
                <label>
                    {!!Form::checkbox('freeze_first_column', !empty($data->sheet->freeze_first_column) ? $data->sheet->freeze_first_column : 1, false, array('class' => ''))!!} Freeze First Column							          					          								          	
                </label>
            </div>
        </div>

<div class="form-group">
    {!!Form::label('first_column_width','Default First Column Width', array('class'=>'control-label required'))!!}
    <div class="input-group">
        {!!Form::input('number', 'first_column_width', !empty($data->sheet->first_column_width) ? $data->sheet->first_column_width : 320, array('class'=>'form-control required', 'step' => '1', 'required' => 'required'))!!}
        <span class="input-group-addon">px</span>
    </div>
</div>

        <div class="form-group">
            {!!Form::label('column_width','Default Column Width', array('class'=>'control-label required'))!!}
            <div class="input-group">
                {!!Form::input('number', 'column_width', !empty($data->sheet->column_width) ? $data->sheet->column_width : 125, array('class'=>'form-control required', 'step' => '1', 'required' => 'required'))!!}
                <span class="input-group-addon">px</span>
            </div>
        </div>
																	  							
        <div class="form-group">
            {!!Form::label('all_user_accsess','Users Who Can Access Sheet', array('class'=>'control-label'))!!}
            <div class="checkbox">
                <label>
                    {!!Form::checkbox('all_user_accsess', 1, !empty($data->sheet->all_user_accsess) ? $data->sheet->all_user_accsess :  0, array('class' => 'forAllUsers users-checkboxes'))!!} All 
                </label>
            </div>
            @if(count($data->active_users) > 0)
                @foreach($data->active_users as $user)
                <div class="checkbox">
                    <label>
                        {!!Form::checkbox('users[]', $user->id, !empty($data->sheet_access) && in_array($user->id, $data->sheet_access) ? true : false, array('class' => 'users-chk users-checkboxes',  ($data->sheet->all_user_accsess) ? 'disabled' :"" ))!!} {{$user->name}} 
                    </label>
                </div>
                @endforeach  
            @endif
        </div>
        
        <div class="form-group">
            {!!Form::label('all_transaction_type','Transaction Types to include in Sheet', array('class'=>'control-label'))!!}
            <div class="checkbox">
                <label>
                    {!!Form::checkbox('all_transaction_type', 1, !empty($data->sheet->all_transaction_type) ? $data->sheet->all_transaction_type :  0, array('class' => 'forAllTransTypes'))!!} All 
                </label>
            </div>
            @if(count($data->transaction_types) > 0)
                @foreach($data->transaction_types as $tKey => $tVal)
                <div class="checkbox">
                    <label>
                        {!!Form::checkbox('transaction_types[]', $tKey, !empty($data->sheet_types) && in_array($tKey, $data->sheet_types) ? true : false, array('class' => 'transType-chk',  ($data->sheet->all_transaction_type) ? 'disabled' :"" ))!!} {{$tVal}} 
                    </label>
                </div>
                @endforeach  
            @endif
        </div>
        
        <div class="form-group">
            {!!Form::label('all_transaction_status','Transaction Status to include in Sheet', array('class'=>'control-label'))!!}
            <div class="checkbox">
                <label>
                    {!!Form::checkbox('all_transaction_status', 1, !empty($data->sheet->all_transaction_status) ? $data->sheet->all_transaction_status :  0, array('class' => 'forAllTransStatus'))!!} All 
                </label>
            </div>
            @if(count($data->transaction_statuses) > 0)
                @foreach($data->transaction_statuses as $tKey => $tVal)
                <div class="checkbox">
                    <label>
                        {!!Form::checkbox('transaction_status[]', $tKey, !empty($data->sheet_statuses) && in_array($tKey, $data->sheet_statuses) ? true : false, array('class' => 'transStatus-chk',  ($data->sheet->all_transaction_status) ? 'disabled' :""))!!} {{$tVal}} 
                    </label>
                </div>
                @endforeach  
            @endif
        </div>

        @if(count($data->column_order) > 0)
        <div class="form-group">
            {!!Form::label('column_order','Column Order', array('class'=>'control-label required'))!!}
            {!!Form::select('column_order', $data->column_order, !empty($data->sheet->column_order) ? $data->sheet->column_order : 0, array('class'=>'form-control required', 'required'))!!}
        </div>	
        @endif

        @if(count($data->status) > 0)
        <div class="form-group">
            {!!Form::label('status', 'Status', array('class'=>'control-label required'))!!}
            {!!Form::select('status',  $data->status, !empty($data->sheet->status) ? $data->sheet->status : "Active", array('class'=>'form-control', 'required'))!!}
        </div>
        @endif

    <div class="form-group">   	
        {!! Form::submit(!empty($data->mode) && $data->mode == 'Edit' ? 'Update Sheet' : 'Save Sheet', array('class'=>'btn  btn-primary pull-right'))!!}
    </div>
		           				 
{!! Form::close() !!}	
@extends('app')

@section('style')
    <link href="{{ asset('/css/sheets/sheet.show.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/spectrum.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">--}}
@stop

@section('script')
    <script>
        var tableData = {!! !empty($data->tableData) ? $data->tableData : "[]" !!};
        var escrow_officers = {!! !empty($data->escrowOfficers) ? json_encode($data->escrowOfficers) : "[]" !!};
        var loan_officers = {!! !empty($data->loanOfficers) ? json_encode($data->loanOfficers) : "[]" !!};
        var subtractor = {!! $data->item->row_height != 0 ?$data->item->row_height : 0 !!};
        var remember_token = "{!! $data->auth->remember_token !!}";
    </script>
    <script src="{{ asset('/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/js/sheets/ExpandSelect_1.00.js') }}"></script>
    <script src="{{ asset('/js/jquery-cookie-master/src/jquery.cookie.js') }}"></script>
    <script src="{{ asset('/js/typeahead.js-master/dist/typeahead.jquery.js') }}"></script>
    <script src="{{ asset('/js/sheets/sheets.js') }}"></script>
    <script src="{{ asset('/js/sheets/sheet.show.js') }}"></script>
    <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
    {{--<script src="{{ asset('/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>--}}
    <script src="{{ asset('/js/spectrum.js') }}"></script>

    <script>
        $(function(){

            $( ".sheet-grid thead tr th, .sheet-grid tbody tr th, .sheet-grid tbody tr td" ).css( "width", function( index ) {
                //return {!! $data->item->column_width !!};
            });
            $( ".sheet-grid thead tr th, .sheet-grid tbody tr th, .sheet-grid tbody tr td").first().css( "width", function( index ) {
                //return {!! $data->item->first_column_width !!};
            });


        });

    </script>
@stop

@section('content')
    <?php
    $data->item->row_height = $data->item->row_height == 0 ? 0 : $data->item->row_height - 1;

            ?>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-primary">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left" style="padding-top: 7.5px;">Filters</h4>
                    <div class="btn-group pull-right">
                        <button class="btn btn-sm btn-default" type="button" data-toggle="collapse" data-target="#sheetFilters" aria-expanded="false" aria-controls="sheetFilters">
                            <i class="glyphicon glyphicon-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="panel-body collapse" id="sheetFilters">
                    <div class="btn-group pull-right" role="group" aria-label="...">
                        <button type="button" class="btn btn-default uncheckAll">Uncheck All</button>
                        <button type="button" class="btn btn-default checkAll">Check All</button>
                    </div>
                    <form class="hidden-xs" role="form">
                        @if(isset($data->transactions))
                            @foreach($data->transactions as $tKey => $tVal)
                            <label class='checkbox-inline'><input class="column-views" data-target="transaction-{!! $tVal->id !!}" type="checkbox" checked/>{!! $tVal->client->last_name !!}{!! empty($tVal->nickname)?'':' - '.$tVal->nickname !!}</label>
                            @endforeach
                        @endif
                    </form>
                    <form class="visible-xs">
                        @if(isset($data->transactions))
                        <select class="form-control column-views-select">
                            <option data-target=""></option>
                            @foreach($data->transactions as $tKey => $tVal)
                                    <option data-target="transaction-{!! $tVal->id !!}">
                                        {!! $tVal->client->last_name !!}{!! empty($tVal->nickname)?'':' - '.$tVal->nickname !!}
                                    </option>
                            @endforeach
                        </select>
                        @endif
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="{!!route('newclient') !!}" class="form-control btn btn-primary" role='button' >Add Client</a>
                </div>
            </div>
        </div>
        <div class="col-md-2">

                                  <!--<a href="{!!route('sheets') !!}" class="btn btn-default  btn-xs pull-right" role="button">Cancel</a>-->
                                    <div class="panel panel-default">
                              <div class="panel-body">
                                  <a href="{!!route('sheets') !!}" class="form-control btn btn-primary" role="button">Cancel</a>

                      </div>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          <div style="box-shadow:inherit;padding-left:20px;">
              <h3>{{$data->sheet->name}} - # of Records:&nbsp;&nbsp;<span id="columnViewsCount"></span></h3>
          </div>
        </div>
      </div>

</div>
<div class="col-md-12 mobileView hidden-xs"><div class="panel" style="box-shadow:inherit;"><div class="panel-body">

<div class="scrollContainer">
        <table id="header-fixed" class="sheet-grid resizable table-bordered table-condensed"></table>
        <table id="column-header-fixed" class="sheet-grid resizable table-bordered table-condensed"></table>
        <table id="column-fixed" class="sheet-grid resizable table-bordered table-condensed"></table>

        <table id="grid" class="sheet-grid resizable table-bordered table-condensed" data-sheet-id="{!! $data->sheet_id !!}">
                @if(isset($data->rows->client))
                        <colgroup>
                            <col data-transaction="" class="transaction-label hidden-xs {!! $data->item->freeze_first_column == 1 ? 'frozen-column' : 'hot-column' !!}" style="width:{!! $data->item->first_column_width !!}px;"/>
                            @foreach($data->transactions as $tKey => $tVal)
                                    <col data-transaction="{!! $tVal->id !!}" class="hot-column hidden-xs transaction-common transaction-{!! $tVal->id !!}" style="width:{!! $data->item->column_width !!}px;"/>
                            @endforeach
                        </colgroup>
                @endif
            <thead>
                @if(isset($data->rows->client))
                        <tr class="colHeaders" {!! $data->item->row_height!=0? 'style="height:'. $data->item->row_height .'px;"':'' !!}>
                            <th class="transaction-label hidden-xs ui-resizable {!! $data->item->freeze_first_column == 1 ? 'frozen-column' : 'hot-column' !!}" style="{!! !empty($data->item->row_height) ? 'height:' . $data->item->row_height . 'px;' : null !!}">
                                <span class="columnLabel">{{$data->sheet->name}}</span>
                                <div class="resizeHelper ui-resizable-handle ui-resizable-e">&nbsp;</div>
                            </th>
                            @foreach($data->transactions as $tKey => $tVal)
                                <th data-type="{!! $tVal->transaction_type !!}" class="hot-column ui-resizable transaction-label hidden-xs transaction-common transaction-{!! $tVal->id !!}" style="
                                {!! !empty($data->transaction_type_defaults[$tVal->transaction_type]['header_color']) ? 'background-color:'.$data->transaction_type_defaults[$tVal->transaction_type]['header_color'].' !important;' : null !!}
                                ">
                                    <span class="columnLabel">{!! $tVal->client->last_name !!}</span>
                                    <div class="resizeHelper ui-resizable-handle ui-resizable-e">&nbsp;</div>
                                </th>
                            @endforeach
                        </tr>
                @endif
            </thead>
            <tbody>
                <?php $rows_froze = 0; ?>
                @if(isset($data->rows->client))
                    @foreach($data->rows->client as $rKey => $rVal)
                        <tr class="header-row {!! $data->item->rows_to_freeze !!} {!! $rows_froze !!} {!! $data->item->rows_to_freeze >= $rows_froze ? 'deep-freeze-frosty-cold-brrr' : 'hot' !!}" {!! $data->item->row_height!=0? 'style="height:'. $data->item->row_height .'px;"':''!!}>
                            <?php $rows_froze += 1; ?>
                            <td class="transaction-label hidden-xs {!! $data->item->freeze_first_column == 1 ? 'frozen-column' : 'hot-column' !!}"><div class="text-label">{!! $rVal['name'] !!}</div></td>
                            @foreach($data->transactions as $tKey => $tVal)
                                <td class="hot-column header-cell hidden-xs transaction-common transaction-{!! $tVal->id !!}">
                                    <div class='input-group'>
                                        <input class="form-control" type="text" data-input-type='{!! $data->field_types[$rKey] !!}' data-contex="{!!route('client.edit', array('client' => $tVal->client->id)) !!}" data-contex-add="{!!route('clients.details', array('client' => $tVal->client->id)) !!}?transaction=add" data-item='{!! $tVal->client->id !!}' data-field='{!! $rKey !!}' data-cell-type='Client' data-editable="true" data-_token="{!! csrf_token() !!}" value="{!! $tVal->client->$rKey == '1970-01-01' ? null : $rKey=='full_name'? $tVal->client->last_name.', '.$tVal->client->first_name :$tVal->client->$rKey!!}">
                                        <span class="input-group-addon">
                                            <span class="savingspinner hidden"></span>
                                        </span>
                                    </div>
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                @endif

                @if(isset($data->rows->transaction))
                    @foreach($data->rows->transaction as $rKey => $rVal)
                        <tr class="header-row {!! $data->item->rows_to_freeze !!} {!! $rows_froze !!} {!! $data->item->rows_to_freeze >= $rows_froze ? 'deep-freeze-frosty-cold-brrr"' : 'hot"' !!} {!! $data->item->row_height != 0 ? 'style="height:'. $data->item->row_height .'px;"':''!!}>
                            <?php $rows_froze += 1; ?>
                            <td class="transaction-label hidden-xs {!! $data->item->freeze_first_column == 1 ? 'frozen-column' : 'hot-column' !!}"><div class="text-label">{!! $rVal['name'] !!}</div></td>
                            @foreach($data->transactions as $tKey => $tVal)
                                <td class="header-cell hot-column transaction-mobile-hidden transaction-{!! $tVal->id !!}">
                                    <!-- {!! $data->field_types[$rKey] !!} -->
                                    <div class='input-group'>
                                        @if($rKey == 'sales_price' || $rKey == 'current_list_price')
                                            <input type='text' pattern="[0-9]*" data-contex="{!!route('clients.details', array('client' => $tVal->client->id)) !!}?transaction=edit&transaction_id={!! $tVal->id !!}" data-input-type='{!! $data->field_types[$rKey] !!}' data-item='{!! $tVal->id !!}' data-field='{!! $rKey !!}' data-cell-type='Transaction' data-editable="true" data-_token="{!! csrf_token() !!}" value="{!! number_format(round($tVal->$rKey)) !!}" step='1' class='form-control {!! $data->field_types[$rKey] !!}' {!! $data->field_types[$rKey] !!}>
                                        @elseif (in_array($data->field_types[$rKey], array('text', 'date', 'number')))
                                            <input type='{!! $data->field_types[$rKey] == "date" ? "text" : $data->field_types[$rKey] !!}' data-contex="{!!route('clients.details', array('client' => $tVal->client->id)) !!}?transaction=edit&transaction_id={!! $tVal->id !!}" data-input-type='{!! $data->field_types[$rKey] !!}' data-item='{!! $tVal->id !!}' data-field='{!! $rKey !!}' data-cell-type='Transaction' data-editable="true" data-_token="{!! csrf_token() !!}" value="{!! $data->field_types[$rKey] == 'date' && date('m/d/y', strtotime($tVal->$rKey)) != '01/01/70' && date('m/d/y', strtotime($tVal->$rKey)) != '00/00/00' ? date('m/d/y', strtotime($tVal->$rKey)) : ($tVal->$rKey == '1970-01-01' ? null : $tVal->$rKey) !!}" step='.01' class='form-control {!! $data->field_types[$rKey] == "date" ? "datepicker" : $data->field_types[$rKey] !!}' {!! $data->field_types[$rKey] == "date" ? "id='".$tVal->id.$rKey."'" : $data->field_types[$rKey] !!}>
                                        @else
                                            @if($rKey == 'lender')
                                                {!!Form::select($rKey, companyOptionsByType('Lender', 1), $tVal->$rKey, array('class' => 'form-control', 'data-input-type' => $data->field_types[$rKey], 'data-item' => $tVal->id, 'data-field' => $rKey, 'data-cell-type' => 'Transaction', 'data-editable' => 'true', 'data-_token' => csrf_token(),'data-contex'=>route('clients.details', array('client' => $tVal->client->id)) .'?transaction=edit&transaction_id='.$tVal->id))!!}
                                            @elseif($rKey == 'zip')
                                                {!!Form::select($rKey, companyOptionsByType('Zip', 1), $tVal->$rKey, array('class' => 'form-control', 'data-input-type' => $data->field_types[$rKey], 'data-item' => $tVal->id, 'data-field' => $rKey, 'data-cell-type' => 'Transaction', 'data-editable' => 'true', 'data-_token' => csrf_token(),'data-contex'=>route('clients.details', array('client' => $tVal->client->id)) .'?transaction=edit&transaction_id='.$tVal->id))!!}
                                            @elseif($rKey == 'city_id')
                                                {!!Form::select($rKey, companyOptionsByType('City', 1), $tVal->$rKey, array('class' => 'form-control', 'data-input-type' => $data->field_types[$rKey], 'data-item' => $tVal->id, 'data-field' => $rKey, 'data-cell-type' => 'Transaction', 'data-editable' => 'true', 'data-_token' => csrf_token(),'data-contex'=>route('clients.details', array('client' => $tVal->client->id)) .'?transaction=edit&transaction_id='.$tVal->id))!!}
                                            @elseif(isset($data->options->$rKey) && is_array($data->options->$rKey) && !empty($data->options->$rKey))
                                                {!!Form::select($rKey, $data->options->$rKey, $tVal->$rKey, array('class' => 'form-control', 'data-input-type' => $data->field_types[$rKey], 'data-item' => $tVal->id, 'data-field' => $rKey, 'data-cell-type' => 'Transaction', 'data-editable' => 'true', 'data-_token' => csrf_token(),'data-contex'=>route('clients.details', array('client' => $tVal->client->id)) .'?transaction=edit&transaction_id='.$tVal->id))!!}
                                            @else
                                                {!! link_to_route('companytools', 'Add', $parameters = array(), $attributes = array('data-editable' => 'true')) !!}
                                            @endif
                                        @endif
                                        <span class="input-group-addon">
                                            <span class="savingspinner hidden"></span>
                                        </span>
                                    </div>
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                @endif

                @if(isset($data->rows->custom))
                    @foreach($data->rows->custom as $rKey => $rVal)

                        <tr class="hot" data-input="{!! $rVal->status == 'Active' && !in_array($rVal->type, array('Date','Checkbox','Single Select','Alphanumeric')) ? 'true' : 'false' !!}" {!!$data->item->row_height!=0? 'style="height:'. $data->item->row_height .'px;"':''!!}>
                            <td data-toggle="tooltip" class="transaction-label hidden-xs {!! $data->item->freeze_first_column == 1 ? 'frozen-column' : 'hot-column' !!}" title="{!! $rVal->tooltip !!}"><div class="text-label {!! $rVal->parent === true ? 'parentrow' : 'childrow' !!}" >{!! $rVal->field_name !!} {!! !empty($rVal->tooltip) ? '<span class="pull-right tooltip-info glyphicon glyphicon-info-sign" style="color:#6699CC"></span>' : null !!}</div></td>

                                @foreach($data->transactions as $tKey => $tVal)
                                    <?php


                                    //$background_color = isset($tVal->custom[$rVal->id]) && !empty($tVal->custom[$rVal->id]['color']) ? $tVal->custom[$rVal->id]['color'] : (isset($tVal->custom[$rVal->id]) && isset($rVal->colors[$tVal->custom[$rVal->id]['field_value']]) ? $rVal->colors[$tVal->custom[$rVal->id]['field_value']] : (!empty($data->sheet_field_defaults[$tVal->transaction_type][$rVal->id]['bg_color']) ? $data->sheet_field_defaults[$tVal->transaction_type][$rVal->id]['bg_color'] : (!empty($data->transaction_type_defaults[$tVal->transaction_type]['bg_color']) ? $data->transaction_type_defaults[$tVal->transaction_type]['bg_color'] : $rVal->default_bg_color)));

                                    if(isset($tVal->custom[$rVal->id]) && isset($rVal->colors[$tVal->custom[$rVal->id]['field_value']])){
                                        //Custom by Value Set in Edit Sheet
                                        $background_color = $rVal->colors[$tVal->custom[$rVal->id]['field_value']];
                                    } elseif(isset($tVal->custom[$rVal->id]) && !empty($tVal->custom[$rVal->id]['color'])) {
                                        //Custom - Set in Sheet
                                        $background_color = $tVal->custom[$rVal->id]['color'];
                                    } elseif(!empty($data->sheet_field_defaults[$tVal->transaction_type][$rVal->id]['bg_color'])) {
                                        //Field Default Set in Edit Sheet/Field
                                        $background_color = $data->sheet_field_defaults[$tVal->transaction_type][$rVal->id]['bg_color'];
                                    } elseif(!empty($data->transaction_type_defaults[$tVal->transaction_type]['bg_color'])) {
                                        //Transaction Default Set in Edit Sheet/Transaction
                                        $background_color = $data->transaction_type_defaults[$tVal->transaction_type]['bg_color'];
                                    } else {
                                        //Default
                                        $background_color = $rVal->default_bg_color;
                                    }

                                    //$background_color = isset($tVal->custom[$rVal->id]) && !empty($tVal->custom[$rVal->id]['color']) ? $tVal->custom[$rVal->id]['color'] : (isset($tVal->custom[$rVal->id]) && isset($rVal->colors[$tVal->custom[$rVal->id]['field_value']]) ? $rVal->colors[$tVal->custom[$rVal->id]['field_value']] : (!empty($data->transaction_type_defaults[$tVal->transaction_type]['bg_color']) ? $data->transaction_type_defaults[$tVal->transaction_type]['bg_color'] : $rVal->default_bg_color));
                                    ?>
                                <td class='hot-column transaction-mobile-hidden transaction-{!! $tVal->id !!}' style="{!! $rVal->type != 'Text Label-No Input' && !empty($background_color) ? 'background-color:'.$background_color.';' : null !!}">
                                    <div class='input-group' >
                                        @if($rVal->status != 'Active')
                                            <input class='form-control' value='' data-editable='true' readonly>
                                        @elseif($rVal->type == 'Date')
                                            {!!Form::input(
                                                'text',
                                                '',
                                                isset($tVal->custom[$rVal->id]) && !empty($tVal->custom[$rVal->id]['field_value']) ? date('m/d/y', strtotime($tVal->custom[$rVal->id]['field_value'])) : null,
                                                array(
                                                    'data-contex-menu'          => isset($rVal->contex_menu) ? $rVal->contex_menu : "false",
                                                    'data-contex-menu-items'    => isset($rVal->contex_menu_items) ? htmlentities($rVal->contex_menu_items) : "{}",
                                                    'data-input-type'   => $rVal->type,
                                                    'data-item'         => isset($tVal->custom[$rVal->id]) ? $tVal->custom[$rVal->id]['id'] : '',
                                                    'data-comment'      => isset($tVal->custom[$rVal->id]) ? $tVal->custom[$rVal->id]['comment'] : '',
                                                    'data-trans-id'     => $tVal->id,
                                                    'data-field-id'     => $rVal->id,
                                                    'data-editable'     => 'true',
                                                    'data-cell-type'     => 'Custom',
                                                    'data-_token'       => csrf_token(),
                                                    'style'             => 'background-color: '.$background_color.';',
                                                    'data-colors'       => htmlentities(json_encode($rVal->colors)),
                                                    'data-default-color'=> $rVal->default_bg_color,
                                                    'class'             => implode(' ', array('form-control', 'datepicker', isset($tVal->custom[$rVal->id]) && !empty($tVal->custom[$rVal->id]['comment']) ? 'comment' : null)),
                                                )
                                            )!!}
                                        @elseif($rVal->type == 'Checkbox')
                                            {!!Form::checkbox(
                                                '',
                                                1,
                                                isset($tVal->custom[$rVal->id]) ? $tVal->custom[$rVal->id]['field_value'] : null,
                                                array(
                                                    'data-contex-menu'          => isset($rVal->contex_menu) ? $rVal->contex_menu : "false",
                                                    'data-contex-menu-items'    => isset($rVal->contex_menu_items) ? htmlentities($rVal->contex_menu_items) : "{}",
                                                    'data-input-type'   => $rVal->type,
                                                    'data-item'         => isset($tVal->custom[$rVal->id]) ? $tVal->custom[$rVal->id]['id'] : '',
                                                    'data-comment'      => isset($tVal->custom[$rVal->id]) ? $tVal->custom[$rVal->id]['comment'] : '',
                                                    'data-trans-id'     => $tVal->id,
                                                    'data-field-id'     => $rVal->id,
                                                    'data-editable'     => 'true',
                                                    'data-cell-type'    => 'Custom',
                                                    'data-_token'       => csrf_token(),
                                                    'style'             => 'background-color: '.$background_color.';',
                                                    'data-colors'       => htmlentities(json_encode($rVal->colors)),
                                                    'data-default-color'=> $rVal->default_bg_color,
                                                    'class'             => implode(' ', array('form-control', isset($tVal->custom[$rVal->id]) && !empty($tVal->custom[$rVal->id]['comment']) ? 'comment' : null)),
                                                )
                                            )!!}
                                        @elseif($rVal->type == 'Single Select')
                                            <?php

                                            if(isset($tVal->custom[$rVal->id])) {
                                                $select_value = $tVal->custom[$rVal->id]['field_value'];
                                            } elseif(!empty($data->sheet_field_defaults[$tVal->transaction_type][$rVal->id]['value'])) {
                                                $select_value = $data->sheet_field_defaults[$tVal->transaction_type][$rVal->id]['value'];
                                            } elseif(
                                                    !empty($data->transaction_type_defaults[$tVal->transaction_type]['value']) &&
                                                    array_key_exists($data->transaction_type_defaults[$tVal->transaction_type]['value'],
                                                            array('' => '') +
                                                            array_combine(
                                                                    preg_replace('/\s+/','',explode("\n", $rVal->select_options)),
                                                                    preg_replace('/\s+/','',explode("\n", $rVal->select_options))
                                                            )
                                                            )
                                                    )
                                            {
                                                    $select_value = $data->transaction_type_defaults[$tVal->transaction_type]['value'];
                                            } else {
                                                $select_value = null;
                                            }
                                            ?>
                                            {!!Form::select(
                                                '',
                                                array('' => '') + array_combine(preg_replace('/\s+/','',explode("\n", $rVal->select_options)),preg_replace('/\s+/','',explode("\n", $rVal->select_options))),
                                                $select_value,
                                                array(
                                                    'data-contex-menu'          => isset($rVal->contex_menu) ? $rVal->contex_menu : "false",
                                                    'data-contex-menu-items'    => isset($rVal->contex_menu_items) ? htmlentities($rVal->contex_menu_items) : "{}",
                                                    'data-input-type'   => $rVal->type,
                                                    'data-item'         => isset($tVal->custom[$rVal->id]) ? $tVal->custom[$rVal->id]['id'] : '',
                                                    'data-comment'      => isset($tVal->custom[$rVal->id]) ? $tVal->custom[$rVal->id]['comment'] : '',
                                                    'data-trans-id'     => $tVal->id,
                                                    'data-field-id'     => $rVal->id,
                                                    'data-editable'     => 'true',
                                                    'data-cell-type'    => 'Custom',
                                                    'data-_token'       => csrf_token(),
                                                    'style'             => 'background-color: '.$background_color.';',
                                                    'data-colors'       => htmlentities(json_encode($rVal->colors)),
                                                    'data-default-color'=> $rVal->default_bg_color,
                                                    'class'             => implode(' ', array('form-control', isset($tVal->custom[$rVal->id]) && !empty($tVal->custom[$rVal->id]['comment']) ? 'comment' : null)),
                                                )
                                            )!!}
                                        @elseif($rVal->type == 'Alphanumeric')
                                            <?php

                                            if(isset($tVal->custom[$rVal->id])) {
                                                $alpha_value = $tVal->custom[$rVal->id]['field_value'];
                                            } elseif(!empty($data->sheet_field_defaults[$tVal->transaction_type][$rVal->id]['value'])) {
                                                $alpha_value = $data->sheet_field_defaults[$tVal->transaction_type][$rVal->id]['value'];
                                            } elseif(!empty($data->transaction_type_defaults[$tVal->transaction_type]['value'])) {
                                                $alpha_value = $data->transaction_type_defaults[$tVal->transaction_type]['value'];
                                            } else {
                                                $alpha_value = null;
                                            }
                                            ?>
                                            {!!Form::text(
                                                '',
                                                $alpha_value,
                                                array(
                                                    'data-contex-menu'          => isset($rVal->contex_menu) ? $rVal->contex_menu : "false",
                                                    'data-contex-menu-items'    => isset($rVal->contex_menu_items) ? htmlentities($rVal->contex_menu_items) : "{}",
                                                    'data-input-type'   => $rVal->type,
                                                    'data-item'         => isset($tVal->custom[$rVal->id]) ? $tVal->custom[$rVal->id]['id'] : '',
                                                    'data-comment'      => isset($tVal->custom[$rVal->id]) ? $tVal->custom[$rVal->id]['comment'] : '',
                                                    'data-trans-id'     => $tVal->id,
                                                    'data-field-id'     => $rVal->id,
                                                    'data-editable'     => 'true',
                                                    'data-cell-type'    => 'Custom',
                                                    'data-_token'       => csrf_token(),
                                                    'style'             => 'background-color: '.$background_color.';',
                                                    'data-colors'       => htmlentities(json_encode($rVal->colors)),
                                                    'data-default-color'=> $rVal->default_bg_color,
                                                    'maxlength'         => 100,
                                                    'class'             => implode(' ', array('form-control')),
                                                )
                                            )!!}
                                        @elseif($rVal->type == 'Text Label-No Input')
                                                <input class='form-control label-no-input' value='{!! $rVal->field_name !!}' data-editable='true' readonly style='{!!'background-color: '.(isset($tVal->custom[$rVal->id]) && !empty($tVal->custom[$rVal->id]['color']) ? $tVal->custom[$rVal->id]['color'] : (isset($tVal->custom[$rVal->id]) && isset($rVal->colors[$tVal->custom[$rVal->id]['field_value']]) ? $rVal->colors[$tVal->custom[$rVal->id]['field_value']] : $rVal->default_bg_color)).';'!!}'>
                                        @endif
                                        @if($rVal->type != 'Text Label-No Input')
                                        <span class="input-group-addon">
                                            <a href="#" class="ajax {!! !isset($tVal->custom[$rVal->id]) || empty($tVal->custom[$rVal->id]['comment']) ? 'hidden' : null !!}" data-_token="{!! csrf_token() !!}" data-trans-id="{!! $tVal->id !!}" data-field-id="{!! $rVal->id !!}" data-target="#modal-container" data-path="CompanySheet" data-route="contex_action" data-action="View Comment"><span class="glyphicon glyphicon-comment"></span></a>
                                            <span class="savingspinner hidden"></span>
                                        </span>
                                        @endif
                                    </div>
                                </td>
                                @endforeach
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
</div>
        </div>
        </div>
        </div>
<div id="modal-container"></div>
@stop

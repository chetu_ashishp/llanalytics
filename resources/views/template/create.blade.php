@extends('app')

@section('style')
	
@stop

@section('script')   
   <script src="{{ asset('/js/templates/templates.js') }}"></script>
@stop


@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
			  <div class="panel-heading">		  	
					<div class="container">
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-notes"></i> {{$form}} template </h4>
                            <a href="{!! isset($data->item->id) ? route('sheet.details.edit', array('sheet' => $data->item->id)) : route('templates') !!}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>
						</div>
					</div>
			  </div>
		  
		  <div class="panel-body">
			<div class="container datalist">		    	
				<div class="jumbotron container">
							           
					{!! Form::open( array('role' => 'form', 'method' => 'post', 'files'=>true, 'class'=>'form-horizontal', 'id'=>'add-template',  'autocomplete'=>"off", 'url' => array('templates/store'))) !!}
					
					 			
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	   
					    	 	<a href="{!! isset($data->item->id) ? route('sheet.details.edit', array('sheet' => $data->item->id)) : route('templates') !!}" class="btn btn-default  btn-sm  pull-right left10" role="button">Back</a>  	
								{!! Form::submit('Save', array('class'=>'btn  btn-info  btn-sm  pull-right '))!!}								
					    	</div>
						</div>
		           </div>
		           			
		           			
		           			
					<div class="row">					
						<div class="col-sm-6">				                  	                  	
			               		            	
			            	<div class="form-group">
			                        {!!Form::label('name','Template Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('name', isset($data->item->name) ? $data->item->name : null, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('purpose','Template Purpose ', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::textarea('purpose', isset($data->item->purpose) ? $data->item->purpose : null, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>           	 
			            	
			            	<div class="form-group">
			                        {!!Form::label('inc_first_name','Client Fields to Include', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	
				                    	 <div class="checkbox">
									        <label>
									          	{!!Form::checkbox('transaction_fileds[]', 'first_name', isset($data->fields) && in_array('first_name', $data->fields) ? true : false, array('class' => 'atleastone'))!!} First Name
									        </label>
									     </div>  
									     <div class="checkbox"> 
									        <label>  	 
									          	{!!Form::checkbox('transaction_fileds[]', 'last_name', isset($data->fields) && in_array('last_name', $data->fields) ? true : false,array('class' => 'atleastone'))!!} Last Name
									        </label>
									     </div>
			                		</div>
			            	</div>
			            	
					
					 		  
					
							<div class="form-group">
							    {!!Form::label('mls1','Transaction Fields to Include', array('class'=>'col-sm-5 control-label'))!!}
							     <div class="col-sm-7"> 
							     
								      <div class="checkbox">
								        <label>
								          	{!!Form::checkbox('transaction_fileds[]', 'mls1', isset($data->fields) && in_array('mls1', $data->fields) ? true : false,array('class' => ' atleastone'))!!} MLS # - 1 
								       </label>
								      </div>
								       
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'mls2', isset($data->fields) && in_array('mls2', $data->fields) ? true : false, array('class' => ' atleastone'))!!} MLS # - 2
								        </label>
								      </div>
								      
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'contract_date', isset($data->fields) && in_array('contract_date', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Contract Date
								        </label>
								      </div>
									 <div class="checkbox">
										 <label>
											 {!!Form::checkbox('transaction_fileds[]', 'current_list_price' 	, isset($data->fields) && in_array('current_list_price', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Current List Price
										 </label>
									 </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'sales_price',isset($data->fields) && in_array('sales_price', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Sales Price
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'lender', isset($data->fields) && in_array('lender', $data->fields) ? true : false,array('class' => ' atleastone'))!!} Cash / Lender
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'title_company', isset($data->fields) && in_array('title_company', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Title Company
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'escrow_officer', isset($data->fields) && in_array('escrow_officer', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Escrow Officer
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'nickname', isset($data->fields) && in_array('nickname', $data->fields) ? true : false,array('class' => ' atleastone'))!!} Short Address
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'property_house' , isset($data->fields) && in_array('property_house', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Property House #
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'property_addr1', isset($data->fields) && in_array('property_addr1', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Property Address Line 1
								        </label>
								      </div>
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'city', isset($data->fields) && in_array('city', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Property City
								        </label>
								      </div>	
								      
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'state', isset($data->fields) && in_array('state', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Property State
								        </label>
								      </div>
								   
								      
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'subdivision' , isset($data->fields) && in_array('subdivision', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Property Sub-division
								        </label>
								      </div>	
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 	'listing_date' , isset($data->fields) && in_array('listing_date', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Listing Date
								        </label>
								      </div>	
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'listing_expiration_date' , isset($data->fields) && in_array('listing_expiration_date', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Listing Expiration Date
								        </label>
								      </div>	
								      <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'other_agent_name' , isset($data->fields) && in_array('other_agent_name', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Other Agent
								        </label>
								      </div>
									 <div class="checkbox">
										 <label>
											 {!!Form::checkbox('transaction_fileds[]', 'lockbox' 	, isset($data->fields) && in_array('lockbox', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Lockbox #
										 </label>
									 </div>
		 							  <div class="checkbox">
								        <label>     	
								          	{!!Form::checkbox('transaction_fileds[]', 'transaction_status', isset($data->fields) && in_array('transaction_status', $data->fields) ? true : false, array('class' => ' atleastone'))!!} Status
								        </label>
								      </div>
							    </div>
							</div>
				
							<div class="form-group">
			                    {!!Form::label('rows_to_freeze','# Rows to Freeze ', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::select('rows_to_freeze', range(0, 12), isset($data->item->rows_to_freeze) ? $data->item->rows_to_freeze : 0, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
							  	
							<div class="form-group">
							    <div class="col-sm-offset-5 col-sm-7">
							      <div class="checkbox">
							        <label>
							          	{!!Form::checkbox('freeze_first_column', 1, !empty($data->item->freeze_first_column) ? true : false, array('class' => ''))!!} Freeze First Column							          					          								          	
							        </label>
							      </div>
							    </div>
							</div> 	
																	  							
							<div class="form-group">
							    {!!Form::label('mls1','Users Who Can Access Sheet', array('class'=>'col-sm-5 control-label'))!!}
							     <div class="col-sm-7">
							     	  <div class="checkbox">
								        <label>
								          	{!!Form::checkbox('all_user_accsess', 1, !empty($data->item->all_user_accsess) ? true : 0, array('class' => 'forAllUsers'))!!} All 
								       </label>
								      </div>
							     	@foreach($activeusers as $user)
								      <div class="checkbox">
								        <label>
								          	{!!Form::checkbox('users[]', $user->id, isset($data->access) && in_array($user->id, $data->access) ? true : false, array('class' => 'users-chk'))!!} {{$user->name}} 
								       </label>
								      </div>
								    @endforeach  
								 </div>
							</div>	      
																		
							<div class="form-group">
			                    {!!Form::label('column_order','Column Order', array('class'=>'col-sm-5 control-label required'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::select('column_order', App\Template::$column_order, isset($data->item->column_order) ? $data->item->column_order : 0, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>	
																  	
							<div class="form-group">
				                {!!Form::label('status', 'Status', array('class'=>'col-sm-5 control-label required'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('status',  array("Active"=>"Active", "Inactive"=>"Inactive") ,isset($data->item->status) ? $data->item->status : "Active", array('class'=>'form-control', 'required'))!!}
				               	</div>
				            </div>
							  		            	           		
			           	</div>
		           	</div>
		           			
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	   
					    	 	<a href="{!! isset($data->item->id) ? route('sheet.details.edit', array('sheet' => $data->item->id)) : route('templates') !!}" class="btn btn-default  btn-sm  pull-right left10" role="button">Back</a>  	
								{!! Form::submit('Save', array('class'=>'btn  btn-info  btn-sm  pull-right '))!!}								
					    	</div>
						</div>
		           </div>
		           				 
					{!! Form::close() !!}	
				</div>   		
			</div>    	
		  </div>
		
      		</div>
		</div>
    </div>
</div>

@stop
@extends('app')

@section('style')
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

	<style type="text/css">
		.errors { background-color: #a94442; color: white; font-weight: bold; }
		.errors ul { list-style-type: none; padding: 5px; }
		
		.form-horizontal .control-label { padding-top: 0 }
		
		<?php if (!empty($boldrows)): ?>
		<?php
			$boldstyle = "";
			
			foreach ($boldrows as $bv)
			{
				$boldstyle .= "table.data-table tr:nth-child(".($bv+1).") td,";
			}
			
			echo substr($boldstyle,0,-1)."{ font-weight: bold }";
			
			foreach ($columns as $ck => $col)
			{
				if (!empty($col['align']))
				{
					echo "table.data-table th:nth-child(".($ck+1).") { text-align: {$col['align']} }";
					echo "table.data-table td:nth-child(".($ck+1).") { text-align: {$col['align']} }";
				}
			}
		?>
		<?php endif; ?>
		
		<?php if (!empty($bluerows)): ?>
		<?php
			$boldstyle = "";
			
			foreach ($bluerows as $bv)
			{
				$boldstyle .= "table.data-table tr[data-index='".($bv)."'] td,";
			}
			
			echo substr($boldstyle,0,-1)."{ background-color: #D1E4FC }";
		?>
		<?php endif; ?>		
	</style>	
@stop

@section('script')
<script>var tableData = {!! $tableData !!};</script>
<script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
   <script type="text/javascript">
		$(function () {
			$('.datepicker').datetimepicker({
				format: 'MM/DD/YYYY'
			});
		});	
   </script>
@stop


@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i> Sales Report </h4>
						</div>
				  	</div>
				</div>		
				  <div class="panel-body report-body">
				  
					<?php if (!empty($errs)): ?>
					<div class="errors">
						<ul>
							<?php foreach ($errs as $err): ?>
							<li><?php echo $err; ?></li>
							<?php endforeach; ?>
						</ul>
					</div>
					<?php endif; ?>
					
				    <div class="container datalist">		    	
				    	<div class="jumbotron container">				    		
				    	
					    	 {!!Form::open(array('url' => '/reports/sales', 'method' => 'post', 'data-path' => 'Sales Reports', 'class' => 'form-horizontal', 'data-_token'=>csrf_token()))!!}							 
									@foreach ($filters as $filtergroup)
				                  	<div class="col-sm-4">
										@foreach ($filtergroup as $filter)
				                       <div class="form-group">
											<?php 
												if (!empty($_POST[$filter['field']]))
													$filter['default'] = $_POST[$filter['field']];
												else if (!isset($filter['default']))
													$filter['default'] = "";
											?>
				                            {!!Form::label($filter['field'],$filter['label'], array('class'=>'col-sm-6 control-label'))!!}
				                            <div class="col-sm-6">
												<?php if ($filter['type'] == "radio"): ?>
													@foreach ($filter['options'] as $ok => $ov)
														<?php 
															$radio_default = ($ok == $filter['default']); 
														?>
														{!!Form::radio($filter['field'], $ok, $radio_default)!!} <?php echo $ov; ?><br/>
													@endforeach
												<?php elseif ($filter['type'] == "select"): ?>
													
														<?php 
															if (empty($filter['default']))
																$filter['default'] = "";
														?>
														
														<?php if (!empty($filter['multiple'])): ?>
														{!!Form::select($filter['field'], $filter['options'], $filter['default'],array( 'class'=>'form-control', 'multiple'=>'multiple', 'size'=>'10', 'name'=>$filter['field'].'[]'))!!}
														<?php else: ?>
														{!!Form::select($filter['field'], $filter['options'], $filter['default'], array( 'class'=>'form-control'))!!}
														<?php endif; ?>
												<?php elseif ($filter['type'] == "date"): ?>
												<?php
													$filter['default_from'] = (!empty($_POST[$filter['field'].'_from']) ? $_POST[$filter['field'].'_from'] : date("1/1/Y"));
													$filter['default_to'] = (!empty($_POST[$filter['field'].'_to']) ? $_POST[$filter['field'].'_to'] : "");
												?>
												{!!Form::text($filter['field'].'_from', $filter['default_from'], array('class'=>'form-control datepicker', 'placeholder' => 'From Date', 'data-provide' => 'datepicker', "pattern"=>"(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" ))!!}

												{!!Form::text($filter['field'].'_to', $filter['default_to'], array('class'=>'form-control datepicker', 'placeholder' => 'To Date', 'data-provide' => 'datepicker', "pattern"=>"(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" ))!!}													
												<?php else: ?>
				                            	{!!Form::text($filter['field'], $filter['default'], array('class'=>'form-control'))!!}
												<?php endif; ?>
				                            </div>
				                       </div>
										@endforeach
									</div>
				                  	@endforeach		
				                  	<div class="col-sm-12" style="text-align: center">                 	
				                  	  				                                          
				                    	<div class="form-group">
									    	<div class="col-sm-12">
									       		{!! Form::submit('Search', array('class'=>'btn btn-xs btn-info'))!!}
									    	</div>
									  	</div>
				                  	</div>                  	
							{!!Form::close()!!}
						</div>		    				
						<div id="list">	
						<div>
							Report run: <?php echo date("m/d/Y H:i:s"); ?>
						</div>						
						 <table class="data-table not_sortable">
		                    <thead>
		                        <tr>
									@foreach ($columns as $col)
		                            <th data-field="<?php echo $col['field']; ?>" data-sortable="true"><?php echo $col['label']; ?></th>
									@endforeach
		                        </tr>
		                    </thead>
		                    <tbody>
		                    </tbody>
		                	</table>		                
						</div>
					</div>   	
				  </div>
				</div>          
            </div>
        </div>
    </div>

@endsection
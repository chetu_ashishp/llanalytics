@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading">Home</div>
				<div class="panel-body">	
				
				<div class="container datalist">
						
						@if(App\Role::isAllow(Auth::user()->role_id,'clients'))						
							{!!HTML::Link('company','Companies', array('class'=>'list-group-item'))!!}
						@endif
						
						<!--@if(App\Role::isAllow(Auth::user()->role_id,'clients'))						
							{!!HTML::Link('clients','Clients', array('class'=>'list-group-item'))!!}
						@endif!-->
						
						@if(App\Role::isAllow(Auth::user()->role_id,'clients'))						
							{!!HTML::Link('staff','OMS Staff', array('class'=>'list-group-item'))!!}
						@endif
						
						<!---@if(App\Role::isAllow(Auth::user()->role_id,'templates'))						
							{!!HTML::Link('companyportal','CompanyPortal', array('class'=>'list-group-item'))!!}
						@endif!-->
						
						@if(App\Role::isAllow(Auth::user()->role_id,'templates'))						
							{!!HTML::Link('templates','Templates', array('class'=>'list-group-item'))!!}
						@endif	
						
						@if(App\Role::isAllow(Auth::user()->role_id,'sheets'))						
							{!!HTML::Link('sheets','Sheets', array('class'=>'list-group-item'))!!}
						@endif	
						
						@if(App\Role::isAllow(Auth::user()->role_id,'companytools'))						
							{!!HTML::Link('companytools','Company Options', array('class'=>'list-group-item'))!!}
						@endif	
						
						@if(App\Role::isAllow(Auth::user()->role_id,'home'))						
							{!!HTML::Link('reports','Reports', array('class'=>'list-group-item'))!!}
						@endif	
							
						{!!HTML::Link('home','Account', array('class'=>'list-group-item'))!!}
						
						@if(App\Role::isAllow(Auth::user()->role_id,'datamain'))						
							{!!HTML::Link('datamain','Tools', array('class'=>'list-group-item'))!!}
						@endif	
						
                	</div>			  
				</div>				
			</div>
		</div>
	</div>
</div>


 
 
@endsection


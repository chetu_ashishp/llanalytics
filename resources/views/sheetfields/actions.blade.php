{!! Form::button('Edit', array('class'=>'btn  btn-warning btn-xs ajax', 'data-id'=>$id, 'data-route' => 'editField', 'data-target' => '#editfield-div', 'data-path' => 'SheetFields', 'data-sheet_id'=>$sheet_id, 'data-_token'=>csrf_token() ))!!}
@if(App\Role::isAllow(Auth::user()->role_id,'sheetfields/{sheetfields}', 'DELETE'))
{!! Form::button('Delete', array('class'=>'btn  btn-danger btn-xs deletefield', 'data-id'=>$id,  'data-route' => 'deleteField', 'data-path' => 'SheetFields', 'data-_token'=>csrf_token() ))!!}
@endif

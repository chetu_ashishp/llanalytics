<div class="panel panel-info" id="sec{{$number}}">
			<div class="panel-heading" role="tab" id="heading{{$number}}">
				<h4 class="panel-title">
					<a  data-toggle="collapse" data-parent="#accordion" href="#collapse{{$number}}" aria-expanded="false" aria-controls="collapse{{$number}}">
						{{$output['type']}}
					</a>
					 <span aria-hidden="true" class="pull-right red del-addr-rec" data-section="{{$number}}">&times;</span>
				</h4>
			</div>
			<div id="collapse{{$number}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$number}}">
				<div class="panel-body">
			
			
     
						<div class="form-group">
			            	{!!Form::label('type','Name', array('class'=>'col-sm-5 control-label required'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('type', $output['type'], array('class'=>'form-control', 'required'=>"required", "name"=>"type[]" ))!!}
			                	</div>
			            </div>
			            
			            <div class="form-group">
			            	{!!Form::label('addr_line1','Address Line 1', array('class'=>'col-sm-5 control-label'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('addr_line1', $output['addr_line1'], array('class'=>'form-control', "name"=>"addr_line1[]" ))!!}
			                	</div>
			            </div>
			            			            
			            <div class="form-group">
			            	{!!Form::label('addr_line2','Address Line 2', array('class'=>'col-sm-5 control-label'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::text('addr_line2', $output['addr_line2'], array('class'=>'form-control', "name"=>"addr_line2[]"  ))!!}
			                	</div>
			            </div>
			           
			          
			           <div class="form-group">
		                    {!!Form::label('city','City', array('class'=>'col-sm-5 control-label'))!!}
		                    <div class="col-sm-7">		                    	
		                    	{!!Form::text('city', $output['city'], array('class'=>'form-control', "name"=>"city[]"))!!}
		                	</div>
		            	</div>
		            	
		            			            	
		            	         	 
			            <div class="form-group">
			            	{!!Form::label('state_id','State', array('class'=>'col-sm-5 control-label'))!!}
		                	<div class="col-sm-7">
		                    	{!!Form::select('state_id', getStates(), $output['state_id'], array('class'=>'form-control', "name"=>"state_id[]"  ))!!}
		                	</div>
			            </div>
			            
			            
			            <div class="form-group">
			            	{!!Form::label('zip','Zip Code', array('class'=>'col-sm-5 control-label'))!!}
		                	<div class="col-sm-7">		                    	
		                    	
		                    		{!!Form::text('zip', $output['zip'], array('class'=>'form-control', "name"=>"zip[]", "pattern"=>"(\d{5}([\-]\d{4})?)"  ))!!}
		                	</div>
			            </div>
			            
			            
			             <div class="form-group">
						    <div class="col-sm-offset-5 col-sm-7">
						      <div class="checkbox">
						        <label>
						        	@if(isset($output['mailing_addr']) && $output['mailing_addr']=='Yes') 
				      					{!!Form::checkbox('mailing_addr', 'Yes' , true, array('class'=>'mailing', "name"=>"mailing_addr[]" ) )!!} Mailing Address?
				      				@else 
				      					{!!Form::checkbox('mailing_addr', 'Yes' , false, array('class'=>'mailing', "name"=>"mailing_addr[]" ) )!!} Mailing Address?
				      				@endif				      				
						        </label>
						      </div>
						    </div>
						 </div>
					      
				</div>
			</div>
		</div>
	
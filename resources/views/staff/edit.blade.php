<?php //echo '<pre/>'; print_r($staffData);
	$firstName = isset($staffData[0]->first_name) ? $staffData[0]->first_name : '';
	$lastName  = isset($staffData[0]->last_name) ? $staffData[0]->last_name : '';
	$Title = isset($staffData[0]->name) ? $staffData[0]->name : '';
	$email = isset($staffData[0]->email) ? $staffData[0]->email : '';
	$primaryContact = isset($staffData[0]->mobile_phone) ? $staffData[0]->mobile_phone :'';
	$AlternateContact = isset($staffData[0]->alternate_phone) ? $staffData[0]->alternate_phone :'';
	$linkedinUrl = isset($staffData[0]->linkedin_url) ? $staffData[0]->linkedin_url : '';
	$facebookUrl = isset($staffData[0]->facebook_url) ? $staffData[0]->facebook_url : '';
	$twitterUrl = isset($staffData[0]->twitter_url) ? $staffData[0]->twitter_url : '';
	$googleplusUrl = isset($staffData[0]->googleplus_url) ? $staffData[0]->googleplus_url : '';
	$status = isset($staffData[0]->status) ? $staffData[0]->status : '';
	$primaryStaff = isset($staffData[0]->primary_contact) ? $staffData[0]->primary_contact : 0;
    $role_id = isset($staffData[0]->role_id) ? $staffData[0]->role_id : 0;
?>	 
<div class="panel-body">
			<div class="container datalist">
				<div class="row">    
					<div class="col-md-12">
						<div class="panel panel-info">
						  <div class="panel-heading">		  	
								<div class="container">
									<h4 class="panel-title pull-left">{{$form}} Staff + Staff Member</h4>
									
								</div>		  	
						  </div>
						</div>
					</div>
				</div>
				<div class="jumbotron container">
					
					{!! Form::open( array('id' => 'staff-edit-form', 'data-route' => 'updateStaff', 'class'=>'form-horizontal form-edit',  'autocomplete'=>"off", 'data-path' => 'Staffs', 'data-_token'=>csrf_token() )) !!}
					
					 	
					<div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	     	
								{!! Form::button('Cancel', array('class'=>'btn  btn-sm btn-default pull-right left10', 'id'=>'cancelAddForm'))!!}
								{!! Form::submit('Save', array('class'=>'btn btn-sm btn-info pull-right'))!!}							
					    	</div>
						</div>
		           </div>
		           
		           
					<div class="row">					
						<div class="col-sm-6">				                  	                  	
			               	{!!Form::hidden('company_id', $company_id)!!}
							{!!Form::hidden('id', $user_id)!!}
							<div class="form-group">
			                        {!!Form::label('fname','First Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('fname', $firstName, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>	            	            	
			            	
			            	<div class="form-group">
			                        {!!Form::label('lname','Last Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('lname', $lastName, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('title','Title', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('title', $Title, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('email','Email', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::email('email', $email, array('class'=>'form-control'))!!}
			                	</div>
			            	</div>
							<div class="form-group">
			                        {!!Form::label('primary_phone','Primary Phone', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('primary_phone', $primaryContact, array('class'=>'form-control phoneUS required', 'required' ))!!}
			                	</div>
			            	</div> 
							<div class="form-group">
			                        {!!Form::label('alternate_phone','Alternate Phone', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('alternate_phone', $AlternateContact, array('class'=>'form-control phoneUS' ))!!}
									</div>
			            	</div>
			            	@if($form == 'add')
							<div class="form-group">
			                        {!!Form::label('password','Password', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::password('password', null, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('password_confirmation','Confirm Password', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::password('password_confirmation', null, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
							@endif
			       </div>
						
						<div class="col-sm-6">
							
							<div class="form-group">
			                        {!!Form::label('primary_contact','Company Primary Contact', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	@if($primaryStaff == 1)
										{!!Form::select('primary_contact',  array("1"=>"Yes") ,$primaryStaff, array('class'=>'form-control readonly','readonly'))!!}
									@elseif($primaryStaff == 0)
										{!!Form::select('primary_contact',  array("1"=>"Yes", "0"=>"No") ,$primaryStaff, array('class'=>'form-control'))!!}
									@endif	
									</div>
			            	</div>
			            	<div class="form-group">
			                        {!!Form::label('role_id','Role', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::select('role_id',  array("" => "") + $roles, $role_id, array('class'=>'form-control  required', 'required'))!!}
										
									</div>
			            	</div>	
							<div class="form-group">
				                {!!Form::label('linkedin_url','LinkedIn URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('linkedin_url', $linkedinUrl, array('class'=>'form-control', 'placeholder'=>"https://linkedin.com", 'id'=>'linkedin_url'))!!}
				               	</div>
				            </div> 
							
							<div class="form-group">
				                {!!Form::label('facebook_url','Facebook URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('facebook_url', $facebookUrl, array('class'=>'form-control', 'placeholder'=>"https://facebook.com", 'id'=>'facebook_url'))!!}
				               	</div>
				            </div> 
							
							<div class="form-group">
				                {!!Form::label('twitter_url','Twitter URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('twitter_url', $twitterUrl, array('class'=>'form-control', 'placeholder'=>"https://twitter.com", 'id'=>'twitter_url'))!!}
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('googleplus_url','Google+ URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('googleplus_url', $googleplusUrl, array('class'=>'form-control', 'placeholder'=>"https://google.com", 'id'=>'googleplus_url'))!!}
				               	</div>
				            </div>
							<div class="form-group">
				                {!!Form::label('status', 'Status', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('status',  array("Active"=>"Active", "Inactive"=>"Inactive", 'Suspended'=>'Suspended', 'Terminated'=>'Terminated') ,$status, array('class'=>'form-control'))!!}
				                 	
				               	</div>
				            </div>	 
				            	           		
			           	</div>
		           	</div>
		 
		       	
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	     	
								{!! Form::button('Cancel', array('class'=>'btn  btn-sm btn-default pull-right left10', 'id'=>'cancelAddForm'))!!}
								{!! Form::submit('Save', array('class'=>'btn-sm btn btn-info pull-right','data-company_id'=>$company_id))!!}
																
					    	</div>
						</div>
		           </div>
		           				 
					{!! Form::close() !!}	
				</div>   		
			</div>    	
		  </div>
  </div>		
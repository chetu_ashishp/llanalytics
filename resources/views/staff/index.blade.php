@extends('innertab')

@section('script')
 
<script>
var tableData = {!! $tableData !!};
</script>
@endsection

@section('content')
<div class="container top20">
    <div class="row">    
        <div class="col-md-12">
	       	
	        <div class="panel panel-info" id="transactions-div">
	            				
	        	<div class="panel-body">	
                    {!! Form::open( array('staffsearch' => 'form', 'method' => 'put', 'files'=>true, 'class'=>'form-horizontal form-staff-active-search',  'autocomplete'=>"off")) !!}                    
	        		<div class="container bottom20">
                        <div class="search-radio-btn pull-right"> 
                            {!! Form::button('<i class="fa fa-plus pull-right"></i> Add Staff', array('class'=>'btn btn-xs  addstaff btn-info pull-right left10 ajax', 'data-company_id'=>$company_id, 'data-route' => 'addStaff', 'data-target' => '#newtransaction-div', 'data-path' => 'Staffs', 'data-_token'=>csrf_token()))!!}
                            {!! Form::hidden('data-company_id',$company_id, ['class' => 'ng-hide'] ) !!}
                           
                        </div>   
			        	<div class="search-radio-btn pull-left">  
			        		
                           <div class="input-field"> {!! Form::radio('staffstatus', 'Active', true, ['class' => 'left10 right10','id' => 'staffstatus']) !!} Active</div>
                             <div class="input-field">{!! Form::radio('staffstatus', 'NonActive', null, ['class' => 'left10','id' => 'staffstatus']) !!} NonActive</div>
                            <div class="input-field"> {!! Form::submit('Search', array('class'=>'btn btn-xs btn-info', 'data-path'=>'Staffs','data-company_id'=>$company_id,'data-route' => 'searchstaff','data-_token'=>csrf_token()))!!}</div>
                        </div>
			        </div>
			       {!! Form::close() !!} 			        
				    <div class="container datalist">
						<div id="list">
							 <table class="data-table" data-toggle="table" data-pagination="true" data-sort-name="start_date" data-sort-order="desc">
			                    <thead>
			                        <tr>
			                            <th data-field="first_name" data-sortable="true">First Name</th>
			                            <th data-field="last_name" data-sortable="true">Last Name</th>
			                            <th data-field="title" data-sortable="true">Title #</th>
			                            <th data-field="email" data-sortable="true">Email #</th>
			                            <th data-field="primary_phone" data-sortable="true">Primary Phone</th>
										<th data-field="role_id" data-sortable="true">Role</th>
										<th data-field="primary_contact" data-sortable="true">Primary</th>	
			                            <th data-field="status" data-sortable="true">Status</th> 
										<th data-field="actions">Actions</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    </tbody> 
			                </table>		                
						</div>
					</div>   	
				</div>
			</div>
									
			<div class="panel panel-info"  id="newtransaction-div" style="display:none">				
			 	
      		</div>      		
			 
			 <div class="panel panel-info"  id="editstaff-div" style="display:none"></div>    
		</div>
	</div>
</div>
<input type="hidden" id="data-ajax" value="Transactions">
@endsection


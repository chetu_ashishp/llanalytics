
<?php if($is_deleted) { ?>
{!! Form::button('ReCall', array('class'=>'btn  btn-danger btn-xs resetStaff', 'data-id'=>$id,  'data-item'=>$id,  'data-route' => 'resetstaff', 'data-path' => 'Staffs', 'data-_token'=>csrf_token() ))!!}
<?php } else { ?>
{!! Form::button('Edit', array('class'=>'btn  btn-warning btn-xs ajax edittransaction-btn', 'data-id'=>$id, 'data-route' => 'editStaff', 'data-target' => '#editstaff-div', 'data-path' => 'Staffs','data-company_id'=>$company_id,'data-user_id'=>$id, 'data-_token'=>csrf_token() ))!!}
{!! Form::button('Delete', array('class'=>'btn  btn-danger btn-xs deleteStaff', 'data-id'=>$id,  'data-item'=>$id,  'data-route' => 'removeCheck', 'data-path' => 'Staffs', 'data-_token'=>csrf_token() ))!!}
<?php } ?>

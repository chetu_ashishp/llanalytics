 	 <div class="panel-body">
			<div class="container datalist">
				<div class="row">    
					<div class="col-md-12">
						<div class="panel panel-info">
						  <div class="panel-heading">		  	
								<div class="container">
									<h4 class="panel-title pull-left">{{$form}} Staff</h4>
									
								</div>		  	
						  </div>
						</div>
					</div>
				</div>
				<div class="jumbotron container">
					
					{!! Form::open( array('id' => 'staff-add-form', 'data-route' => 'storeStaff', 'class'=>'form-horizontal form-add',  'autocomplete'=>"off", 'data-path' => 'Staffs', 'data-_token'=>csrf_token() )) !!}
					
					 	
					<div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	     	
								{!! Form::button('Cancel', array('class'=>'btn  btn-sm btn-default pull-right left10', 'id'=>'cancelAddForm'))!!}
								{!! Form::submit('Save', array('class'=>'btn btn-sm btn-info pull-right'))!!}							
					    	</div>
						</div>
		           </div>
		           
		           
					<div class="row">					
						<div class="col-sm-6">				                  	                  	
			               	{!!Form::hidden('company_id', $company_id)!!}
							<div class="form-group">
			                        {!!Form::label('fname','First Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('fname', null, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>	            	            	
			            	
			            	<div class="form-group">
			                        {!!Form::label('lname','Last Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('lname', null, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('title','Title', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('title', null, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('email','Email', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::email('email', null, array('class'=>'form-control'))!!}
			                	</div>
			            	</div>
			            	
							<!--<div class="form-group">
			                        {!!Form::label('password','Password', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::password('password', null, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('password_confirmation','Confirm Password', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::password('password_confirmation', null, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>!-->
							<div class="form-group">
			                        {!!Form::label('primary_phone','Primary Phone', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('primary_phone', null, array('class'=>'form-control phoneUS required', 'required' ))!!}
			                	</div>
			            	</div>  	
			            	<div class="form-group">
			                        {!!Form::label('alternate_phone','Alternate Phone', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('alternate_phone', null, array('class'=>'form-control phoneUS' ))!!}
									</div>
			            	</div>
							
			       </div>
						
						<div class="col-sm-6">
							<div class="form-group">
			                        {!!Form::label('role_id','Role', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::select('role_id',  array("" => "") + $roles, null, array('class'=>'form-control  required', 'required'))!!}
										
									</div>
			            	</div>	
							<div class="form-group">
				                {!!Form::label('linkedin_url','LinkedIn URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('linkedin_url',  null, array('class'=>'form-control', 'placeholder'=>"https://linkedin.com", 'id'=>'linkedin_url'))!!}
				               	</div>
				            </div> 
							
							<div class="form-group">
				                {!!Form::label('facebook_url','Facebook URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('facebook_url',  null, array('class'=>'form-control', 'placeholder'=>"https://facebook.com", 'id'=>'facebook_url'))!!}
				               	</div>
				            </div> 
							
							<div class="form-group">
				                {!!Form::label('twitter_url','Twitter URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('twitter_url',  null, array('class'=>'form-control', 'placeholder'=>"https://twitter.com", 'id'=>'twitter_url'))!!}
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('googleplus_url','Google+ URL', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::url('googleplus_url',  null, array('class'=>'form-control', 'placeholder'=>"https://google.com", 'id'=>'googleplus_url'))!!}
				               	</div>
				            </div>
							<div class="form-group">
				                {!!Form::label('status', 'Status', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('status',  array("Active"=>"Active", "Inactive"=>"Inactive", 'Suspended'=>'Suspended', 'Terminated'=>'Terminated') ,"Active", array('class'=>'form-control'))!!}
				                 	
				               	</div>
				            </div>	 
				            	           		
			           	</div>
		           	</div>
		 
		       	
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	     	
								{!! Form::button('Cancel', array('class'=>'btn  btn-sm btn-default pull-right left10', 'id'=>'cancelAddForm'))!!}
								{!! Form::submit('Save', array('class'=>'btn-sm btn btn-info pull-right','data-company_id'=>$company_id))!!}
																
					    	</div>
						</div>
		           </div>
		           				 
					{!! Form::close() !!}	
				</div>   		
			</div>    	
		  </div>
  </div>		
@extends('app')

@section('script')
<!-- Page-Level Plugin Scripts -->
{!! HTML::script('js/settings/settings.js'); !!}
<script src="{{ asset('/js/role.js') }}"></script>
@endsection
    		   	    
@section('content')



<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-envelope"></i> Settings </h4>				  										  		
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">
				    
						{!!Form::open(array('data-path' => 'Settings', 'data-route'=>'update', 'class' => 'form-horizontal form-edit', 'data-_token'=>csrf_token()))!!}
				  		
                        <div class="form-group">
                            {!!Form::label('from_name_email_notice','From Name for Email Notices', array('class'=>'required col-sm-3 control-label'))!!}
                            <div class="col-sm-6">
                            	{!!Form::text('from_name_email_notice', App\Settings::where('key','=','from_name_email_notice')->count()>0?App\Settings::where('key','=','from_name_email_notice')->first()->value:null, array('class'=>'form-control', 'placeholder'=>'From Name for Email Notices', 'required'=>'required'))!!}
                            </div>
                        </div>                        
                     
                       <div class="form-group">
                            {!!Form::label('from_email_notice','From Email for Email Notices', array('class'=>'required col-sm-3 control-label'))!!}
                            <div class="col-sm-6">
                            	{!!Form::email('from_email_notice', App\Settings::where('key','=','from_email_notice')->count()>0?App\Settings::where('key','=','from_email_notice')->first()->value:null, array('class'=>'form-control', 'placeholder'=>'From Email for Email Notices', 'required'=>'required'))!!}
                            </div>
                        </div> 
                                           
                        <div class="form-group">
                            {!!Form::label('email_testing','Email Testing Flag', array('class'=>'required col-sm-3 control-label'))!!}
                            <div class="col-sm-6">
                            	{!! Form::select('email_testing',array('yes'=>'Yes','no'=>'No'),App\Settings::where('key','=','email_testing')->count()>0?App\Settings::where('key','=','email_testing')->first()->value:null, array('class'=>'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::label('testing_email','Email Address For Testing', array('class'=>'col-sm-3 control-label'))!!}
                            <div class="col-sm-6">
                            	{!! Form::email('testing_email', App\Settings::where('key','=','testing_email')->count()>0?App\Settings::where('key','=','testing_email')->first()->value:null, array('class'=>'form-control', 'placeholder'=>'From Name for Email Notices')) !!}
							</div>
                        </div>
                                                              
                    	 <div class="form-group">
					    	<div class="col-sm-offset-3 col-sm-2">
					       		{!! Form::submit('Update', array('class'=>'btn  btn-info  style_save updatesetting', 'data-path'=>'settings', 'data-route'=>'removeCheck', 'data-_token'=>csrf_token()))!!}
					    	</div>
					  	</div>
					
					{!!Form::close()!!}
					</div>   	
				  </div>
		</div>
      </div>
   </div>
</div>
@stop







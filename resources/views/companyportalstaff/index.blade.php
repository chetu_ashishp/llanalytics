@extends('company')
@section('style')	
	<link href="{{ asset('/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}" rel="stylesheet">
@stop
@section('script')
<script>var tableData = {!! $tableData !!};</script>
<script src="{{ asset('/js/jquery.mask.js') }}"></script>  
 <script src="{{ asset('/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('/js/companyportal/staff.js') }}"></script>
<!-- Page-Level Plugin Scripts -->
@endsection
@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i> Company Portal Staff </h4>							
				  			<a href="{{route('newcpstaff')}}" class="btn btn-info  storeomsstaff btn-xs pull-right" data-path="OmsStaff" data-route="store" data-_token="{{ csrf_token() }}" role="button"><i class="fa fa-plus"></i> Add New Staff</a>				  						  		
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">		    	
				    			    				
						<div id="list">						
						 <table class="data-table" data-toggle="table" data-pagination="true" data-search="true" data-sort-name="clname" data-sort-order="asc">
		                     <thead>
			                        <tr>
			                            <th data-field="first_name" data-sortable="true">First Name</th>
			                            <th data-field="last_name" data-sortable="true">Last Name</th>
			                            <th data-field="title" data-sortable="true">Title #</th>
			                            <th data-field="email" data-sortable="true">Email #</th>
			                            <th data-field="primary_phone" data-sortable="true">Primary Phone</th>
										<th data-field="role_id" data-sortable="true">Role</th>
										<th data-field="primary_contact" data-sortable="true">Primary</th>	
			                            <th data-field="status" data-sortable="true">Status</th> 
										<th data-field="actions">Actions</th>
			                        </tr>
			                    </thead>
		                    <tbody>
		                    </tbody>
		                	</table>		                
						</div>
					</div>   	
				  </div>
				</div>          
            </div>
        </div>
    </div>
@endsection
@extends('app')
<?php //echo '<pre/>';
//print_r($stateName['state_name']);
//echo print_r($company_info[0]);
//print_r($company_info); die; ?>
@section('style')
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
	<link href="{{ asset('/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}" rel="stylesheet">
@stop

@section('script')

<!-- Page-Level Plugin Scripts -->
   <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('/js/typeahead.js-master/dist/typeahead.jquery.js') }}"></script>
   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>   
    <script src="{{ asset('/js/jquery.mask.js') }}"></script>   
   <script src="{{ asset('/js/company/company.js') }}"></script>
   
   
 <script src="{{ asset('/js/clients/jquery.formatCurrency-1.4.0.min.js') }}"></script>
 <script src="{{ asset('/js/company/staff.js') }}"></script>
 <script src="{{ asset('/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
 <script src="{{ asset('/vendor/925491/jquery.ui.autocomplete.html.js') }}"></script>
  
@stop

@section('content')

<div class="panel panel-info">
  <div class="panel-heading">		  	
		<div class="container">
			<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-info-sign"></i> Company:  {{$company_info[0]['company_name']}} </h4>
			<a href="{{route('company')}}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>			
		</div>		  	
  </div>
  <div class="panel-body">    		
		<!-- Nav tabs -->		
		  <ul class="nav nav-tabs" role="tablist">
		        <li class="active"><a href="#core_details" data-toggle="tab">Core Details</a></li>
		        <li><a href="#staffs" id="lstaff" class="ajax" data-path="Staffs" data-route="show" data-item="{{ $company_info[0]['id'] }}"  data-_token="{{csrf_token()}}" data-toggle="tab" >Staff Details</a></li>
		  </ul>
			
		<!-- Tab panes -->
	    <div class="tab-content">
	        <div class="tab-pane active" id="core_details">
	            <div class="row">
	                 <div class="col-lg-8 col-lg-offset-1">
	                 	 <div class="row">
		                    <div class="top10 col-lg-7">
		                   
								<a href="#" id="" class="btn  btn-sm pull-right left10 btn-danger deleteCompany", data-id="{{$company_info[0]['id']}}" data-item="{{$company_info[0]['id']}}" data-route ="removeCheck" data-path="Company" data-_token="{{csrf_token()}}">Delete</a>
		                    	<a href="{{route('company')}}" class="btn btn-default  btn-sm pull-right left10" role="button">Cancel</a>	                        
								<a href="{{route('company.edit', array('company' => $company_info[0]['id'])) }}" class="btn btn-warning btn-sm pull-right" role='button' >Edit</a>
							  
		                    </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-lg-8 col-lg-offset-1">
	                    <div class="row">
	                        <div class="col-lg-7">	                            
	                            @foreach($lables as $key=>$value)
	                     	        
		                     	    		<div class="row detail-cell">
				                                <div class="col-lg-6"><p class = "detail-cell-label"> {{$value}} </p></div> 
				                               @if($key=='state_id')
												<div class="col-lg-6"><p>{{$stateName['state_name']}}</p></div>
												@elseif($key=='referral_source')
												<div class="col-lg-6"><p>{{$refsource['option_name']}}</p></div>												
												@elseif($key=='company_type')
												<div class="col-lg-6"><p>{{$companyList['option_name']}}</p></div>												
												@else
											 
				                                	<div class="col-lg-6"><p>{{$company_info[0][$key]}}</p></div>
				                               	
				                                  @endif
				                            </div>				                        
	                     	    	                    	
	                            @endforeach
								
	                        </div>
	                    </div>
	                </div>
	            </div>
	            
	              <div class="row">
	                 <div class="col-lg-8 col-lg-offset-1">
	                 	 <div class="row">
		                    <div class="top10 col-lg-7">	
		                  
		                    	<a href="{{route('company')}}" class="btn btn-default  btn-sm pull-right left10" role="button">Cancel</a>	                        
		                        <a href="{{route('company.edit', array('company' => $company->id)) }}" class="btn btn-warning btn-sm pull-right" role='button' >Edit</a>
		                     
		                    </div>
	                    </div>
	                </div>
	            </div>
	            
	        </div>
	        <div class="tab-pane" id="staffs"></div>
	    </div>
	    
	    		
  </div>
</div>

 
    
    
    
    
    
    
@stop


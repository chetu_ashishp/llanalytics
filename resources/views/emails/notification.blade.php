Sender : {!! $maildata['sender']->first_name." ".$maildata['sender']->last_name !!}<br>
Date/Time Sent: {!! date('m/d/y h:i:s', strtotime($maildata['item']->created_at)) !!}<br>
Message: {!! str_limit($maildata['item']->message, 200) !!}<br>
Sheet Name: {!! $maildata['sheet']->name !!}<br>
Field Name: {!! $maildata['field']->field_name !!}<br>
Client: {!! $maildata['client']->first_name !!} {!! $maildata['client']->last_name !!}<br>
Transaction Status: {!! $maildata['transactionstatus']->option_name !!}<br>
Transaction Type: {!! $maildata['transactiontype']->option_name !!}<br>
MLS#: {!! $maildata['transaction']->mls1 !!}<br>
Short Address: {!! $maildata['transaction']->nickname !!}<br>
Property House #: {!! $maildata['transaction']->property_house !!}<br>
Property Address Line 1: {!! $maildata['transaction']->property_addr1 !!}<br>
Property Address Line 2: {!! $maildata['transaction']->property_addr2 !!}<br>
Status: Unread<br>

<a href="{!!route('sheet.details.entry', array('template' => $maildata['sheet']->id)) !!}">Login</a>
 	<div class="panel-body">
		<div class="container datalist">		    	
			<div class="jumbotron container">			
				{!! Form::open( array('id' => 'trasaction-add-form', 'data-route' => 'storeTransaction', 'class'=>'form-horizontal form-add',  'autocomplete'=>"off", 'data-path' => 'Transactions', 'data-_token'=>csrf_token() )) !!}				 
           		<div class="form-group">
			    	<div class="col-sm-12">							
					    <div class="pull-left">
					      <p class="form-control-static text-info"> {{$clientobj->last_name}}, {{$clientobj->first_name}}</p>
					    </div>										    
					    {!! Form::button('Cancel', array('class'=>'btn  btn-sm btn-default pull-right left10', 'id'=>'cancelAddForm'))!!}
						{!! Form::submit('Save', array('class'=>'btn btn-sm pull-right btn-info '))!!}
			    	</div>
				</div>
				 						
				<div class="row">					
					<div class="col-sm-4">			               					                    	
		                {!!Form::hidden('client_id', $client_id)!!}	
		                					            			            	
		            	<div class="form-group">
		                    {!!Form::label('start_date','Entry Date', array('class'=>'col-sm-5 control-label required'))!!}
		                    <div class="col-sm-7">					                    	
		                    	{!!Form::text('start_date', "", array('class'=>'form-control', 'required'))!!}	
		                	</div>
		            	</div>
		      
		            	
		            	<div class="form-group">
		                    {!!Form::label('transaction_type','Type', array('class'=>'col-sm-5 control-label required'))!!}
		                    <div class="col-sm-7">
		                    	{!!Form::select('transaction_type', companyOptionsByType('Transaction Type', $company_id), '', array('class'=>'form-control required', 'required'))!!}
		                	</div>
		            	</div>
		            	            	
		            	<div class="form-group">
		                    {!!Form::label('transaction_status','Status', array('class'=>'col-sm-5 control-label required '))!!}
		                    <div class="col-sm-7">
		                    	{!!Form::select('transaction_status', companyOptionsByType('Transaction Status', $company_id), '', array('class'=>'form-control', 'required'))!!}
		                	</div>
		            	</div>
		            	
		            	<div class="form-group">
		                    {!!Form::label('referral_source','Source', array('class'=>'col-sm-5 control-label'))!!}
		                    <div class="col-sm-7">
		                    	{!!Form::select('referral_source', companyOptionsByType('Referral Source', $company_id), '', array('class'=>'form-control'))!!}
		                	</div>
		            	</div>
		            	
		            	<div class="form-group">
		                        {!!Form::label('relatedtransid','Related Transaction', array('class'=>'col-sm-5 control-label'))!!}
		                        <div class="col-sm-7">
		                    	{!!Form::text('relatedtransid', null, array('class'=>'form-control ui-autocomplete-input'))!!}
		                    	{!!Form::hidden('related_trans_id', null, array('id'=>'related_trans_id'))!!}
		                	</div>
		            	</div>
					          
					    <fieldset class="list_info"><legend>  Listing Info</legend> 	
				        
							<div class="form-group">
				                {!!Form::label('mls1','MLS # - 1', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('mls1',  null, array('class'=>'form-control'))!!}
				               	</div>
				            </div> 	
				            
							<div class="form-group">
				                {!!Form::label('mls2','MLS # - 2', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('mls2',  null, array('class'=>'form-control'))!!}
				               	</div>
				            </div>
				            
				            <div class="form-group">
			                    {!!Form::label('listing_date','Listing Date', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('listing_date', null, array('class'=>'form-control findates'))!!}	
			                	</div>
			            	</div>
				            
				            <div class="form-group">
			                    {!!Form::label('listing_expiration_date','Listing Expiration Date', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('listing_expiration_date', null, array('class'=>'form-control '))!!}	
			                	</div>
			            	</div>
				            
				            
				            <div class="form-group">
			                    {!!Form::label('origin_list_price','Original List Price', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	<div class="input-group">
				                		<span id="basic-addon8" class="input-group-addon">$</span>
				                 		{!!Form::text('origin_list_price', null, array('class'=>'form-control finance','aria-describedby'=>"basic-addon8", 'placeholder'=>"value", 'pattern'=>"^((\d+)|(\d{1,3})(\,\d{3}|)*)(\.\d{2}|)$" ))!!}
				                 	</div>	
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                    {!!Form::label('current_list_price','Current List Price', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">	
			                    		<div class="input-group">
				                		<span id="basic-addon9" class="input-group-addon">$</span>
				                 		{!!Form::text('current_list_price', null, array('class'=>'form-control finance','aria-describedby'=>"basic-addon9", 'placeholder'=>"value", 'pattern'=>"^((\d+)|(\d{1,3})(\,\d{3}|)*)(\.\d{2}|)$" ))!!}
				                 	</div>
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                    {!!Form::label('lockbox','Lockbox #', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('lockbox', null, array('class'=>'form-control'))!!}	
			                	</div>
			            	</div>
					     </fieldset>         
					</div>
								
					<div class="col-sm-4">
						  		  
					     <fieldset><legend>Property Information</legend> 	
   								
			            	<div class="form-group">
			                    {!!Form::label('nickname','Short Address', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('nickname', null, array('class'=>'form-control'))!!}	
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                    {!!Form::label('property_house','Property House #', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('property_house', null, array('class'=>'form-control'))!!}	
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                    {!!Form::label('property_addr1','Property Address Line 1', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('property_addr1', null, array('class'=>'form-control'))!!}	
			                	</div>
			            	</div>
			            						            	
			            	<div class="form-group">
			                    {!!Form::label('property_addr2','Property Address Line 2', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('property_addr2', null, array('class'=>'form-control'))!!}	
			                	</div>
			            	</div>
			            						            						            	
			            	<div class="form-group">
			                    {!!Form::label('city_id','City', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::select('city_id', companyOptionsByType('City', $company_id), '', array('class'=>'form-control'))!!}
			                	</div>
			            	</div>
			            						            						            	
			            	<div class="form-group">
				            	{!!Form::label('state','State', array('class'=>'col-sm-5 control-label'))!!}
			                	<div class="col-sm-7">
			                    	{!!Form::select('state', getStates(), $company->default_state, array('class'=>'form-control' ))!!}
			                	</div>
				            </div>
				            
			            	<div class="form-group">
			                    {!!Form::label('zip','Zip', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::select('zip', companyOptionsByType('Zip', $company_id), '', array('class'=>'form-control'))!!}	
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                    {!!Form::label('subdivision','Subdivision', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('subdivision', null, array('class'=>'form-control'))!!}	
			                	</div>
			            	</div>
			            						            	
			            	<div class="form-group">
			                    {!!Form::label('hoa','HOA', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('hoa', null, array('class'=>'form-control'))!!}	
			                	</div>
			            	</div>
					   	</fieldset>
					   	    
					    <fieldset><legend>Contract Info </legend>
					            	   						            	
			            	<div class="form-group">
			                    {!!Form::label('contract_date','Contract Date', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('contract_date', null, array('class'=>'form-control findates'))!!}	
			                	</div>
			            	</div>
			            	   					            	
			            	<div class="form-group">
			                    {!!Form::label('coe_date','COE Date', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('coe_date', null, array('class'=>'form-control findates'))!!}	
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                    {!!Form::label('other_agent_name','Other Agent Name', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('other_agent_name', null, array('class'=>'form-control'))!!}	
			                	</div>
			            	</div>
			            		
			            	<div class="form-group">
			                    {!!Form::label('other_company_name','Other Company Name', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">					                    	
			                    	{!!Form::text('other_company_name', null, array('class'=>'form-control'))!!}	
			                	</div>
			            	</div>
			            								            
				            <div class="form-group">
			                    {!!Form::label('lender','Cash / Lender', array('class'=>'col-sm-5 control-label '))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::select('lender', companyOptionsByType('Lender', $company_id), '', array('class'=>'form-control'))!!}
			                	</div>
			            	</div>
			            	
			            	 <div class="form-group">
			                    {!!Form::label('title_company','Title Company', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">
			                    	{!!Form::select('title_company', companyOptionsByType('Title Company', $company_id), '', array('class'=>'form-control'))!!}
			                	</div>
			            	</div>
				            			            		            	
							<div class="form-group">
				                {!!Form::label('escrow_officer','Escrow Officer', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('escrow_officer',  null, array('class'=>'form-control'))!!}
				               	</div>
				            </div> 	
				            
				            <div class="form-group">
				                {!!Form::label('loan_officer','Loan Officer', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('loan_officer',  null, array('class'=>'form-control'))!!}
				               	</div>
				            </div> 	
				            
				            
				             <div class="form-group">
				                {!!Form::label('notes','Notes', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::textarea('notes',  null, array('class'=>'form-control', 'rows'=>'4'))!!}
				               	</div>
				            </div> 		
						 </fieldset>
						    	           		
					</div>
					           	
					<div class="col-sm-4">           	
					           	
			           	<fieldset><legend> Financial Info</legend> 	
			            	
			            	<div class="form-group">
			                        {!!Form::label('sales_price','Sales Price', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                        	<div class="input-group">
			                        		<span id="basic-addon1" class="input-group-addon">$</span>
			                    			{!!Form::text('sales_price', null, array('class'=>'form-control finance', 'aria-describedby'=>"basic-addon1", 'placeholder'=>"dollar value", 'pattern'=>"^((\d+)|(\d{1,3})(\,\d{3}|)*)(\.\d{2}|)$"  ))!!}
			                    		</div>
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
		                        {!!Form::label('bonus','Bonus', array('class'=>'col-sm-5 control-label'))!!}
		                        <div class="col-sm-7">
		                        	<div class="input-group">
		                        		<span id="basic-addon2" class="input-group-addon">$</span>
		                    			{!!Form::text('bonus', null, array('class'=>'form-control finance', 'aria-describedby'=>"basic-addon2", 'placeholder'=>"dollar value ", 'pattern'=>"^((\d+)|(\d{1,3})(\,\d{3}|)*)(\.\d{2}|)$" ))!!}
		                    		</div>
			                	</div>
			            	</div>
			            	
			            	<div class="form-group">
			                    {!!Form::label('agent_proportio','Agent Proportion', array('class'=>'col-sm-5 control-label'))!!}
			                    <div class="col-sm-7">
			                    	<div class="input-group">					                        	
			                    		{!!Form::text('agent_proportio', null, array('class'=>'form-control finance', 'aria-describedby'=>"basic-addon3", 'placeholder'=>"percentage", 'pattern'=>'^((\d+)|(\d{1,3})(\,\d{3}|)*)(\.\d{6}|)$'))!!}
			                    		<span id="basic-addon3" class="input-group-addon">%</span>
			                    	</div>	
			                	</div>
			                </div>
			                 
			            	<div class="form-group">
				                {!!Form::label('commission_rate','Commission Rate', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                	<div class="input-group">
				                 		{!!Form::text('commission_rate', null, array('class'=>'form-control finance', 'placeholder'=>"percentage" , 'aria-describedby'=>"basic-addon4", 'pattern'=>'^((\d+)|(\d{1,3})(\,\d{3}|)*)(\.\d{6}|)$'))!!}
				                 		<span id="basic-addon4" class="input-group-addon">%</span>
				                	</div> 					                 	
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('number_of_units','Number Of Units', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('number_of_units',  0, array('class'=>'form-control'))!!}
				               	</div>
				            </div>
				              
			            	<div class="form-group">
				                {!!Form::label('agent_split','Agent Split', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                	<div class="input-group">
				                 		{!!Form::text('agent_split', null, array('class'=>'form-control finance', 'aria-describedby'=>"basic-addon5", 'placeholder'=>"percentage" , 'pattern'=>'^((\d+)|(\d{1,3})(\,\d{3}|)*)(\.\d{6}|)$'))!!}
				                 		<span id="basic-addon5" class="input-group-addon">%</span>
			                    	</div>					                 	
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('agent_split2','Agent Split 2', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                	<div class="input-group">
				                 		{!!Form::text('agent_split2', null, array('class'=>'form-control finance', 'aria-describedby'=>"basic-addon6", 'placeholder'=>"percentage", 'pattern'=>'^((\d+)|(\d{1,3})(\,\d{3}|)*)(\.\d{6}|)$' ))!!}
				                 		<span id="basic-addon6" class="input-group-addon">%</span>
			                    	</div>				                 	
				               	</div>
				            </div> 
				            
				             <div class="form-group">
				                {!!Form::label('broker_flat_fee','Broker Flat Fee', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                	<div class="input-group">
				                		<span id="basic-addon7" class="input-group-addon">$</span>
				                 		{!!Form::text('broker_flat_fee', null, array('class'=>'form-control finance', 'aria-describedby'=>"basic-addon7", 'placeholder'=>"value", 'pattern'=>"^((\d+)|(\d{1,3})(\,\d{3}|)*)(\.\d{2}|)$" ))!!}
				                 	</div>					                 	
				               	</div>
				            </div>
				            
				            <div class="form-group">
				                {!!Form::label('assist_name','Assist Name', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('assist_name', null, array('class'=>'form-control '))!!}			                 	
				               	</div>
				            </div> 
				           
				            <div class="form-group">
				                {!!Form::label('assist_fee','Assist Fee', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                	<div class="input-group">
				                		<span id="basic-addon7" class="input-group-addon">$</span>
				                 		{!!Form::text('assist_fee', null, array('class'=>'form-control finance', 'aria-describedby'=>"basic-addon7", 'pattern'=>"^((\d+)|(\d{1,3})(\,\d{3}|)*)(\.\d{2}|)$"))!!}
				                 	</div>				                 	
				               	</div>
				            </div> 						            
				            						            
				            <div class="form-group">
				                {!!Form::label('gross_commission','Gross Commission', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('gross_commission', null, array('class'=>'form-control',  'readonly'))!!}			                 	
				               	</div>
				            </div> 
				            
				            <div class="form-group">
				                {!!Form::label('net_commission','Net Commission', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('net_commission', null, array('class'=>'form-control', 'readonly'))!!}			                 	
				               	</div>
				            </div>	
				            
				            <div class="form-group">
				                {!!Form::label('final_net_commission','Final Net Commission', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('final_net_commission', null, array('class'=>'form-control', 'readonly'))!!}			                 	
				               	</div>
				            </div>		
				            						            
				            <div class="form-group">
				                {!!Form::label('sp_of_list','SP as % of List', array('class'=>'col-sm-5 control-label'))!!}
				               	<div class="col-sm-7">
					                <div class="input-group">
					                 	{!!Form::text('sp_of_list', null, array('class'=>'form-control', 'aria-describedby'=>"basic-addon10", 'readonly'))!!}
					                 	<span id="basic-addon10" class="input-group-addon">%</span>			                 	
					               	</div>
					            </div>   	
				            </div>		
				            						            
				            <div class="form-group">
				                {!!Form::label('sp_of_origin_list','SP as % of Original List', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
					                <div class="input-group">
					                 	{!!Form::text('sp_of_origin_list', null, array('class'=>'form-control',  'aria-describedby'=>"basic-addon11", 'readonly'))!!}
					                 	<span id="basic-addon11" class="input-group-addon">%</span>			                 	
					               	</div>
					            </div>   	
				            </div>		
				            						            
				            <div class="form-group">
				                {!!Form::label('dom_to_contract','DOM to Contract', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('dom_to_contract', null, array('class'=>'form-control', 'readonly'))!!}			                 	
				               	</div>
				            </div>		
				            						            
				            <div class="form-group">
				                {!!Form::label('dom_to_close','DOM to Close', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::text('dom_to_close', null, array('class'=>'form-control', 'readonly'))!!}			                 	
				               	</div>
				            </div>
				            
				            <div class="form-group">
						    	<div class="col-sm-12">	     	
									{!! Form::button('Cancel', array('class'=>'btn  btn-sm btn-default pull-right left10', 'id'=>'cancelAddForm'))!!}
									{!! Form::submit('Save', array('class'=>'btn btn-sm pull-right btn-info '))!!}
									
						    	</div>
							</div>
							
				        </fieldset>	
				        		            	       	
			         </div>
					       	
	           		
				</div>              
				{!! Form::close() !!}	
								
		</div>    	
  	</div>
  </div>		
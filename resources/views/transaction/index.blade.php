@extends('innertab')

@section('script')
 
<script>
var tableData = {!! $tableData !!};
</script>
@endsection

@section('content')
<div class="container top20">
    <div class="row">    
        <div class="col-md-12">
	       	
	        <div class="panel panel-info" id="transactions-div">
	            				
	        	<div class="panel-body">	        	
	        		<div class="container bottom20">
			        	<div class="row">
			        		{!! Form::button('<i class="fa fa-plus"></i> Add Transaction', array('class'=>'btn btn-xs  btn-info pull-right ajax', 'data-company_id'=>$company_id, 'data-client_id'=>$client_id, 'data-route' => 'addTransaction', 'data-target' => '#newtransaction-div', 'data-path' => 'Transactions', 'data-_token'=>csrf_token()))!!}
			        		
			        	</div>		
			        </div>
			        			        
				    <div class="container datalist">
						<div id="list">
							 <table class="data-table" data-toggle="table" data-pagination="true" data-sort-name="start_date" data-sort-order="desc" >
			                    <thead>
			                        <tr>
			                            <th data-field="start_date" data-sortable="true">Entry Date</th>
			                            <th data-field="transaction_type" data-sortable="true">Type</th>
			                            <th data-field="mls1" data-sortable="true">MLS #</th>
			                            <th data-field="property_house" data-sortable="true">Property House #</th>
			                            <th data-field="property_addr1" data-sortable="true">Property Address Line 1</th>
			                            <th data-field="coe_date" data-sortable="true">COE Date</th>
			                            <th data-field="transaction_status" data-sortable="true">Status</th>                            
			                            <th data-field="actions">Actions</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    </tbody> 
			                </table>		                
						</div>
					</div>   	
				</div>
			</div>
									
			<div class="panel panel-info"  id="newtransaction-div" style="display:none">				
			 	
      		</div>      		
			 
			 <div class="panel panel-info"  id="edittransaction-div" style="display:none"></div>    
		</div>
	</div>
</div>
<input type="hidden" id="data-ajax" value="Transactions">
@endsection


{!! Form::button('Edit', array('class'=>'btn  btn-warning btn-xs ajax edittransaction-btn', 'data-id'=>$id, 'data-route' => 'editTransaction', 'data-target' => '#edittransaction-div', 'data-path' => 'Transactions','data-company_id'=>$company_id, 'data-client_id'=>$client_id, 'data-_token'=>csrf_token() ))!!}

{!! Form::button('Delete', array('class'=>'btn  btn-danger btn-xs deleteTransaction', 'data-id'=>$id,  'data-item'=>$id,  'data-route' => 'removeCheck', 'data-path' => 'Transactions', 'data-_token'=>csrf_token() ))!!}

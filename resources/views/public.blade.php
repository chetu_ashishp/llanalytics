<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	@if($_SERVER["SERVER_NAME"]!='www.re-oms.com')
		<title>Staging – OMS</title>
	@else 
		<title>OMS Software</title>
	@endif

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/custom.css') }}" rel="stylesheet">	
	<link href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
  	@yield('style')
  
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/publicportal') }}">Laurie Lundeen</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
				<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>							
							<ul class="dropdown-menu" role="menu">
                            	<li><a href="{{ url('/') }}">Internal Portal</a></li>                            	
                            	<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>				
				</ul>
		</div>
	</nav>

	 <div class="container">
	 	<div class="row">
	 		 <div class="header__row"></div>
	 	</div>
	 </div>
	  
	@yield('content')

	<!-- Scripts -->
        <script src="{{ asset('/vendor/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>        
        <script> var ajaxUrl = "<?php echo url('/'); ?>/";</script>
    @yield('script')
</body>
</html>

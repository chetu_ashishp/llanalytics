<?php if($is_deleted){ ?>
<a href="{{route('ostaff.edit', array('staff' => $id)) }}" data-id="{{$id}}" data-path="OmsStaff" data-route="editOmsStaff"  data-_token={{ csrf_token() }} class="btn btn-warning2 btn-sm  editOmsStaff left10" role='button' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
<a href="#" id="" class="btn  btn-sm  btn-danger resetomsstaff", data-id="{{$id}}" data-item="{{$id}}" data-route ="resetomsstaff" data-path="OmsStaff" data-_token="{{csrf_token()}}">ReCall</a>
<?php } else { ?>
<a href="{{route('ostaff.edit', array('staff' => $id)) }}" data-id="{{$id}}" data-path="OmsStaff" data-route="editOmsStaff"  data-_token={{ csrf_token() }} class="btn btn-warning btn-sm  editOmsStaff left10" role='button' >Edit</a>
<a href="#" id="" class="btn  btn-sm  btn-danger deleteOmsStaff", data-id="{{$id}}" data-item="{{$id}}" data-route ="removeCheck" data-path="OmsStaff" data-_token="{{csrf_token()}}">Delete</a>
<?php } ?>

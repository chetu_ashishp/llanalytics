@extends('app')
@section('style')
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
	<link href="{{ asset('/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}" rel="stylesheet">
@stop

@section('script')
<script>var tableData = {!! $tableData !!};</script>


<!-- Page-Level Plugin Scripts -->
   <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('/js/typeahead.js-master/dist/typeahead.jquery.js') }}"></script>
   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>   
    <script src="{{ asset('/js/jquery.mask.js') }}"></script>
  
   
 <script src="{{ asset('/js/clients/jquery.formatCurrency-1.4.0.min.js') }}"></script>
 <script src="{{ asset('/js/company/staff.js') }}"></script>
 <script src="{{ asset('/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
 <script src="{{ asset('/vendor/925491/jquery.ui.autocomplete.html.js') }}"></script>
  
@endsection


@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
				<div class="panel-heading">		  	
					<div class="container">			
						<div class="row">
							<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-user"></i> OMS Internal Staff </h4>							
				  			<a href="{{route('newstaff')}}" class="btn btn-info  storeomsstaff btn-xs pull-right" data-path="OmsStaff" data-route="store" data-_token="{{ csrf_token() }}" role="button"><i class="fa fa-plus"></i> Add New Staff</a>				  						  		
						</div>
				  	</div>
				</div>		
				  <div class="panel-body">
				    <div class="container datalist">
                         {!! Form::open( array('staffsearch' => 'form', 'method' => 'put', 'files'=>true, 'class'=>'form-horizontal form-omsstaff-active-search',  'autocomplete'=>"off")) !!} 
				    	<div class="search-radio-btn pull-left">
                            <div class="input-field">{!! Form::radio('staffstatus', 'Active', true, ['class' => 'left10 right10','id' => 'staffstatus']) !!} Active </div>
                            <div class="input-field">{!! Form::radio('staffstatus', 'NonActive', null, ['class' => 'left10 right10','id' => 'staffstatus']) !!} NonActive</div>
                            <div class="input-field">{!! Form::submit('Search', array('class'=>'btn btn-xs btn-info', 'data-path'=>'Staffs','data-route' => 'searchstaff','data-_token'=>csrf_token()))!!}</div>
                        </div>
                        {!! Form::close() !!}
						<div id="list">
                        
                       
						 <table class="data-table" data-toggle="table" data-pagination="true" data-search="true" data-sort-name="clname" data-sort-order="asc">
                                <thead>
			                        <tr>
			                            <th data-field="first_name" data-sortable="true">First Name</th>
			                            <th data-field="last_name" data-sortable="true">Last Name</th>
			                            <th data-field="email" data-sortable="true">Email #</th>
			                            <th data-field="mobile_phone" data-sortable="true">Primary Phone</th>
										<th data-field="role_id" data-sortable="true">Role</th>
			                            <th data-field="status" data-sortable="true">Status</th> 
										<th data-field="actions">Actions</th>
			                        </tr>
			                    </thead>		                   
		                 </table>		                
						</div>
					</div>   	
				  </div>
				</div>          
            </div>
        </div>
    </div>

@endsection
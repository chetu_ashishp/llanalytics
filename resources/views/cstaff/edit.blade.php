@extends('app')
<?php //echo '<pre/>'; print_r($staffDetails); die; ?>

@section('style')
	<link href="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
@stop

@section('script')


<!-- Page-Level Plugin Scripts -->
   <script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
   <script src="{{ asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>   
   <script src="{{ asset('/js/jquery.mask.js') }}"></script>
   <script src="{{ asset('/js/company/staff.js') }}"></script>
    
@stop



@section('content')
<div class="container">
    <div class="row">    
        <div class="col-md-12">
        	<div class="panel panel-info">
			  <div class="panel-heading">		  	
					<div class="container">
						<h4 class="panel-title pull-left">{{$form}} OMS Staff</h4>
						<a href="{{route('staff')}}" class="btn btn-default  btn-xs pull-right" role="button">Back</a>
					</div>	  	
			  </div>
		  
		  <div class="panel-body">
			<div class="container datalist">		    	
				<div class="jumbotron container">
					
					{!! Form::open( array('role' => 'form', 'method' => 'post', 'files'=>true, 'class'=>'form-horizontal',  'autocomplete'=>"off", 'url' => array('staff/update/'.$staffDetails->id))) !!}
					{!!Form::hidden('staff_id', $staffDetails->id)!!}	
					<div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	     	
								<a href="{{route('staff')}}" class="btn btn-default  btn-sm pull-right left10" role="button">Cancel</a>
								{!! Form::submit('Save', array('class'=>'btn btn-sm btn-info pull-right'))!!}							
					    	</div>
						</div>
		           </div>
		           
		           
					<div class="row">					
						<div class="col-sm-6">
			            	
			            	<div class="form-group">
			                        {!!Form::label('fname','First Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('fname', $staffDetails->first_name, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>	            	            	
			            	
			            	<div class="form-group">
			                        {!!Form::label('lname','Last Name', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::text('lname', $staffDetails->last_name, array('class'=>'form-control required', 'required'))!!}
									</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('email','Email Address', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::email('email', $staffDetails->email, array('class'=>'form-control'))!!}
									</div>
			            	</div>
							  
							<!--<div class="form-group">
			                        {!!Form::label('password','Password', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::password('password', null, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>
							
							<div class="form-group">
			                        {!!Form::label('password_confirmation','Confirm Password', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::password('password_confirmation', null, array('class'=>'form-control required', 'required'))!!}
			                	</div>
			            	</div>!-->
			            	         	
			        	</div>
						
						<div class="col-sm-6">	
							
			            	<div class="form-group">
			                        {!!Form::label('primary_phone','Mobile Phone', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('primary_phone', $staffDetails->mobile_phone, array('class'=>'form-control phoneUS' ))!!}
									</div>
			            	</div>
			            	
			            	<div class="form-group">
			                        {!!Form::label('alternate_phone','Alternate Phone', array('class'=>'col-sm-5 control-label'))!!}
			                        <div class="col-sm-7">
			                    	{!!Form::text('alternate_phone', $staffDetails->alternate_phone, array('class'=>'form-control phoneUS' ))!!}
									</div>
			            	</div>
							<div class="form-group">
			                        {!!Form::label('role_id','Role', array('class'=>'col-sm-5 control-label required'))!!}
			                        <div class="col-sm-7">
										{!!Form::select('role_id',  array("" => "") + $roles, $staffDetails->role_id, array('class'=>'form-control  required', 'required'))!!}
										
									</div>
			            	</div>
				          	<div class="form-group">
				                {!!Form::label('status', 'Status', array('class'=>'col-sm-5 control-label'))!!}
				                <div class="col-sm-7">
				                 	{!!Form::select('status',  array("Active"=>"Active", "Inactive"=>"Inactive", 'Suspended'=>'Suspended', 'Terminated'=>'Terminated') ,$staffDetails->status, array('class'=>'form-control'))!!}
				                 	
				               	</div>
				            </div>
				            	           		
			           	</div>
		           	</div>
		 
		       	
		           <div class="row">
		           		<div class="form-group">
					    	<div class="col-sm-12">	     	
					    	
								<a href="{{route('staff')}}" class="btn btn-default  btn-sm  pull-right left10" role="button">Cancel</a>
								{!! Form::submit('Save', array('class'=>'btn-sm btn btn-info pull-right'))!!}
																
					    	</div>
						</div>
		           </div>
		           				 
					{!! Form::close() !!}	
				</div>   		
			</div>    	
		  </div>
		
      		</div>
		</div>
    </div>
</div>

@stop
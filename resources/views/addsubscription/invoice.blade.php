<?php //echo '<pre/>'; print_r($arr[0]);
 $subscription_name = !empty($arr[0]['subscription_name']) ? $arr[0]['subscription_name'] : '';
 $invoice_date = !empty($arr[0]['invoice_date']) ? date('m-d-Y', strtotime($arr[0]['invoice_date'])) : '';
 $total_amount = !empty($arr[0]['total_amount']) ? $arr[0]['total_amount'] : '';
 $term = !empty($arr[0]['term']) ? $arr[0]['term'] : '';
	
?>	 
<div class="panel-body">	        	
	        		  	
			<div class="tab-pane active" id="core_details">	          
	            <div class="row">
                    <h4 class="panel-title">View Invoice Details</h4>
	                <div class="col-lg-8 col-lg-offset-1">
                        {!! Form::open( array('id' => 'subscription-purchase-form', 'data-route' => 'purchaseSubscription', 'class'=>'form-horizontal form-add',  'autocomplete'=>"off", 'data-path' => 'Company', 'data-route' => 'purchaseSubscription', 'data-_token'=>csrf_token() )) !!}
	                    <div class="row">
	                        <div class="col-lg-11">	                            
	                            @foreach($lables as $key=>$value)
	                     	      <!--{{ $key }} !-->
									<div class="row detail-cell">
                                        @if($key=='line_item')
                                        <div class="col-lg-6"><p class = "detail-cell-label">  {{$value}} </p></div> 
										 <div class="container datalist">
                                            <div id="list">
                                                 <table class="data-table" >
                                                    <thead>
                                                        <tr>
                                                            <th data-field="subscription_name" data-sortable="true">Subscription Name</th>
                                                            <th data-field="invoice_date" data-sortable="true">Start Date of Subscription</th>
                                                            <th data-field="total_amount" data-sortable="true">Amount</th>
                                                             <th data-field="term" data-sortable="true">Term</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody><tr><td>{{ $subscription_name}}</td><td>{{ $invoice_date }}</td><td>{{ $total_amount }}</td><td>{{ $term }}</td></tr></tbody> 
                                                </table>		                
                                            </div>
                                        </div>   
                                        @elseif($key=='payments_or_adjustments')
                                        {!! Form::open( array('role' => 'form', 'method' => 'post', 'files'=>true, 'class'=>'form-horizontal', 'id'=>'add-template',  'autocomplete'=>"off", 'url' => array('subscriptions/store'))) !!}
                                         <div class="col-lg-6"><p class = "detail-cell-label">  {{$value}} </p></div>
                                         <div class="container datalist">
                                            <div id="list">
                                                 <table class="data-table" >
                                                    <thead>
                                                        <tr>
                                                            <th data-field="payment_adjustment_date" data-sortable="true">Date</th>
                                                            <th data-field="payment_adjustment_amount" data-sortable="true">Amount</th>
                                                            <th data-field="payment_adjustment_reason" data-sortable="true">Reason</th>
                                                            <th data-field="payment_adjustment_note" data-sortable="true">Note</th>
                                                            <th data-field="payment_user_note" data-sortable="true">User Adding</th>
                                                            <th data-field="status" data-sortable="true">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                     
                                                              <?php if(isset($paymentAdjustmentListData) && count($paymentAdjustmentListData > 0)) {
                                                                    foreach($paymentAdjustmentListData as $key => $value) {
                                                                      echo "<tr data-index='0'>
                                                                                <td style=''>".$paymentAdjustmentListData[$key]['payment_adjustment_date']."</td>
                                                                                <td style=''>".$paymentAdjustmentListData[$key]['payment_adjustment_amount']."</td> 
                                                                                <td style=''>".$paymentAdjustmentListData[$key]['payment_adjustment_reason']."</td>
                                                                                <td style=''>".$paymentAdjustmentListData[$key]['payment_adjustment_note']."</td>
                                                                                <td style=''>".$paymentAdjustmentListData[$key]['payment_adjustment_useradding']."</td>
                                                                                <td data-field= 'status' style=''>".$paymentAdjustmentListData[$key]['status']."</td>
                                                                            </tr>";
                                                                    
                                                                  }
                                                                }
                                                                ?>                                                          
                                                      </tbody> 
                                                </table>		                
                                            </div>
                                        </div>
                                         {!! Form::close() !!}
                                         @elseif($key=='control')
                                           <?php echo $arr[0][$key]; ?>
                                        @else
                                      
										<div class="col-lg-6"><p class = "detail-cell-label">  {{$value}} </p></div> 
									  
										<div class="col-lg-6">{{$arr[0][$key]}}</div>	
                                        @endif
									</div>				                        
	                     	    		                    	
	                           @endforeach
								
	                        </div>
	                    </div>
                       {!! Form::close() !!}
	                </div>
	            </div>  	            
	        </div>
  </div>		
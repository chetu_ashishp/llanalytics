@extends('app')


@section('style')
	{{--<link href="{{ asset('/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">--}}
	<link href="{{ asset('/css/spectrum.css') }}" rel="stylesheet">
	<link href="{{ asset('/vendor/bootstrap-table/src/extensions/reorder-rows/bootstrap-table-reorder-rows.css') }}" rel="stylesheet">
@stop


@section('script')
<!-- Page-Level Plugin Scripts -->
   	<script src="{{ asset('/js/templates/templates.js') }}"></script>   
   	{{--<script src="{{ asset('/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>--}}
	<script src="{{ asset('/js/spectrum.js') }}"></script>
   	<script src="{{ asset('/js/templates/fields.js') }}"></script>   
   	<script src="{{ asset('/vendor/TableDnD/dist/jquery.tablednd.js') }}"></script>
	<script src="{{ asset('/vendor/bootstrap-table/src/extensions/reorder-rows/bootstrap-table-reorder-rows.js') }}"></script>   
@stop


@section('content')
<div class="panel panel-info">
  <div class="panel-heading">		  	
		<div class="container">
			<h4 class="panel-title pull-left"><i class="glyphicon glyphicon-info-sign"></i> Template Details </h4>	
			<a href="{{route('templates')}}" class="btn btn-default  btn-xs pull-right" role="button">Back to Templates</a>	
		</div>		  	
  </div>
  <div class="panel-body">    		
		<!-- Nav tabs -->
		
		  <ul class="nav nav-tabs" role="tablist">
		       <li class="active"><a href="#core_details" data-toggle="tab">Core Details</a></li>
		       <li><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab" id="lfields" data-tpl="{{$template->id}}">Fields</a></li>
		  </ul>
			
		<!-- Tab panes -->
	    <div class="tab-content">
	        <div class="tab-pane active" id="core_details">
	            <div class="row">
	                <div class="col-lg-6">
	                    <div class="top10">                
	                        
	                        	<button class="btn btn-danger btn-sm templateDelete pull-right left10" role='button' data-id="{{$template->id}}" data-_token="{!!csrf_token()!!}" >Delete</button>	                        			                        
	                       
	                        <a href="{!!route('templates.edit', array('template' => $template->id)) !!}" class="btn btn-warning btn-sm pull-right " role='button' >Edit</a>
	                        	                         
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-lg-8 col-lg-offset-1">
	                    <div class="row">
	                        <div class="col-lg-7">	                          
                     	    		<div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Template Name</p></div>
			                        	<div class="col-lg-6"><p>{{$template->name}}</p></div>     
			                        </div>
			                        
			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Template Purpose</p></div>
			                        	<div class="col-lg-6"><p>{{$template->purpose}}</p></div>     
			                        </div>
			                        
			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Client Fields to Include</p></div>
			                        	<div class="col-lg-6">			                        	
			                        		@if(!empty($temIncludeFields) && in_array('first_name', $temIncludeFields))	
			                        			<p>First Name</p>
			                        		@endif			
			                        		@if(!empty($temIncludeFields) && in_array('last_name', $temIncludeFields))	
			                        			<p>Last Name</p>
			                        		@endif
			                        	</div>	
			                        </div>
			                       
			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Client Fields to Include</p></div>
			                        	<div class="col-lg-6">			                        	
			                        		@if(!empty($temIncludeFields))			                        		
			                        			@foreach($trans_labels as $key=>$field)
			                        				@if(in_array($key, $temIncludeFields))	
			                        					<p>{{$field}}</p>
			                        				@endif
			                        			@endforeach			                        		
			                        		@endif						                        		
			                        	</div>	
			                        </div>
			                      
			                      	<div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label"># Rows to Freeze </p></div>
			                        	<div class="col-lg-6"><p>{{$template->rows_to_freeze}}</p></div>     
			                        </div>
			                       
			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label"># Freeze First Column </p></div>
			                        	<div class="col-lg-6"><p>@if($template->freeze_first_column==1) Yes @else No @endif</p></div>     
			                        </div>
			                        	
			                        	 
			                     	<div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Users Who Can Access Sheet</p></div>
			                        	<div class="col-lg-6">
			                        	
			                        		@if($template->all_user_accsess==1)
			                        				<p>All company users</p>
			                        		@elseif(!empty($user_access))			                        		
			                        			@foreach($user_access as $username)			                        					
			                        					<p>{{$username}}</p>			                        				
			                        			@endforeach			                        		
			                        		@endif	
			                        	</div>     
			                        </div>  
			                      	<div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Column Order</p></div>
			                        	<div class="col-lg-6"><p>{{$column_order[$template->column_order]}}</p></div>     
			                        </div>  
			                        <div class="row detail-cell">
			                        	<div class="col-lg-6"><p class = "detail-cell-label">Status</p></div>
			                        	<div class="col-lg-6"><p>{{$template->status}}</p></div>     
			                        </div>                         
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-lg-6">
	                    <div class="top10">	                        
	                        
	                        	<button class="btn btn-danger btn-sm templateDelete pull-right left10" role='button' data-id="{{$template->id}}" data-_token="{!!csrf_token()!!}" >Delete</button>	                        			                        
	                       
	                        <a href="{!!route('templates.edit', array('template' => $template->id)) !!}" class="btn btn-warning btn-sm pull-right " role='button' >Edit</a>
	                        	                         
	                    </div>
	                </div>
	            </div>
	            
	        </div>
	        <div role="tabpanel" class="tab-pane fade" id="fields">
	        	<div class="container top20">
                        <div class="panel panel-info" id="fields-div">  			  		  			 
                            <div class="panel-body">
                            	<div class="container bottom20">
						        	<div class="row">
						        		{!! Form::button('<i class="fa fa-plus"></i> Add Field', array('class'=>'btn btn-xs  btn-info pull-right ajax',  'data-template_id'=>$template->id, 'data-route' => 'createField', 'data-target' => '#newfield-div', 'data-path' => 'Fields', 'data-_token'=>csrf_token()))!!}
						        	</div>		
						        </div>	        
							    <div class="container datalist">
	                               
	                                    <div id="list">
	                                        <table class="fieldtable" data-pagination="false" data-search="true" data-sort-name="priority_order" data-sort-order="asc" >
	                                            <thead>
	                                                <tr>
	                                                
	                                                	<th data-field="id" data-visible="false">Id</th>
	                                                    <th data-field="field_name" >Name</th>
	                                                    <th data-field="type" >Type</th>
	                                                    <th data-field="tooltip">Tooltip</th>
	                                                    <th data-field="priority_order" >Priority Order</th>		                                                       
	                                                    <th data-field="status" >Status</th>   
	                                                    <th data-field="actions">Actions</th>
	                                                </tr>
	                                            </thead>                                           
	                                        </table>
	                                    </div>
	                                </div> 
                                  	
                            </div>
                        </div>                                                
                        <div class="panel panel-info"  id="newfield-div" style="display:none"></div>
			 			<div class="panel panel-info"  id="editfield-div" style="display:none"></div>			 
                    </div>
             </div>       
	    </div>		
  </div>
</div>
    
    <input type="hidden" id="data-ajax" value="fields">
    {!!Form::hidden('template_id', $template->id, array('class'=>'template_id'))!!}    
    {!! Form::token() !!}
@stop


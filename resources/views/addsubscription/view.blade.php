@extends('innertab')

@section('script')
 
<script>
var tableData = {!! $tableData !!};
</script>
@endsection

@section('content')
<div class="container top20">
    <div class="row">    
        <div class="col-md-12">
	       	
	        <div class="panel panel-info" id="transactions-div">
	            				
	        	<div class="panel-body">	        	
	        			        
				    <div class="container datalist">
						<div id="list">
							 <table class="data-table" data-toggle="table" data-pagination="true" data-sort-name="start_date" data-sort-order="desc" >
			                    <thead>
			                        <tr>
			                            <th data-field="subscription_name" data-sortable="true">Subscription Name</th>
			                            <th data-field="start_date" data-sortable="true">Start Date of Subscription</th>
                                        <th data-field="end_date" data-sortable="true">End Date of Subscription</th>                                        
                                        <th data-field="created_at" data-sortable="true">Date/Time Added to Company</th>
                                        <th data-field="user_added_first_name" data-sortable="true">User Who Added</th>
                                        <th data-field="action">Related Invoice</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    </tbody> 
			                </table>		                
						</div>
					</div>   	
				</div>
			</div>
									
		    		
			 <div class="panel panel-info"  id="viewinvoice-div"  style="display:none">	</div>
			 <div class="panel panel-info"  id="edittransaction-div" style="display:none"> </div>
             <div class="panel panel-info"  id="add_payment_adjustment-div" style="display:none"> </div>
             
		</div>
	</div>
</div>
<input type="hidden" id="data-ajax" value="Transactions">
@endsection


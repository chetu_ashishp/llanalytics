<!DOCTYPE html>
<html lang="en">
<head>
	@if($_SERVER["SERVER_NAME"]!='www.re-oms.com')
		<title>Staging – OMS</title>
	@else 
		<title>OMS Software</title>
	@endif

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendor/bootstrap-table/dist/bootstrap-table.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendor/bootstrap-table-filter/src/bootstrap-table-filter.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    @yield('style')

            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

@if($_SERVER['SERVER_ADDR']!=='216.120.234.142')
<div id="testbanner">
    TEST SERVER
</div>
@endif
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">OMS Internal Portal</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav master-nav">

                @if (Auth::check())

                 
                        <li class="dropdown">
                            <a href="{{ url('/data') }}" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">Global System Tools <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{url('/datamain') }}">Tools Main Page</a>
                                </li>
                                <li><a href="{{ url('/data') }}" class="dataoption" data-path="data" data-route="dataview"  data-_token="{{ csrf_token() }}">Data Options</a></li>
                                <li><a href="{{ url('/settings') }}" class="setting" data-path="settings" data-route="settingsview"  data-_token="{{ csrf_token() }}">Settings</a></li>
                            </ul>
                        </li>
                  
					<li><a href="{{ url('/company') }}" class="company" data-path="company" data-route="" data-_token="{{ csrf_token() }}">Companies</a></li>
                    <!--<li><a href="{{ url('/clients') }}">Clients</a></li>!-->
					<li><a href="{{ url('/staff') }}" class="oms_Staff" data-path="OmsStaff" data-route="staff"  data-_token="{{ csrf_token() }}">OMS Staff</a></li>
                    <li><a href="{{ url('/templates') }}" class="templates" data-path="templates" data-route="templateview"  data-_token="{{ csrf_token() }}">Templates</a></li>
                    <li><a href="{{ url('/sheets') }}" class="sheets" data-path="sheets" data-route="sheetview"  data-_token="{{ csrf_token() }}">Sheets</a></li>
                    <li><a href="{{ url('/companytools') }}" class="sheets" data-path="companytools" data-route=""  data-_token="{{ csrf_token() }}">Company Options</a></li>
                    <li><a href="{{ url('reports') }}" class="reportsview" data-path="reports" data-route="reportsview"  data-_token="{{ csrf_token() }}">Reports</a></li>
                    <li><a href="#">Account</a></li>                  
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false">Subscription <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">                       
                        <li><a href="{{ url('/subscriptions') }}">Subscription</a></li>
                        <li><a id="companysubscribed" href="{{ url('/companysubscribed') }}">Company Subscribed</a></li>
                        <li><a id="companysubscribed" href="{{ url('/emailtemplates') }}">Email Templates</a></li>
                        <li><a id="companysubscribed" href="{{ url('/roles') }}">Role Permissions</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>

                    <ul class="dropdown-menu" role="menu">
                        <!-- <li><a href="{{ url('/data') }}">Data Options</a></li> -->
                        <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                    </ul>
                </li>
                @endif

            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="header__row col-md-12">

            <!--[if lt IE 7]>
            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
                your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome
                Frame</a> to improve your experience.
            </div>
            <![endif]-->
            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('message')}}
                    {{Session::forget('message')}}
                </div>
            @endif

            @if(Session::has('error'))
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('error')}}
                    {{Session::forget('error')}}
                </div>
            @endif

            @if($errors->has())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </div>
    </div>
</div>

@yield('content')

        <!-- Scripts -->
<script src="{{ asset('/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap-table/dist/bootstrap-table.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap-table/dist/extensions/filter/bootstrap-table-filter.min.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap-table/dist/locale/bootstrap-table-en-US.min.js') }}"></script>
<script src="{!! asset('/vendor/blockUI/jquery.blockUI.js') !!}"></script>
<script> var ajaxUrl = "<?php echo url('/'); ?>/";</script>
@yield('script')
<script src="{{ asset('/js/script.js') }}"></script>
<script src="{{ asset('/vendor/moment/min/moment.min.js') }}"></script>
<script src="{!! asset('/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}"></script>

<div class="device-xs visible-xs"></div>
<div class="device-sm visible-sm"></div>
<div class="device-md visible-md"></div>
<div class="device-lg visible-lg"></div>
</body>
</html>
